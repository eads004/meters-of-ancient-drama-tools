#!/usr/bin/env python
# coding: utf-8

import sys
import pandas as pd


def excel_to_listperson(excelpath):
    df = pd.read_excel(excelpath)
    personlist = "".join([f"      <person>\n        <persName>" + 
                          f"{r['name']}</persName>\n" + 
                          f"        <sex>{r['gender']}</sex>\n" + 
                          f"        <socecStatus>{r['status']}</socecStatus>\n" + 
                          "        <trait><label>isGod</label><desc>" + 
                          f"{r['isGod']}</desc></trait>\n" + 
                          f"        <nationality>{r['isGreek']}</nationality>" +
                          "\n      </person>\n" 
                          for r in df.to_dict(orient="records")])
    listperson = "<profileDesc>\n  <particDesc>\n    " + \
        f"<listPerson>\n{personlist}    </listPerson>\n  " + \
        "</particDesc>\n</profileDesc>"
    return listperson


if __name__=="__main__":
    fname = sys.argv[1]
    if len(sys.argv) < 2:
        print("Usage: ./excel_to_listperson.py [filename]")
    else:
        print(excel_to_listperson(fname))






