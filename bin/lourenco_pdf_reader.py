# HOW TO USE:
# Obtain a copy of the `Lourenço_2011.pdf` file.
# Find the starting and ending page numbers of the play in question and the number of the first footnote that you find.
# Use the syntax `python3 lourenco_pdf_reader.py XML_FILEPATH LORENCO_FILEPATH RESULT_FILEPATH FIRST_FOOTNOTE_NUMBER [FIRST_PAGE LAST_PAGE]`.
# !!NOTE!! The page numbers are NOT based on the page numbers of the pdf on the bottom of the page, but rather the page number based on the order of the sheets. The second page on the pdf will be page 2, regardless of the page number at the bottom of the page. These numberings are usually clearly demarcated on any given pdf reader of choice.
# XML_FILEPATH: the XML file of the play
# LORENCO_FILEPATH: the Lorenco file mentioned above
# RESULT_FILEPATH: the file that is to contain the final product, which is a summary of all the differences in meter found between the Lourenco text and the XML file
# FIRST_FOOTNOTE_NUMBER: within the section of the pdf that contains the play in question, the first footnote number (e.g., in Orestes, the first footnote number is 150)
# FIRST_PAGE: the first page of the section of the pdf that contains the play in question, with numbering according to the note above
# LAST_PAGE: the last page of the section of the pdf that contains the play in question, with numbering according to the note above


import PyPDF2
import re
from bs4 import BeautifulSoup
import sys

final_result = ""

def extract_line_dict(text, first_footnote): # TODO: update function to check for footnotes that are next to the line numbers
	footnote_number = first_footnote
	meter_dict = {}
	potential_footnote_number = str(footnote_number)
	for line in text.split("\n")[1:]: # skips the first line, which contains line number and title
		previous_line_number = None
		if line[0] in "0123456789":
			potential_line_number = str(line[0])
			next_char_index = 1

			# read through line until encountering whitespace to determine line number
			while True:
				if re.search(r"\s+", line[next_char_index]):
					break
				else:
					potential_line_number += str(line[next_char_index])
				next_char_index += 1

			# check if line number matches the current footnote number
			if potential_line_number == str(footnote_number):
				if previous_line_number is not None and potential_line_number in [str(get_only_one_line_number(previous_line_number) + 1), str(get_only_one_line_number(previous_line_number) + 2)]:
					# let this one go
					pass
				else:
					# update footnote number and check if there's another footnote
					footnote_number += 1

					continue

			# read through the meter portion of the line
			try:
				meter = ""
				while True:
					next_char_index += 1
					if re.search(r"\s+", line[next_char_index]) or line[next_char_index] in "∪—:†∩":
						if line[next_char_index] in "∪—:†∩":
							meter += line[next_char_index]
					else:
						break
			except IndexError:
				# this means that there is no meter name portion
				meter_dict[potential_line_number] = (meter, None)
				previous_line_number = potential_line_number
				continue

			# read through the meter name portion of the line
			meter_name = re.sub(r"\s+", "", line[next_char_index:])
			if potential_footnote_number == line[-len(potential_footnote_number):]:
				meter_name = meter_name[:-len(potential_footnote_number)]
				potential_footnote_number = str(int(potential_footnote_number) + 1)

			# add to dictionary
			meter_dict[potential_line_number] = (meter, meter_name)

			previous_line_number = potential_line_number

		else:
			continue

	return meter_dict, footnote_number

def get_only_one_line_number(string):
	return int(string.split("-")[0])

def check_meter_dict_with_xml(meter_dict, xml_filepath):
	with open(xml_filepath) as xmlfile: 
		content = xmlfile.readlines()
		content = "".join(content)
		xml_soup = BeautifulSoup(content, "lxml")
	for line_number, tup in meter_dict.items():
		
		lorenco_meter = tup[0]
		meter_name = tup[1]
		line = xml_soup.find("l", n=line_number)
		xml_meter = ""
		if line is None:
			add_error("no_xml_line", line_number, xml_meter, no_character_swap_lorenco_meter)
			continue
		for seg_tag in line.find_all("seg", type="syll"):
			if seg_tag['ana'] == "short":
				xml_meter += "∪"
			elif seg_tag['ana'] == "long":
				xml_meter += "—"
			elif seg_tag['ana'] == "shortn":
				xml_meter += "∩"
			else:
				xml_meter += "ERROR"

		# compare lorenco and xml meters
		# print(line_number, "xml:", "'" + xml_meter + "'", "lorenco:", "'" + lorenco_meter + "'")
		no_character_swap_lorenco_meter = re.sub(":", "", lorenco_meter)
		if lorenco_meter == xml_meter:
			continue
		if "ERROR" in xml_meter:
			add_error("invalid_ana", line_number, xml_meter, no_character_swap_lorenco_meter)
			continue
		if "†" in lorenco_meter:
			add_error("uncertain_text", line_number, xml_meter, no_character_swap_lorenco_meter)
			continue
		if len(no_character_swap_lorenco_meter) != len(xml_meter):
			add_error("num_syllables", line_number, xml_meter, no_character_swap_lorenco_meter)
			continue
		for i in range(len(no_character_swap_lorenco_meter)):
			if no_character_swap_lorenco_meter[i] != xml_meter[i]:
				if no_character_swap_lorenco_meter[i] == "∩" or xml_meter[i] == "∩":
					add_error("brevis_in_longo", line_number, xml_meter, no_character_swap_lorenco_meter)
				else:
					add_error("meter", line_number, xml_meter, no_character_swap_lorenco_meter)
				break


def add_error(error_type, line_no, xml_meter, lorenco_meter):
	global final_result
	global errors
	errors[error_type] += 1
	final_result += line_no + ": " + error_type + "\n" + "XML Meter:".ljust(16) + xml_meter + "\n" + "Lourenço Meter:".ljust(17) + lorenco_meter + "\n\n"

if __name__ == "__main__":
	FIRST_PAGE = None
	LAST_PAGE = None

	try:
		XML_FILE = sys.argv[1]
		LORENCO_FILE = sys.argv[2]
		RESULT_FILE = sys.argv[3]
		FIRST_FOOTNOTE = int(sys.argv[4])
		if (len(sys.argv) > 5):
			FIRST_PAGE = int(sys.argv[5]) - 1
			LAST_PAGE = int(sys.argv[6])
	except:
		sys.exit("Syntax is `python3 lourenco_pdf_reader.py XML_FILEPATH LORENCO_FILEPATH RESULT_FILEPATH FIRST_FOOTNOTE_NUMBER [FIRST_PAGE LAST_PAGE]`.")


	# LORENCO_FILE = "lourenco_orestes_only.pdf"
	lorenco_file = open(LORENCO_FILE, "rb")

	fileReader = PyPDF2.PdfFileReader(lorenco_file)
	play_pages = fileReader.pages[FIRST_PAGE:LAST_PAGE]
	# first_footnote = 150
	first_footnote = FIRST_FOOTNOTE
	play_meter_dict = {}

	for page in play_pages:
		# print(page.extract_text())
		page_meter_dict, first_footnote = extract_line_dict(page.extract_text(), first_footnote)
		# print(first_footnote)
		play_meter_dict.update(page_meter_dict)

	# XML_FILE = "../texts/HANDCHECKED/Orestes.xml"
	errors = {"invalid_ana": 0, "uncertain_text": 0, "num_syllables": 0, "meter": 0, "brevis_in_longo": 0, "no_xml_line": 0}

	check_meter_dict_with_xml(play_meter_dict, XML_FILE)

	summary = "SUMMARY:\n"

	for key, value in errors.items():
		summary += "    " + key + ": " + str(value) + "\n"

	final_result = re.sub("—","-","XML: " + XML_FILE + "\nLORENÇO: " + LORENCO_FILE + "\n\n" + summary + "\n" + final_result)

	# RESULT_FILE = "orestes_meter_check.txt"

	# record results
	with open(RESULT_FILE, "w") as cursor:
	    cursor.write(final_result)
	

