# pairs with update_astj_changes.py
import json
import sys

import re

from compare_line_diggle import get_line

if len(sys.argv) != 4:
    raise Exception("Three arguments needed.")

s_file = sys.argv[1] # the structure file
t_file = sys.argv[2] # the translation file
r_file = sys.argv[3] # the result file

s_json = json.load(open(s_file))
t_json = json.load(open(t_file))

all_lines_dict = {} # a variable that maps every line number to a specific part of the play

for sect_dict in s_json["children"]: # iterate through the structure file to fill all_lines_dict
    sect_name = sect_dict["name"]
    sect_start = get_line(sect_dict["start"])
    sect_end = get_line(sect_dict["end"])

    for line in range(sect_start, sect_end + 1):
        all_lines_dict[line] = sect_name

for part_dict in t_json: # find which section the translation part is properly placed at
    t_part = part_dict["translation"]

    prop_sect = "?" # the proposed section where the translation is at
    
    if t_part is not None:
        for num_encl in re.findall(r"\[.{1,4}\]", t_part): # "[5]", "[10]", "[500]", and "[1000]" would all match
            num = int(num_encl[1:-1]) # the line number written inside the text

            if prop_sect == "?":
                prop_sect = all_lines_dict[num]
            elif prop_sect != all_lines_dict[num]:
                # if the translation contains line numbers that belong to different proper sections, then make sure the output reflects that difference
                prop_sect = "BELONGS TO MULTIPLE DIFFERENT SECTIONS"

    part_dict["proposed section"] = prop_sect

def fill_gaps(index, mrec_sec, t_json):
    if index >= len(t_json):
        return mrec_sec

    if t_json[index]["proposed section"] == "?":
        next_fg = fill_gaps(index + 1, mrec_sec, t_json)
        t_json[index]["proposed section"] = next_fg
        return next_fg
    
    fill_gaps(index + 1, t_json[index]["proposed section"], t_json)
    if t_json[index]["proposed section"] == mrec_sec:
        return mrec_sec
    else:
        return "?"

fill_gaps(0, "Prologos", t_json)

file = open(r_file, "w")
file.write(json.dumps(t_json, sort_keys=True, indent=4))
file.close()
