"""
SYNTAX:
`python compare_text.py {XML_FILE} {OXFORD_HTML_FILE} {PERSEUS_INITIAL_WEBSITE} {RESULT_FILE} {LINES_TO_INCLUDE_DIGGLE}`

XML_FILE: the file path of the XML file to be displayed
OXFORD_HTML_FILE: The file containing the HTML data for Diggle's edition of the play.
PERSEUS_INITIAL_WEBSITE: The website containing the Perseus 
RESULT_FILE: the file that is to contain the final product, which will print out the lines of the text and mark any differences between texts above the lines. The top line is the XML version of the text, while the bottom line is the Diggle or Perseus version of the text. Line numbers are marked (though may be inaccurate for the Diggle or Perseus versions of the text), and divisions between lines are marked with a '/'. A single difference in character is represented by a '*'. A character on the bottom line, but not the top line, (i.e., an addition, relative to the top line) is represented as an "a". A character on the top line, but not on the bottom line, (i.e., a deletion, relative to the top line) is represented as a "b".
LINES_TO_INCLUDE_DIGGLE: Lines that the program compares with Diggle's text. The lines not included are compared with Perseus's text. In the form of a continuous string as follows: "140-207,316-347,807-843,845,847"

How to get OXFORD_HTML_FILE: Obtain the html of Diggle's text from Oxford Scholarly Editions through Inspect Element. Copy and paste ONLY the div tag (and everything underneath it) that corresponds with the "Main Body Text" to a new file on the computer. The div tag encompassing the main body text should look like either <div class="scroll-content columnContent work-top" style="height: 471px;"> or <div class="bodyCssRoot" id="contentRoot">. INCLUDE ALL THE OTHER TAGS UNDERNEATH IT when copying. This can easily be done on Chrome in the Inspect Element menu by collapsing all the other tags and then pressing control+C. Save this file as follows: ${playname}_OSEO.html in the OSEO_HTML folder.
How to get PERSEUS_INITIAL_WEBSITE: Go to the page containing the first line of the Perseus online version of the text to be compared. Scroll to the bottom, where you will find an orange XML button below the MAIN TEXT (NOT the one below the left column). Click it, and you will be directed to an XML file hosted on the Perseus website. Get the website link (on the browser bar), and DELETE THE VERY LAST NUMBER (which should be a 1). This is the PERSEUS_INITIAL_WEBSITE value that you should provide to the program.
"""

from Levenshtein import editops, distance
from compare_line_diggle import scrape_oxford, replace_marking_tag
from compare_lines_perseus import get_line
from bs4 import BeautifulSoup, Tag, Comment, NavigableString
from cltk.alphabet import grc
from itertools import zip_longest
from intspan import intspan
import sys

import requests

def compare_play_texts(text_a, lines_a, text_b, lines_b):
    errors_string = ""
    a_mod = 0 # how many characters forward a has to be pushed forward to match the errors string
    b_mod = 0

    ins_list = []
    del_list = []

    lines_a_str = ""
    lines_b_str = ""

    for op in editops(text_a, text_b):
        op_name = op[0]
        a_index = op[1]
        b_index = op[2]

        if op_name == "replace":
            errors_string = errors_string + (a_index + a_mod - len(errors_string)) * " " # assuming that the replace function is at the same place for both strings when enacted (a_index + a_mod = b_index + b_mod)
            errors_string = errors_string + "*" 
        if op_name == "insert": # a needs a space at the location
            errors_string = errors_string + (a_index + a_mod - len(errors_string)) * " "
            errors_string = errors_string + "a"

            a_mod = a_mod + 1

            ins_list.append(a_index)
        if op_name == "delete": # b needs a space at the location
            errors_string = errors_string + (b_index + b_mod - len(errors_string)) * " "
            errors_string = errors_string + "b"

            b_mod = b_mod + 1

            del_list.append(b_index)

    # get the a string with spaces in between
    text_a_str_list = []
    last_index = 0
    for ins_index in ins_list:
        text_a_str_list.append(text_a[last_index:ins_index])
        last_index = ins_index
    text_a_str_list.append(text_a[last_index:])

    text_a_str = " ".join(text_a_str_list)

    last_line = ""
    i = 0 # index of lines_a
    space_count = 0
    # get the a_lines string
    for section in text_a_str_list:

        first_line = True
        lines = section.split("/")
        # split into lines
        for j in range(len(lines)):
            
            if first_line:
                first_line = False

                actual_line = last_line + lines[j]
            else:
                actual_line = lines[j]

            if j == len(lines) - 1: # if this is the last line
                last_line = actual_line
                # print(actual_line, lines_a[i])
            else:
                if len(actual_line) < len(lines_a[i]):
                    lines_a_str = lines_a_str + " " * (len(actual_line) + space_count + 1) # the +1 due to elimination of "/" in .split
                else:
                    lines_a_str = lines_a_str + lines_a[i] + " " * (len(actual_line) - len(lines_a[i]) + space_count + 1)

                space_count = 0
                i = i + 1

        space_count = space_count + 1
            
            

    # get the b string with spaces in between
    text_b_str_list = []
    last_index = 0
    for del_index in del_list:
        text_b_str_list.append(text_b[last_index:del_index])
        last_index = del_index
    text_b_str_list.append(text_b[last_index:])

    text_b_str = " ".join(text_b_str_list)

    last_line = ""
    i = 0 # index of lines_b
    space_count = 0
    # get the a_lines string
    for section in text_b_str_list:

        first_line = True
        lines = section.split("/")
        # split into lines
        for j in range(len(lines)):
            
            if first_line:
                first_line = False

                actual_line = last_line + lines[j]
            else:
                actual_line = lines[j]

            if j == len(lines) - 1: # if this is the last line
                last_line = actual_line
            else:
                if len(actual_line) < len(lines_b[i]):
                    lines_b_str = lines_b_str + " " * (len(actual_line) + space_count + 1) # the +1 due to elimination of "/" in .split
                else:
                    lines_b_str = lines_b_str + lines_b[i] + " " * (len(actual_line) - len(lines_b[i]) + space_count + 1)

                i = i + 1
                space_count = 0

        space_count = space_count + 1

    return errors_string, lines_a_str, text_a_str, lines_b_str, text_b_str
    
if __name__ == "__main__":
    XML_FILE = sys.argv[1] # "../texts/HANDCHECKED/Orestes.xml"
    OXFORD_HTML_FILE = sys.argv[2] # "OSEO_HTML/Orestes_OSEO.html"
    PERSEUS_INITIAL_WEBSITE = sys.argv[3] # "http://www.perseus.tufts.edu/hopper/xmlchunk?doc=Perseus%3Atext%3A1999.01.0115%3Acard%3D"
    RESULT_FILE = sys.argv[4] # "results0/compare.txt"
    LINES_TO_INCLUDE_DIGGLE = sys.argv[5] # "140-207,316-347,807-843,960-1017,1246-1310,1353-1365,1369-1502,1537-1549"

    # open xml file 
    with open(XML_FILE) as xmlfile: # may not work if xml file is way too big, but it can handle at least 20k lines
        content = xmlfile.readlines()
        content = "".join(content)
        xml_soup = BeautifulSoup(content, "lxml")

    with open(OXFORD_HTML_FILE) as oxford_html_file:
        content = oxford_html_file.readlines()
        content = "".join(content)
        oxford_soup = BeautifulSoup(content, "lxml")

    diggle_text_dict, diggle_line_order_list = scrape_oxford(oxford_soup)

    # get only the lines that are to be compared
    big_diggle_str = ""
    diggle_line_comp_list = []
    for line_no in diggle_line_order_list:
        if int(get_line(line_no)) in intspan(LINES_TO_INCLUDE_DIGGLE):
            big_diggle_str += grc.tonos_oxia_converter(grc.expand_iota_subscript(diggle_text_dict[line_no])) + "/"
            diggle_line_comp_list.append(line_no)

    big_diggle_str = big_diggle_str.replace("〈", "<").replace("〉", ">")
    # print(big_diggle_str)



    xml_text_dict = {}
    xml_line_order_list = []

    big_xml_str_vs_dig = ""
    
    replace_marking_tag("sic", "†", "†", xml_soup)
    replace_marking_tag("supplied", "<", ">", xml_soup)
    replace_marking_tag("surplus", "[", "]", xml_soup)

    for line in xml_soup.find_all("l"): # get all <l> tags
        print("Current line:", line['n'], end="         \r")
        if line['n'] != "0": # exclude non-line <l> tags
            for speaker in line.find_all("milestone", {"unit": "speaker"}): # deletes speaker changes
                speaker.replace_with("")
            
            # print(line['n'], line_no)
            # print(line_no)
            
            # get combined text
            combined_text = "" # the combined text of the syllables, which is the second component to check

            for milestone in line.find_all("milestone", {"unit": "word"}): # get all <milestone unit="word"> tags within the syllable tags
                milestone.replace_with(" ") # replace the milestones with a space, indicating a new word
            
            for comment in line.find_all(string=lambda text: isinstance(text, Comment)):
                if isinstance(comment.next_sibling, NavigableString):
                    comment.next_sibling.replace_with(comment.next_sibling.strip())
                if isinstance(comment.previous_sibling, NavigableString):
                    comment.previous_sibling.replace_with(comment.previous_sibling.strip())

            combined_text += line.get_text()
            combined_text = combined_text.strip().replace("\n", "")
            # if line['n'] == "964": #debug!
            #     print(combined_text, "\n", combined_text_expanded)

            xml_text_dict[line['n']] = combined_text
            xml_line_order_list.append(line['n'])

    # get only the lines that are to be compared to diggle
    big_xml_str_vs_perseus = ""
    xml_line_list_vs_dig = []
    xml_line_list_vs_perseus = []
    for line_no in xml_line_order_list:
        if int(get_line(line_no)) in intspan(LINES_TO_INCLUDE_DIGGLE):
            big_xml_str_vs_dig += xml_text_dict[line_no] + "/"
            xml_line_list_vs_dig.append(line_no)
        else:
            big_xml_str_vs_perseus += xml_text_dict[line_no] + "/"
            xml_line_list_vs_perseus.append(line_no)

    big_xml_str_vs_dig = grc.tonos_oxia_converter(grc.expand_iota_subscript(big_xml_str_vs_dig))
    big_xml_str_vs_perseus = grc.tonos_oxia_converter(big_xml_str_vs_perseus)
    # print(big_xml_str)

    # get the lines that are to be compared to perseus




    # open xml file 
    with open(XML_FILE) as xmlfile: # may not work if xml file is way too big, but it can handle at least 20k lines
        content = xmlfile.readlines()
        content = "".join(content)
        xml_soup = BeautifulSoup(content, "lxml")

    # set up for Perseus file
    perseus_soup = None
    next_soup = BeautifulSoup(requests.get(PERSEUS_INITIAL_WEBSITE + "2").content, "lxml")
    line_no = "0"
    final_list = ""

    # parse Perseus xml file
    perseus_lines_array = []
    while next_soup != perseus_soup:
        perseus_soup = next_soup
        for p in perseus_soup.find_all("p"): # find all <p> tags
            # if p.lb: # sees if the <p> tag has an <lb> tag underneath
            for to_unwrap in p.find_all("del"):
                to_unwrap.replace_with("[", *to_unwrap.contents, "]")
                # print("del near", line_no, "with status", to_unwrap["status"]) if to_unwrap.has_attr("status") else print("del near", line_no)
            for token in p.contents: # p.contents returns list of contents underneath the tag
                if isinstance(token, Tag):
                    if token.name == "lb":
                        if token.has_attr("n"): # checks for line number
                            line_no = token['n']
                        else:
                            line_no = str(get_line(line_no) + 1)
                    if token.name == "gap":
                        line_no = str(get_line(line_no) - 1)
                else:
                    # add the line to line_array
                    if token.strip() != "":
                        if len(perseus_lines_array) != 0 and perseus_lines_array[len(perseus_lines_array) - 1][0] == line_no:
                            old_line, old_token = perseus_lines_array.pop()
                            perseus_lines_array.append((line_no, old_token + token))
                        else:
                            perseus_lines_array.append((line_no, token.strip()))
        next_soup = BeautifulSoup(requests.get(PERSEUS_INITIAL_WEBSITE + str(get_line(line_no) + 2)).content, "lxml")
        if line_no == "0":
            sys.exit("HTML is invalid.")
        # print("ending on line", line_no, end="             \n")



    perseus_arrays = []
    big_perseus_str = ""
    for i, line in perseus_lines_array:
        if get_line(i) not in intspan(LINES_TO_INCLUDE_DIGGLE):
            perseus_arrays.append(i)
            big_perseus_str += line.strip() + "/"

    big_perseus_str = grc.tonos_oxia_converter(big_perseus_str).replace("᾽", "ʼ").replace(":", "·").replace("\n", " ")

    n = 100
    out = []

    for text in compare_play_texts(big_xml_str_vs_dig, xml_line_list_vs_dig, big_diggle_str, diggle_line_comp_list):
        out.append([text[i:i+n] for i in range(0, len(text), n)])

    lines_to_write = ["BOILERPLATE MESSAGE:", "Lines to compare to Diggle: " + LINES_TO_INCLUDE_DIGGLE, "In the first section of this printout, every line in the range listed above is compared to Diggle. All other lines are compared to Perseus in the second section below.", "NOTE: This section of the program expands iota subscripts and sets to lowercase the letters of the XML text for the sake of comparison with Diggle. Therefore, this section of the program SHOULD NOT be used to verify whether iota subscripts or capitalization are handled correctly.", "", ""]
    for error, xml_ln, xml, diggle_ln, diggle in zip_longest(out[0], out[1], out[2], out[3], out[4], fillvalue=" "):
        lines_to_write.extend([error, xml_ln, xml, diggle_ln, diggle, "\n"])

    out = []
    for text in compare_play_texts(big_xml_str_vs_perseus, xml_line_list_vs_perseus, big_perseus_str, perseus_arrays):
        out.append([text[i:i+n] for i in range(0, len(text), n)])

    lines_to_write.extend(["", "", "Here begins the second section of the comparison, this time with Perseus. Any lines not compared with Diggle will be compared with Perseus.", "", ""])

    for error, xml_ln, xml, p_ln, p in zip_longest(out[0], out[1], out[2], out[3], out[4], fillvalue=" "):
        lines_to_write.extend([error, xml_ln, xml, p_ln, p, "\n"])

    with open(RESULT_FILE, "w") as rfile:
        rfile.write('\n'.join(lines_to_write) + '\n')


