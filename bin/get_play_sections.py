from bs4 import BeautifulSoup, Tag
import json
from pathlib import PurePath

XML_FILE = PurePath("../texts/HANDCHECKED/Orestes.xml")

with open(XML_FILE) as xmlfile: # may not work if xml file is way too big, but it can handle at least 20k lines
	content = xmlfile.readlines()
	content = "".join(content)
	xml_soup = BeautifulSoup(content, "lxml")

def get_meter_name(div_attributes):

	"""
	type attribute --> .json file equivalent
	iambic --> ia3
	trochee --> tr4^
	anapests --> anap, ia3
	stasimon, episode, mesode, strophe, antistrophe --> AC, ia
	epode --> mixed
	lyric --> DO

	i.e. there isn't a one-to-one or many-to-one mapping
	"""

	return div_attributes['type'] # temporary filler function


total_size = 100000

file_json = {"name" : XML_FILE.stem, "children" : []} 

for section in xml_soup.find_all("div1"):
	section_json = {"name" : section['type'], "children" : [], "click" : 0, \
		"start" : section.find("l")["n"], "end" : section.find_all("l")[-1]["n"]}

	actual_size = 0

	subsections = section.find_all("div2")

	for subsection in subsections:
		subsection_json = {"name" : get_meter_name(subsection.attrs), "size" : total_size // len(subsections), \
			"start": subsection.find("l")["n"], "end": subsection.find_all("l")[-1]["n"]}

		actual_size += total_size // len(subsections)

		section_json["children"].append(subsection_json)

	file_json["children"].append(section_json)

	total_size = actual_size - 1

print(json.dumps(file_json, indent=4))

