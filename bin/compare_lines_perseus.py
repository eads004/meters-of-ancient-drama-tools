# To compare between the xml version and Perseus's online version

# HOW TO USE:
# Go to the page containing the first line of the Perseus online version of the text to be compared. Scroll to the bottom, where you will find an orange XML button below the MAIN TEXT (NOT the one below the left column). Click it, and you will be directed to an XML file hosted on the Perseus website. Get the website link (on the browser bar), and DELETE THE VERY LAST NUMBER (which should be a 1). This is the PERSEUS_INITIAL_XML_WEBSITE value that you should provide to the program.

# The program might take about a minute to run, since it's scraping XML from the Perseus website directly.

# XML_FILEPATH: the file path of the XML file to be displayed
# PERSEUS_INITIAL_XML_WEBSITE: see "HOW TO USE" above
# RESULT_FILEPATH: the file that is to contain the final product, which is a summary of all the differences found between the Perseus text and the XML text.
# LINES_TO_EXCLUDE: the lines that the program should not check
# Line Text refers to the text found in between the <seg type="line_text"> tag.
# Combined Text refers to the text found within the <seg type="syll"> tags that corresponds to the line of the text in question.

import re, sys
from Levenshtein import editops, distance
from bs4 import BeautifulSoup, Tag
import requests
from compare_line_diggle import replace_marking_tag, upper_lower_pairs
from intspan import intspan
from cltk.alphabet import grc

def get_line(str_):
    line_no = ""
    for char in str(str_):
        if char in '0123456789':
            line_no += char
        else:
            break
    return int(line_no)

def add_error(type_array, line_no, line_text, syll_text, pers_texts):
    if len(type_array) == 0:
        return
    global final_result
    for type_ in type_array:
        errors[type_] += 1
    type_array_set = set(type_array)
    if not type_array_set.issubset(DIFFERENCES_TO_EXCLUDE):
        final_result += line_no + ": " + " ".join(type_array) + "\n" + "Line Text:".ljust(15) + line_text + "\n" + "Combined Text:".ljust(15) + syll_text + "\n" + "".join(["Perseus Text:".ljust(15) + text +"\n" for text in pers_texts]) + "\n"

def normalize_punctuation(string, old, new):
    result = string
    for i in range(len(old)):
        result = result.replace(old[i], new[i])
    return result

def consider(consideration_list):
    global word_edits
    a_str_cons = consideration_list[0][1]
    a_str_cons_value = True
    b_str_cons = consideration_list[0][2]
    b_str_cons_value = True
    for consideration in consideration_list[1:]:
        if a_str_cons_value and consideration[1] != a_str_cons:
            a_str_cons_value = False
        if b_str_cons_value and consideration[2] != b_str_cons:
            b_str_cons_value = False
        if not a_str_cons_value and not b_str_cons_value:
            return False
    for consideration in consideration_list:
        word_edits.add(consideration)
    return True

def find_faults(line_text_raw, combined_text_raw):
    line_text = re.sub(r"[†〈〉\[\]]", "", line_text_raw)
    combined_text = re.sub(r"[†〈〉\[\]]", "", combined_text_raw)
    edits = editops(line_text, combined_text)
    
    if len(edits) >= 3:

        # if there's a lot of insert tags and/or delete tags near the beginning or end of a line, then there are likely different line divisions
        insert_counter = 0
        delete_counter = 0 
        line_division_edits = set() 

        for edit in edits:
            if edit[0] == 'insert' and (edit[1] == 0 or edit[1] == len(line_text)):
                insert_counter += 1
                line_division_edits.add(edit)
            elif edit[0] == 'delete' and (edit[2] == 0 or edit[2] == len(combined_text)):
                delete_counter += 1
                line_division_edits.add(edit)
        if insert_counter >= 3 or delete_counter >= 3:
            error_set.add("line_division")
        else:
            line_division_edits = set() # reset the counter if line division is not an error
        
        edits = [edit for edit in edits if edit not in line_division_edits] # removes already processed edits in edits for the point changes check

    # if there's still a lot of concentrated insert, delete, or change tags, then there are likely differences in words

    if len(edits) >= 3:
        global word_edits

        consideration_list = edits[:min(len(edits), 5)] 
        word_edits = set()
        
        
        if consider(consideration_list[:-1]) or consider(consideration_list[1:]):
                error_set.add("words")
        for edit in edits[min(len(edits), 5):]:
            consideration_list.append(edit)
            consideration_list = consideration_list[1:]
            if consider(consideration_list[:-1]) or consider(consideration_list[1:]):
                error_set.add("words")
        
        edits = [edit for edit in edits if edit not in word_edits]
    # else, if there's less than 5 edits, the differences are probably point changes
    if len(edits) < 5: 
        char_set = set()
        for edit in edits:
            if edit[0] in ['insert', 'replace']:
                char_set.add(combined_text[edit[2]])
            if edit[0] in ['delete', 'replace']:
                char_set.add(line_text[edit[1]])

        skip_set = set()
        for char in char_set:
            if char in skip_set:
                continue
            if char in '᾽\'ʼ",.·()[]〈〉\u200e†;\u037e!"#$%&()*+,-./:;<=>?@[\]^_`{|}~':
                error_set.add("punctuation")
                skip_set.add(char)
            elif char in " \n":
                error_set.add("spacing")
                skip_set.add(char)
            else: # error is probably a difference in letters
                for upper, lower in upper_lower_pairs:
                    # print(upper, lower)
                    if upper in char_set and lower in char_set:
                        error_set.add("capitalization")
                        skip_set.add(upper)
                        skip_set.add(lower)
                if len(char_set) > len(skip_set):
                    error_set.add("spelling")

if __name__ == "__main__":
    # get file_names from terminal
    try:
        XML_FILE = sys.argv[1]
        PERSEUS_INITIAL_WEBSITE = sys.argv[2]
        RESULT_FILE = sys.argv[3]
        LINES_TO_EXCLUDE = sys.argv[4]
        DIFFERENCES_TO_EXCLUDE = set(sys.argv[5:])
    except:
        sys.exit("Syntax is python3 compare_lines_perseus.py XML_FILEPATH PERSEUS_INITIAL_XML_WEBSITE RESULT_FILEPATH LINES_TO_EXCLUDE [DIFFERENCES_TO_EXCLUDE].")

    # open xml file 
    with open(XML_FILE) as xmlfile: # may not work if xml file is way too big, but it can handle at least 20k lines
        content = xmlfile.readlines()
        content = "".join(content)
        xml_soup = BeautifulSoup(content, "lxml")

    # set up for Perseus file
    perseus_soup = None
    next_soup = BeautifulSoup(requests.get(PERSEUS_INITIAL_WEBSITE + "2").content, "lxml")
    line_no = "0"
    final_list = ""

    # parse Perseus xml file
    perseus_lines_array = []
    while next_soup != perseus_soup:
        perseus_soup = next_soup
        for p in perseus_soup.find_all("p"): # find all <p> tags
            if p.lb: # sees if the <p> tag has an <lb> tag underneath
                for to_unwrap in p.find_all("del"):
                    to_unwrap.unwrap()
                for token in p.contents: # p.contents returns list of contents underneath the tag
                    if isinstance(token, Tag):
                        if token.name == "lb":
                            if token.has_attr("n"): # checks for line number
                                line_no = token['n']
                            else:
                                line_no = str(get_line(line_no) + 1)
                        if token.name == "gap":
                            line_no = str(get_line(line_no) - 1)
                    else:
                        # add the line to line_array
                        if token.strip() != "":
                            perseus_lines_array.append((line_no, token.strip()))
        next_soup = BeautifulSoup(requests.get(PERSEUS_INITIAL_WEBSITE + str(get_line(line_no) + 2)).content, "lxml")
        if line_no == "0":
            sys.exit("HTML is invalid.")
        # print("ending on line", line_no)

    # parse regular xml file
    final_result = "DIFFERENCES:\n" # what should be written in RESULT_FILE
    errors = {"spacing": 0, "punctuation": 0, "spelling": 0, "capitalization": 0, "words": 0, "line_division": 0, "no_line_text": 0, "no_syllables": 0, "no_perseus": 0, "no_seg_tags": 0}

    replace_marking_tag("sic", "†", "†", xml_soup)
    replace_marking_tag("supplied", "〈", "〉", xml_soup)
    replace_marking_tag("surplus", "[", "]", xml_soup)

    for line in xml_soup.find_all("l"): # get all <l> tags
        if line['n'] != "0": # exclude non-line <l> tags
            line_no = line['n']
            if line['n'][-1] in "abαβ":
                line_int = line['n'][:-1]
            elif "-" in line['n']:
                line_int = line['n'].split("-")[0]
            else:
                line_int = line['n']
            # print(line_no, line_int)
            if int(line_int) in intspan(LINES_TO_EXCLUDE):
                continue
            line_text = line['line_text'] if line['line_text'] is not None else "" # get the line text, which is the first component to check; if line_text_seg is None, return nothing 

            combined_text = "" # the combined text of the syllables, which is the second component to check
            for syllable in line.find_all("seg", {"type": "syll"}): # get all <seg type="syll"> tags
                for milestone in syllable.find_all("milestone", {"unit": "word"}): # get all <milestone unit="word"> tags within the syllable tags
                    milestone.replace_with(" ") # replace the milestones with a space, indicating a new word
                combined_text += syllable.get_text()
            combined_text = combined_text.strip()

            perseus_texts = [normalize_punctuation(line[1], ":᾽","·ʼ") for line in perseus_lines_array if line_no == line[0]]
            if len(perseus_texts) == 0:
                perseus_texts = [normalize_punctuation(line[1], ":᾽","·ʼ") for line in perseus_lines_array if get_line(line_no) == get_line(line[0])]

            # debugging code
            # final_result += line_text + "\n" + combined_text.strip() + "\n\n"
            
            # compare the two strings using editops
            error_set = set()
            are_equal = False
            if len(perseus_texts) == 1 and line_text == combined_text and line_text == perseus_texts[0]:
                are_equal = True
            if len(perseus_texts) == 0:
                error_set.add("no_perseus")
            elif line_text == "" and combined_text == "":
                error_set = {"no_seg_tags"}
            elif line_text == "":
                error_set = {"no_line_text"}
            elif combined_text == "":
                error_set = {"no_syllables"}   
            elif not are_equal:
                # find_faults(line_text, combined_text)
                for perseus_text in perseus_texts:
                    find_faults(combined_text, perseus_text)
                    # find_faults(line_text, perseus_text)    
            add_error(error_set, line_no, line_text, combined_text, perseus_texts)

    summary = "SUMMARY:\n"

    for key, value in errors.items():
        summary += "    " + key + ": " + str(value) + "\n"

    final_result = "XML: " + XML_FILE + "\nPERSEUS: " + PERSEUS_INITIAL_WEBSITE + "\nEXCLUDED_LINES: " + LINES_TO_EXCLUDE + "\n\n" + summary + "\n" + final_result

    # record results
    with open(RESULT_FILE, "w") as cursor:
        cursor.write(final_result)



