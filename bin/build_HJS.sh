#=======BUILD SCRIPT FOR TIM MOORE'S ANCIENT METER DATABASE=============#
#=======DEVELOPED IN CONJUNCTION WITH WUSTL'S HDW, ESP.=================#
#=============STEPHEN PENTECOST AND HENRY SCHOTT========================#

#=======================================================================#
#	Note that this script runs a python script which expects py3	#
#	Is there a way to check that a user HAS python or python3 and	#
#	perhaps make an alias in this shell script to ensure that it	#
#	will work regardless of python, python3 or py?			#
#=======================================================================#

#=======================================================================#
#   This script should be run in meters-of-ancient-drama-tools/bin      #
#   It requires bs4, pandas, lxml modules                                    #
#=======================================================================#

#=======================================================================#
#	First, we establish the working directory as repodir.		#
#	We then clean out whatever is living in the /dist/ directory	#
#	as well as the /temp/ directory.				#
#	The /temp/ directory is where /dist/ looks for the XML to	#
#	render on the site.						#
#=======================================================================#

cd ../..
repodir=$(pwd)
texts_dir_name="meters-of-ancient-drama-texts"
tools_dir_name="meters-of-ancient-drama-tools"

texts_repo_path=$repodir/$texts_dir_name
tools_repo_path=$repodir/$tools_dir_name

echo CLEANING DIRECTORIES
rm -rf $tools_repo_path/dist
mkdir $tools_repo_path/dist
rm -rf $tools_repo_path/temp
mkdir $tools_repo_path/temp

#=======================================================================#
# 	Next, we copy the source files into the /dist/ directory	#
#	so that further editing any file in /src/ does not affect	#
#	the entire site while we work.					#
#=======================================================================#

echo COPYING SRC TO DIST
cp -r $tools_repo_path/src/* $tools_repo_path/dist
cp $texts_repo_path/meterscomplete.json $tools_repo_path/dist

#=======================================================================#
#	Next, we copy text XML files into the /temp/ directory		#
#	after munge-ing them (to standardize the formatting and 	#
#	prepare it for interacting with main.js				#
#									#
#	We do this for the same reason we copy /src/ into /dist/	#
#	Namely, so that any edits to the base files do not permeate	#
#	the site prematurely.						#
#=======================================================================#

echo MUNGING XML
for i in $texts_repo_path/HANDCHECKED/*.xml;
do
  java -jar $tools_repo_path/saxon/saxon-he-11.3.jar -xsl:$tools_repo_path/bin/munge_xml.xsl -s:$i -o:$tools_repo_path/dist/`basename "$i"`;
done

python $tools_repo_path/bin/add_indent.py $tools_repo_path/dist/

#=======================================================================#
#	At this point, we begin compiling the jsons for the stats	#
#	pages that are dependent on the plays rather than hand-crafted	#
#	mark up.							#
#=======================================================================#

for i in $tools_repo_path/dist/*.xml;
do
  java -jar $tools_repo_path/saxon/saxon-he-11.3.jar -xsl:$tools_repo_path/bin/get_character_json.xsl -s:$i -o:${i%.xml}Characters.json;
done

#=======================================================================#
#	Finally, we extract the crossfilter data from the completed	#
#	XML. For now, there is a line in the python script below	#
#	that must be edited manually to include any new play.		#
#=======================================================================#

echo EXTRACTING CROSSFILTER DATA

python $tools_repo_path/bin/extract_crossfilter_csvs.py

cp $tools_repo_path/dist/stats_crossfilter/data/*.csv $tools_repo_path/dist/combined_statistics/data/

echo BUILD COMPLETE
echo CREATING LOCAL HOST
cd $tools_repo_path/dist
python -m http.server
#=======================================================================#
#		DEVELOPING THIS SCRIPT FURTHER:				#
#	1) It would be nice to have a helper script to create 		#
#		mergedCharacters.json					#
#=======================================================================#
