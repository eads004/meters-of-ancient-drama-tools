#!/usr/bin/env python

import glob, json, sys, re
from lxml import etree
from collections import Counter
from bs4 import BeautifulSoup

import copy

# ----------------------------------------------------------------------

def get_meter_name(node, meters_data): # meters data in meterscomplete.json
    
    line_meter_name = 'MARKUP PROBLEM?'
    line_meter_type = 'MARKUP PROBLEM?'
    original_values = 'MARKUP PROBLEM?'
    
    if node.get('met') == None:
        
        div = node.xpath('ancestor::tei:div2', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}) # get any div2 ancestors
        if len(div) == 0:
            div = node.xpath('ancestor::tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}) # if there are no div2 ancestors, get div1 ancestors
            
        if div[0].get('type') != None: # if the div has no type attribute
        
            line_meter_name = div[0].get('met')  # get the met attribute for the name
            line_meter_type = 'not in meterscomplete'
            if div[0].get('met') != None: # if the met attribute exists, set original values to the met attribute
                original_values = div[0].get('met')

            for m in meters_data:
                if m['ours'] == div[0].get('met'): # if the meters data matches anything in meterscomplete.json

                    line_meter_name = m['name']
                    line_meter_type = m['type']

            if line_meter_type == 'not in meterscomplete': 
                print('div', div[0].get('met'), div[0].get('type')) # prints out on the console which lines are not in meterscomplete.json
            
    else: # if the node (line in this case, since seg and milestone inherit line meter data from their line) does have a met attribute, do the same thing as above, except with the node rather than the div. Line meter data overrides div section data.
        
        line_meter_name = node.get('met')
        line_meter_type = 'not in meterscomplete'
        if node.get('met') != None:
            original_values = node.get('met')
        
        for m in meters_data:
            if m['ours'] == node.get('met'):
        
                line_meter_name = m['name']
                line_meter_type = m['type']

        if line_meter_type == 'not in meterscomplete':
            print('line', node.get('met'), node.get('type')) # prints out on the console which lines are not represented in meterscomplete.json
            
    #print('get_meter_name RESULT', line_meter_name, line_meter_type, original_values, node.get('met'), node.get('n'))

    return line_meter_name, line_meter_type, original_values
        
# ----------------------------------------------------------------------
  
FOLDER = '../../meters-of-ancient-drama-texts/HANDCHECKED/'
TEXTS_TO_PROCESS = ['Orestes', 'Hekabe', 'Elektra']
RESULT_FOLDER = 'meters-of-ancient-drama-tools/dist/stats_crossfilter/data/'

character_data = json.load(open('meters-of-ancient-drama-tools/src/mergedCharacters.json', 'r', encoding='utf-8'))
meters_data = json.load(open('meters-of-ancient-drama-texts/meterscomplete.json', 'r', encoding='utf-8')) 
          
def equals_without(a, b, regex):
    return re.sub(regex, "", a) == re.sub(regex, "", b)

# ----------------------------------------------------------------------
# FIRST, MAKE EXPLICIT ALL THE LOOKUPS, SO WE HAVE SOMETHING TO GREP WHEN QA-ING.
# ----------------------------------------------------------------------

for p in glob.glob(FOLDER + '*.xml'):
    
    play = p.split('/')[-1].split('.')[0] # Orestes, Hekabe, or Elektra
    
    if play not in TEXTS_TO_PROCESS:
        print('extract_crossfilter_csvs.py skipping', p)
        continue
    
    # generate soup
    with open(p) as xmlfile: 
        content = xmlfile.readlines()
        content = "".join(content)
        xml_soup = BeautifulSoup(content, "lxml")
    
    # unwrap all <seg type="metron"> tags, so that the word parser can be accurate about who is speaking
    for seg in xml_soup.find_all("seg", attrs={"type": "metron"}):
        seg.unwrap()

    print('parsing', p)

    tree = etree.fromstring(str(xml_soup).encode('utf-8')).getroottree() # get the tree for the play
    
    text = tree.xpath('//tei:text', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})[0] # get the text portion of the play
        
    print('extract_crossfilter_csvs.py processing', p)
    
    # ------------------------------------------------------------------
    # PATCH DIV1 VALUES
    # ------------------------------------------------------------------

    for n, div1 in enumerate(text.xpath('//tei:div1', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})): # enumerate function adds an index to an iterable object
        div1.set('n_type', str(n + 1).rjust(2, '0')) # add an "n_type" attribute with 01, 02, ... before the play sections
    
    # ------------------------------------------------------------------
    # LINES
    # ------------------------------------------------------------------
    
    for line in text.xpath('//tei:l', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # for each line
        # print("line", line.get('n'))
                 
        for div1 in line.xpath('ancestor::tei:div1', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # for each(?) div1 ancestor of the line

            # set the line's div1_type and div1_n_type tags. If there are none for either, write "MISSING".
            if div1.get('type') == None: 
                line.set('div1_type', 'MISSING')
            else:
                line.set('div1_type', div1.get('type'))

            if div1.get('n_type') == None:
                line.set('div1_n_type', 'MISSING')
            else:
                line.set('div1_n_type', div1.get('n_type'))
            
            break
        
        if len(line.xpath('preceding::tei:milestone[@unit="speaker"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0: # if there is a speaker milestone before the line (*I'm assuming there always will be...*)
            
            milestone = line.xpath('preceding::tei:milestone[@unit="speaker"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})[-1] # set the milestone variable to the last speaker milestone before the line TODO: figure out what to do with multiple speakers on one line!
                        
            line.set('speaker', milestone.get('n')) # put that information onto the line itself
            
            speaker_details = None 
            for c in character_data: # for each character in ../src/mergedCharacters.json
                if c[0] == play.lower(): # if the character is in this play
                    if equals_without(c[1]['name'], milestone.get('n'), "[〈〉]"): # and if the character is equivalent to the one found on the milestone tag
                        speaker_details = c[1] # set the speaker_details variable to this character's speaker details
                        break
                        
            # set the corresponding values of the line to the information found in the json file
            if speaker_details == None:
                line.set('gender', 'MISSING')
                line.set('status', 'MISSING')
                line.set('isGod', 'MISSING')
                line.set('isGreek', 'MISSING')
            else:
                line.set('gender', speaker_details['gender'])
                line.set('status', speaker_details['status'])
                line.set('isGod', speaker_details['isGod'])
                line.set('isGreek', speaker_details['isGreek'])



            # SET SPEAKER/RELATED ATTRIBUTES ON SYLLABLES TODO: check if xpath returns one result or multiple 
            seg_speaker = milestone.get('n') # default speaker is the first speaker, as recorded by the line
            current_pre_sib_list = []
            for seg in line.xpath('.//tei:seg[@type="syll"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # for each syllable seg tag
                next_pre_sib_list = seg.xpath('preceding-sibling::tei:milestone[@unit="speaker"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
                
                # print("next_pre_sib_list", next_pre_sib_list)

                if current_pre_sib_list != next_pre_sib_list: # if the preceding sibling list has changed, then change the seg_speaker
                    current_pre_sib_list = next_pre_sib_list
                    seg_speaker = current_pre_sib_list[-1].get('n') # TODO: check if the indexing is correct
                
                in_speaker_list = seg.xpath('.//tei:milestone[@unit="speaker"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

                if len(in_speaker_list) > 0: # if the speaker is inside the seg-tag
                    seg_speaker = in_speaker_list[-1].get('n') # length will usually be one, but take the last one, just in case
                
                seg.set('speaker', seg_speaker)

                speaker_details = None 
                for c in character_data: # for each character in ../src/mergedCharacters.json
                    if c[0] == play.lower(): # if the character is in this play
                        if equals_without(c[1]['name'], seg_speaker, "[〈〉]"): # and if the character is equivalent to the one found on seg_speaker
                            speaker_details = c[1] # set the speaker_details variable to this character's speaker details
                            break
                            
                # set the corresponding values of the line to the information found in the json file
                if speaker_details == None:
                    seg.set('gender', 'MISSING')
                    seg.set('status', 'MISSING')
                    seg.set('isGod', 'MISSING')
                    seg.set('isGreek', 'MISSING')
                else:
                    seg.set('gender', speaker_details['gender'])
                    seg.set('status', speaker_details['status'])
                    seg.set('isGod', speaker_details['isGod'])
                    seg.set('isGreek', speaker_details['isGreek'])

            word_speaker = milestone.get('n')
            current_l_csl = [] # current speaker list according to <l> tag children (Children Speaker List)
            for word in line.xpath('.//tei:milestone[@unit="word"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # go through the line xpaths and then test for milestone-speaker tags before the word on the level of the seg-tags as well as on the level of the milestone-word tags. If at any point, the speaker tag changes on either level, then change the speaker to the new speaker TODO: check if xpath returns one result or multiple
                in_seg_csl = word.xpath('preceding-sibling::tei:milestone[@unit="speaker"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

                # print("in_seg_csl", in_seg_csl)

                new_l_csl = word.xpath('parent::tei:seg[@type="syll"]/preceding-sibling::tei:milestone[@unit="speaker"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

                # print("new_l_csl", new_l_csl)

                if (len(in_seg_csl) > 0):
                    # print("new_seg_csl", new_seg_csl)
                    word_speaker = in_seg_csl[-1].get('n')

                if (new_l_csl != current_l_csl):
                    word_speaker = new_l_csl[-1].get('n')
                    current_l_csl = new_l_csl

                word.set('speaker', word_speaker)

                speaker_details = None 
                for c in character_data: # for each character in ../src/mergedCharacters.json
                    if c[0] == play.lower(): # if the character is in this play
                        if equals_without(c[1]['name'], word_speaker, "[〈〉]"): # and if the character is equivalent to the one found on word_speaker
                            speaker_details = c[1] # set the speaker_details variable to this character's speaker details
                            break
                            
                # set the corresponding values of the line to the information found in the json file
                if speaker_details == None:
                    word.set('gender', 'MISSING')
                    word.set('status', 'MISSING')
                    word.set('isGod', 'MISSING')
                    word.set('isGreek', 'MISSING')
                else:
                    word.set('gender', speaker_details['gender'])
                    word.set('status', speaker_details['status'])
                    word.set('isGod', speaker_details['isGod'])
                    word.set('isGreek', speaker_details['isGreek'])

                
        
                
        # get and set the meter name, type, and values based on the meterscomplete.json file
        line_meter_name, line_meter_type, original_met_values = get_meter_name(line, meters_data) 
        
        line.set('line_meter_name', line_meter_name)
        line.set('line_meter_type', line_meter_type)
        line.set('original_met_values', original_met_values)


        # get the corresponding values to the div2 tag. div2 marks either the strophe-antistrophe sections (and?)/or the meter typing of the lines beneath it
        div2_met = None
        div2_ana = None
        div2_type = None
        div2_n = None

        for div2 in line.xpath('ancestor::tei:div2', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

            if div2.get('met') != None:
                div2_met = div2.get('met')
            if div2.get('ana') != None:
                div2_ana = div2.get('ana')
            if div2.get('type') != None:
                div2_type = div2.get('type')
            if div2.get('n') != None:
                div2_n = div2.get('n')
                
            break
        
        line.set('div2_met', str(div2_met))
        line.set('div2_ana', str(div2_ana))
        line.set('div2_n', str(div2_n))
        
        lyric_non_lyric = ''
        
        if div2_type != None and div2_type !='': # if the 'type=' was not empty
            lyric_non_lyric = div2_type # set the lyric_non_lyric variable to that value
        elif 'anapest' in line_meter_name or \
                'iambic3' in line_meter_name or \
                'trochaic4' in line_meter_name:
                    lyric_non_lyric = 'nonlyric' # set the lyric_non_lyric variable to nonlyric if the line is an anapest, iambic trimeter, or trochaic tetrameter
        else:
            lyric_non_lyric = 'lyric' # 
            
        line.set('lyric_non_lyric', str(lyric_non_lyric))

        # if any of the line's ancestors or descendants are one of the editorial marks, mark this line as the corresponding certainty.
        if len(line.xpath('.//tei:sic | ./ancestor::tei:sic',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0: 
            line.set('uncertain', 'sic (†...†)')
        elif len(line.xpath('.//tei:supplied | ./ancestor::tei:supplied',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0:
            line.set('uncertain', 'supplied (&lt;...&gt;)')
        elif len(line.xpath('.//tei:surplus | ./ancestor::tei:surplus',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0:
            line.set('uncertain', 'deletion ([...])')
        else:
            line.set('uncertain', 'no editorial marking')

        syllables_string = ""
        for syllable in line.xpath('descendant::tei:seg[@type="syll"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # generate a syllable string based upon the lengths of the syllables. l == long, s == short, n == shortn
            if syllable.get("ana") == "long":
                syllables_string += "l"
            elif syllable.get("ana") == "short":
                syllables_string += "s"
            elif syllable.get("ana") == "shortn":
                syllables_string += "n"

        line.set('syllables_string', syllables_string)
    
    # ------------------------------------------------------------------
    # METRON
    # ------------------------------------------------------------------
    
    # for seg in text.xpath('//tei:seg[@type="metron"]', 
    #                     namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                 
    #     for line in seg.xpath('ancestor::tei:l', 
    #                     namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                    
    #         for a in ['div1_type', 'div1_n_type', 'speaker', 'gender', 'status', 'isGod', 
    #                     'div2_ana', 'div2_type', 'div2_n',
    #                     'isGreek', 'line_meter_name', 'line_meter_type',
    #                     'original_met_values', 'lyric_non_lyric', 'uncertain', 'n']:
                            
    #             att_value = line.get(a)
    #             if att_value == None:
    #                 att_value = '(metron) line missing ' + a + '?'
                            
    #             seg.set(a, att_value)
                
    #         break

    #     syllables_string = ""
    #     for syllable in seg.xpath('descendant::tei:seg[@type="syll"]', 
    #                     namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # generate a syllable string based upon the lengths of the syllables. l == long, s == short, n == shortn
    #         if syllable.get("ana") == "long":
    #             syllables_string += "l"
    #         elif syllable.get("ana") == "short":
    #             syllables_string += "s"
    #         elif syllable.get("ana") == "shortn":
    #             syllables_string += "n"

    #     seg.set('syllables_string', syllables_string)
    
    # ------------------------------------------------------------------
    # SYLLABLES
    # The attributes which lines can have in multiples include: speaker (and the corresponding qualities gender, status, isGod, isGreek) and uncertain.
    # Note on speaker: for the most part, syllables will belong to one or another speaker. But there are times in which one speaker will switch midsyllable. In these cases, it usually is the case that the word spoken by the initial speaker ends on a consonant, while the next word spoken by the next speaker starts with a vowel and comprises more of the meaning of that syllable. So the syllable will be given to the latter speaker rather than the former.
    # ------------------------------------------------------------------

    for seg in text.xpath('//tei:seg[@type="syll"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

        # if any of the seg's ancestors or descendants are one of the editorial marks, mark this line as the corresponding certainty.
        if len(seg.xpath('.//tei:sic | ./ancestor::tei:sic',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0: 
            seg.set('uncertain', 'sic (†...†)')
        elif len(seg.xpath('.//tei:supplied | ./ancestor::tei:supplied',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0:
            seg.set('uncertain', 'supplied (&lt;...&gt;)')
        elif len(seg.xpath('.//tei:surplus | ./ancestor::tei:surplus',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0:
            seg.set('uncertain', 'deletion ([...])')
        else:
            seg.set('uncertain', 'no editorial marking')

        # set the syllables string variable in accord with the length of the syllable
        if seg.get('ana') == "long":
            seg.set('syllables_string', 'l')
        elif seg.get('ana') == "short":
            seg.set('syllables_string', 's')
        elif seg.get('ana') == "shortn":
            seg.set('syllables_string', 'n')
                 
        for line in seg.xpath('ancestor::tei:l', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

            copy_list = ['div1_type', 'div1_n_type',
                        'div2_ana', 'div2_type', 'div2_n',
                        'line_meter_name', 'line_meter_type',
                        'original_met_values', 'lyric_non_lyric', 'n']

            for a in copy_list:
                            
                att_value = line.get(a)
                if att_value == None:
                    att_value = '(syllable) line missing ' + a + '?'
                            
                seg.set(a, att_value)
                
            break
    
    # ------------------------------------------------------------------
    # WORDS
    # ------------------------------------------------------------------
    
    # get the metrical pattern of the word itself
    syllables_string = ""
    for seg in text.xpath('//tei:seg[@type="syll"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        text_milestone_arr = seg.xpath('.//text()|.//tei:milestone[@unit="word"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
        
        milestone_arr = seg.xpath('.//tei:milestone[@unit="word"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
        
        # print(text_milestone_arr, milestone_arr)

        for milestone in milestone_arr:
            if milestone != text_milestone_arr[-1]: # if the milestone isn't at the very end of the seg tag
                milestone.set("syllables_string", syllables_string)
                # print(syllables_string, end=" ")
                syllables_string = ""
             
        if seg.get("ana") == "long":
            syllables_string += "l"
        elif seg.get("ana") == "short":
            syllables_string += "s"
        elif seg.get("ana") == "shortn":
            syllables_string += "n"
        
        for milestone in milestone_arr:
            if milestone == text_milestone_arr[-1]: # if the milestone is at the very end of the seg tag
                milestone.set("syllables_string", syllables_string)
                # print(syllables_string, end=" ")
                syllables_string = ""
                
        
    

    for milestone in text.xpath('//tei:milestone[@unit="word"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

        # if any of the milestone's ancestors or descendants are one of the editorial marks, mark this line as the corresponding certainty.
        if len(milestone.xpath('./ancestor::tei:sic',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0: 
            milestone.set('uncertain', 'sic (†...†)')
        elif len(milestone.xpath('./ancestor::tei:supplied',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0:
            milestone.set('uncertain', 'supplied (&lt;...&gt;)')
        elif len(milestone.xpath('./ancestor::tei:surplus',
                namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})) > 0:
            milestone.set('uncertain', 'deletion ([...])')
        else:
            milestone.set('uncertain', 'no editorial marking')

                 
        for line in milestone.xpath('ancestor::tei:l', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                    
            for a in ['div1_type', 'div1_n_type', 
                        'div2_ana', 'div2_type', 'div2_n',
                        'line_meter_name', 'line_meter_type',
                        'original_met_values', 'lyric_non_lyric', 'n']:
                            
                att_value = line.get(a)
                if att_value == None:
                    att_value = '(milestone) line missing ' + a + '?'
                            
                milestone.set(a, att_value)
                
            break

        # syllables_string = ""
        # for syllable in milestone.xpath('descendant::tei:seg[@type="syll"]', 
        #                 namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}): # generate a syllable string based upon the lengths of the syllables. l == long, s == short, n == shortn
        #     if syllable.get("ana") == "long":
        #         syllables_string += "l"
        #     elif syllable.get("ana") == "short":
        #         syllables_string += "s"
        #     elif syllable.get("ana") == "shortn":
        #         syllables_string += "n"
        #     else:
        #         syllables_string += "ERROR"

        # milestone.set('syllables_string', syllables_string)
                
# ----------------------------------------------------------------------
# DONE W/THIS STEP.  WRITE TEMP XML.
# ----------------------------------------------------------------------

    tree.write('../temp/' + play + '.xml', pretty_print=True, encoding='utf-8')
                
# ----------------------------------------------------------------------
# EXTRACT CSV'S FROM temp XML.
# ----------------------------------------------------------------------

def get_line_text(node, play):

    line_node = node
    if node.tag != '{http://www.tei-c.org/ns/1.0}l':
        line_node = node.xpath('ancestor::tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})[0]
    
    # text_contents = []

    # Shawn's attempt at it
    # line_copy = copy.deepcopy(line_node)
    combined_text = ""
    for syllable in line_node.xpath('descendant::tei:seg[@type="syll"]', 
                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        for child_node in syllable.xpath('child::node()', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
            if etree.iselement(child_node):
                if child_node.tag == "{http://www.tei-c.org/ns/1.0}milestone" and child_node.get("unit") == "word": # if this is a <milestone unit="word"/> element
                    combined_text += " "
            else:
                combined_text += child_node
        # if child.tag.endswith('milestone'):
        #     text_contents.append(' ')
        #     if child.tail != None and child.tail > '':
        #         text_contents.append(child.tail)
        #     print("milestone", text_contents)
        # else:
        #     #text_contents.append(child.text)
        #     text_contents.append(''.join(child.itertext()))
        #     print("no-milstn", text_contents)

    # for child in line_node.xpath('descendant::tei:seg[@type="syll"]|descendant::tei:milestone[@unit="word"]', 
    #                 namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                        
    #     if child.tag.endswith('milestone'):
    #         text_contents.append(' ')
    #         if child.tail != None and child.tail > '':
    #             text_contents.append(child.tail)
    #         print("milestone", text_contents)
    #     else:
    #         #text_contents.append(child.text)
    #         text_contents.append(''.join(child.itertext()))
    #         print("no-milstn", text_contents)

    #print('get_line_text', play[0], line_node.get('n'), text_contents) 

    return '<span>' + play[0] + '.' + line_node.get('n') + '</span> ' + combined_text


def get_play_section(node, play):
    return '<span>' + play[0] + '.' + node.get('div1_n_type') + '</span> ' + node.get('div1_type')

def get_char(node, play):
    return node.get('speaker') + '<span>' + play[:2] + '</span>'
# ----------------------------------------------------------------------

lines = []
# metra = []
syllables = []
words = []

for p in glob.glob('../temp/*.xml'):
    
    tree = etree.parse(p) 
    
    text = tree.xpath('//tei:text', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})[0]
    
    play = p.split('/')[-1].split('.')[0]

    print('xml-to-csv', p, play)
    
    for line in text.xpath('//tei:l', 
                            namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                        
        line_text = get_line_text(line, play)

        play_section = get_play_section(line, play)

        character = get_char(line, play)

        lines.append({'play': play, 
                          'div1_type': play_section,
                          'div2_type': line.get('div2_type'),
                          'lyric_non_lyric': line.get('lyric_non_lyric'),
                          'div2_n': line.get('div2_n'),
                          'speaker': character,
                          'gender': line.get('gender'),
                          'status': line.get('status'),
                          'isGod': line.get('isGod'),
                          'isGreek': line.get('isGreek'),
                          'line_number': line.get('n'),
                          'original_met_values': line.get('original_met_values'),
                          'line_meter_name': line.get('line_meter_name'),
                          'line_meter_type': line.get('line_meter_type'),
                          'uncertain' : line.get('uncertain'),
                          'syllables_string' : line.get('syllables_string'),
                          'line_text': line_text})
    
    # for seg in text.xpath('//tei:seg[@type="metron"]', 
    #                     namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                        
    #     line_text = get_line_text(seg, play)
        
    #     metra.append({'play': play, 
    #                       'div1_type': '<span>' + seg.get('div1_n_type') + '</span> ' + seg.get('div1_type'),
    #                       'div2_type': seg.get('div2_type'),
    #                       'lyric_non_lyric': seg.get('lyric_non_lyric'),
    #                       'div2_n': seg.get('div2_n'),
    #                       'speaker': seg.get('speaker'),
    #                       'gender': seg.get('gender'),
    #                       'status': seg.get('status'),
    #                       'isGod': seg.get('isGod'),
    #                       'isGreek': seg.get('isGreek'),
    #                       'line_number': seg.get('n'),
    #                       'original_met_values': seg.get('original_met_values'),
    #                       'line_meter_name': seg.get('line_meter_name'),
    #                       'line_meter_type': seg.get('line_meter_type'),
    #                       'uncertain' : seg.get('uncertain'),
    #                       'syllables_string' : line.get('syllables_string'),
    #                       'line_text': line_text})
                          
    #                       #'seg_met': seg.get('met')})
    
    for seg in text.xpath('//tei:seg[@type="syll"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                        
        line_text = get_line_text(seg, play)

        play_section = get_play_section(seg, play)

        character = get_char(seg, play)
        
        syllables.append({'play': play, 
                          'div1_type': play_section,
                          'div2_type': seg.get('div2_type'),
                          'lyric_non_lyric': seg.get('lyric_non_lyric'),
                          'div2_n': seg.get('div2_n'),
                          'speaker': character,
                          'gender': seg.get('gender'),
                          'status': seg.get('status'),
                          'isGod': seg.get('isGod'),
                          'isGreek': seg.get('isGreek'),
                          'line_number': seg.get('n'),
                          'original_met_values': seg.get('original_met_values'),
                          'line_meter_name': seg.get('line_meter_name'),
                          'line_meter_type': seg.get('line_meter_type'),
                          'uncertain' : seg.get('uncertain'),
                          'syllables_string': seg.get('syllables_string'),
                          'line_text': line_text})

                          #'seg_ana': seg.get('ana')})
    
    for milestone in text.xpath('//tei:milestone[@unit="word"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                        
        line_text = get_line_text(milestone, play)

        play_section = get_play_section(milestone, play)

        character = get_char(milestone, play)
        
        words.append({'play': play, 
                          'div1_type': play_section,
                          'div2_type': milestone.get('div2_type'),
                          'lyric_non_lyric': milestone.get('lyric_non_lyric'),
                          'div2_n': milestone.get('div2_n'),
                          'speaker': character,
                          'gender': milestone.get('gender'),
                          'status': milestone.get('status'),
                          'isGod': milestone.get('isGod'),
                          'isGreek': milestone.get('isGreek'),
                          'line_number': milestone.get('n'),
                          'original_met_values': milestone.get('original_met_values'),
                          'line_meter_name': milestone.get('line_meter_name'),
                          'line_meter_type': milestone.get('line_meter_type'),
                          'uncertain' : milestone.get('uncertain'),
                          'syllables_string' : milestone.get('syllables_string'),
                          'line_text': line_text})
    
# ----------------------------------------------------------------------
# WRITE CSV'S
# ----------------------------------------------------------------------  
      
import pandas as pd

df = pd.DataFrame(lines)
df.to_csv(RESULT_FOLDER + 'lines.csv', index=False)

df = pd.DataFrame(words)
df.to_csv(RESULT_FOLDER + 'words.csv', index=False)

df = pd.DataFrame(syllables)
df.to_csv(RESULT_FOLDER + 'syllables.csv', index=False)

# df = pd.DataFrame(metra)
# df.to_csv(RESULT_FOLDER + 'metra.csv', index=False)
        
        
        
    
    

