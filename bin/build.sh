#!/bin/bash

#=======BUILD SCRIPT FOR TIM MOORE'S ANCIENT METER DATABASE=============#
#=======DEVELOPED IN CONJUNCTION WITH WUSTL'S HDW, ESP.=================#
#=============STEPHEN PENTECOST AND HENRY SCHOTT========================#

# Exit immediately if a command exits with a non-zero status
set -e

#=======================================================================#
# Handle help option                                                  #
#=======================================================================#
# Allows users to get usage instructions by passing '--help' as an argument
if [ "$1" == "--help" ]; then
    echo "Usage: ./build.sh"
    echo "Ensure Python 3 and Java are installed and accessible via PATH."
    echo "Run the script from any directory."
    exit 0
fi

#=======================================================================#
# Function to find the Python 3 interpreter                            #
#=======================================================================#
find_python3() {
    if command -v python3 &>/dev/null; then
        PYTHON_CMD=python3
    elif command -v python &>/dev/null; then
        # Check if 'python' refers to Python 3
        PYTHON_VERSION=$(python --version 2>&1)
        if echo "$PYTHON_VERSION" | grep -q "Python 3"; then
            PYTHON_CMD=python
        else
            echo "Error: 'python' is not Python 3."
            exit 1
        fi
    elif command -v py &>/dev/null; then
        # Windows Python Launcher fallback
        PYTHON_CMD="py -3"
    else
        echo "Error: Python 3 is not installed."
        exit 1
    fi
}

#=======================================================================#
# Function to find the Java interpreter                               #
#=======================================================================#
find_java() {
    if command -v java &>/dev/null; then
        JAVA_CMD=java
    else
        echo "Error: Java is not installed or not in PATH."
        exit 1
    fi
}

#=======================================================================#
# Function to check if required commands are available               #
#=======================================================================#
# Verifies that essential system commands are available before proceeding
check_command() {
    if ! command -v "$1" &>/dev/null; then
        echo "Error: Required command '$1' not found. Please install it."
        exit 1
    fi
}

#=======================================================================#
# Detect interpreters and check essential commands                   #
#=======================================================================#
find_python3    # Identify the Python 3 interpreter
find_java        # Identify the Java interpreter

# List of essential commands that must be available for the script to run
ESSENTIAL_COMMANDS=("rm" "mkdir" "cp" "basename" "grep" "java")

# Iterate over each essential command and verify its availability
for cmd in "${ESSENTIAL_COMMANDS[@]}"; do
    check_command "$cmd"
done

#=======================================================================#
# Determine the directory where the script is located                #
#=======================================================================#
# This ensures that the script can be run from any location
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

#=======================================================================#
# Establish the repository directory (repodir) based on SCRIPT_DIR   #
#=======================================================================#
# Moves up two levels from the script's directory to reach 'Moore'
repodir="$(dirname "$(dirname "$SCRIPT_DIR")")"

#=======================================================================#
# Define directory names                                              #
#=======================================================================#
# Names of the texts and tools repositories relative to 'repodir'
texts_dir_name="meters-of-ancient-drama-texts"
tools_dir_name="meters-of-ancient-drama-tools"

# Full paths to the texts and tools repositories
texts_repo_path="$repodir/$texts_dir_name"
tools_repo_path="$repodir/$tools_dir_name"

#=======================================================================#
# Print paths for debugging                                         #
#=======================================================================#
# Outputs the resolved paths to assist with troubleshooting
echo "Repository Directory: $repodir"
echo "Texts Repository Path: $texts_repo_path"
echo "Tools Repository Path: $tools_repo_path"

#=======================================================================#
# Function to clean and create directories                           #
#=======================================================================#
# Ensures that target directories are clean before use
clean_and_create_dir() {
    local dir_path="$1"
    if [ -d "$dir_path" ]; then
        rm -rf "$dir_path"  # Remove existing directory and its contents
    fi
    mkdir -p "$dir_path"    # Create the directory
}

#=======================================================================#
# CLEANING DIRECTORIES                                               #
#=======================================================================#
# Cleans and recreates the 'dist' and 'temp' directories
echo "CLEANING DIRECTORIES"
clean_and_create_dir "$tools_repo_path/dist"
clean_and_create_dir "$tools_repo_path/temp"

#=======================================================================#
# COPY SOURCE FILES INTO /dist/ DIRECTORY                            #
#=======================================================================#
# Copies all necessary source files and JSON configurations to 'dist'
echo "COPYING SRC TO DIST"

# Verify that the 'src' directory exists before copying
if [ -d "$tools_repo_path/src" ]; then
    cp -r "$tools_repo_path/src/"* "$tools_repo_path/dist/"
else
    echo "Error: Source directory '$tools_repo_path/src' does not exist."
    exit 1
fi

# Verify that the 'meterscomplete.json' file exists before copying
if [ -f "$texts_repo_path/meterscomplete.json" ]; then
    cp "$texts_repo_path/meterscomplete.json" "$tools_repo_path/dist/"
else
    echo "Error: File '$texts_repo_path/meterscomplete.json' does not exist."
    exit 1
fi

#=======================================================================#
# MUNGING XML FILES AND COPYING THEM INTO /dist/                     #
#=======================================================================#
# Processes XML files to standardize formatting and prepare for further use
echo "MUNGING XML"

# Verify that the 'HANDCHECKED' directory exists before processing
if [ -d "$texts_repo_path/HANDCHECKED" ]; then
    # Iterate over each XML file in the 'HANDCHECKED' directory
    for i in "$texts_repo_path/HANDCHECKED/"*.xml; do
        # Check if any XML files exist
        if [ ! -e "$i" ]; then
            echo "Warning: No XML files found in '$texts_repo_path/HANDCHECKED/'. Skipping XML munging."
            break
        fi
        # Use Java and Saxon to apply the XSLT transformation to each XML file
        "${JAVA_CMD}" -jar "$tools_repo_path/saxon/saxon-he-11.3.jar" \
            -xsl:"$tools_repo_path/bin/munge_xml.xsl" \
            -s:"$i" \
            -o:"$tools_repo_path/dist/$(basename "$i")"
    done
else
    echo "Error: Directory '$texts_repo_path/HANDCHECKED' does not exist."
    exit 1
fi

#=======================================================================#
# RUN PYTHON SCRIPT TO ADD INDENTATION                                #
#=======================================================================#
# Enhances the readability of XML files by adding proper indentation
if [ -f "$tools_repo_path/bin/add_indent.py" ]; then
    "${PYTHON_CMD}" "$tools_repo_path/bin/add_indent.py" "$tools_repo_path/dist/"
else
    echo "Error: Python script '$tools_repo_path/bin/add_indent.py' does not exist."
    exit 1
fi

#=======================================================================#
# COMPILE JSONS FOR STATS PAGES                                       #
#=======================================================================#
# Generates JSON files required for statistical analysis on the website
echo "COMPILING JSONS FOR STATS PAGES"

# Iterate over each XML file in the 'dist' directory to compile JSONs
for i in "$tools_repo_path/dist/"*.xml; do
    # Check if any XML files exist
    if [ ! -e "$i" ]; then
        echo "Warning: No XML files found in '$tools_repo_path/dist/'. Skipping JSON compilation."
        break
    fi
    # Use Java and Saxon to apply the XSLT transformation for JSON compilation
    "${JAVA_CMD}" -jar "$tools_repo_path/saxon/saxon-he-11.3.jar" \
        -xsl:"$tools_repo_path/bin/get_character_json.xsl" \
        -s:"$i" \
        -o:"${i%.xml}Characters.json"
done

#=======================================================================#
# EXTRACT CROSSFILTER DATA FROM COMPLETED XML                         #
#=======================================================================#
# Runs a Python script to extract data for crossfilter functionality
echo "EXTRACTING CROSSFILTER DATA"

# Change the current working directory to 'repodir' to ensure correct relative paths
cd "$repodir"

# Verify that the 'extract_crossfilter_csvs.py' script exists before executing
if [ -f "$tools_repo_path/bin/extract_crossfilter_csvs.py" ]; then
    "${PYTHON_CMD}" "$tools_repo_path/bin/extract_crossfilter_csvs.py"
else
    echo "Error: Python script '$tools_repo_path/bin/extract_crossfilter_csvs.py' does not exist."
    exit 1
fi

#=======================================================================#
# COPY EXTRACTED CSVS TO COMBINED STATISTICS DATA DIRECTORY           #
#=======================================================================#
# Ensures that the extracted CSV files are available for combined statistics
combined_data_dir="$tools_repo_path/dist/combined_statistics/data"
stats_data_dir="$tools_repo_path/dist/stats_crossfilter/data"

# Check if both source and destination directories exist before copying
if [ -d "$stats_data_dir" ] && [ -d "$combined_data_dir" ]; then
    cp "$stats_data_dir/"*.csv "$combined_data_dir/"
else
    echo "Error: One or both directories '$stats_data_dir' or '$combined_data_dir' do not exist."
    exit 1
fi

echo "BUILD COMPLETE"

#=======================================================================#
# START A LOCAL HTTP SERVER TO SERVE THE /dist/ DIRECTORY              #
#=======================================================================#
# Launches a local server to preview the built site
echo "CREATING LOCAL HOST"

# Navigate to the 'dist' directory to serve its contents
if cd "$tools_repo_path/dist"; then
    :
else
    echo "Error: Cannot navigate to dist directory '$tools_repo_path/dist'."
    exit 1
fi

#=======================================================================#
# DETERMINE AN AVAILABLE PORT                                           #
#=======================================================================#
# Starts the server on the default port (8000) or finds the next available port
DEFAULT_PORT=8000
PORT=$DEFAULT_PORT

# Function to check if a port is already in use
is_port_in_use() {
    lsof -i :"$1" &>/dev/null
}

# Loop to find an available port between 8000 and 8100
while is_port_in_use "$PORT"; do
    PORT=$((PORT + 1))
    if [ "$PORT" -gt 8100 ]; then
        echo "Error: No available ports between $DEFAULT_PORT and 8100."
        exit 1
    fi
done

echo "Starting HTTP server on port $PORT"

#=======================================================================#
# START THE HTTP SERVER                                                #
#=======================================================================#
# "${PYTHON_CMD}" -m http.server "$PORT" &
# SERVER_PID=$!

# # Create a cleanup function
# cleanup() {
#     echo "Shutting down server..."
#     kill -TERM $SERVER_PID 2>/dev/null
#     wait $SERVER_PID 2>/dev/null
# }

# # Set up traps for different termination signals
# trap cleanup SIGINT SIGTERM EXIT

# # Keep script running until interrupted
# while true; do
#     sleep 1
# done


echo "CREATING LOCAL HOST"

cd "$tools_repo_path/dist" || exit 1

DEFAULT_PORT=8000
PORT=$DEFAULT_PORT

while lsof -i :"$PORT" &>/dev/null; do
    PORT=$((PORT + 1))
    if [ "$PORT" -gt 8100 ]; then
        echo "Error: No available ports between $DEFAULT_PORT and 8100."
        exit 1
    fi
done

echo "Starting browser-sync on port $PORT"

browser-sync start --server --files "**/*.html, **/*.css, **/*.js" --watch --port $PORT &
SERVER_PID=$!

cleanup() {
    echo "Shutting down server..."
    kill -TERM $SERVER_PID 2>/dev/null
    wait $SERVER_PID 2>/dev/null
}

trap cleanup SIGINT SIGTERM EXIT

while true; do
    sleep 1
done

