#!/usr/bin/env python

import glob, json, sys
from lxml import etree

FOLDER = sys.argv[1]
PLAYS = ['Orestes']

for play in PLAYS:
    
    tree = etree.parse(FOLDER + play + '.xml')
    
    line_numbers_with_split_words = []
    
    for line in tree.xpath('//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        
        line_ends_with_word = False
        
        line_children = [node for node in line \
                            if ('_Comment' in str(type(node))) == False and \
                                node.get('unit') not in ['comment', 'period', 'cont']]
        
        #if line_children[-1].tag == '{http://www.tei-c.org/ns/1.0}milestone':
        #    print(play, line.get('n'), line_children[-1].tag, line_children[-1].get('unit'))
        
        if line_children[-1].tag == '{http://www.tei-c.org/ns/1.0}seg' and \
            line_children[-1].get('type') == 'syll':
            
            text_content = ''.join(line_children[-1].itertext()).strip()
            
            if text_content.endswith('-'):

                #print(play, line.get('n'), line_children[-1].tag, 
                #      line_children[-1].get('type'),
                #         text_content)
                
                line_numbers_with_split_words.append(line.get('n'))
        
        if line_children[-1].tag == '{http://www.tei-c.org/ns/1.0}seg' and \
            line_children[-1].get('type') == 'metron':
                
            metron_children = [node for node in line_children[-1] \
                            if ('_Comment' in str(type(node))) == False and \
                                node.get('unit') not in ['comment', 'period', 'cont']]
            
            text_content = ''.join(metron_children[-1].itertext()).strip()
            
            if text_content.endswith('-'):
            
                #print(play, line.get('n'), metron_children[-1].tag, 
                #      metron_children[-1].get('type'),
                #         text_content)
                
                line_numbers_with_split_words.append(line.get('n'))
        
    all_lines = tree.xpath('//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
       
    print()
    print(play, line_numbers_with_split_words)
        
    for a, line in enumerate(all_lines):
        all_lines[a].set('indent', 'False')
        
    for a, line in enumerate(all_lines):
        if line.get('n') in line_numbers_with_split_words:
            all_lines[a + 1].set('indent', 'True')
            
    tree.write(FOLDER + play + '.xml', encoding='utf-8')
    
print('ok')

