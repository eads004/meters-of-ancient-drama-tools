<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="//tei:profileDesc//tei:listPerson"/>
    </xsl:template>
    
    <xsl:template match="tei:listPerson">[<xsl:apply-templates select="tei:person"/>]</xsl:template>
    
    <xsl:template match="tei:person">
  {
    "name": "<xsl:value-of select="tei:persName"/>",
    "gender": "<xsl:value-of select="tei:sex"/>",
    "status": "<xsl:value-of select="tei:socecStatus"/>",
    "isGod": "<xsl:value-of select="tei:trait/tei:desc"/>",
    "isGreek": "<xsl:value-of select="tei:nationality"/>"
  }<xsl:if test="position() ne last()">,</xsl:if>
    </xsl:template>
    
    
</xsl:stylesheet>