#
#   Note that this script runs a python script which expect py3
#


# To start, a simple build script that copies /src to /dist and /texts to /dist

repodir=$(dirname $0)/..

# clean dist and temp

rm -rf $repodir/dist
mkdir $repodir/dist
rm -rf $repodir/temp
mkdir $repodir/temp

# copy source files

cp -r $repodir/src/* $repodir/dist
cp $repodir/texts/meterscomplete.json $repodir/dist

# copy text XML files

for i in $repodir/texts/*.xml;
do
  java -jar /Users/pentecost/0/saxon9he.jar -xsl:$repodir/bin/munge_xml.xsl -s:$i -o:$repodir/dist/`basename "$i"`;
done

./add_indent.py ../dist/

for i in $repodir/dist/*.xml;
do
  java -jar /Users/pentecost/0/saxon9he.jar -xsl:$repodir/bin/get_character_json.xsl -s:$i -o:${i%.xml}Characters.json;
done

# Create stats data.  Note that there's line in the python script that needs to change when a new play is finished and ready for the site; otherwise, the new play will not be included.

# Note also that it would be nice to have a helper script which created mergedCharacters.json.

./extract_crossfilter_csvs.py

cp ../dist/stats_crossfilter/data/*.csv ../dist/combined_statistics/data/


