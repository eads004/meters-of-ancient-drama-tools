# pairs with align_structure_translation_json.py
import json
import sys

if len(sys.argv) != 3:
    raise Exception("Two arguments needed.")

t_file = sys.argv[1] # the translation file
r_file = sys.argv[2] # the result file

t_json = json.load(open(t_file))

for part_dict in t_json:
    part_dict["part"] = part_dict["proposed section"]
    del part_dict["proposed section"]

file = open(r_file, "w")
file.write(json.dumps(t_json, sort_keys=True, indent=4))
file.close()