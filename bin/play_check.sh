# make a new folder to put new results in

bindir=$(dirname $0)
repodir=$bindir/..

# get the next folder

n=0
while ! mkdir results$n
do
    n=$((n+1))
done

# copy the play from meters-of-ancient-drama-texts/HANDCHECKED to text/HANDCHECKED (overrides the temporary file!)

cp $repodir/../meters-of-ancient-drama-texts/HANDCHECKED/Orestes.xml $repodir/texts/HANDCHECKED


# Upon ctrl-C, remove result folder

trap "{ rm -r results$n ; echo removing result$n ; exit 255; }" INT

for i in $repodir/texts/HANDCHECKED/*.xml;
do
	# get file names from texts/HANDCHECKED folder
	filename=$(basename -- "$i")
	extension="${filename##*.}"
	filename="${filename%.*}"

	# set default variable values
	diggle_lines="0"
	perseus_parameter="error"
	first_footnote_lourenco="error"
	first_page_lourenco="error"
	last_page_lourencno="error"
	source config/${filename}.config

	echo "Comparing ${filename} for Text and Scansion"

	echo "Comparing Diggle and Perseus Lines"
	python compare_text.py $i OSEO_HTML/${filename}_OSEO.html ${perseus_parameter} results${n}/${filename}_printout.txt ${diggle_lines}

	# echo "Comparing Diggle Lines"
	# python compare_line_diggle.py $i OSEO_HTML/${filename}_OSEO.html results${n}/${filename}_Diggle.txt ${lines_to_exclude_diggle}

	# echo "Comparing Perseus Lines"
	# python compare_lines_perseus.py $i ${perseus_parameter} results${n}/${filename}_Perseus.txt ${lines_to_exclude_perseus}

	echo "Comparing Lourenco Scansion"
	python lourenco_pdf_reader.py $i Lourenço_2011.pdf results${n}/${filename}_Lourenco.txt ${first_footnote_lourenco} ${first_page_lourenco} ${last_page_lourenco}
done

echo "Results in results${n} Directory"

exit 0
