#
#   Note that this script runs a python script which expect py3
#

usage() { echo "Usage: $0 [-RIF]" 1>&2; exit 1; }

while getopts "RIF" opt; do
    case "${opt}" in
        R)
          rebuild=true
          ;;
        I)
          update_interface=true
          ;;
        F)
          update_filters=true
          ;;
        *) 
          usage
          exit
          ;;
    esac
done

# To start, a simple build script that copies /src to /dist and /texts to /dist
# if option -R
if [ "$rebuild" = true ] ;
then
  repodir=$(dirname $0)/..

  echo CLEANING DIRECTORIES
  rm -rf $repodir/temp
  mkdir $repodir/temp

  # copy the play and the json from meters-of-ancient-drama-texts to texts (overrides the temporary files!)
  if [ "$update_filters" = true ] || [ "$update_interface" = true ] ;
  then
    echo clearing dist
    rm -rf $repodir/dist
    mkdir $repodir/dist

    echo copying meters-of-ancient-drama-texts to texts
    rm -rf $repodir/texts
    mkdir $repodir/texts
    cp -r $repodir/../meters-of-ancient-drama-texts/* $repodir/texts

    # echo removing Elektra.xml and Hekabe.xml for now
    # rm $repodir/texts/HANDCHECKED/Elektra.xml
    # rm $repodir/texts/HANDCHECKED/Hekabe.xml
  fi

  echo COPYING SRC TO DIST
  cp -r $repodir/src/* $repodir/dist
  cp $repodir/texts/meterscomplete.json $repodir/dist

  if [ "$update_interface" = true ] ;
  then
    echo MUNGING XML
    for i in $repodir/texts/HANDCHECKED/*.xml;
    do
      java -jar ~/Desktop/Programming/SaxonHE11-3J/saxon-he-11.3.jar -xsl:$repodir/bin/munge_xml.xsl -s:$i -o:$repodir/dist/`basename "$i"`;
    done

    echo ADDING INDENTS AND MORE MUNGING
    for i in $repodir/dist/*.xml;
    do
      # get file names from dist folder
      filename=$(basename -- "$i")
      extension="${filename##*.}"
      filename="${filename%.*}"

      indent_lines="0"
      source config/${filename}.config

      python add_indent_diggle.py ../dist/${filename}.xml ${indent_lines} # just do this to the handchecked xml itself
      java -jar ~/Desktop/Programming/SaxonHE11-3J/saxon-he-11.3.jar -xsl:$repodir/bin/get_character_json.xsl -s:$i -o:${i%.xml}Characters.json;
    done
  fi

  if [ "$update_filters" = true ] ;
  then
    echo EXTRACTING CROSSFILTER DATA
    python extract_crossfilter_csvs.py

    cp ../dist/stats_crossfilter/data/*.csv ../dist/combined_statistics/data/
  fi

  echo BUILD COMPLETE
fi

echo CREATING LOCAL HOST
cd ../dist
osascript -e 'display notification "build_SD.sh is complete" with title "Website Build Complete"'
python -m http.server