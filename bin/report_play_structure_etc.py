#!/usr/bin/env python

import glob, json, sys
from lxml import etree
from collections import Counter

# ----------------------------------------------------------------------
# QA code for now, and not actually used in building        
# ----------------------------------------------------------------------
  
FOLDER = '../texts/HANDCHECKED/'
TEXTS_TO_PROCESS = ['Orestes', 'Hekabe']
          
# ----------------------------------------------------------------------

for p in glob.glob(FOLDER + '*.xml'):
    
    play = p.split('/')[-1].split('.')[0]
    
    if play not in TEXTS_TO_PROCESS:
        print('extract_crossfilter_csvs.py skipping', p)
        continue

    print()
    
    tree = etree.parse(p) 

    for div1 in tree.xpath('//tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

        lines = div1.xpath('descendant::tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

        print(play, div1.get('type'), lines[0].get('n'), lines[-1].get('n'))
          
# ----------------------------------------------------------------------

print()

data = json.load(open('../src/OrestesStructure.json', 'r', encoding='utf-8'))

for c in data['children']:
    print(c['name'], c['start'], c['end'])
          
# ----------------------------------------------------------------------

from collections import Counter

all_short_long = []

for p in glob.glob(FOLDER + '*.xml'):
    
    play = p.split('/')[-1].split('.')[0]
    
    if play not in TEXTS_TO_PROCESS:
        print('skipping', p)
        continue
    
    tree = etree.parse(p) 

    for seg in tree.xpath('//tei:seg[@type="syll"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        all_short_long.append(seg.get('ana'))

for w in Counter(all_short_long).most_common():
    print(w[0], w[1])

        
    
    

