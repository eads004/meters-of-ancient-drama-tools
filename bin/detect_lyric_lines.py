# HOW TO USE:
# Obtain a copy of the `Lourenço_2011.pdf` file.
# Find the starting and ending page numbers of the play in question and the number of the first footnote that you find.
# Use the syntax `python3 lourenco_pdf_reader.py XML_FILEPATH LORENCO_FILEPATH RESULT_FILEPATH FIRST_FOOTNOTE_NUMBER [FIRST_PAGE LAST_PAGE]`.
# !!NOTE!! The page numbers are NOT based on the page numbers of the pdf on the bottom of the page, but rather the page number based on the order of the sheets. The second page on the pdf will be page 2, regardless of the page number at the bottom of the page. These numberings are usually clearly demarcated on any given pdf reader of choice.
# XML_FILEPATH: the XML file of the play
# LORENCO_FILEPATH: the Lorenco file mentioned above
# RESULT_FILEPATH: the file that is to contain the final product, which is a summary of all the differences in meter found between the Lourenco text and the XML file
# FIRST_FOOTNOTE_NUMBER: within the section of the pdf that contains the play in question, the first footnote number (e.g., in Orestes, the first footnote number is 150)
# FIRST_PAGE: the first page of the section of the pdf that contains the play in question, with numbering according to the note above
# LAST_PAGE: the last page of the section of the pdf that contains the play in question, with numbering according to the note above


import PyPDF2
import re
from bs4 import BeautifulSoup
import sys
from lourenco_pdf_reader import extract_line_dict, get_only_one_line_number

if __name__ == "__main__":
	FIRST_PAGE = None
	LAST_PAGE = None

	try:
		XML_FILE = sys.argv[1]
		LORENCO_FILE = sys.argv[2]
		RESULT_FILE = sys.argv[3]
		FIRST_FOOTNOTE = int(sys.argv[4])
		if (len(sys.argv) > 5):
			FIRST_PAGE = int(sys.argv[5]) - 1
			LAST_PAGE = int(sys.argv[6])
	except:
		sys.exit("Syntax is `python3 lourenco_pdf_reader.py XML_FILEPATH LORENCO_FILEPATH RESULT_FILEPATH FIRST_FOOTNOTE_NUMBER [FIRST_PAGE LAST_PAGE]`.")


	# LORENCO_FILE = "lourenco_orestes_only.pdf"
	lorenco_file = open(LORENCO_FILE, "rb")

	fileReader = PyPDF2.PdfFileReader(lorenco_file)
	play_pages = fileReader.pages[FIRST_PAGE:LAST_PAGE]
	# first_footnote = 150
	first_footnote = FIRST_FOOTNOTE
	play_meter_dict = {}

	for page in play_pages:
		# print(page.extract_text())
		page_meter_dict, first_footnote = extract_line_dict(page.extract_text(), first_footnote)
		# print(first_footnote)
		play_meter_dict.update(page_meter_dict)

	prev_ln = None
	for line_no in play_meter_dict.keys():
		ln = ""
		for char in line_no:
			if char in '0123456789-':
				ln += char
	
		if prev_ln == None:
			prev_ln = get_only_one_line_number(ln)
			continue
		if get_only_one_line_number(ln) - prev_ln == 0 or get_only_one_line_number(ln) - prev_ln == 1:
			pass
		else:
			print(line_no)
		prev_ln = get_only_one_line_number(ln)
	print(play_meter_dict.keys())
	

