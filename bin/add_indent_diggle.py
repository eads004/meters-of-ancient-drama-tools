from compare_line_diggle import scrape_oxford
from bs4 import BeautifulSoup, Tag, NavigableString
import sys

"""
SYNTAXES:
`python3 add_indent_diggle.py {OXFORD_HTML_FILE}`
Prints out all of the lines in the play to indent given the filepath to the Diggle HTML version of the play.
OXFORD_HTML_FILE: The filepath to the Diggle HTML version of the play.

`python3 add_indent_diggle.py {XML_FILE} {INDENT_LINES}` (e.g. `python3 add_indent_diggle.py test.xml "1 2"`) OR
`python3 add_indent_diggle.py {XML_FILE} {INDENT_LINES...}` (e.g. `python3 add_indent_diggle.py test.xml 1 2`)
Updates the XML file given a string of numbers separated by whitespace that correspond to the lines to be indented. The indented lines can also be separated on the terminal command itself.
XML_FILE: The XML file to be modified.
INDENT_LINES: The lines to be modified.
"""

def check_indents(oxford_html_file):
    with open(oxford_html_file) as html_file:
        content = html_file.readlines()
        content = "".join(content)
        oxford_soup = BeautifulSoup(content, "lxml")

    diggle_text_dict, line_order_list = scrape_oxford(oxford_soup, replace_lunate_string=False)

    pot_indent_lines = []
    for key, value in diggle_text_dict.items():
        if "\xa0\xa0\xa0\xa0" in value:
            pot_indent_lines.append(key)
    print(" ".join(pot_indent_lines)) # this result needs to be checked against Lourenco.

def update_xml(xml_file, indent_lines):
    with open(xml_file) as file:
        content = file.readlines()
        content = "".join(content)
        xml_soup = BeautifulSoup(content, "xml")
    
    for line in xml_soup.find_all("l"):
        if line["n"] != 0:
            if line["n"] in indent_lines if "-" not in line["n"] else line["n"] in indent_lines or line['n'].split("-")[0] in indent_lines or str(int(line['n'].split("-")[0]) + 1) in indent_lines:
                line["indent"] = True
            else:
                line["indent"] = False

    with open(xml_file, "w") as file:
        file.write(str(xml_soup))

if __name__ == "__main__":
    
    if len(sys.argv) <= 2:
        check_indents(sys.argv[1])
    elif len(sys.argv) == 3:
        if sys.argv[2] == "0":
            pass # do nothing if the build script enters in "0"
        else: 
            update_xml(sys.argv[1], sys.argv[2].split())
    else:
        update_xml(sys.argv[1], sys.argv[2:])

# check_indents("OSEO_HTML/Orestes_OSEO.html")
# update_xml(sys.argv[1], ['150', '163', '186', '207', '326', '327', '328', '330', '342', '343', '344', '346', '813', '815', '817', '818', '825', '827', '829', '830', '843', '966b', '977b', '991b', '999b', '1008', '1010', '1249-50', '1270', '1370', '1371', '1376', '1383', '1389a', '1410', '1411', '1413', '1419', '1435', '1445', '1446a','1448b', '1458', '1462', '1470', '1482', '1448b', '1499', '1502']) # TODO: make this into a command line command so that it can be executed as-needed.