from docx import Document
import sys
import re
import copy
from lxml import etree

PLAYNAME = sys.argv[1]

DOC_TO_PROCESS = "lyric_templates/Euripides/" + PLAYNAME + ".docx"
XML_TO_PROCESS = "flattened_xml/" + PLAYNAME + "_Greek.xml"

doc = Document(DOC_TO_PROCESS)

tree = etree.parse(XML_TO_PROCESS)

div1s = []
div2s = []


# I'm not exactly sure why I wrote this. It is very necessary with how the program works though.
# Do not edit or remove it unless you really know what you're doing.

# I'm pretty sure it had something to do with reckoning between the pointers to tree objects and the comparison of elements later in the program. 
# By mapping them to a static location, one can lookup whatever they'd like and make comparisons. N.B. That explanation could be wrong.
def build_line_number_map(tree):
    line_number_map = {}
    l_elements = tree.xpath('//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
    for l_element in l_elements:
        line_number = l_element.get('n')
        if line_number is not None:
            line_number_map[line_number] = l_element
    return line_number_map

# This function should try to find the XML <l> tag that has the same n attribute of unit_ptr, if it cannot, it will recursively search backwards and return the nearest <l> tag.
# E.G. if one searches for 1225a and there is no such line, one should get 1225.
#       if there is no line 1225, one should get 1224, and so on until an <l> tag is found.
def find_closest_match(unit_ptr, line_number_map):
    if unit_ptr in line_number_map:
        return line_number_map[unit_ptr]
    closest_match = min((key for key in line_number_map if key >= unit_ptr), default=None)
    return line_number_map[closest_match] if closest_match is not None else None

#This function takes our docx lines and extracts ONLY the relevantly formatted information.
#It goes line by line (run by run) and appends text either to bolded_text or underlined_text
#if the run matches the desired formatting. If a line should be partially formatted,
#only the formatted characters are added for processing.
def extract_formatted_runs(doc):
    bolded_text = []
    underlined_text = []

    for paragraph in doc.paragraphs:
        curr_bold_text = []
        curr_under_text = []

        for run in paragraph.runs:
            if run.bold:
                curr_bold_text.append(run.text.strip())
            else:
                if curr_bold_text:
                    bolded_text.append(''.join(curr_bold_text))
                    curr_bold_text = []

            if run.underline:
                curr_under_text.append(run.text.strip())
            else:
                if curr_under_text:
                    underlined_text.append(''.join(curr_under_text))
                    curr_under_text = []

        if curr_bold_text:
            bolded_text.append(''.join(curr_bold_text))
        if curr_under_text:
            underlined_text.append(''.join(curr_under_text))
    return bolded_text, underlined_text

# I made this its own function for scalability. 
# It's a little redundant on its own in this script.
def parse_docx(DOCUMENT):
    doc = Document(DOCUMENT)
    Bs, Us = extract_formatted_runs(doc)

    return Bs, Us


###                                       ###
#   This is where the script's logic begins.#
###                                       ###

line_number_map = build_line_number_map(tree)
div1s, div2s = parse_docx(DOC_TO_PROCESS)

#Debugging: Ensure that div1s and div2s are expected values
#print("DIV1s: ", div1s)
#print()
#print("DIV2s: ", div2s)


###                                     ###
#               PROCESS THE DIV1s.        #
#           PARSE THE NAMES AND RANGES    #
#       EXAMPLES                          #
#           PROLOGOS: 1-116               #
#           PARODOS: 117-146              #
#           . . .                         #
###                                     ###


for div1 in div1s:  #['PROLOGOS: 1-116', PARODOS: 117-146',...]
    #SKIP all lines that are metra with lingering bold/italic formatting. It's easier to skip them then go through and hunt down all the formatting in the docx...
    if sum(c.isalpha() for c in div1) == 0:
        continue

    unit_name, unit_range = map(str.strip, div1.split(':'))

    #Common typos and formatting mistakes. Like above, it's easier to just deal with it here.
    unit_name = unit_name.replace("PROLOGUS", "PROLOGOS").replace("PARADOS", "PARODOS").replace("EXODUS", "EXODOS").replace("FIRSTSTASIMON", "FIRST STASIMON").replace("SECONDSTASIMON", "SECOND STASIMON").replace("THIRDSTASIMON", "THIRD STASIMON").replace("FIRSTEPISODE", "FIRST EPISODE").replace("SECONDEPISODE", "SECOND EPISODE")

    unit_type = unit_name.lower().capitalize().replace('\n', ' ')
    unit_begin, unit_end = "", ""

    split_range = unit_range.split('-')

    #This conditional tries to catch all lines that may have been written with a parenthetical (lyric) or (nonlyric) at their tail. E.G. STROPHE 1: 122-156 (lyric)
    #We only care about the numbers so we only get them.
    if sum(c.isalpha() for c in unit_range) <= 1:
        unit_begin, unit_end = map(str.strip, split_range)
    else:
        unit_begin = split_range[0]
        unit_end = split_range[1].split(' ')[0]
        unit_note = split_range[1].split(' ')[1]
    

    ###                                     ###
    #   DEBUG STATEMENTS FOR DIV1 PROCESSING  #
    ###                                     ###

    print("DIV1 parts: ", unit_name, unit_range)
    print("DIV1 Unit begin: ", unit_begin)
    print("DIV1 Unit end: ", unit_end)


    ###                                                                                                         ###
    #                                           XML MANIPULATION                                                  #
    #       1. Create a list of nodes to append to our tree                                                       #
    #       2. Look for the element that has the same n attribute in its <l> tag as the starting element          #
    #       3. Look for the element that has the same n attribute in its <l> tag as the ending element            #
    #                                                                                                             #
    #       N.B. Steps 2 and 3 have internal logic to find the closest match in case the XML differs from Achiele #
    #       See find_closest_match(STRING, MAP) above for more.                                                   #
    ###                                                                                                         ###

    nodes_to_move = []
    starting_element = find_closest_match(unit_begin, line_number_map)
    ending_element = find_closest_match(unit_end, line_number_map)

    #   We use a state machine to track when we're done appending nodes to our div.
    state = 'before-section'

    for l in tree.xpath('//tei:l | //tei:milestone', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        if l == starting_element:
            state = 'to-copy'
        if state == 'to-copy':
            nodes_to_move.append(l)
        if l == ending_element:
            state = 'after-section'

    ###                                                                   ###
    #   DEBUG STATEMENTS TO ENSURE WE CAUGHT ALL THE NODES WE'RE EXPECTING  #
    ###                                                                   ###

    #print("# of div1 nodes to move:", len(nodes_to_move))
    #print()

    div1_element = etree.Element("div1", type=unit_type)
    nodes_to_move[0].addprevious(div1_element)

    for node in nodes_to_move:
        n = copy.deepcopy(node)
        div1_element.append(n)
        node.getparent().remove(node)

#   rewrite the tree so we definitively know where the div1s are. WE MAY NOT NEED TO DO THIS...

TMP_XML = 'work_files/temp.xml'

tree.write(TMP_XML, pretty_print = True, encoding = 'utf-8')

tree = etree.parse(TMP_XML)
line_number_map = build_line_number_map(tree)

###                                                   ###
#     PROCESS DIV2s. See within for individual cases    #
###                                                   ###  

for div2 in div2s:
    # We set default values for the variables we track for debugging statements below. We don't want to accidentally call a variable that doesn't exist for our case.
    # We know that nothing has happened to the variable if it remains blank. If it doesn't apply to what we're doing, we set the variable as "N/A"
    unit_name = ""
    unit_resp = ""
    unit_begin = ""
    unit_end = ""
    unit_met = ""
    unit_type = ""

    if any(char.isdigit() for char in div2):
        div2_parts = div2.split(':')

        if len(div2_parts) == 2 and sum(c.isalpha() for c in div2_parts[0]) > 1:
            #####                                                                                                                                         #####
            #                                                                       CASE 1                                                                    #
            #                                       The div2 only tracks the div's name, its responsion if any, and its range.                                #
            #                                                                                                                                                 #
            #                                   E.G.                        STROPHE 1: 231-245                                                                #
            #                                                               EPODE: 790-801                                                                    #
            #                                                               ANTISTROPHE: 613-622                                                              #
            #                                                                                                                                                 #
            #                       In cases where a number is not provided on the left-hand side of the colon, we set the responsion to "N/A"                #
            #                           UNLESS the term on the left-hand side contains the string "STROPHE" (this also catches ANTISTROPHE).                  #
            #       In this case, we default responsion to 1. This is a stylistic choice and keeps strophic verses consistently tracked in the database.      #
            #####                                                                                                                                         #####

            name_and_resp = div2_parts[0]
            name_parts = name_and_resp.split(' ')
            if len(name_parts) == 1:
                unit_name = name_parts[0]
                unit_resp = "1" if "STROPHE" in unit_name else "N/A"
            else:
                unit_name = name_parts[0]
                unit_resp = name_parts[1]

            range_parts = div2_parts[1].strip().split('-')
            unit_begin = range_parts[0]
            unit_end = range_parts[1].split('(')[0].strip()

            unit_met = "N/A"
            unit_type = "lyric"

        else:
            #####                                                                                                                                         #####
            #                                                                       CASE 2                                                                    #
            #                                           The div2 tracks the range, the meter, and whether or not it is lyric.                                 #
            #                                                                                                                                                 #
            #                                   E.G.                        1-40: ia3 (nonlyric)                                                              #
            #                                                               618-645: an2^ (lyric)                                                             #
            #####                                                                                                                                         #####

            unit_name = "N/A"
            unit_met = "N/A"

            range_parts = div2_parts[0].strip()
            meter_type = div2_parts[1].split('(')[0].strip()
            unit_type = 'nonlyric' if 'nonlyric' in div2_parts[1] else 'lyric'

            if '-' in range_parts:
                unit_begin, unit_end = map(str.strip, range_parts.split('-'))
            else:
                #This statement should only be used when we have a div2 that is a single line long. This occurs at least once in Euripides.
                unit_begin = div2_parts[0]
                unit_end = unit_begin

        #XML MANIP
        nodes_to_move = []
        state = 'before-section'

        starting_element = find_closest_match(unit_begin, line_number_map)
        ending_element = find_closest_match(unit_end, line_number_map)

        for l in tree.xpath('//tei:l | //tei:milestone', namespaces={'tei':'http://www.tei-c.org/ns/1.0'}):
            if l.text == starting_element.text:
                state = 'to-copy'
            if state == 'to-copy':
                nodes_to_move.append(l)
            if l.text == ending_element.text:
                state = 'after-section'

        print("DIV2 parts:", div2_parts)
        print("DIV2 Unit begin: ", unit_begin)
        print("DIV2 Unit end: ", unit_end)

        #create the div2 element
        if unit_name != "N/A":
            div2 = etree.Element("div2", name=unit_name, type=unit_type, n=unit_resp)
        else:
            div2 = etree.Element("div2", type=unit_type, met=unit_met)

        for node in nodes_to_move:
            n = copy.deepcopy(node)
            div2.append(n)

        #find the parent element of the starting element
        parent_element = starting_element.getparent()

        #insert the div2 element before the starting_element
        parent_element.insert(parent_element.index(starting_element), div2)
        for node in nodes_to_move:
            parent_element.remove(node)
    else:
        continue

OUTPUT_FILE = 'work_files/templated/' + PLAYNAME + '.xml'

tree.write(OUTPUT_FILE, pretty_print=True, encoding='utf-8')

print("ok")
