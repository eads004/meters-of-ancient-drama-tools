# Ctrl-C-Ctrl-V'ed from Stephen Pentecost's meters-of-ancient-drama-scansion repository. Modified to include brevis in longo in creation of the json file

import glob, json, re, string, sys

from cltk.alphabet.grc import filter_non_greek

FOLDER = sys.argv[1]

def containsAny(str, set):
    return 1 in [c in str for c in set]

for p in glob.glob('from_tim/' + FOLDER + '/*.md'):
    
    #if 'AeschylusAgamemnon' not in p:
    #    continue
    
    #if 'EuripidesHecabe' in p or 'EuripidesOrestes' in p:
    #    pass
    #else:
    #    continue
    
    lines = [l.strip() for l in open(p, 'r', encoding='utf-8').read().split('\n') if l.strip() > '']
    
    #print()
    #print(lines[:50])
    #print()
    
    line_pairs = []

    print(p)
    
    for a in range(0, len(lines) - 1):
        if filter_non_greek(lines[a]) == lines[a] and '˘' in lines[a] or '⏑' in lines[a] or '‒' in lines[a]: # first expression is to check if lines have any greek in them; if there is, it by necessity does not contain scansion information; therefore, skip it for the next line.
            
            line_text = lines[a + 1].replace('\u200e', '').replace('\[', '[').replace('\]', ']').strip()
            post_line_data = []
            
            end_of_line = -1
            for b, letter in enumerate(line_text):
                if letter in '1234567890':
                    end_of_line = b
                    break
            if end_of_line > -1:
                post_line_data = line_text[end_of_line:].strip().split(' ')
                line_text = line_text[:end_of_line].replace('|', '').strip()
                
            scansion = lines[a].replace('\u200e', '').strip().replace('|', '')
            scansion = re.sub("[^˘⏑‒∩]", "", scansion) # removes any non-scansion characters
            scansion = scansion.replace('˘', 'S')
            scansion = scansion.replace('⏑', 'S')
            scansion = scansion.replace('‒', 'L') 
            scansion = scansion.replace('∩', 'N')
            scansion.replace(' ', '').strip()
            final_scansion = []
            for c in scansion:
                final_scansion.append(c)
            final_scansion = ' '.join(final_scansion)
                
            if containsAny(line_text, '&*' + string.ascii_letters): 
                pass
            elif '˘' in line_text or '⏑' in line_text:
                pass
            # elif '∩' in scansion:
            #     pass
            else:
                line_pairs.append([final_scansion, line_text, post_line_data])
    
    if len(line_pairs) == 0:
        print(p)
        print(len(lines))
        print(len(line_pairs))
    else:
            
        file_name = p.split('/')[-1]

        import subprocess
        subprocess.run(["mkdir", "from_tim/" + FOLDER + "/json"])

        f = open('from_tim/' + FOLDER + '/json/' + file_name.replace('.md', '.json'), 'w', encoding='utf-8')
        f.write(json.dumps(line_pairs, indent=4, ensure_ascii=False))
        f.close()
    
print('Done')