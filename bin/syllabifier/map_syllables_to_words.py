#!/home/spenteco/anaconda2/envs/py3/bin/python

import re, traceback, json
from Greek_Prosody.characters import *
from Greek_Prosody.syllables import syllabify_moore_rules

def map_syllables_to_words(words, syllables, lengths, DEBUG=False):
    
    word_positions = [{'word': words[0], 
                        'start': 0, 
                        'end': len(words[0]) - 1,
                        'word_number': 0,
                        'syllables': []}]
                        
    for w in words[1:]:
        word_positions.append({'word': w, 
                        'start': word_positions[-1]['end'] + 1, 
                        'end': word_positions[-1]['end'] + len(w),
                        'word_number': word_positions[-1]['word_number'] + 1,
                        'syllables': []})
    
    syllables_positions = [{'syllable': syllables[0], 
                            'start': 0,
                            'end': len(syllables[0]) - 1,
                            'syllable_number': -1,
                            'words': []}]
    for s in syllables[1:]:
        syllables_positions.append({'syllable': s, 
                            'start': syllables_positions[-1]['end'] + 1, 
                            'end': syllables_positions[-1]['end'] + len(s),
                            'syllable_number': -1,
                            'words': []})
        
    # ------------------------------------------------------------------
    
    for a, w in enumerate(word_positions):
        
        temp_word_parts = [p for p in re.split('([' + ''.join(VOWELS) + ']+)', basic_chars(w['word'])) \
                        if p > '']
                                
        if DEBUG:
            print()
            print(w['word'], 'temp_word_parts', temp_word_parts)
                        
        word_parts = []
        for p in temp_word_parts:
            if len(p) == 1:
                word_parts.append(p)
            elif p[0] not in VOWELS:
                word_parts.append(p)
            else:
                
                has_diphthongs = []
                for d in DIPHTHONGS:
                    if d in p:
                        has_diphthongs.append(d)
                                
                if DEBUG:
                    print()
                    print(w['word'], 'has_diphthongs', has_diphthongs)
                        
                if len(has_diphthongs) == 0:
                    for v in p:
                        word_parts.append(v)
                else:
                    
                    vowel_parts = [v for v in re.split('(' + '|'.join(has_diphthongs) + ')', p) \
                                    if v > '']
                                
                    if DEBUG:
                        print()
                        print(w['word'], 'vowel_parts', vowel_parts)
                                    
                    for v in vowel_parts:
                        if v in DIPHTHONGS:
                            word_parts.append(v)
                        else:
                            for c in v:
                                word_parts.append(c)
                                
        if DEBUG:
            print()
            print(w['word'], 'word_parts', word_parts)
        
        vowel_positions = []
        
        running_position = 0
        for p in word_parts:
            if p[0] in VOWELS:
                vowel_positions.append(w['start'] + running_position)
            running_position += len(p)
            
        word_positions[a]['vowel_positions'] = vowel_positions
    
    for a, s in enumerate(syllables_positions):
        
        syllable_parts = [p for p in re.split('([' + ''.join(VOWELS) + ']+)', basic_chars(s['syllable'])) if p > '']
                                
        if DEBUG:
            print()
            print(s['syllable'], 'syllable_parts', syllable_parts)
        
        vowel_positions = []
        
        running_position = 0
        for p in syllable_parts:
            if p[0] in VOWELS:
                vowel_positions.append(s['start'] + running_position)
            running_position += len(p)
            
        syllables_positions[a]['vowel_positions'] = vowel_positions
        
    # ------------------------------------------------------------------
        
    if DEBUG:
        
        print()
        for w in word_positions:
            print('(1)', w)
            
        print()
        for s in syllables_positions:
            print('(1)', s)
        
    # ------------------------------------------------------------------
    
    for a, s in enumerate(syllables_positions):
        
        for b, w in enumerate(word_positions):
            
            if s['vowel_positions'][0] in w['vowel_positions']:
                    
                syllables_positions[a]['words'].append({'word': w['word'],
                                                        'start': w['start'],
                                                        'end': w['end'],
                                                        'word_number': b})
                                                        
                syllables_positions[a]['syllable_number'] = len(word_positions[b]['syllables'])
                                                        
                word_positions[b]['syllables'].append({'syllable': s['syllable'],
                                                        'start': w['start'],
                                                        'end': w['end'],
                                                        'syllable_number': 
                                                            len(word_positions[b]['syllables'])})
                
    # ------------------------------------------------------------------
    has_error = False
    # ------------------------------------------------------------------
    
    check_a = []
    for word in word_positions:
        for s in word['syllables']:
            check_a.append(s['syllable'])
    
    check_b = []
    for s in syllables_positions:
        check_b.append(s['syllable'])
        
    if len(check_a) != len(check_b):
        if DEBUG:
            print()
            print('map_syllables_to_words ERROR number of syllables', len(check_a), len(check_b))
        has_error = True
                
    # ------------------------------------------------------------------
    
    check_a = []
    for word in word_positions:
        check_a.append(word['word_number'])
        
    n_vowelless_words = 0
    for word in word_positions:
        if re.sub('[' + ''.join(VOWELS) + ']', 
                    '', 
                    basic_chars(word['word'])) == basic_chars(word['word']):
            n_vowelless_words += 1
    
    check_b = []
    for s in syllables_positions:
        for w in s['words']:
            check_b.append(w['word_number'])
            
    check_b = list(set(check_b))
        
    if len(check_a) == len(check_b) or \
        len(check_a) == len(check_b) + n_vowelless_words:
        pass
    else:
        if DEBUG:
            print()
            print('map_syllables_to_words ERROR number of words', 
                    len(check_a), len(check_b),
                    check_a, check_b)
        has_error = True
                
    # ------------------------------------------------------------------
        
    if DEBUG and has_error:
        
        print()
        print('map_syllables_to_words ERROR words', words)
        print('map_syllables_to_words ERROR syllables', syllables)
        print('map_syllables_to_words ERROR lengths', lengths)
        
        print()
        for w in word_positions:
            print('map_syllables_to_words ERROR', w)
            
        print()
        for s in syllables_positions:
            print('map_syllables_to_words ERROR', s)
        
    # ------------------------------------------------------------------
    
    return word_positions, syllables_positions, has_error

def get_all_syllable_word_structures(line, DEBUG=False):

    all_syllable_word_structures = []
    map_syllables_to_words_error = False
    
    try:

        lengths = []
        if 'lengths' in line:
            lengths = line['lengths'].split(' ')
        if '8_final_scansion' in line:
            lengths = line['8_final_scansion']

        word_positions, syllables_positions, map_syllables_to_words_error = \
            map_syllables_to_words(line['line_text'].replace('_', ' ').split(' '), 
                                      line['syllables'].split(' '), 
                                      lengths,
                                      DEBUG=DEBUG)
        
        if DEBUG == True:
            print()
            print('(0) word_positions', word_positions)
            print()
            print('(0) syllables_positions', syllables_positions)
          
        if map_syllables_to_words_error == False:
                
            # ------------------------------------------------------------------

            MY_VOWELS = ''.join(VOWELS)
            MY_CONSONANTS = ''.join(CONSONANTS) + ''.join(DOUBLE_CONS)
            
            for syllable_n_line, syllable in enumerate(syllables_positions):
                
                basic_syllable = basic_chars(syllable['syllable'])
                
                syllable_parts = [part for part in re.split('([' + MY_CONSONANTS + ']+)', basic_syllable) \
                                             if part > '']

                for a in range(0, len(syllable_parts)):
                    syllable_parts[a] = re.sub('[^' + MY_VOWELS + MY_CONSONANTS + ']', '', syllable_parts[a])

                vowel_position = -1
                vowel_starts_in_original = 0
                for a in range(0, len(syllable_parts)):
                    if syllable_parts[a] in VOWELS or syllable_parts[a] in DIPHTHONGS:
                        vowel_position = a
                        break
                    else:
                        vowel_starts_in_original += len(syllable_parts[a])

                vowel = syllable_parts[vowel_position]
                original_vowel = \
                    syllable['syllable'][vowel_starts_in_original: vowel_starts_in_original + len(vowel)]

                vowel_has_circumflex = has_circumflex(original_vowel)
                
                word = syllable['words'][0]['word']
                
                result = {
                    'syllable': syllable['syllable'],
                    'syllable_n_line': syllable_n_line,
                    'syllable_n_word': syllable['syllable_number'],
                    'basic_syllable': basic_syllable,
                    'syllable_parts': syllable_parts,
                    'vowel': original_vowel,
                    'basic_vowel': basic_chars(original_vowel),
                    'vowel_position': vowel_position,
                    'consonant_before': '',
                    'consonant_after': '',
                    'word': word,
                    'has_circumflex': vowel_has_circumflex,
                    'is_capitalized': (word.upper() == word),
                    'is_last_syllable_of_word': False,
                    'is_last_syllable_of_line': False,
                    'length': lengths[syllable_n_line]
                }
                
                all_syllable_word_structures.append(result)
            
            if DEBUG == True:
                print()
                print('(1) word_positions', word_positions)
                print()
                print('(1) syllables_positions', syllables_positions)
            
            for syllable_number, syllable in enumerate(all_syllable_word_structures):
                if syllable['syllable_n_word'] == 0 and syllable_number > 0:
                    all_syllable_word_structures[syllable_number - 1]['is_last_syllable_of_word'] = True
                
                consonant_positions = [
                        syllable['vowel_position'] - 1,
                        syllable['vowel_position'] + 1]
                        
                if consonant_positions[0] > -1:
                    all_syllable_word_structures[syllable_number]['consonant_before'] = \
                        all_syllable_word_structures[syllable_number]['syllable_parts'][consonant_positions[0]]
                else:
                    previous_syllable_part = ''
                    
                    if syllable_number > 0:
                        possible_syllable_part = all_syllable_word_structures[syllable_number - 1]['syllable_parts'][-1]
                        if possible_syllable_part[0] not in MY_VOWELS:
                            previous_syllable_part = possible_syllable_part
                    
                    all_syllable_word_structures[syllable_number]['consonant_before'] = previous_syllable_part
                    
                if consonant_positions[1] < \
                    len(all_syllable_word_structures[syllable_number]['syllable_parts']):
                    all_syllable_word_structures[syllable_number]['consonant_after'] = \
                        all_syllable_word_structures[syllable_number]['syllable_parts'][consonant_positions[1]]
                else:
                    next_syllable_part = ''
                    
                    if syllable_number < len(all_syllable_word_structures) - 1:
                        possible_syllable_part = all_syllable_word_structures[syllable_number + 1]['syllable_parts'][0]
                        if possible_syllable_part[0] not in MY_VOWELS:
                            next_syllable_part = possible_syllable_part
                    
                    all_syllable_word_structures[syllable_number]['consonant_after'] = next_syllable_part
                
            all_syllable_word_structures[-1]['is_last_syllable_of_word'] = True
            all_syllable_word_structures[-1]['is_last_syllable_of_line'] = True
        
    except:
        if DEBUG:
            print()
            print('ERROR', 'line', line)
            print()
            print('ERROR', 'all_syllable_word_structures', all_syllable_word_structures)
            print()
            traceback.print_exc()
        
        map_syllables_to_words_error = True
        
    if map_syllables_to_words_error == True:
        
        if DEBUG:
            print('ERROR_LINE FOR TESTING', line)
        
        all_syllable_word_structures = []
            
    return all_syllable_word_structures, map_syllables_to_words_error

if __name__ == "__main__":
    
    # ----------------------------------------------------------------------
    #
    # ----------------------------------------------------------------------
    
    #word_positions, syllables_positions, map_syllables_to_words_error = map_syllables_to_words(
    #                        ['κεκλιμένοι', 'καλῇσιν', 'ἐπάλξεσιν·', 'αὐτὰρ', 'Ἀχαιοὶ'],
    #                        ['κε', 'κλι', 'μέ', 'νοι', 'κα', 'λῇ', 'σι', 'νἐ', 'πάλ', 
    #                            'ξε', 'σι', 'ν·αὐ', 'τὰ', 'ρἈ', 'χαι', 'οὶ'],
    #                        ['L', 'S', 'S', 'L', 'L', 'L', 'S', 'S', 'L', 'S', 'S', 'L', 
    #                            'S', 'S', 'L', 'L'],
    #                        DEBUG=False)
    
    # ----------------------------------------------------------------------
    #
    # ----------------------------------------------------------------------
    
    #word_positions, syllables_positions, map_syllables_to_words_error = map_syllables_to_words(
    #                        'καὶ τὴν τάλαιναν ἀθλίαν δάμαρτʼ ἐμὴν'.split(' '),
    #                        'καὶ τὴν τά λαι να νἀ θλί αν δά μαρ τʼἐ μὴν'.split(' '),
    #                        'L L X L X X X L X L S L'.split(' '))
                            
    #print()
    #print(word_positions)
    #print()
    #print(syllables_positions)
    
    # ----------------------------------------------------------------------
    #
    # ----------------------------------------------------------------------

    #test_line = {'type': 'line', 'play': 'Orestes', 'div1_type': 'Exodos', 'div2_type': 'ia3', 'div2_ana': 'nonlyric', 'n': '1564', 'line_text': 'καὶ τὴν τάλαιναν ἀθλίαν δάμαρτʼ ἐμὴν', 'syllables': ['καὶ', 'τὴν', 'τά', 'λαι', 'να', 'νἀ', 'θλί', 'αν', 'δά', 'μαρ', 'τʼἐ', 'μὴν'], '8_final_scansion': ['L', 'L', 'X', 'L', 'X', 'X', 'X', 'L', 'X', 'L', 'S', 'L']}


    #all_syllable_word_structures, map_syllables_to_words_error = \
    #        get_all_syllable_word_structures(test_line, DEBUG=True)

    #import json

    #print()
    #print('(99) all_syllable_word_structures')
    #print()
    #print(json.dumps(all_syllable_word_structures, indent=4, ensure_ascii=False))
    
    # ----------------------------------------------------------------------
    #
    # ----------------------------------------------------------------------

    #test_line = {'play': 'apollonius3', 'meter': 'hexameter', 'line_n': '103', 'line_text': 'μυ_θεῖσθαι πάντεσσιν· ἅλις εἰδυῖα καὶ αὐτή.', 'syllables': 'μυ θεῖσ θαι πάν τεσ σι ν·ἅ λι ςεἰ δυῖ α καὶ αὐ τή.', 'n_syllables': 14, 'lengths': 'L L L L L S S L L L S S L L', 'n_lengths': 14}

    test_line = {'8_final_scansion': ['L', 'L', 'X', 'L', 'L', 'L', 'X', 'S', 'L', 'L', 'L', 'S', 'L'], 'line_text': 'γῆμαι πέπρωταί σʼ ἑρμιόνην· ὃς δʼ οἴεται', 'syllables': 'γῆ μαι πέ πρω ταί σʼἑρ μι ό νη ν·ὃς δʼοἴ ε ται'}

    all_syllable_word_structures, map_syllables_to_words_error = \
        get_all_syllable_word_structures(test_line, DEBUG=True)

    import json

    print()
    print('(99) all_syllable_word_structures')
    print()
    print(json.dumps(all_syllable_word_structures, indent=4, ensure_ascii=False))
        
