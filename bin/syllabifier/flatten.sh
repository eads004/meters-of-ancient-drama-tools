
repodir=$(dirname $0)/..

#Flatten the XML

for f in /home/hjschott/Moore_Meter_Tools/texts/Raw_XML/Euripides/*/*_Greek.xml; #for each play in the raw XML directory
do
	java -jar /home/hjschott/Moore_Meter_Tools/saxon/saxon-he-11.3.jar -xsl:$repodir/syllabifier/html/flatten_xml.xsl -s:$f -o:$repodir/syllabifier/flattened_xml/`basename "$f"`;
	#apply the xsl template to the play
	#output into syllabifier/templated_xml
done
