Syllabifying a Greek Play for Handcheck purposes:

If you plan on running the code in this repository locally, make sure you have the following Python libraries installed: 
bs4, copy, glob, joblib, json, lxml, pandas, pip [if any are missing!], re, sklearn, subprocess, sys, traceback
(some may installed by default, but it never hurts to run a pip install on them to be sure!)

Also ensure that your shell can run the following bash commands:
java, python3, tidy, xslt

i) All of Perseus' raw XML can be found in texts/Raw_XML/...

ii) These texts have already been 'flattened' in such a way that all of the extraneous Perseus tags have been removed.
	a. These files can be found at syllabifier/templated_xml/...

MAKING TEMPLATED XML
1) Once a template has been finalized and approved by Tim:
	a) Convert the .docx file of the template in Box to .html
		-Many word processors have an "Export as..." option under "File"
		-If you're running on a Linux machine with libreoffice, you can simply run:
			libreoffice --headless --convert-to html <file>
		-Move the html to bin/syllabifier/html as <PLAYNAME>.html
		
	b) go to the /bin/ directory and run: python3 apply_templates.py <PLAYNAME>
		- This python script takes the Raw, Flattened XML and inserts the <div1> and <div2> tags at appropriate places.
		- If anything breaks at this step, reach out to Henry (henry.j.schott@wustl.edu)
	
	c) Next we need to syllabify the templated XML.
		- run python3 scanning_XML.py <PLAYNAME>
		- this creates a file called <PLAYNAME>.scanned.json in the working_files/ directory
			- The 'scanned' json contains a breakdown of longs and shorts as deduced from the various scansion rules.
			- If there appears to be any consistently wrong syllables in the Greek, one should consider emending the rules of
				scanning_XML.py OR its dependencies
	
	d) Finally, we need to turn that json back into well-formatted XML.
		- run python3 play_json_to_xml.py <PLAYNAME>
		- this creates a file called <PLAYNAME>.xml in the texts/TO_BE_CHECKED directory
			- This step doesn't manipulate the values of the XML. If something is wrong in the final product, go back one step.
