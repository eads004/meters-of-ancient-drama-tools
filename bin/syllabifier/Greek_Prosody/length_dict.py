"""
LENGTH DICTIONARY

Length determination and morphology follow the forms standard in Attic drama, which leads to some differences (primarily morphological)
from the forms and lengths one might find in Classical Attic.

The following resources were used to supplement the word lists: (due to the limitations of .py files, italics have been nixed in the citations, although the citations should still be clear.)
- Anna Conser's lists of enclitics and proclitics, which can be found at https://github.com/aconser/Greek-Poetry/blob/master/Analysis/class_word.py.
- Diggle, James, Bruce Fraser, Patrick James, Oliver Simkin, Anne Thompson, Simon Westripp, eds. The Cambridge Greek Lexicon.
    2 vols. Cambridge: Cambridge University Press, 2021.
- Maltby, Edward. A New and Complete Greek Gradus. 3rd. ed. London: Gilbert & Rivington, 1850. https://archive.org/embed/newcompletegreek00malt.
- Sidgwick, Arthur, and Francis David Morice. An Introduction to Greek Verse Composition with Exercises. 3rd rev. ed.
    Waterloo Place, London: Rivingtons, 1885. https://www.google.com/books/edition/An_Introduction_to_Greek_Verse_Compositi/1aoBAAAAYAAJ?hl=en.

    
@author: Will Corbin, Washington University in St. Louis, corbin@wustl.edu
@license: MIT
"""

#MONOSYLLABLES
MONOSYLLABLES = [{'wrd':"ἄν", "scan":"S"}, {"word":"γάρ", "scansion":"S"}, {"word":"γὰρ", "scansion":"S"}]
SHORT_ = ["ἀνʼ", "γάρ", "γὰρ", "διʼ",## "δίʼ",### I don't think the acute would be possible (elision would necessitate adjacent words [enclitics tho?])
          "δὶʼ", "καθʼ", "κατʼ", "νιν", "νίν", "ὅταν", "παρʼ","πάρʼ", "πρίν", "πρὶν",
         "σύ", "σὺ","σύν","σὺν", "τι", "τί", "τὶ", "τινʼ", "τίνʼ",
         "τισʼ", "τίσʼ", "τις", "τίς", "τὶς", "τρίς", "τρὶς", "ὑπʼ", "ὑφʼ","φάν","φὰν"]
LONG_ = ["δρύς","δρὺς","πάν","πὰν"]

#DISYLLABLES
SHORT_SHORT = ["ἀνά", "ἀνὰ", "ἄνα", "ἀπʼ","ἄπ", "ἀπό", "ἀπὸ", "ἄπο", "ἀπὺ", "ἀπύ", "ἄπυ", "βαρύ", "βαρὺ",
               "βαρύν","βαρὺν","βαρύς","βραχύ", "βραχὺ", "βραχύν","βραχὺν","βραχύς", "βραχὺς",
               "διά", "διὰ", "δία", "ἐνί", "ἐνὶ", "ἔνι", "ἐπί", "ἐπὶ", "ἔπι",
               "κατά", "κατὰ", "κάτα", "μετά", "μετὰ", "μέτα",
               "παρά", "παρὰ", "πάρα","περί", "περὶ", "πέρι", "ποθι", "ποθί", "ποθὶ", "ποτί", "ποτὶ", "προτί", "προτὶ", "πρότι",
               "ὑπέρ", "ὑπὲρ", "ὕπερ", "ὑπό", "ὑπὸ", "ὕπο",
               "τινα", "τινά", "τινὰ", "τινας",  "τινάς", "τινὰς", "τινε", "τινὲ", "τινέ",  "τινες", "τινές", "τινὲς",
               "τινος", "τινός", "τινὸς", "τινι", "τινί", "τινὶ", "τισι", "τισί", "τισὶ", "φάθι"
               "φαμεν", "φαμέν", "φαμὲν", "φατε", "φατέ", "φατὲ", ##"φατον", "φατόν", "φατὸν"##
               ]

SHORT_LONG = ["τινοιν","τινοῖν","τινων","τινῶν"]
LONG_SHORT = ["ἀμφί", "ἀμφὶ", "ἄμφι", "ἀμφίς", "ἀμφὶς", "ἀντί", "ἀντὶ", "ἄντι",
              "εἰμι", "εἰμί", "εἰμὶ", "εἰσι", "εἰσί", "εἰσὶ", "ἐστι", "ἐστί", "ἐστὶ", "φασι", "φασί", "φασὶ", "φάτω"
               "φημι", "φημί", "φημὶ", "φησι", "φησί", "φησὶ", "φῶσι", "φῶσιν", "χωρίς", "χωρὶς"]

LONG_LONG = []

#TRISYLLABLES
SHORT_SHORT_SHORT = ["ἔφαμεν","ἔφατε","ἔφασαν"]

SHORT_SHORT_LONG = []
SHORT_LONG_SHORT = []
LONG_SHORT_SHORT = []

SHORT_LONG_LONG = []
LONG_SHORT_LONG = []
LONG_LONG_SHORT = []

LONG_LONG_LONG = []

#QUADRISYLLABLES
SHORT_SHORT_SHORT_SHORT = []

SHORT_SHORT_SHORT_LONG = []
SHORT_SHORT_LONG_SHORT = []
SHORT_LONG_SHORT_SHORT = []
LONG_SHORT_SHORT_SHORT = []

SHORT_SHORT_LONG_LONG = []
SHORT_LONG_SHORT_LONG = []
SHORT_LONG_LONG_SHORT = []
LONG_SHORT_SHORT_LONG = []
LONG_SHORT_LONG_SHORT = []
LONG_LONG_SHORT_SHORT = []

SHORT_LONG_LONG_LONG = []
LONG_LONG_SHORT_LONG = []
LONG_SHORT_LONG_LONG = []
SHORT_LONG_LONG_LONG = []

LONG_LONG_LONG_LONG = []