"""
LENGTH MARKER

This program takes a list of syllables and the corresponding string of Greek text and, after checking each
word against a dictionary of words sorted by the natural length of their syllables, returns the original
list of syllables, except those syllables that were found in a length dictionary have been marked 
with diacritics indicating natural length.

@author: Will Corbin, Washington University in St. Louis, corbin@wustl.edu
@license: MIT
"""
import unicodedata
from .length_dict import *
from cltk.alphabet.grc import UPPER, UPPER_ACUTE, UPPER_GRAVE, UPPER_SMOOTH, UPPER_SMOOTH_ACUTE, UPPER_SMOOTH_GRAVE, UPPER_SMOOTH_CIRCUMFLEX, UPPER_ROUGH, UPPER_ROUGH_ACUTE, UPPER_ROUGH_GRAVE, UPPER_ROUGH_CIRCUMFLEX, UPPER_DIAERESIS, UPPER_MACRON, UPPER_BREVE, LOWER, LOWER_ACUTE, LOWER_GRAVE, LOWER_SMOOTH, LOWER_SMOOTH_ACUTE, LOWER_SMOOTH_GRAVE, LOWER_SMOOTH_CIRCUMFLEX, LOWER_ROUGH, LOWER_ROUGH_ACUTE, LOWER_ROUGH_GRAVE, LOWER_ROUGH_CIRCUMFLEX, LOWER_DIAERESIS, LOWER_MACRON, LOWER_BREVE, LOWER_DIAERESIS_ACUTE, LOWER_DIAERESIS_GRAVE, LOWER_DIAERESIS_CIRCUMFLEX

#GREEK LETTERS AND OTHER CONSTANTS
PUNCTUATION = ".,;·:'<>[]{}()=+\u037e\u0387\u00B7⟨⟩†—-…–"
ADDITIONAL_PUNCTUATION = '"'
PUNCTUATION = PUNCTUATION + ADDITIONAL_PUNCTUATION

vowels = UPPER + UPPER_ACUTE + UPPER_GRAVE + UPPER_SMOOTH + UPPER_SMOOTH_ACUTE + UPPER_SMOOTH_GRAVE + UPPER_SMOOTH_CIRCUMFLEX + UPPER_ROUGH + UPPER_ROUGH_ACUTE + UPPER_ROUGH_GRAVE + UPPER_ROUGH_CIRCUMFLEX + UPPER_DIAERESIS + UPPER_MACRON + UPPER_BREVE + LOWER + LOWER_ACUTE + LOWER_GRAVE + LOWER_SMOOTH + LOWER_SMOOTH_ACUTE + LOWER_SMOOTH_GRAVE + LOWER_SMOOTH_CIRCUMFLEX + LOWER_ROUGH + LOWER_ROUGH_ACUTE + LOWER_ROUGH_GRAVE + LOWER_ROUGH_CIRCUMFLEX + LOWER_DIAERESIS + LOWER_MACRON + LOWER_BREVE + LOWER_DIAERESIS_ACUTE + LOWER_DIAERESIS_GRAVE + LOWER_DIAERESIS_CIRCUMFLEX

#Diacritical Marks
#accents
acute = u'\u0301'
grave = u'\u0300'
circumflex = u'\u0342'
accents = [acute, grave, circumflex]

#breathings
smooth = u'\u0313'
rough = u'\u0314'
psili_cap = u'\u1fbf' # smooth breathing before capital letters (e.g., Ἀ)
dasia_cap = u'\u1ffe' #rough breathing before capital letters (e.g., Ἁ)
breathings = [smooth, rough, psili_cap, dasia_cap]

#length markings
macron = u'\u0304'
breve = u'\u0306'
length_markers = [macron, breve]

#other diacritics
iota_subscript = u'\u0342'
diaresis = u'\0308'
other_diacritics = [iota_subscript, diaresis]

all_diacritics = accents + length_markers + other_diacritics + breathings

def strip_punctuation(text):
    """Takes a string of text and removes punctuation from that string based on a predefined list of punctuation.

    :param: str text: a string of Greek text
    :return: str text: the original string minus any specified punctuation
    """
    text = unicodedata.normalize("NFD", text)
    cleaned = ""
    for ch in text:
        if ch not in PUNCTUATION:
            cleaned += ch
    return cleaned


def match_word_to_syls(line, syl_list):
    """
    Takes a line of Greek text and an ordered list of the syllables in that line and associates each
    syllable with the corresponding word. The logic is fairly convoluted, but on the highest level the program works by assinging
    each vowel a unique identifier and matching vowels between syllables and words.

    :param: str line: a string of Greek text. Words must be separated by spaces.
    :param: list syl_list: a list of the Greek text broken into syllables
    :return: result list: a list list containing a dictionary for each word. The first key of each dictionary
        ("word") contains each word as a string, and the second key ("syls") contains a list of the
        syllables associated with that word.
    """
    result = [{"word":"", "syls":[]}]
    line = unicodedata.normalize("NFD", line)
    for i,syl in enumerate(syl_list):
        syl_list[i] = unicodedata.normalize("NFD", syl)
    wp = 0
    vp = 1
    word_sequence = {0:[]}
    wsp = 0
    current_word_sequence = []
    for i,ch in enumerate(line): # ch ==> character
        prev_ch = "" #prev_ch is present to ensure double or triple spaces do not alter the word counter
        if i > 0:
            prev_ch = line[i-1]
        if ch == " " and prev_ch != " ":
            result.append({"word":"", "syls":[]})
            wp += 1
        result[wp]["word"] += ch # add a charcter to the word attribute of the current word #move this line to right before "if ch == " " and prev_ch != " "", if you want spaces at syllable boundaries to be grouped with the preceding word rather than the following
        #
        if ch == " ":
            word_sequence[wsp] = current_word_sequence
            current_word_sequence = []
            wsp += 1 # wsp ==> word sequence position, perhaps different because of different dissections of words in different programs?
        elif ch in vowels:
            current_word_sequence.append(vp)
            vp += 1 # vp ==> vowel position
    word_sequence[wsp] = current_word_sequence
#
    vp = 1
    sp = 0
    syl_sequence = {0:[]}
    current_syl_sequence = []
    for syl in syl_list:
        for ch in syl:
            if ch in vowels:
                current_syl_sequence.append(vp)
                vp += 1
        syl_sequence[sp] = current_syl_sequence
        current_syl_sequence = []
        sp += 1
#
    to_delete = []
    for i,syls in enumerate(syl_sequence):
        if len(syl_sequence[i]) == 0:
            to_delete.append(i)


    if len(to_delete) != 0:
        for index in to_delete:
            del(syl_sequence[index])
        old_syl_sequence = syl_sequence
        syl_sequence = {}
        for i,key in enumerate(old_syl_sequence):
            syl_sequence[i] = old_syl_sequence[key]
#
    wp = 0
    for i,syl in enumerate(syl_list):
        # print(i, wp, result, line, syl_list)
        if syl_sequence[i][-1] in word_sequence[wp]:
            result[wp]["syls"].append(syl)
        else:
            wp += 1
            if syl_sequence[i][-1] in word_sequence[wp]:
                result[wp]["syls"].append(syl)
            else:
                wp += 1
                result[wp]["syls"].append(syl)
    return result

def add_length_diacritic(syl, length_mark):
    """Given any syllable adds a macron or breve to the first vowel character.
    :param: str syl: any Greek syllable
    :return: str adjusted: that same syl with the appropriate length diacritic added to the vowel
    """
    adjusted = ""
    before_first_vowel = True
    for ch in syl:
        if ch.lower() in vowels and before_first_vowel == True:
            ch += length_mark
            adjusted += ch
            before_first_vowel = False
        else:
            adjusted += ch
    return adjusted

def add_length_diacritics(scansion, syl_list):
    """Given a list of syllables and the natural lengths of at least some of those syllables, adds diacritics to indicate the given natural
        lengths
    
    :param: str scansion: a string marking the scansion of each syllable. It should be comprised of the characters "L," "S," and "X,"
        where each character represents the length of the syllable in syl_list at the same index.
    :param: list syl_list: a list of syllables
    :return: list spruced_syl_list: the same list of syllables passed in, except length markings have been added to the appropriate syllables
    """
    spruced_syl_list = []
    for i,syl in enumerate(syl_list):
        syl = unicodedata.normalize("NFD",syl)
        if scansion[i] == "S":
            syl = add_length_diacritic(syl, breve)
            spruced_syl_list.append(syl)
        if scansion[i] == "L":
            syl = add_length_diacritic(syl, macron)
            spruced_syl_list.append(syl)
    return spruced_syl_list
            

def check_word(word, num_syls):
    """Checks  applicable lists in length_dict.py for matches between the word passed through the function and words in various lists sorted by natural quantity.
    :param str word: any greek word
    :param int num_syls: the number of syllables in that word
    :return str: a string marking the natural scansion of the word or stating that the scansion could not be determined
    """
    word = strip_punctuation(word.strip())
    word = unicodedata.normalize("NFC", word)
    if num_syls == 1:
        if word in SHORT_:
            return "S"
        elif word in LONG_:
            return "L"
        else:
            return "word not found"

    elif num_syls == 2:
        if word in SHORT_SHORT:
            return "SS"
        elif word in SHORT_LONG:
            return "SL"
        elif word in LONG_SHORT:
            return "LS"
        else:
            return "word not found"

    elif num_syls == 3:
        if word in SHORT_SHORT_SHORT:
            return "SSS"
        elif word in SHORT_SHORT_LONG:
            return "SSL"
        elif word in SHORT_LONG_SHORT:
            return "SLS"
        elif word in LONG_SHORT_SHORT:
            return "LSS"
        elif word in SHORT_LONG_LONG:
            return "SLL"
        elif word in LONG_SHORT_LONG:
            return "LSL"
        elif word in LONG_LONG_SHORT:
            return "LLS"
        elif word in LONG_LONG_LONG:
            return "LLL"
        else:
            return "word not found"

    elif num_syls == 4:
        if word in SHORT_SHORT_SHORT_SHORT:
            return "SSSS"
        elif word in SHORT_SHORT_SHORT_LONG:
            return "SSSL"
        elif word in SHORT_SHORT_LONG_SHORT:
            return "SSLS"
        elif word in SHORT_LONG_SHORT_SHORT:
            return "SLSS"
        elif word in LONG_SHORT_SHORT_SHORT:
            return "LSSS"
        elif word in SHORT_SHORT_LONG_LONG:
            return "SSLL"
        elif word in SHORT_LONG_SHORT_LONG:
            return "SLSL"
        elif word in SHORT_LONG_LONG_SHORT:
            return "SLLS"
        elif word in LONG_SHORT_SHORT_LONG:
            return "LSSL"
        elif word in LONG_SHORT_LONG_SHORT:
            return "LSLS"
        elif word in LONG_LONG_SHORT_SHORT:
            return "LLSS"
        elif word in SHORT_LONG_LONG_LONG:
            return "SLLL"
        elif word in LONG_LONG_SHORT_LONG:
            return "LLSL"
        elif word in LONG_SHORT_LONG_LONG:
            return "LSLL"
        elif word in SHORT_LONG_LONG_LONG:
            return "SLLL"
        elif word in LONG_LONG_LONG_LONG:
            return "LLLL"
        else:
            return "word not found"

def mark_length(line, syl_list, diacritics=True):
    """Given a line and list of syllables, runs the code required to check the word against a dictionary, attempting to determine 
    the nautral length of syllables that contain ambiguous vowels not forced long by position.
    :param str line: line of greek text. Words must be delineated by spaces.
    :param list syl_list: an ordered list of the syllables. The syllables can contain punctuation and white space, depending on individual preference.
    :param boolean diacritics: determines whether the function adds macra and breves to the syllables or returns a list spelling out natural length.
        By default the boolean is set to true, adding diacrtitics to the syllable list as its return value.
    :return list marked_syl_list: a copy of syl_list with ambiguous vowels marked by macra and breves to the extent possible.
    :return list scansion_list: a list containing a string for each word, composed of S (short), L (long), and X (unknown). Each letter marks the natural length of one syllable.
    """
    word_syl_list = match_word_to_syls(line, syl_list)
    for word_dict in word_syl_list:
        num_syls = len(word_dict["syls"])
        word = word_dict["word"]
        scansion = check_word(word, num_syls)
        if scansion != "word not found":
            word_dict["scansion"] = scansion
    if diacritics:
        marked_syl_list = []
        for word_dict in word_syl_list:
            try:
                scansion = word_dict["scansion"]
                if scansion != None:
                    word_syls = word_dict["syls"]
                    marked_word = add_length_diacritics(scansion, word_syls)
                    for syl in marked_word:
                        marked_syl_list.append(syl)
            except KeyError:
                for syl in word_dict["syls"]:
                    if syl != "":
                        marked_syl_list.append(syl)
            except:
                print("mark_length 'if diacritics' unidentified error: ", line, syl_list)
        return marked_syl_list
    else:
        scansion_list = []
        for word_dict in word_syl_list:
            try:
                scansion_list.append(word_dict["scansion"])
            except:
                num_unknowns = len(word_dict["syls"])
                scansion_list.append(num_unknowns * "X")
        return scansion_list