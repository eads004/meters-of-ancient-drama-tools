<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei"
    version="3.0">
    
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:template  match="tei:speaker">
        <milestone unit="speaker" n="{.}"/>
    </xsl:template>
    
    <xsl:template match="tei:sp">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:div">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="@rend"/>

    <xsl:template match="@xml:base"/>

    <xsl:template match="tei:l[@type='hidden']"/>
    
    <xsl:template match="tei:milestone"/>
    <xsl:template match="tei:pb"/>
    
</xsl:stylesheet>
