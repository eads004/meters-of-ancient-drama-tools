**Euripides *Trojan Women***

Prepared by Timothy Moore and Shangwei Deng

Text: *Euripidis Fabulae*, vol. 2, ed. J. Diggle (Oxford: Clarendon
Press, 1981 \[reprt. with corrections, 1986\]).\
Scansion of lyrics: Frederico Lourenço, *The Lyric Metres of Euripidean
Drama* (Coimbra: Centro de Estudos Clássicos e Humanisticos da
Universidade de Coimbra, 2011).

**Prologue (1-229)**

**1-97: ia3**

**98-229: an**

**98-121 (nonlyric)**

ΕΚΑΒΗ‎

ˇ ˇ ˉ ˉ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

ἄνα, δύϲδαιμον·‎ πεδόθεν κεφαλὴν‎ 98 an2

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˇ ˇ ˉ ˉ

ἐπάειρε δέρην‎ 〈τ‎ʼ〉· οὐκέτι Τροία ‎99 an2

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˉ ˉ ˉ

τάδε καὶ βαϲιλῆϲ ἐϲμεν Τροίαϲ.‎ ‎ 100 an2

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˇ ˇ ˉ ˉ

μεταβαλλομένου δαίμονοϲ ἄνϲχου.‎ 101 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˇ ˇ ˉ ˇ ˇ

πλεῖ κατὰ πορθμόν, πλεῖ κατὰ δαίμονα,‎ 102 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

μηδὲ προϲίϲτη πρῶιραν βιότου‎ 103 an2

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˉ

πρὸϲ κῦμα πλέουϲα τύχαιϲιν.‎ 104 par||

ˉ ˉ ˉ ˉ

αἰαῖ αἰαῖ· ‎105 extrametric

ˇ ˇ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

τί γὰρ οὐ πάρα μοι μελέαι ϲτενάχειν,‎ 106 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˇ ˇ ˉ ˇ ˇ

ἧι πατρὶϲ ἔρρει καὶ τέκνα καὶ πόϲιϲ;‎ 107 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

ὦ πολὺϲ ὄγκοϲ ϲυϲτελλόμενοϲ‎ 108 an2

ˇ ˇ ˉ ˉ ˉ | ˇ ˇ ˉ ∩

προγόνων, ὡϲ οὐδὲν ἄρʼ ἦϲθα.‎ 109 par||B

ˇ ˇ ˉ ˉ ˉ | ˇ ˇ ˉ ˉ ˉ

τί με χρὴ ϲιγᾶν;‎ τί δὲ μὴ ϲιγᾶν;‎ 110 an2

ˇ ˇ ˉ ˉ ˉ

τί δὲ θρηνῆϲαι;‎ 111 an1

ˉ ˉ ˇ ˇ ˉ | ˉ ˇ ˇ ˉ ˇ ˇ

δύϲτηνοϲ ἐγὼ τῆϲ βαρυδαίμονοϲ‎ 112 an2

ˉ ˉ ˉ ˉ | ˉ ˇ ˇ ˉ ˉ

ἄρθρων κλίϲεωϲ, ὡϲ διάκειμαι,‎ 113 an2

ˉ ˉ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

νῶτʼ ἐν ϲτερροῖϲ λέκτροιϲι ταθεῖϲ‎ʼ. 114 an2

ˉ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

οἴμοι κεφαλῆϲ, οἴμοι κροτάφων‎ 115 an2

ˉ ˉ ˉ ˉ | ˇ ˇ ˉ ˉ ˉ

πλευρῶν θʼ,‎ ὥϲ μοι πόθοϲ εἱλίξαι‎ 116 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˇ ˇ ˉ ˉ

καὶ διαδοῦναι νῶτον ἄκανθάν τʼ‎ 117 an2

ˉ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

εἰϲ ἀμφοτέρουϲ τοίχουϲ μελέων,‎ 118 an2

ˇ ˇ ˉ ˉ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

ἐπιοῦϲʼ αἰεὶ δακρύων ἐλέγουϲ.‎ 119 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˉ ˉ

μοῦϲα δὲ χαὔτη τοῖϲ δυϲτήνοιϲ‎ 120 an2

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˉ

ἄταϲ κελαδεῖν ἀχορεύτουϲ. 121 par|||

**122-152: Hecuba Monody (lyric, with glyconics)**[^1]

‒ ‒ ‒ ‒ | ‒ ‒ ‒

πρῷραι ναῶν, ὠκείαις 122 par

‒ ⏑⏑⏑⏑‒ | ‒ ‒ ‒

Ἴλιον ἱερὰν αἳ κώπαις 123 par

⏑ ⏑⏑ ‒ ⏑⏑ ‒ ‒ ‒

δι᾽ ἅλα πορφυροειδῆ καὶ 124 gly

⏑⏑⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

λιμένας Ἑλλάδος εὐόρμους 125 gly

‒ ‒ ‒ ‒ ‒ ‒ ‒

αὐλῶν παιᾶνι στυγνῷ 126 par

‒ ‒ ‒ ‒ | ‒ ‒ ‒ ‒

συρίγγων τ᾽ εὐφθόγγων φωνᾷ 127 2an

‒ ‒ ‒ ‒ | ‒ ‒ ‒ ‒

βαίνουσαι †πλεκτὰν Αἰγύπτου 128

‒ ‒ ‒ ‒| ‒ ‒ ‒

παιδείαν ἐξηρτήσασθ᾽†, 129

‒ ‒ ‒ ‒ | ‒ ‒ ‒

αἰαῖ, Τροίας ἐν κόλποις 130 par

‒ ⏑⏑ ‒ ‒ | ⏑ ⏑‒ ⏑ ⏑ ‒

τὰν Μενελάου μετανισόμεναι 131 2an

‒ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

στυγνὰν ἄλοχον, Κάστορι λώβαν 132 2an

‒ ‒ ‒ ‒ | ‒ ‒ ∩

τῷ τ᾽ Εὐρώτᾳ δυσκλείαν, 133 par||B

‒ ‒ ‒ ‒

ἃ σφάζει μὲν 134 an

‒ ‒ ‒ ‒ | ⏑⏑ ‒ ⏑ ⏑ ‒

τὸν πεντήκοντ᾽ ἀροτῆρα τέκνων 135 2an

⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑‒ ⏑ ⏑ ‒

†Πρίαμον, ἐμέ τε μελέαν Ἑκάβαν† 136 ?

‒ ‒ ‒ ‒ | ‒ ‒ ‒

ἐς τάνδ᾽ ἐξώκειλ᾽ ἄταν. 137 par

‒ ‒ ‒ ‒ | ‒ ‒ ‒ ‒

ὤμοι, θάκους οἵους θάσσω, 138 2an

‒ ‒ ⏑⏑ ‒ | ⏑ ⏑ ‒ ⏑⏑‒

σκηναῖς ἐφέδρους Ἀγαμεμνονίαις. 139 2an

‒ ‒ ⏑ ⏑‒

δούλα δ᾽ ἄγομαι 140 an

‒ ‒ ‒ ‒ | ‒ ‒ ‒

γραῦς ἐξ οἴκων πενθήρη 141 par

‒ ‒ ‒ ‒ | ‒ ‒ ‒

κρᾶτ᾽ ἐκπορθηθεῖσ᾽ οἰκτρῶς. 142 par

‒ ‒ ‒ ‒ | ‒ ‒ ‒ ‒

ἀλλ᾽ ὦ τῶν χαλκεγχέων Τρώων 143a 2an

⏑ ⏑ ‒ ⏑ ⏑‒

ἄλοχοι μέλεαι, 143b an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

†καὶ κοῦραι κοῦραι δύσνυμφαι†, 144

‒ ⏑⏑ ‒ ⏑⏑| ‒ ‒ ‒ ‒

τύφεται Ἴλιον, αἰάζωμεν. 145 2an

‒ ‒ ‒ ‒ | ‒ ‒ ‒ ‒

μάτηρ δ᾽ ὡσεί πτανοῖς κλαγγάν 146 2an

‒ ‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒

†ὄρνισιν, ὅπως ἐξάρξω 'γὼ 147 ?

‒ ‒ ‒ ‒ ‒ ‒

μολπάν, οὐ τὰν αὐτὰν† 148 ?

‒ ‒ ⏑ ⏑ ‒

οἵαν ποτὲ δὴ 1449 an

‒ ‒ ⏑⏑ ‒ | ⏑⏑‒ ⏑ ⏑ ‒

σκήπτρῳ Πριάμου διερειδομένου 150 2an

⏑ ⏑ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑⏑ ‒

ποδὸς ἀρχεχόρου πληγαῖς Φρυγίους 151 2an

‒ ‒ ‒ ‒ | ‒ ‒ ‒

εὐκόμποις ἐξῆρχον θεούς. 152 par

**PARODOS (an, 153-229)**

**153-175: an (lyric)**

STROPHE 1

*Ἡμιχόριον Α*

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

Ἑκάβη, τί θροεῖς; τί δὲ θωΰσσεις; 153 2an

‒ ⏑ ⏑ ‒ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒

ποῖ λόγος ἥκει; διὰ γὰρ μελάθρων 154 2an

‒⏑⏑[^2] ‒ ‒ ‒ ‒ ‒ ‒

ἄιον οἴκτους οὓς οἰκτίζῃ. 155 2an

⏑⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ‒ ‒

διὰ δὲ στέρνων φόβος ἄισσει 156 2an

‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒ ‒

Τρῳάσιν, αἳ τῶνδ᾽ οἴκων εἴσω 157 2an

‒ ‒ ‒ ‒ ‒ ‒ ∩

δουλείαν αἰάζουσιν. 158 par||B

*Ἑκάβη*

‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒ ‒

ὦ τέκν᾽, Ἀχαιῶν πρὸς ναῦς ἤδη 159 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

κινεῖται κωπήρης χείρ. 160 par

*Ἡμιχόριον Α*

‒ ‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒

οἲ ᾽γώ, τί θέλουσ᾽, ἦ πού μ᾽ ἤδη 161 2an

‒ ‒ ‒ ‒ ⏑ ⏑‒ ‒ ‒

ναυσθλώσουσιν πατρίας ἐκ γᾶς; 162 2an

*Ἑκάβη*

‒ ‒ ‒ ‒ ‒ ‒ ‒

οὐκ οἶδ᾽, εἰκάζω δ᾽ ἄταν. 163 par

*Ἡμιχόριον Α*

‒ ‒ ‒ ‒

ἰὼ ἰώ. 164 an

⏑ ⏑‒ ‒ ‒ ⏑ ⏑ ‒ ⏑⏑‒

μέλεαι μόχθων ἐπακουσόμεναι 165 2an

‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒ ‒

Τρῳάδες, ἐξορμίζεσθ᾽ οἴκων: 166 2an

‒ ‒ ‒ ‒ ‒ ‒ ∩

στέλλουσ᾽ Ἀργεῖοι νόστον. 167 par||B

*Ἑκάβη*

⏑ ⏑

ἒ ἒ.[^3] extrametric

‒ ‒ ‒ ‒

μή νύν μοι τὰν 168 an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ἐκβακχεύουσαν Κασσάνδραν, 169 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

αἰσχύναν Ἀργείοισιν, 171 par

‒ ‒ ‒ ‒

πέμψητ᾽ ἔξω, 170 an

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

μαινάδ᾽, ἐπ᾽ ἄλγεσι δ᾽ ἀλγυνθῶ. 172a par

‒ ‒ ‒ ‒

ἰώ ἰώ. 172b an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

Τροία Τροία δύσταν᾽, ἔρρεις, 173 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

δύστανοι δ᾽ οἵ σ᾽ ἐκλείποντες 174 2an

‒ ‒ ‒ ‒ ‒ ‒ ∩

καὶ ζῶντες καὶ δμαθέντες. 175 par||B

**176-196: an (lyric)**

ANTISTROPHE 2

*Ἡμιχόριον Β*

‒ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ⏑‒

οἴμοι. τρομερὰ σκηνὰς ἔλιπον 176 2an

‒ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑‒

τάσδ᾽ Ἀγαμέμνονος ἐπακουσομένα, 177 2an

⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒

βασίλεια, σέθεν: μή με κτείνειν 178 2an

‒ ‒ ‒ ‒ ‒ ‒ ⏑⏑‒

δόξ᾽ Ἀργείων κεῖται μελέαν; 179 2an

‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒ ‒

ἢ κατὰ πρύμνας ἤδη ναῦται 180 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

στέλλονται κινεῖν κώπας; 181 par

*Ἑκάβη*

‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ‒ ‒

ὦ τέκνον, ὀρθρεύου σὰν ψυχάν. 182 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

ἐκπληχθεῖσ᾽ ἦλθον φρίκᾳ. 183 par||H

*Ἡμιχόριον Β*

‒ ‒ ⏑ ⏑‒ ⏑ ⏑‒ ‒ ‒

ἤδη τις ἔβα Δαναῶν κῆρυξ; 184 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

τῷ πρόσκειμαι δούλα τλάμων; 185 2an

*Ἑκάβη*

‒ ‒ ‒ ‒ ‒ ‒ ‒

ἐγγύς που κεῖσαι κλήρου. 186 par

*Ἡμιχόριον Β*

‒ ‒ ‒ ‒

ἰὼ ἰώ. 187a an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

τίς μ᾽ Ἀργείων ἢ Φθιωτᾶν 187b 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ἢ νησαίαν μ᾽ ἄξει χώραν 188 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

δύστανον πόρσω Τροίας; 189 par

*Ἑκάβη*

‒ ‒

φεῦ φεῦ. 190a[^4] extrametric

‒ ‒ ‒ ‒

τῷ δ᾽ ἁ τλάμων 190b an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ποῦ πᾷ γαίας δουλεύσω γραῦς, 191 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

ὡς κηφήν, ἁ δειλαία, 192a par

‒ ‒ ‒ ‒

νεκροῦ μορφά, 192b an

⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ∩

νεκύων ἀμενηνὸν ἄγαλμα, 193a par||H[^5]B

‒ ‒ ‒ ‒

αἰαῖ αἰαῖ 193b an

‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒

τὰν παρὰ προθύροις φυλακὰν κατέχουσ᾽ 194 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ἢ παίδων θρέπτειρ᾽, ἃ Τροίας 195 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

ἀρχαγοὺς εἶχον τιμάς; 196 par

**197-213: an (lyric)**

STROPHE 2

*Χορός*

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

αἰαῖ αἰαῖ, ποίοις δ᾽ οἴκτοις 197 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

τάνδ᾽ ἂν λύμαν ἐξαιάζοις; 198 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ⏑ ⏑

οὐκ Ἰδαίοις ἱστοῖς κερκίδα 199 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

δινεύουσ᾽ ἐξαλλάξω. 200 par

⏑⏑‒ ⏑⏑‒ ‒ ⏑⏑ ‒ ‒

νέατον τεκέων σώματα λεύσσω, 201 2an

⏑⏑‒ ‒ ‒ ‒ ‒ ‒ ‒

νέατον. μόχθους &lt;δ᾽&gt; ἕξω κρείσσους, 202 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ἢ λέκτροις πλαθεῖσ᾽ Ἑλλάνων 203 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

（ἔρροι νὺξ αὕτα καὶ δαίμων.） 204 2an

‒ ‒ ‒ ‒ ‒ ‒ ⏑ ⏑‒

ἢ Πειρήνας ὑδρευομένα 205 2an

‒ ⏑ ⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

πρόσπολος οἰκτρὰ σεμνῶν ὑδάτων. 206 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

τὰν κλεινὰν εἴθ᾽ ἔλθοιμεν 207 par

‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ‒

Θησέως εὐδαίμονα χώραν. 208-9 par

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

μὴ γὰρ δὴ δίναν γ᾽ Εὐρώτα, 210 2an

‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

τὰν &lt;τ᾽&gt; ἐχθίσταν θεράπναν Ἑλένας, 211 2an

‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ‒ ‒

ἔνθ᾽ ἀντάσω Μενέλᾳ δούλα, 212 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

τῷ τᾶς Τροίας πορθητᾷ. 213 par

**214-229: an (lyric)**

ANTISTROPHE 2

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

τὰν Πηνειοῦ σεμνὰν χώραν, 214 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

κρηπῖδ᾽ Οὐλύμπου καλλίσταν, 215 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ὄλβῳ βρίθειν φάμαν ἤκουσ᾽ 216 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

εὐθαλεῖ τ᾽ εὐκαρπείᾳ: 217 par

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑‒

τάδε δεύτερά μοι μετὰ τὰν ἱερὰν 218 2an

‒ ‒ ⏑ ⏑‒ ‒ ‒ ‒ ‒

Θησέως ζαθέαν ἐλθεῖν χώραν. 219 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

καὶ τὰν Αἰτναίαν Ἡφαίστου 220 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

Φοινίκας ἀντήρη χώραν, 221 2an

⏑ ⏑ ‒ ⏑⏑‒ ‒ ⏑ ⏑[^6] ‒ ‒

Σικελῶν ὀρέων ματέρ᾽, ἀκούω 222 2an

‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

καρύσσεσθαι στεφάνοις ἀρετᾶς. 223 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

τάν τ᾽ ἀγχιστεύουσαν γᾶν 224 par

⏑⏑⏑‒ ‒ ‒ ‒ ‒

† Ἰονίῳ ναύται πόντῳ, † 225 ?

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ἃν ὑγραίνει καλλιστεύων 226 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒ ‒

ὁ ξανθὰν χαίταν πυρσαίνων 227 2an

‒ ‒ ⏑ ⏑‒ ‒ ‒ ⏑ ⏑ ‒

Κρᾶθις ζαθέαις πηγαῖσι τρέφων 228 2an

‒ ‒ ‒ ‒ ‒ ‒ ‒

εὔανδρόν τ᾽ ὀλβίζων γᾶν. 229 par

**230-234: an (nonlyric)**

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

καὶ μὴν Δαναῶν ὅδʼ ἀπὸ ϲτρατιᾶϲ ‎230 2an

ˉ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

κῆρυξ, νεοχμῶν μύθων ταμίαϲ,‎ 231 2an

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

ϲτείχει ταχύπουν ἴχνοϲ ἐξανύτων.‎ 232 2an

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˉ ˉ ˉ

τί φέρει;‎ τί λέγει;‎ δοῦλαι γὰρ δὴ ‎233 2an

ˉ ˇ ˇ ˉ ˉ | ˇ ˇ ˉ ˉ

Δωρίδοϲ ἐϲμὲν χθονὸϲ ἤδη. 234 par

**FIRST EPISODE (235-510)**

**235-291: ia3 and dochmiacs**[^7]

*Ταλθύβιος*

⏑⏑ ‒

Ἑκάβη, πυκνὰς γὰρ οἶσθά μ᾽ ἐς Τροίαν ὁδοὺς 235 3ia

ἐλθόντα κήρυκ᾽ ἐξ Ἀχαιικοῦ στρατοῦ, 236 3ia

ἐγνωσμένος δὲ καὶ πάροιθέ σοι, γύναι, 237 3ia

Ταλθύβιος ἥκω καινὸν ἀγγελῶν λόγον. 238 3ia[^8]

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

†τόδε τόδε, φίλαι γυναῖκες† ὃ φόβος ἦν πάλαι. 239 ? +doc

*Ταλθύβιος*

ἤδη κεκλήρωσθ᾽, εἰ τόδ᾽ ἦν ὑμῖν φόβος. 240 3ia

*Ἑκάβη*

‒ ‒ ⏑ ‒

αἰαῖ, τίν᾽ ἢ 241 ia

‒ ⏑ ⏑‒ ⏑ ‒ ‒⏑ ⏑ ‒ ⏑ ‒

Θεσσαλίας πόλιν Φθιάδος εἶπας ἢ 242a 2doc

⏑ ‒ ‒ ⏑ ‒

Καδμείας χθονός; 242b doc

*Ταλθύβιος*

κατ᾽ ἄνδρ᾽ ἑκάστη κοὐχ ὁμοῦ λελόγχατε. 243 3ia

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑⏑ ⏑ ⏑ ‒ ⏑‒

τίν᾽ ἄρα τίς ἔλαχε; τίνα πότμος εὐτυχὴς 244 2doc

‒ ⏑⏑‒ ⏑ ‒

Ἰλιάδων μένει; 245 doc||H

*Ταλθύβιος*

οἶδ᾽: ἀλλ᾽ ἕκαστα πυνθάνου, μὴ πάνθ᾽ ὁμοῦ. 246 3ia

*Ἑκάβη*

‒ ‒ ⏑‒

τοὐμὸν τίς ἆρ᾽ 247 ia

⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

ἔλαχε τέκος, ἔνεπε, τλάμονα Κασσάνδραν; 248 2doc

*Ταλθύβιος*

ἐξαίρετόν νιν ἔλαβεν Ἀγαμέμνων ἄναξ. 249 3ia

*Ἑκάβη*

‒ ‒ ⏑ ⏑ ‒ ⏑⏑‒ ‒ ‒

ἦ τᾷ Λακεδαιμονίᾳ νύμφᾳ 250 ‒D+sp

‒ ‒ ‒ ‒ ‒

δούλαν; ὤμοι μοι. 251 doc

*Ταλθύβιος*

οὔκ, ἀλλὰ λέκτρων σκότια νυμφευτήρια. 252 3ia

*Ἑκάβη*

‒ ‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ⏑

ἦ τὰν τοῦ Φοίβου παρθένον, ᾇ γέρας ὁ 253 2doc

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑‒

χρυσοκόμας ἔδωκ᾽ ἄλεκτρον ζόαν; 254 2doc

*Ταλθύβιος*

ἔρως ἐτόξευσ᾽ αὐτὸν ἐνθέου κόρης. 255 3ia

*Ἑκάβη*

‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑

ῥῖπτε, τέκνον, ζαθέους κλα- 256 D⏑∫

‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ⏑

δας καὶ ἀπὸ χροὸς ἐνδυ- 257 D⏑∫

‒ ⏑ ⏑‒ ⏑⏑‒ ‒ ‒

τῶν στεφέων ἱεροὺς στολμούς. 258 ibyc, chol

*Ταλθύβιος*

οὐ γὰρ μέγ᾽ αὐτῇ βασιλικῶν λέκτρων τυχεῖν; 259 3ia

*Ἑκάβη*

⏑ ⏑ ⏑‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

τί δ᾽ ὃ νεοχμὸν ἀπ᾽ ἐμέθεν ἐλάβετε τέκος, 260 2doc

‒ ‒

†ποῦ μοι†; 261

*Ταλθύβιος*

Πολυξένην ἔλεξας, ἢ τίν᾽ ἱστορεῖς; 262 3ia

*Ἑκάβη*

‒ ‒ ‒ ⏑ ⏑ ‒ ‒ ‒

ταύταν: τῷ πάλος ἔζευξεν; 263 par?

*Ταλθύβιος*

τύμβῳ τέτακται προσπολεῖν Ἀχιλλέως. 264 3ia

*Ἑκάβη*

‒ ⏑⏑ ‒ ⏑ ‒ ‒ ⏑ ⏑ ⏑⏑ ⏑ ‒

ὤμοι ἐγώ: τάφῳ πρόσπολον ἐτεκόμαν. 265 2doc

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑

ἀτὰρ τίς ὅδ᾽ ἢ νόμος ἢ τί 266 erasmonidean

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

θέσμιον, ὦ φίλος, Ἑλλάνων; 267 ibyc, chol

*Ταλθύβιος*

εὐδαιμόνιζε παῖδα σήν: ἔχει καλῶς. 268 ia3

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ⏑

τί τόδ᾽ ἔλακες; 269 cr

‒ ⏑ ⏑ ‒⏑⏑‒ ‒ ‒

ἆρά μοι ἀέλιον λεύσσει; 270 ibyc, chol

*Ταλθύβιος*

ἔχει πότμος νιν, ὥστ᾽ ἀπηλλάχθαι πόνων. 271 ia3

*Ἑκάβη*

⏑ ‒ ‒ ‒ ⏑⏑ ‒ ⏑⏑ ‒ ⏑ ‒[^9] ‒ ‒

τί δ᾽ ἁ τοῦ χαλκεομήστορος Ἕκτορος δάμαρ, 272 ba+enoplian?

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒

Ἀνδρομάχα τάλαινα, τίν᾽ ἔχει τύχαν; 273 2doc

*Ταλθύβιος*

καὶ τήνδ᾽ Ἀχιλλέως ἔλαβε παῖς ἐξαίρετον. 274 3ia

*Ἑκάβη*

⏑ ‒ ⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἐγὼ δὲ τῷ πρόσπολος ἁ τριτοβάμονος 275 ia+ibyc

‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ⏑∩

δευομένα βάκτρου γεραιᾷ χερί; 276 2doc||BH[^10]

*Ταλθύβιος*

Ἰθάκης Ὀδυσσεὺς ἔλαχ᾽ ἄναξ δούλην σ᾽ ἔχειν. 277 3ia

*Ἑκάβη*

⏑ ⏑

ἒ ἔ. 278 extra metric

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑∩

ἄρασσε κρᾶτα κούριμον, 279 2ia||B

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

ἕλκ᾽ ὀνύχεσσι δίπτυχον παρειάν. 280 “ch”+ia+ba

⏑‒ ‒ ‒

ἰώ μοί μοι. 281 extra metric

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑

μυσαρῷ δολίῳ λέλογχα 282 diom

‒ ⏑ ‒ ‒ ‒

φωτὶ δουλεύειν, 283 hypodoc

⏑ ⏑⏑‒ ⏑‒ ⏑ ⏑⏑ ‒ ⏑ ‒

πολεμίῳ δίκας, παρανόμῳ δάκει, 284 2doc

‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ὃς πάντα τἀκεῖθεν ἐνθάδ&lt;ε στρέφει, 285 ia+cr+ia

⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑

τὰ δ᾽&gt; ἀντίπαλ᾽ αὖθις ἐκεῖσε 286 erasmonidean

‒ ⏑ ‒ ‒ ‒

διπτύχῳ γλώσσᾳ 287 hypodoc

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑⏑ ⏑ ⏑ ⏑ ‒ ⏑‒

φίλα τὰ πρότερ᾽ ἄφιλα τιθέμενος πάλιν. 288 2doc

⏑‒ ‒ ‒ ⏑ ‒ ⏑

†γοᾶσθ᾽, ὦ Τρῳάδες, με. 289 ?

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

βέβακα δύσποτμος. οἴχομαι ἁ† 290 ?

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

τάλαινα, δυστυχεστάτῳ 290 2ia

⏑ ⏑⏑ ‒ ‒ ‒

προσέπεσον κλήρῳ. 291 doc

**292-307: ia3**

**CASSANDRA’S MONODY (308-341)**

STROPHE

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒

Ἄνεχε: πάρεχε. φῶς φέρε· σέβω φλέγω — 308 2doc

⏑ ‒ ⏑ ‒

ἰδού, ἰδού — 309 ia

‒ ⏑⏑ ⏑ ⏑⏑∩ ‒ ⏑ ⏑ ‒ ⏑ ‒

λαμπάσι τόδ᾽ ἱερόν. ὦ Ὑμέναι᾽ ἄναξ: 310 2doc

⏑ ⏑⏑⏑ ⏑⏑ ⏑ ‒

μακάριος ὁ γαμέτας: 311 kaebelian

⏑ ⏑ ⏑‒ ⏑ ‒ ⏑ ⏑⏑ ‒ ‒ ‒

μακαρία δ᾽ ἐγὼ βασιλικοῖς λέκτροις 312 2doc

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

κατ᾽ Ἄργος ἁ γαμουμένα. 313 2ia||H

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

Ὑμήν, ὦ Ὑμέναι᾽ ἄναξ. 314 gly

⏑‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

ἐπεὶ σύ, μᾶτερ, †ἐπὶ δάκρυσι καὶ† 315 ?

⏑‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑⏑ ‒ ⏑⏑ ⏑

γόοισι τὸν θανόντα πατέρα πατρίδα τε 316-7 3ia

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

φίλαν καταστένουσ᾽ ἔχεις, 318 2ia||

⏑ ‒ ⏑⏑ ⏑ ‒ ⏑ ‒

ἐγὼ δ᾽ ἐπὶ γάμοις ἐμοῖς 319 ia+cr

⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

ἀναφλέγω πυρὸς φῶς 320 ith

⏑ ‒ ‒ ⏑ ‒ ‒

ἐς αὐγάν, ἐς αἴγλαν, 321 2ba

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

διδοῦσ᾽, ὦ Ὑμέναιε, σοί, 322 gly

⏑ ‒ ‒ ⏑ ⏑ ‒ ‒

διδοῦσ᾽, ὦ Ἑκάτα, φάος, 323 pher

‒ ⏑ ‒ ⏑⏑ ‒ ‒

παρθένων ἐπὶ λέκτροις 324a pher

‒ ⏑⏑ ⏑ ‒

ᾇ νόμος ἔχει. 324b ia?

ANTISTROPHE

‒ ⏑ ⏑ ‒ ⏑⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ∩

πάλλε πόδ᾽ αἰθέριον &lt;ἄναγ’&gt; ἄναγε χορόν— 325 2doc||B

⏑ ‒ ⏑ ‒

εὐἅν, εὐοἵ— 326 ia

‒ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑‒ ⏑ ‒

ὡς ἐπὶ πατρὸς ἐμοῦ μακαριωτάταις 327 2doc

⏑ ‒ ⏑ ⏑ ⏑ ⏑⏑⏑

τύχαις: ὁ χορὸς ὅσιος. 328 kaebelian

⏑⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ‒ ‒

ἄγε σύ, Φοῖβε, νῦν: κατὰ σὸν ἐν δάφναις 329 2doc

⏑ ‒ ⏑ ‒ ⏑‒ ⏑ ‒

ἀνάκτορον θυηπολῶ, 330 2ia||H

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

Ὑμήν, ὦ Ὑμέναι᾽, Ὑμήν. 331 gly

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

χόρευε, μᾶτερ, χόρευμ’ ἄναγε πόδα σὸν 332 ia+cr+ia

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

ἕλισσε τᾷδ᾽ ἐκεῖσε μετ᾽ ἐμέθεν ποδῶν 333 3ia

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

φέρουσα φιλτάταν βάσιν. 334 2ia||

⏑‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒

βόασον Ὑμέναιον, ὤ, 335 ia+cr

⏑ ⏑⏑‒ ⏑ ‒ ‒

μακαρίαις ἀοιδαῖς 336 ith

⏑‒ ‒ ⏑ ‒ ‒

ἰαχαῖς τε νύμφαν. 337 ba+ba||

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἴτ᾽, ὦ καλλίπεπλοι Φρυγῶν 338 gly

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

κόραι, μέλπετ᾽ ἐμῶν γάμων 339 gly

‒ ⏑ ‒ ⏑ ⏑ ‒ ‒

τὸν πεπρωμένον εὐνᾷ 340 pher

⏑ ⏑ ⏑ ⏑ ∩

πόσιν ἐμέθεν. 341 ia?

**342-443: ia3**

**444-461: trochaic tetrameters catalectic**

ἀλλὰ γὰρ τί τοὺϲ Ὀδυϲϲέωϲ ἐξακοντίζω πόνουϲ;‎

ϲτεῖχʼ ὅπωϲ τάχιϲτʼ·‎ ἐν Ἅιδου νυμφίωι γημώμεθα.‎

ἦ κακὸϲ κακῶϲ ταφήϲηι νυκτόϲ, οὐκ ἐν ἡμέραι,‎

ὦ δοκῶν ϲεμνόν τι πράϲϲειν, Δαναϊδῶν ἀρχηγέτα.‎

κἀμέ τοι νεκρὸν φάραγγεϲ γυμνάδʼ ἐκβεβλημένην‎

ὕδατι χειμάρρωι ῥέουϲαι νυμφίου πέλαϲ τάφου‎

θηρϲὶ δώϲουϲιν δάϲαϲθαι, τὴν Ἀπόλλωνοϲ λάτριν.‎

ὦ ϲτέφη τοῦ φιλτάτου μοι θεῶν, ἀγάλματʼ εὔια,‎

χαίρετʼ·‎ ἐκλέλοιφ̓ ἑορτὰϲ αἷϲ πάροιθʼ ἠγαλλόμην.‎

ἴτʼ ἀπʼ ἐμοῦ χρωτὸϲ ϲπαραγμοῖϲ, ὡϲ ἔτʼ οὖϲʼ ἁγνὴ χρόα‎

δῶ θοαῖϲ αὔραιϲ φέρεϲθαι ϲοὶ τάδʼ,‎ ὦ μαντεῖʼ ἄναξ.‎

ποῦ ϲκάφοϲ τὸ τοῦ ϲτρατηγοῦ;‎ ποῖ πόδʼ ἐμβαίνειν με χρή;‎

οὐκέτʼ ἂν φθάνοιϲ φθάνοιϲ ἂν αὔραν ἱϲτίοιϲ καραδοκῶν,‎

ὡϲ μίαν τριῶν Ἐρινὺν τῆϲδέ μʼ ἐξάξων χθονόϲ.‎

χαῖρέ μοι, μῆτερ·‎ δακρύϲηιϲ μηδέν·‎ ὦ φίλη πατρίϲ,‎

οἵ τε γῆϲ ἔνερθʼ ἀδελφοὶ χὠ τεκὼν ἡμᾶϲ πατήρ,‎

οὐ μακρὰν δέξεϲθέ μʼ·‎ ἥξω δʼ ἐϲ νεκροὺϲ νικηφόροϲ‎

καὶ δόμουϲ πέρϲαϲʼ Ἀτρειδῶν, ὧν ἀπωλόμεϲθʼ ὕπο.

**462-510: ia3**

**FIRST STASIMON (511-567)**

STROPHE

‒ ⏑ ⏑ ‒ ⏑⏑ ‒

ἀμφί μοι Ἴλιον, ὦ 511 D

‒ ⏑ ‒ ‒ ‒ ‒

Μοῦσα, καινῶν ὕμνων 512 e ‒ sp

‒ ‒ ‒ ⏑ ⏑‒ ‒ ‒ ⏑ ⏑‒ ‒ ‒

ᾆσον σύν δακρύοις ᾠδὰν ἐπικήδειον: 513-4 D(contr) ‒ D(contr)

‒ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑‒ ‒ ‒

νῦν γὰρ μέλος ἐς Τροίαν ἰαχήσω, 515 ‒ d ‒ e sp

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

τετραβάμονος ὡς ὑπ᾽ ἀπήνας 516 enoplian paroemiac

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑⏑⏑ ‒ ∩

Ἀργείων ὀλόμαν τάλαινα δοριάλωτος, 517-8 D(contr) ⏑ith||B

⏑ ⏑ ⏑ ⏑‒ ⏑ ‒ ⏑⏑⏑

ὅτ᾽ ἔλιπον ἵππον οὐράνια 519 2ia

⏑‒ ⏑ ⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑

βρέμοντα χρυσεοφάλαρον ἔνο- 520 2ia

⏑ ‒ ⏑ ‒ ⏑‒ ‒

πλον ἐν πύλαις Ἀχαιοί: 521 ia+ba||H

⏑ ⏑ ⏑ ⏑ ‒ ‒ ⏑‒

ἀνὰ δ᾽ ἐβόασεν λεὼς 522 ia+cr

‒ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

Τρῳάδος ἀπὸ πέτρας σταθείς: 523 2ia

⏑ ‒ ⏑ ‒ ⏑‒ ⏑ ‒

Ἴτ᾽, ὦ πεπαυμένοι πόνων, 524 2ia

⏑ ⏑⏑⏑ ⏑⏑ ⏑ ‒ ⏑⏑ ⏑

τόδ᾽ ἱερὸν ἀνάγετε ξόανον 525 2ia

⏑ ⏑⏑⏑ ⏑⏑⏑ ‒ ⏑ ‒

Ἰλιάδι Διογενεῖ κόρᾳ. 526 2ia

⏑ ‒ ⏑ ‒ ⏑‒ ⏑‒

τίς οὐκ ἔβα νεανίδων, 527 2ia[^11]

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

τίς οὐ γεραιὸς ἐκ δόμων; 528 2ia

⏑‒ ⏑ ‒ ⏑‒ ‒

κεχαρμένοι δ᾽ ἀοιδαῖς 529 ia+ba

⏑⏑⏑ ‒ ⏑ ‒ ‒

δόλιον ἔσχον ἄταν. 530 ith

ANTISTROPHE

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

πᾶσα δὲ γέννα Φρυγῶν 531 D

‒ ⏑ ‒ ‒ ‒ ‒

πρὸς πύλας ὡρμάθη, 532 e ‒ sp

‒ ‒ ‒ ⏑⏑‒ ‒ ‒ ⏑ ⏑ ‒ ‒ ‒

πεύκαν οὐρεΐαν ξεστὸν λόχον Ἀργείων 533-4 D(contr) ‒ D(contr)||

‒ ‒ ⏑⏑‒ ‒ ‒ ⏑‒ ‒ ‒

καὶ Δαρδανίας ἄταν θέᾳ δώσων, 535 ‒ d ‒ e sp

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

χάριν ἄζυγος ἀμβροτοπώλου: 536 enoplian paroemiac

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

κλωστοῦ δ᾽ ἀμφιβόλοις λίνοιο ναὸς ὡσεὶ 537-8 D ⏑ith||Bs

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑

σκάφος κελαινόν, εἰς ἕδρανα 539 2ia

‒⏑⏑ ⏑ ⏑⏑ ⏑ ⏑⏑⏑ ⏑ ⏑

λάινα δάπεδά τε φόνια πατρί- 540 2ia∫

⏑ ‒ ⏑ ‒ ⏑‒ ‒

δι Παλλάδος θέσαν θεᾶς. 541 ia+ba||Hs

⏑ ⏑⏑ ⏑ ‒ ‒ ⏑ ‒

ἐπὶ δὲ πόνῳ καὶ χαρᾷ 542 ia+cr

‒ ⏑⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

νύχιον ἐπεὶ κνέφας παρῆν, 543 2ia

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

Λίβυς τε λωτὸς ἐκτύπει 544 2ia

⏑⏑⏑⏑ ⏑⏑⏑ ‒ ⏑ ‒

Φρύγιά τε μέλεα, παρθένοι δ᾽ 545 2ia

⏑‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

ἄειρον ἀνὰ κρότον ποδῶν 546 2ia

⏑‒ ⏑‒ ⏑ ‒ ⏑ ‒

βοὰν ἔμελπον εὔφρον᾽, ἐν 547 2ia

⏑ ‒ ⏑ ‒ ⏑‒ ⏑ ‒

δόμοις δὲ παμφαὲς σέλας 548 2ia

⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

πυρὸς μέλαιναν αἴγλαν 549 ia+ba

⏑ ‒ ⏑ ‒ ‒

†ἔδωκεν ὕπνῳ†. 550 ?

EPODE

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἐγὼ δὲ τὰν ὀρεστέραν 551 2ia

⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

τότ᾽ ἀμφὶ μέλαθρα παρθένον 552-3 2ia

⏑‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

Διὸς κόραν ἐμελπόμαν 554 2ia

⏑ ‒ ⏑ ‒ ⏑⏑ ⏑ ‒

χοροῖσι: φοινία δ᾽ ἀνὰ 555 ia+cr

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

πτόλιν βοὰ κατεῖχε Περ- 556 2ia∫

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑⏑

γάμων ἕδρας: βρέφη δὲ φίλι- 557 2ia∫

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

α περὶ πέπλους ἔβαλλε μα- 558 2ia∫

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

τρὶ χεῖρας ἐπτοημένας: 559 2ia

⏑‒ ‒ ⏑ ‒ ⏑ ‒

λόχου δ᾽ ἐξέβαιν᾽ Ἄρης, 560 ba+ia

⏑ ‒ ‒ ⏑ ‒ ⏑ ‒

κόρας ἔργα Παλλάδος. 561 ba+ia

⏑ ‒ ‒ ⏑ ‒ ⏑‒

σφαγαὶ δ᾽ ἀμφιβώμιοι 562 ba+ia

⏑ ‒ ‒ ⏑ ‒ ⏑‒

Φρυγῶν, ἔν τε δεμνίοις 563 ba+ia

⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑‒

καράτομος ἐρημία 564 ba+ia

⏑‒ ⏑‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

νεανίδων στέφανον ἔφερεν 565 2ia

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

Ἑλλάδι κουροτρόφον, 566 D

⏑ ‒ ⏑ ⏑ ⏑⏑ ‒ ∩

Φρυγῶν δὲ πατρίδι πένθος. 567 ia+ba

**568-576: an (nonlyric)**

**ˇ ˇ ˉ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ**

Ἑκάβη, λεύϲϲειϲ τήνδʼ Ἀνδρομάχην‎ 568 an2

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

ξενικοῖϲ ἐπʼ ὄχοιϲ πορθμευομένην;‎ 569 an2

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

παρὰ δʼ εἰρεϲίαι μαϲτῶν ἕπεται‎ 570 an2

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˇ ˇ ˉ ˉ

φίλοϲ Ἀϲτυάναξ, Ἕκτοροϲ ἶνιϲ.‎ 571 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

ποῖ ποτʼ ἀπήνηϲ νώτοιϲι φέρηι,‎ 572 an2

ˉ ˉ ˇ ˇ ˉ

δύϲτηνε γύναι, 573 an‎

ˇ ˇ ˉ ˉ ˉ | ˉ ˇ ˇ ˉ ˉ

πάρεδροϲ χαλκέοιϲ Ἕκτοροϲ ὅπλοιϲ ‎ 573a an2

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˉ ˉ

ϲκύλοιϲ τε Φρυγῶν δοριθηράτοιϲ,‎ 574 an2

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˉ ˉ

οἷϲιν Ἀχιλλέωϲ παῖϲ Φθιώταϲ ‎575 an2

ˉ ˉ ˉ ˉ | ˇ ˇ ˉ ˉ

ϲτέψει ναούϲ, ἀπὸ Τροίαϲ; 576 par||

**SECOND EPISODE (577-798)**

**ANDROMACHE AND HECUBA (577-606)**

577-594 : lyric iambics

Strophe 1

⏑ ‒ ‒ ‒ ⏑ ‒ ⏑ ‒ ∩

*Ἀνδρομάχη* Ἀχαιοὶ δεσπόται μ᾽ ἄγουσιν. 577 ba+ith

‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

*Ἑκάβη* οἴμοι. *Ἀνδρομάχη* τί παιᾶν᾽ ἐμὸν στενάζεις; 578 ia+ith

‒ ‒ ‒ ‒ ⏑‒

*Ἑκάβη* αἰαῖ — *Ἀνδρομάχη* τῶνδ᾽ ἀλγέων — 579 mol+cr

‒ ‒ ‒ ‒ ⏑ ‒

*Ἑκάβη* ὦ Ζεῦ — *Ἀνδρομάχη* καὶ συμφορᾶς. 580 mol+cr

⏑⏑⏑ ‒ ⏑ ‒ ‒

*Ἑκάβη* τέκεα, *Ἀνδρομάχη* πρίν ποτ᾽ ἦμεν. 581 ith

Antistrophe 1

⏑ ‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

*Ἑκάβη* βέβακ᾽ ὄλβος, βέβακε Τροία 582 ba+ith

‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

*Ἀνδρομάχη* τλάμων. *Ἑκάβη* ἐμῶν τ᾽ εὐγένεια παίδων. 583 ia+ith

‒ ‒ ‒ ‒ ⏑ ‒

*Ἀνδρομάχη* φεῦ φεῦ. *Ἑκάβη* φεῦ δῆτ᾽ ἐμῶν 584 mol+ia

⏑ ‒ ‒ ‒ ⏑ ‒

*Ἀνδρομάχη* κακῶν. *Ἑκάβη* οἰκτρὰ τύχα 585 ba+cr

⏑⏑⏑ ‒ ⏑ ‒ ‒

*Ἀνδρομάχη* πόλεος, *Ἑκάβη* ἃ καπνοῦται. 586 ith

STROPHE 2

⏑ ‒ ‒ ⏑ ‒ ‒

*Ἀνδρομάχη* μόλοις, ὦ πόσις, μοι — 587 2ba

⏑‒ ‒ ⏑ ‒ ‒

*Ἑκάβη* βοᾷς τὸν παρ᾽ Ἅιδᾳ 588 2ba

‒ ⏑ ⏑ ‒ ⏑⏑‒

παῖδ᾽ ἐμόν, ὦ μελέα. 589 D

‒ ⏑ ‒ ⏑ ‒ ‒

*Ἀνδρομάχη* σᾶς δάμαρτος ἄλκαρ. 590 ith

ANTISTROPHE 2

⏑ ‒ ‒ ⏑ ‒ ‒

†σύ τ᾽†, ὦ λῦμ᾽ Ἀχαιῶν 591 2ba

⏑ ‒ ‒ ⏑ ‒ ‒

*Ἑκάβη* τέκνων δέσποθ᾽ ἁμῶν, 592 2ba

‒ ⏑⏑ ‒ ⏑⏑‒

πρεσβυγενὲς Πριάμῳ, 593 D

‒ ⏑ ‒ ⏑ ‒ ‒

*Ἀνδρομάχη* κοίμισαί μ᾽ ἐς Ἅιδου. 594 ith

595-606: dactylic

STROPHE 3

‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ⏑‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒

*Ἀνδ.* οἵδε πόθοι μεγάλοι *Ἑκ.* σχετλία, τάδε πάσχομεν ἄλγη. 595 6da

‒ ⏑⏑‒ ⏑⏑‒ ⏑⏑ ‒ ⏑⏑ ‒ ⏑⏑ ‒ ‒

*Ἀνδ.* οἰχομένας πόλεως *Ἑκ.* ἐπὶ δ᾽ ἄλγεσιν ἄλγεα κεῖται. 596 6da

*Ἀνδρομάχη*

‒ ⏑ ⏑ ‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

δυσφροσύναισι θεῶν, ὅτε σὸς γόνος ἔκφυγεν Ἅιδαν, 597 6da

‒ ⏑⏑‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑⏑ ‒ ‒

ὃς λεχέων στυγερῶν χάριν ὤλεσε πέργαμα Τροίας: 598 6da

‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ‒

αἱματόεντα δὲ θεᾷ παρὰ Παλλάδι σώματα νεκρῶν 599 6da

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑⏑ ‒ ‒

γυψὶ φέρειν τέταται: ζυγὰ δ᾽ ἤνυσε δούλια Τροίᾳ. 600 6da

ANTISTROPHE 3

‒ ⏑ ⏑ ‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒

*Ἑκ.* ὦ πατρίς, ὦ μελέα *Ἀνδ.* καταλειπομέναν σε δακρύω, 601 6da

‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

*Ἑκ.* νῦν τέλος οἰκτρὸν ὁρᾷς. *Ἀνδ.* καὶ ἐμὸν δόμον ἔνθ᾽ ἐλοχεύθην. 6da
602

*Ἑκάβη*

‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ὦ τέκν᾽, ἐρημόπολις μάτηρ ἀπολείπεται ὑμῶν, 603 6da

‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

†οἷος ἰάλεμος, οἷά τε πένθη† 604 ?

‒ ⏑⏑ ‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ⏑

δάκρυά τ᾽ ἐκ δακρύων καταλείβεται &lt; &gt; 605 ?

‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ‒

ἁμετέροισι δόμοις: ὁ θανὼν δ᾽ ἐπιλάθεται ἀλγέων. 606 6da

**607-781: ia3**

**617a: extrametric, pheu, pheu, Andromache**

**782-798: nonlyric an**

**ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ**

ΤΑ.‎ ἄγε, παῖ, φίλιον πρόϲπτυγμα μεθεὶϲ‎ 782 2an

ˉ ˉ ˇ ˇ ˉ | ˉ ˇ ˇ ˉ ˉ

μητρὸϲ μογερᾶϲ, βαῖνε πατρώιων‎ 783 2an

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

πύργων ἐπʼ ἄκραϲ ϲτεφάναϲ, ὅθι ϲοι‎ 784 2an

ˉ ˇ ˇ ˉ ˉ | ˉ ˇ ˇ ˉ ˉ

πνεῦμα μεθεῖναι ψῆφοϲ ἐκράνθη.‎ 785 2an

ˉ ˇ ˇ ˉ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

λαμβάνετʼ αὐτόν.‎ τὰ δὲ τοιάδε χρὴ‎ 786 2an

ˉ ˉ ˉ ˉ | ˉ ˇ ˇ ˉ ˉ

κηρυκεύειν ὅϲτιϲ ἄνοικτοϲ‎ 787 2an

ˇ ˇ ˉ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

καὶ ἀναιδείαι τῆϲ ἡμετέραϲ‎ 788 2an

ˉ ˉ ˉ ˉ | ˇ ˇ ˉ ∩

γνώμηϲ μᾶλλον φίλοϲ ἐϲτίν.‎ 789 2an||B

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

ΕΚ.‎ ὦ τέκνον, ὦ παῖ παιδὸϲ μογεροῦ,‎ 790 2an

ˉ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

ϲυλώμεθα ϲὴν ψυχὴν ἀδίκωϲ‎ 791 2an

ˉ ˉ ˉ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

μήτηρ κἀγώ.‎ τί πάθω;‎ τί ϲʼ ἐγώ,‎ 792 2an

ˉ ˇ ˇ ˉ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

δύϲμορε, δράϲω;‎ τάδε ϲοι δίδομεν‎ 793 2an

ˉ ˇ ˇ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

πλήγματα κρατὸϲ ϲτέρνων τε κόπουϲ·‎ 794 2an

ˉ ˇ ˇ ˉ ˇ ˇ | ˉ ˉ ˇ ˇ ˉ

τῶνδε γὰρ ἄρχομεν.‎ οἲ‎ ʼγὼ πόλεωϲ,‎ 795 2an

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

οἴμοι δὲ ϲέθεν·‎ τί γὰρ οὐκ ἔχομεν;‎ 796 2an

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˉ ˇ ˇ ˉ

τίνοϲ ἐνδέομεν μὴ οὐ πανϲυδίαι‎ 797 an2

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˉ

χωρεῖν ὀλέθρου διὰ παντόϲ;‎ 798 par||

**SECOND STASIMON (799-859)**

**STROPHE 1**

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

μελισσοτρόφου Σαλαμῖνος ὦ βασιλεῦ Τελαμών, 799 ⏑D⏑D

‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒

νάσου περικύμονος οἰκήσας ἕδραν 800 ‒D‒e

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒

τᾶς ἐπικεκλιμένας ὄχθοις ἱεροῖς, ἵν᾽ ἐλαίας 801 D‒D‒

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒

πρῶτον ἔδειξε κλάδον γλαυκᾶς Ἀθάνα, 802 D‒e‒||Hs

‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

οὐράνιον στέφανον λιπαραῖσί &lt;τε&gt; κόσμον Ἀθήναις, 803 6da

⏑ ‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ἔβας ἔβας τῷ τοξοφόρῳ συναρι- 804 ⏑e‒D∫

‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒

στεύων ἅμ᾽ Ἀλκμήνας γόνῳ 805 ‒e‒e||Hs

‒ ⏑⏑ ‒ ⏑⏑ ‒ ‒ ‒ ⏑ ⏑

Ἴλιον Ἴλιον ἐκπέρσων πόλιν 806 4da

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑

ἁμετέραν τὸ πάροιθεν &lt; &gt; 807 ?

⏑ ⏑ ‒ ‒ ‒ ⏑ ⏑

\[ὅτ᾽ ἔβας ἀφ᾽ Ἑλλάδος\]: \[808\]

ANTISTROPHE 1

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ὅθ᾽ Ἑλλάδος ἄγαγε πρῶτον ἄνθος ἀτυζόμενος 809 ⏑D⏑D

‒ ‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒

πώλων, Σιμόεντι δ᾽ ἐπ᾽ εὐρείτᾳ πλάταν 810 ‒D‒e

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἔσχασε ποντοπόρον καὶ ναύδετ᾽ ἀνήψατο πρυμνᾶν 811 D‒D‒

‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ‒ ‒ ⏑ ‒ ‒

καὶ χερὸς εὐστοχίαν ἐξεῖλε ναῶν, 812 D‒e‒||Hs

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

Λαομέδοντι φόνον: κανόνων δὲ τυκίσματα Φοίβου 813-4 6dac

⏑ ‒ ⏑ ‒ ‒ ‒ ⏑ ⏑‒ ⏑ ⏑ ‒

πυρὸς &lt; πυρὸς &gt; φοίνικι πνοᾷ καθελὼν 815 ⏑e‒D

‒ ‒ ⏑‒ ‒ ‒ ⏑∩

Τροίας ἐπόρθησε χθόνα. 816 ‒e‒e||HsBa

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑⏑

δὶς δὲ δυοῖν πιτύλοιν τείχη περὶ 817 4da

‒ ⏑⏑‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

Δαρδανίδας φόνια κατέλυσεν αἰχμά. 818-9 praxillean

STROPHE 2

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

μάταν ἄρ᾽, ὦ χρυσέαις ἐν οἰνοχόαις ἁβρὰ βαίνων, 820-1 ia+e⏑D‒

‒ ⏑ ⏑ ‒ ⏑⏑ ‒

Λαομεδόντιε παῖ, 822 D

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒

Ζηνὸς ἔχεις κυλίκων πλήρωμα, καλλίσταν λατρείαν: 823-4 D‒e‒e‒||

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

ἁ δέ σε γειναμένα πυρὶ δαίεται: 825-6 4da||Hs

‒⏑⏑‒ ⏑⏑‒

ἠιόνες δ᾽ ἅλιαι 827-8 D||Hs

⏑‒ ⏑ ‒ ‒ ⏑ ‒

ἴακχον οἰωνὸς οἷ- 829 ia+cr∫

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ον τέκνων ὕπερ βοῶς᾽, 830 lek

‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ⏑

ᾇ μὲν εὐνάς, ᾇ δὲ παῖδας, 831 2tr

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

ᾇ δὲ ματέρας γεραιάς. 832 2tr||

⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ‒ ⏑

τὰ δὲ σὰ δροσόεντα λουτρὰ 833 diomedean

‒ ⏑ ⏑‒ ⏑ ⏑ ‒

γυμνασίων τε δρόμοι 834 D

⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑⏑

βεβᾶσι, σὺ δὲ πρόσωπα νεα- 835 2ia∫

⏑ ⏑⏑⏑ ⏑ ⏑ ⏑‒ ⏑ ‒

ρὰ χάρισι παρὰ Διὸς θρόνοις 836 2ia

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒

καλλιγάλανα τρέφεις: Πριάμοιο δὲ γαῖαν 837-8 D\^D‒

‒ ⏑ ‒ ⏑ ‒ ‒

Ἑλλὰς ὤλεσ᾽ αἰχμά. 839 ith

ANTISTROPHE 2

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ∩

Ἔρως Ἔρως, ὃς τὰ Δαρδάνεια μέλαθρά ποτ᾽ ἦλθες 840-1 ia+e⏑D‒||B

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

οὐρανίδαισι μέλων, 842 D

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ∩

ὡς τότε μὲν μεγάλως Τροίαν ἐπύργωσας, θεοῖσι 843-4 D‒e‒e‒||B

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑

κῆδος ἀναψάμενος. τὸ μὲν οὖν Διὸς 845 4da

‒ ⏑ ⏑ ‒ ⏑⏑ ‒

οὐκέτ᾽ ὄνειδος ἐρῶ: 846-7 D||Hs

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

τὸ τᾶς δὲ λευκοπτέρου 848 ia+cr

⏑ ⏑⏑‒ ⏑ ‒ ⏑ ‒

φίλιον Ἁμέρας βροτοῖς 849 lek

‒ ⏑ ⏑ ⏑⏑ ‒ ⏑ ‒ ‒

φέγγος ὀλοὸν εἶδε γαίας, 850 2tr

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

εἶδε Περγάμων ὄλεθρον, 851 2tr||

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑

τεκνοποιὸν ἔχουσα τᾶσδε 852-3 diomedean

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

γᾶς πόσιν ἐν θαλάμοις, 854 D

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑

ὃν ἀστέρων τέθριππος ἔλα- 855 2ia∫

⏑ ⏑ ⏑⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

βε χρύσεος ὄχος ἀναρπάσας, 856 2ia

‒ ⏑⏑ ‒ ⏑ ⏑‒ ⏑⏑ ‒ ⏑ ⏑‒ ⏑

ἐλπίδα γᾷ πατρίᾳ μεγάλαν: τὰ θεῶν δὲ 857-8 D\^D⏑

‒ ⏑ ‒ ⏑ ‒ ‒

φίλτρα φροῦδα Τροίᾳ. 859 ith

**THIRD EPISODE: 860-1059: ia3: agon**

**944a: extrametric *eien*, Helen**

**THIRD STASIMON (1060-1117)**

STROPHE 1

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑‒

οὕτω δὴ τὸν ἐν Ἰλίῳ 1060 gly

‒ ‒ ‒ ⏑⏑‒ ⏑ ‒

ναὸν καὶ θυόεντα βω- 1061 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

μὸν προύδωκας Ἀχαιοῖς, 1062 pher||

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ὦ Ζεῦ, καὶ πελάνων φλόγα 1063 gly

‒ ‒ ‒ ⏑⏑‒ ⏑ ‒

σμύρνης αἰθερίας τε κα- 1064 gly∫

‒ ‒ ‒ ⏑ ⏑⏑⏑‒

πνὸν καὶ Πέργαμον ἱερὰν 1065 pher||

‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒

Ἰδαῖά τ᾽ Ἰδαῖα κισσοφόρα νάπη 1066 ia+cr+ia

⏑⏑⏑ ⏑ ⏑ ⏑⏑ ⏑ ⏑⏑‒

χιόνι κατάρυτα ποταμίᾳ 1067 2ia

‒ ⏑⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑‒

τέρμονα τε πρωτόβολόν θ᾽ ἕῳ, 1068 2ia

‒ ⏑⏑ ‒ ⏑ ⏑‒ ⏑ ⏑‒ ⏑ ‒ ‒

τὰν καταλαμπομέναν ζαθέαν θεράπναν; 1069-70 praxillean

ANTISTROPHE 1

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒

φροῦδαί σοι θυσίαι χορῶν τ᾽ 1071 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

εὔφημοι κέλαδοι κατ᾽ ὄρ- 1072 gly∫

‒ ⏑ ‒ ⏑ ⏑ ‒ ‒

φναν τε παννυχίδες θεῶν, 1073 pher||

‒ ‒ ‒ ⏑⏑‒ ⏑‒

χρυσέων τε ξοάνων τύποι 1074 gly

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒

Φρυγῶν τε ζάθεοι σελᾶ- 1075 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

ναι συνδώδεκα πλήθει. 1076 pher

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

μέλει μέλει μοι τάδ᾽ εἰ φρονεῖς, ἄναξ, 1077 ia+cr+ia

‒ ⏑⏑⏑⏑ ⏑ ⏑ ⏑⏑⏑ ‒

οὐράνιον ἕδρανον ἐπιβεβὼς 1078 2ia

‒ ⏑ ⏑⏑ ⏑ ⏑⏑ ⏑ ⏑ ⏑‒

αἰθέρα τε πόλεος ὀλομένας, 1079 2ia

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

ἃν πυρὸς αἰθομένα κατέλυσεν ὁρμά. 1080-1 prax

STROPHE 2

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ὦ φίλος ὦ πόσι μοι, 1082 D

⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ‒

σὺ μὲν φθίμενος ἀλαίνεις 1083 ia+ba

⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑‒ ⏑ ∩

ἄθαπτος ἄνυδρος, ἐμὲ δὲ πόντιον σκάφος 1085 3ia|B

‒‒ ‒ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἀίσσον πτεροῖσι πορεύσει 1086 ?

‒ ⏑ ⏑ ⏑ ‒ ⏑ ⏑⏑ ⏑ ‒ ‒

ἱππόβοτον Ἄργος, ἵνα &lt;τε&gt; τείχη 1087 2cr+ba||

‒ ⏑⏑ ⏑ ‒ ⏑ ‒ ⏑⏑⏑ ⏑ ‒ ‒

λάινα Κυκλώπι᾽ οὐράνια νέμονται. 1088 2ia+ba

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

τέκνων δὲ πλῆθος ἐν πύλαις 1089 2ia

‒ ⏑⏑ ⏑ ⏑⏑‒[^12] ⏑ ‒ ⏑‒ ⏑‒

δάκρυσι †κατάορα στένει† βοᾷ βοᾷ: 1090 ?

‒ ⏑ ‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ‒ ⏑ ‒

Μᾶτερ, ὤμοι, μόναν δή μ᾽ Ἀχαιοὶ κομί- 1091-2 4cr∫

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

ζουσι σέθεν ἀπ᾽ ὀμμάτων 1093 lek||

‒ ⏑⏑‒ ⏑⏑ ‒

κυανέαν ἐπὶ ναῦν 1094 D

‒ ⏑⏑‒ ⏑ ⏑ ‒

εἰναλίαισι πλάταις 1095 D

‒ ⏑ ⏑‒ ⏑⏑‒

ἢ Σαλαμῖν᾽ ἱερὰν 1096 D

‒ ⏑⏑ ‒ ⏑⏑ ‒

ἢ δίπορον κορυφὰν 1097 D

‒ ⏑⏑ ‒ ⏑⏑ ‒

Ἴσθμιον, ἔνθα πύλας 1098 D

⏑⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

Πέλοπος ἔχουσιν ἕδραι. 1099 ia+ba

ANTISTROPHE 2

‒ ⏑⏑ ‒ ⏑⏑ ‒

εἴθ᾽ ἀκάτου Μενέλα 1100 D

⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ‒

μέσον πέλαγος ἰούσας, 1101 ia+ba

⏑ ‒ ⏑ ⏑⏑⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

δίπαλτον ἱερὸν ἀνὰ μέσον πλατᾶν πέσοι 1102-3 3ia||H/Bs

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑‒ ‒

†αἰγαίου† κεραυνοφαὲς πῦρ, 1104 ?

‒ ⏑⏑⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ‒ ∩

Ἰλιόθεν ὅτε με πολυδάκρυτον 1105 2cr+ba||B

‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑‒ ‒

Ἑλλάδι λάτρευμα γᾶθεν ἐξορίζει, 1106 2ia+ba

‒ ⏑⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

χρύσεα δ᾽ ἔνοπτρα, παρθένων 1107 2ia

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑‒ ⏑ ‒

χάριτας, ἔχουσα τυγχάνει Διὸς κόρα: 1108 3ia||

‒ ⏑ ‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ‒ ⏑ ‒

μηδὲ γαῖάν ποτ᾽ ἔλθοι Λάκαιναν πατρῷ- 1109-10 4cr∫

‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑‒

όν τε θάλαμον ἑστίας, 1111 lek||

‒ ⏑⏑ ‒ ⏑⏑ ‒

μηδὲ πόλιν Πιτάνας 1112 D

‒ ⏑⏑ ‒ ⏑ ⏑‒

χαλκόπυλόν τε θεάν, 1113 D

‒ ⏑⏑ ‒ ⏑⏑ ‒

δύσγαμον αἶσχος ἑλὼν 1114 D

‒ ⏑⏑‒ ⏑⏑ ‒

Ἑλλάδι τᾷ μεγάλᾳ 1115 D

‒ ⏑⏑‒ ⏑⏑ ‒

καὶ Σιμοεντιάσιν 1116 D

⏑ ⏑⏑ ⏑ ⏑⏑⏑‒ ∩

μέλεα πάθεα ῥοαῖσιν. 1117 ia+ba

**1118-1122: nonlyric an**

**ˉ ˉ ˉ ˉ | ˇ ˇ ˉ ˉ ˉ**

καίνʼ ἐκ καινῶν μεταβάλλουϲαι‎ 1118 2an

ˇ ˇ ˉ ˇ ˇ ˉ | ˉ ˇ ˇ ˉ ˉ

χθονὶ ϲυντυχίαι.‎ λεύϲϲετε Τρώων‎ 1119 2an

ˉ ˉ ˇ ˇ ˉ | ˇ ˇ ˉ ˇ ˇ ˉ

τόνδʼ Ἀϲτυάνακτʼ ἄλοχοι μέλεαι‎ 1120 2an

ˇ ˇ ˉ ˉ ˉ | ˉ ˉ ˇ ˇ ˉ

νεκρόν, ὃν πύργων δίϲκημα πικρὸν‎ 1121 2an

ˇ ˇ ˉ ˉ ˉ | ˇ ˇ ˉ ∩

Δαναοὶ κτείναντεϲ ἔχουϲιν. 1122 par||B

**EXODOS (1123-1332)**

**1123-1215: ia3**

1216-1217b: dochmiacs

*Χορός*

⏑ ‒ ⏑ ‒

ἒ ἔ, φρενῶν 1216 ia

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒

ἔθιγες ἔθιγες: ὦ μέγας ἐμοί ποτ᾽ ὢν 1217a 2doc

⏑ ‒ ‒ ⏑ ‒

ἀνάκτωρ πόλεως. 1217b doc

**1218-1225: ia3**

*Ἑκάβη*

ἃ δ᾽ ἐν γάμοισι χρῆν σε προσθέσθαι χροῒ 1218 3ia

Ἀσιατίδων γήμαντα τὴν ὑπερτάτην, 1219 3ia

Φρύγια πέπλων ἀγάλματ᾽ ἐξάπτω χροός. 1220 3ia

σύ τ᾽, ὦ ποτ᾽ οὖσα καλλίνικε, μυρίων 1221 3ia

μῆτερ τροπαίων, Ἕκτορος φίλον σάκος, 1222 3ia

στεφανοῦ: θανῇ γὰρ οὐ θανοῦσα σὺν νεκρῷ: 1223 3ia

ἐπεὶ σὲ πολλῷ μᾶλλον ἢ τὰ τοῦ σοφοῦ 1224 3ia

κακοῦ τ᾽ Ὀδυσσέως ἄξιον τιμᾶν ὅπλα. 1225 3ia

1226-1231: dochmiacs (with iambics)

*Χορός*

⏑ ‒ ⏑ ‒

αἰαῖ αἰαῖ: 1226 ia

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

πικρὸν ὄδυρμα γαῖά σ᾽ ὦ 1227 lek

⏑ ‒ ‒ ⏑ ‒

τέκνον δέξεται. 1228 doc[^13]

⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

στέναζε, μᾶτερ, *Ἑκάβη* αἰαῖ. 1229 ia+ba

*Χορός*

⏑ ‒ ⏑‒ ⏑ ‒ ‒

νεκρῶν ἴακχον. *Ἑκάβη* οἴμοι. 1230 ia+ba||H

*Χορός*

‒ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

οἴμοι δῆτα σῶν ἀλάστων κακῶν. 1231 2doc

**1232-1234: ia3**

*Ἑκάβη*

τελαμῶσιν ἕλκη τὰ μὲν ἐγώ σ᾽ ἰάσομαι, 1232 3ia

τλήμων ἰατρός, ὄνομ᾽ ἔχουσα, τἄργα δ᾽ οὔ: 1233 3ia

τὰ δ᾽ ἐν νεκροῖσι φοντιεῖ πατὴρ σέθεν. 1234 3ia

1235-1239: lyric iambics

*Χορός*

⏑ ‒ ⏑ ‒ ⏑ ‒ ∩

ἄρασσ᾽ ἄρασσε κρᾶτα 1235 ia+ba||B

⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ∩

πιτύλους διδοῦσα χειρός, 1236 doc+ba||B

⏑ ‒ ‒ ‒

ἰώ μοί μοι. 1237 extra metric

*Ἑκάβη*

‒ ‒ ⏑ ‒ ⏑ ‒ ∩

ὦ φίλταται γυναῖκες. 1238 ia+ba||B

*Χορός*

⏑ ⏑ ‒ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ‒ ‒

†Ἑκάβη, σὰς† ἔνεπε: τίνα θροεῖς αὐδάν; 1239

**1240-1250: ia3**

*Ἑκάβη*

†οὐκ ἦν ἄρ᾽ ἐν θεοῖσι† πλὴν οὑμοὶ πόνοι 1240 3ia

Τροία τε πόλεων ἔκκριτον μισουμένη, 1241 3ia

μάτην δ᾽ ἐβουθυτοῦμεν. εἰ δὲ μὴ θεὸς 1242 3ia

ἔστρεψε τἄνω περιβαλὼν κάτω χθονός, 1243 3ia

ἀφανεῖς ἂν ὄντες οὐκ ἂν ὑμνήθεῖμεν ἂν 1244 3ia

μούσαις ἀοιδὰς δόντες ὑστέρων βροτῶν. 1245 3ia

χωρεῖτε, θάπτετ᾽ ἀθλίῳ τύμβῳ νεκρόν: 1246 3ia

ἔχει γὰρ οἷα δεῖ γε νερτέρων στέφη. 1247 3ia

δοκῶ δὲ τοῖς θανοῦσι διαφέρειν βραχύ, 1248 3ia

εἰ πλουσίων τις τεύξεται κτερισμάτων: 1249 3ia

κενὸν δὲ γαύρωμ᾽ ἐστὶ τῶν ζώντων τόδε. 1250 3ia

**1250a-1259: an**

*Χορός*

‒ ‒ ‒ ‒

ἰὼ ἰώ: 1250a an

⏑ ⏑‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

μελέα μήτηρ, ἣ τὰς μεγάλας 1251 2an

‒ ⏑ ⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ἐλπίδας ἐν σοὶ κατέκναψε βίου. 1252 2an

⏑ ⏑ ‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

μέγα δ᾽ ὀλβισθεὶς ὡς ἐκ πατέρων 1253 2an

⏑ ⏑ ‒ ⏑ ⏑ ‒

ἀγαθῶν ἐγένου, 1254 an

‒ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ‒

δεινῷ θανάτῳ διόλωλας. 1255 par

⏑‒⏑‒

ἔα ἔα: 1255a extra metric

⏑ ⏑ ‒⏑⏑ ‒ ‒ ‒ ⏑ ⏑ ‒

τίνας Ἰλιάσιν ταῖσδ᾽ ἐν κορυφαῖς 1256 2an

‒ ‒ ⏑ ⏑‒ ‒ ‒ ⏑ ⏑ ‒

λεύσσω φλογέας δαλοῖσι χέρας 1257 2an

⏑⏑ ‒ ‒ ‒ ‒ ‒ ‒ ‒

διερέσσοντας; μέλλει Τροίᾳ 1258 2an

‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

καινόν τι κακὸν προσέσεσθαι. 1259 par

**1260-1286: ia3**

*Ταλθύβιος*

αὐδῶ λοχαγοῖς, οἳ τέταχθ᾽ ἐμπιμπράναι 1260 3ia

Πριάμου τόδ᾽ ἄστυ, μηκέτ᾽ ἀργοῦσαν φλόγα 1261 3ia

ἐν χερσὶ σῴζειν, ἀλλὰ πῦρ ἐνιέναι, 1262 3ia

ὡς ἂν κατασκάψαντες Ἰλίου πόλιν 1263 3ia

στελλώμεθ᾽ οἴκαδ᾽ ἄσμενοι Τροίας ἄπο. 1264 3ia

ὑμεῖς δ᾽, ἵν᾽ αὑτὸς λόγος ἔχῃ μορφὰς δύο, 1265 3ia

χωρεῖτε, Τρώων παῖδες, ὀρθίαν ὅταν 1266 3ia

σάλπιγγος ἠχὼ δῶσιν ἀρχηγοὶ στρατοῦ, 1267 3ia

πρὸς ναῦς Ἀχαιῶν, ὡς ἀποστέλλησθε γῆς. 1268 3ia

σύ τ᾽, ὦ γεραιὰ δυστυχεστάτη γύναι, 1269 3ia

ἕπου. μεθήκουσίν σ᾽ Ὀδυσσέως πάρα 1270 3ia

οἵδ᾽, ᾧ σε δούλην κλῆρος ἐκπέμπει πάτρας. 1271 3ia

*Ἑκάβη*

οἲ 'γὼ τάλαινα: τοῦτο δὴ τὸ λοίσθιον 1272 3ia

καὶ τέρμα πάντων τῶν ἐμῶν ἤδη κακῶν: 1273 3ia

ἔξειμι πατρίδος, πόλις ὑφάπτεται πυρί. 1274 3ia

ἀλλ᾽, ὦ γεραιὲ πούς, ἐπίσπευσον μόλις, 1275 3ia

ὡς ἀσπάσωμαι τὴν ταλαίπωρον πόλιν. 1276 3ia

ὦ μεγάλα δή ποτ᾽ ἀμπνέουσ᾽ ἐν βαρβάροις 1277 3ia

Τροία, τὸ κλεινὸν ὄνομ᾽ ἀφαιρήσῃ τάχα. 1278 3ia

πιμπρᾶσί σ᾽, ἡμᾶς δ᾽ ἐξάγουσ᾽ ἤδη χθονὸς 1279 3ia

δούλας: ἰὼ θεοί. καὶ τί τοὺς θεοὺς καλῶ; 1280 3ia

καὶ πρὶν γὰρ οὐκ ἤκουσαν ἀνακαλούμενοι. 1281 3ia

φέρ᾽ ἐς πυρὰν δράμωμεν: ὡς κάλλιστά μοι 1282 3ia

σὺν τῇδε πατρίδι κατθανεῖν πυρουμένῃ. 1283 3ia

*Ταλθύβιος*

ἐνθουσιᾷς, δύστηνε, τοῖς σαυτῆς κακοῖς. 1284 3ia

ἀλλ᾽ ἄγετε, μὴ φείδεσθ᾽: Ὀδυσσέως δὲ χρὴ 1285 3ia

ἐς χεῖρα δοῦναι τήνδε καὶ πέμπειν γέρας. 1286 3ia

1287-1387: lyric iambics[^14]

STROPHE 1

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ‒

ὀτοτοτοτοῖ. 1287 ia

⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑⏑ ⏑ ⏑ ⏑

Κρόνιε, πρύτανι Φρύγιε, γενέτα 1288 2ia

⏑ ⏑ ⏑ ‒ ⏑⏑ ‒ ‒ ⏑ ⏑ ‒

†πάτερ, ἀνάξια τᾶς Δαρδανίου† 1289 ?

⏑ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

γονᾶς τάδ᾽ οἷα πάσχομεν δέδορκας; 1290 2ia+ba

*Χορός*

⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

δέδορκεν, ἁ δὲ μεγαλόπολις 1291 2ia

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

ἄπολις ὄλωλεν οὐδ᾽ ἔτ᾽ ἔστι Τροία. 1292-3 2ia+ba

ANTISTROPHE 1

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ‒

ὀτοτοτοτοῖ. 1294 ia

⏑ ‒ ⏑ ‒⏑‒ ‒

†λέλαμπεν Ἴλιος, Περ- 1295 ?

⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑

γάμων τε πυρὶ καταίθεται τέραμνα 1296 ?

‒ ⏑ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒

καὶ πόλις ἄκρα τε τειχέων†. 1297 ?

*Χορός*

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

πτέρυγι δὲ καπνὸς ὥς τις οὐ- 1298 2ia

⏑ ⏑‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

ρανίᾳ πεσοῦσα δορὶ καταφθίνει γᾶ. 1299 2ia+ba

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

\[μαλερὰ μέλαθρα πυρὶ κατάδρομα 1300 \[2ia\]

‒⏑‒ ⏑ ‒ ‒

δαΐῳ τε λόγχᾳ.\] 1301 \[ith\]

STROPHE 2

*Ἑκάβη*

⏑‒ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἰὼ γᾶ τρόφιμε τῶν ἐμῶν τέκνων. 1302 ba+cr+ia

*Χορός*

⏑ ⏑

ἓ ἕ. 1302a extra metric

*Ἑκάβη*

‒ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

ὦ τέκνα, κλύετε, μάθετε ματρὸς αὐδάν. 1303 2ia+ba

*Χορός*

⏑‒ ⏑‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἰαλέμῳ τοὺς θανόντας ἀπύεις. 1304 ia+cr+ia

*Ἑκάβη*

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ∩

γεραιά γ᾽ ἐς πέδον τιθεῖσα μέλε’ &lt;ἐμὰ&gt; 1305 3ia||B/Ha

‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

καὶ χερσὶ γαῖαν κτυποῦσα δισσαῖς. 1306 ia+ith||Ha

*Χορός*

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

διάδοχά σοι γόνυ τίθημι γαίᾳ 1307 ia+ith

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑

τοὺς ἐμοὺς καλοῦσα νέρθεν 1308 2tr

‒ ⏑ ‒ ⏑ ‒ ‒

ἀθλίους ἀκοίτας. 1309 ith||Ha

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

ἀγόμεθα φερόμεθ᾽ *Χορός* ἄλγος ἄλγος βοᾷς. 1310 2ia+cr

*Ἑκάβη*

‒ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

δούλειον ὑπὸ μέλαθρον. *Χορός* ἐκ πάτρας γ᾽ ἐμᾶς. 1311 3ia||

*Ἑκάβη*

⏑‒ ⏑‒ ⏑⏑ ⏑ ⏑ ⏑ ⏑

ἰώ, ἰώ, Πρίαμε Πρίαμε, 1312 2ia

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

σὺ μὲν ὀλόμενος ἄταφος ἄφιλος 1313 2ia

‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἄτας ἐμᾶς ἄιστος εἶ. 1314 2ia

*Χορός*

⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒

μέλας γὰρ ὄσσε κατεκάλυ- 1315 2ia∫

⏑ ⏑ ⏑ ⏑ ⏑ ⏑⏑ ⏑ ⏑ ⏑‒ ⏑ ‒ ∩

ψε θάνατος ὅσιος ἀνοσίαις σφαγαῖσιν. 1316 2ia+ba

ANTISTROPHE 2

*Ἑκάβη*

⏑‒ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἰὼ θεῶν μέλαθρα καὶ πόλις φίλα, 1317 ba+cr+ia

*Χορός*

⏑ ⏑

ἓ ἕ. 1317a extra metric

*Ἑκάβη*

‒ ⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

τὰν φόνιον ἔχετε φλόγα δορός τε λόγχαν. 1318 2ia+ba

*Χορός*

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

τάχ᾽ ἐς φίλαν γᾶν πεσεῖσθ᾽ ἀνώνυμοι. 1319 ia+cr+ia

*Ἑκάβη*

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ∩

κόνις δ᾽ ἴσα καπνῷ πτέρυγι πρὸς αἰθέρα 1320 3ia||B/H

⏑‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

ἄιστον οἴκων ἐμῶν με θήσει. 1321 ia+ith

*Χορός*

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

ὄνομα δὲ γᾶς ἀφανὲς εἶσιν: ἄλλᾳ δ᾽ 1322 ia+ith

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑

ἄλλο φροῦδον, οὐδ᾽ ἔτ᾽ ἔστιν 1323 2tr

‒ ⏑ ‒ ⏑ ‒ ‒

ἁ τάλαινα Τροία. 1324 ith||Ha

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒ ⏑ ∩

*Ἑκάβη* ἐμάθετ᾽, ἐκλύετε; *Χορός* Περγάμων &lt;γε&gt; κτύπον. 1325
2ia+cr

*Ἑκάβη*

⏑ ⏑ ⏑ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ∩

ἔνοσις ἅπασαν ἔνοσις *Χορός* ἐπικλύσει πόλιν. 1326 3ia||B

*Ἑκάβη*

⏑‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

ἰώ &lt;ἰώ&gt;, τρομερὰ τρομερὰ 1327 2ia

⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

μέλεα, φέρετ᾽ ἐμὸν ἴχνος· ἴτ᾽ ἐπί 1328-9 2ia

‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

δούλειον ἁμέραν βίου. 1330 2ia||Ha

*Χορός*

⏑‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ‒

ἰὼ τάλαινα πόλις: ὅμως 1331 2ia

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒

δὲ πρόφερε πόδα σὸν ἐπὶ πλάτας Ἀχαιῶν. 1332 2ia+ba

[^1]: 124-125 (Aeolo-choriambic, but with long penultimate syllable)

[^2]: Lourenço scans this word ⏑⏑‒.

[^3]: Lourenço gives no number to this verse.

[^4]: Lourenço gives no number to this verse and calls the next verse
    190.

[^5]: Lourenço marks only B.

[^6]: Lourenço scans ματέρ᾽ἀ- as ⏑⏑‒.

[^7]: Hecabe in dochmiacs, Talthybios in ia3

[^8]: Identification missing in Lourenço.

[^9]: Lourenço scans this syllable short.

[^10]: Lourenço lists only B.

[^11]: Lourenço adds ||?” here and at 546.

[^12]: Lourenço marks this syllable short.

[^13]: Lourenço scans ‒⏑‒⏑‒ and identifies the colon as hypodochmiac.

[^14]: 1305, 1311, 1320, 1326 are ia3.
