Euripides, *Heracleidai* Lyrics

Prepared by Timothy Moore and Shangwei Deng

Text: *Euripidis Fabulae*, vol. 1, ed. J. Diggle (Oxford: Clarendon
Press: 1984).\
Scansion of lyrics: Frederico Lourenço, *The Lyric Metres of Euripidean
Drama* (Coimbra: Centro de Estudos Clássicos e Humanisticos da
Universidade de Coimbra, 2011).

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-72 **

1-72 ia3 (nonlyric)

**PARODOS: 73-110**

73-74: ia3 (nonlyric)

*Χορός*

ἔα ἔα: τίς ἡ βοὴ βωμοῦ πέλας 73 ia3

ἕστηκε; ποίαν συμφορὰν δείξει τάχα; 74 ia3

*75-110: Epirrhematic Amoibaion; chorus lyrics (lyric, includes
responding stanzas)*; ia3 (nonlyric?) 77, 80=101; (lyric?) 90; Iolaus
ia3 (nonlyric), Herald ia3 (nonlyric)

STROPHE: 75-94

⏑⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

ἴδετε τὸν γέροντ᾽ ἀμαλὸν ἐπὶ πέδῳ 75 2 doc

⏑ ⏑ ⏑ ‒ ⏑ ‒

χύμενον: ὦ τάλας 76 2 doc

&lt; &gt;

‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒

πρὸς τοῦ ποτ᾽ ἐν γῇ πτῶμα δύστηνον πίτνεις; 77 ia3

*Ἰόλαος*

ὅδ᾽ ὦ ξένοι με σοὺς ἀτιμάζων θεοὺς 78 ia3

ἕλκει βιαίως Ζηνὸς ἐκ προβωμίων. 79 ia3

*Χορός*

σὺ δ᾽ ἐκ τίνος γῆς, ὦ γέρον, τετράπτολιν 80 ia3

⏑ ‒ ⏑ ‒ ‒ ‒ ⏑ ‒

ξύνοικον ἦλθες λαόν; ἦ 81 2ia

⏑ ‒ ⏑ ⏑ ⏑‒ ⏑‒

πέραθεν ἁλίῳ πλάτᾳ 82 ia+cr

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ ‒ ‒

κατέχετ᾽ ἐκλιπόντες Εὐβοῖδ᾽ ἀκτάν; 83 2 doc

*Ἰόλαος*

οὐ νησιώτην, ὦ ξένοι, τρίβω βίον, 84 ia3

ἀλλ᾽ ἐκ Μυκηνῶν σὴν ἀφίγμεθα χθόνα. 85 ia3

*Χορός*

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

ὄνομα τί σε, γέρον, Μυκηναῖος ὠ- 86 2doc∫

⏑ ‒ ‒ ⏑ ‒

νόμαζεν λεώς; 87 doc

*Ἰόλαος*

τὸν Ἡράκλειον ἴστε που παραστάτην 88 ia3

Ἰόλαον: οὐ γὰρ σῶμ᾽ ἀκήρυκτον τόδε. 89 ia3

*Χορός*

οἶδ᾽ εἰσακούσας καὶ πρίν: ἀλλὰ τοῦ ποτε 90 ia3||BH

‒ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

ἐν χειρὶ σᾷ κομίζεις κόρους 91 ia+doc

⏑ ⏑ ⏑ ‒ ⏑ ‒

νεοτρεφεῖς; φράσον. 92 doc

*Ἰόλαος*

Ἡρακλέους οἵδ᾽ εἰσὶ παῖδες, ὦ ξένοι, 93 ia3

ἱκέται σέθεν τε καὶ πόλεως ἀφιγμένοι. 94 ia3

ANTISTROPHE: 95-110

*Χορός*

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑⏑⏑ ⏑ ⏑ ⏑ ‒

τί χρέος; ἦ λόγων πόλεος, ἔνεπέ μοι, 95 2doc

⏑ ⏑ ⏑ ‒ ⏑ ‒

μελόμενοι τυχεῖν; 96 doc

*Ἰόλαος*

μήτ᾽ ἐκδοθῆναι μήτε πρὸς βίαν θεῶν 97 ia3

τῶν σῶν ἀποσπασθέντες εἰς Ἄργος μολεῖν. 98 ia3

*Κῆρυξ*

ἀλλ᾽ οὔτι τοῖς σοῖς δεσπόταις τάδ᾽ ἀρκέσει, 99 ia3

οἳ σοῦ κρατοῦντες ἐνθάδ᾽ εὑρίσκουσί σε. 100 ia3

*Χορός*

εἰκὸς θεῶν ἱκτῆρας αἰδεῖσθαι, ξένε, 101 ia3||B

‒ ‒ ⏑‒ ‒ ‒ ⏑ ‒

καὶ μὴ βιαίῳ χειρὶ δαι- 102 2ia∫

⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑‒

μόνων ἀπολιπεῖν σφ᾽ ἕδη: 103 ia+cr

⏑⏑⏑ ‒ ⏑‒ ⏑ ‒ ‒ ⏑ ‒

πότνια γὰρ Δίκα τάδ᾽ οὐ πείσεται. 104 2doc||Ha

*Κῆρυξ*

ἔκπεμπέ νυν γῆς τούσδε τοὺς Εὐρυσθέως, 105 ia3

κοὐδὲν βιαίῳ τῇδε χρήσομαι χερί. 106 ia3

*Χορός*

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

ἄθεον ἱκεσίαν μεθεῖναι πέλει 107 2doc

⏑ ‒ ‒ ⏑ ‒

ξένων προστροπάν. 108 doc

*Κῆρυξ*

καλὸν δέ γ᾽ ἔξω πραγμάτων ἔχειν πόδα, 109 ia3

εὐβουλίας τυχόντα τῆς ἀμείνονος. 110 ia3

Final lines of antistrophe missing, assuming there was responsion.

**FIRST EPISODE: 111-352**

**111-287: ia3**

Diggle assumes lacuna after 149

Diggle assume lacuna of two verses after 217

*288-296: anapests (nonlyric)*

‒ ‒ ˘ ˘ ‒ | ˘ ˘ ‒ ˘ ˘ ‒

ΧΟ. ὥρα προνοεῖν, πρὶν ὅροιϲ πελάϲαι 288 an2

˘ ˘ ‒ ‒ ‒

ϲτρατὸν Ἀργείων· 289 an

˘ ˘ ‒ ˘ ˘ ‒ | ˘ ˘ ‒ ‒ ‒

μάλα δ' ὀξὺϲ Ἄρηϲ ὁ Μυκηναίων, 290 an2

˘ ˘ ‒ ˘ ˘ ‒ | ‒ ˘ ˘ ‒ ‒

ἐπὶ τοῖϲι δὲ δὴ μᾶλλον ἔτ' ἢ πρίν. 291 an2

‒ ˘ ˘ ‒ ‒ | ‒ ‒ ˘ ˘ ‒

πᾶϲι γὰρ οὗτοϲ κήρυξι νόμοϲ, 292 an2

‒ ˘ ˘ ‒ ‒ | ‒ ‒ ˘ ˘ ‒

δὶϲ τόϲα πυργοῦν τῶν γιγνομένων. 293 an2

˘ ˘ ‒ ‒ ‒ | ˘ ˘ ‒ ˘ ˘ ‒

πόϲα νιν λέξειν βαϲιλεῦϲι δοκεῖϲ, 294 an2

‒ ‒ ˘ ˘ ‒ | ‒ ˘ ˘ ‒ ‒

ὡϲ δείνʼ ἔπαθεν καὶ παρὰ μικρὸν 295 an2

‒ ‒ ‒ ‒ | ˘ ˘ ‒ ‒

ψυχὴν ἦλθεν διακναῖϲαι; 296 par

297-352: ia3 (nonlyric)

Diggle brackets 299-301

Diggle assumes lacuna after 311

Diggle brackets 319

**1^ST^ STASIMON: 353-380**

*353-380: lyrics (lyric, includes responding stanzas)*

STROPHE: 353-361

‒ ⏑ ⏑ ‒ ‒ ⏑⏑‒

εἰ σὺ μέγ᾽ αὐχεῖς, ἕτεροι 353 2 chor

‒ ⏑⏑ ‒ ⏑ ‒ ‒

σοῦ πλέον οὐ μέλονται, 354 aristophanean

‒ ⏑ ‒ ⏑ ⏑ ‒ ‒

ξεῖν᾽ &lt;ἀπ᾽&gt; Ἀργόθεν ἐλθών, 355 pherecratean||

⏑ ⏑ ‒ ⏑⏑‒⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

μεγαληγορίαισι δ᾽ ἐμὰς φρένας οὐ φοβήσεις. 356-7 A+ba

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

μήπω ταῖς μεγάλαισιν οὕ- 358 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

τω καὶ καλλιχόροις Ἀθά- 359 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ναις εἴη: σὺ δ᾽ ἄφρων, ὅ τ᾽ Ἄρ- 360 gly∫

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑

γει Σθενέλου τύραννος. 361 aristophanean

ANTISTROPHE: 362-370

‒ ⏑ ⏑ ‒ ‒ ⏑⏑‒

ὃς πόλιν ἐλθὼν ἑτέραν 362 2 chor

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

οὐδὲν ἐλάσσον᾽ Ἄργους, 363 arist

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

θεῶν ἱκτῆρας ἀλάτας 364 ph||

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑‒ ‒

καὶ ἐμᾶς χθονὸς ἀντομένους ξένος ὢν βιαίως 365-6 A+ba

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

ἕλκεις, οὐ βασιλεῦσιν εἴ- 367 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ξας, οὐκ ἄλλο δίκαιον εἰ- 368 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

πών: ποῦ ταῦτα καλῶς ἂν εἴ- 369 gly∫

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑

η παρά γ᾽ εὖ φρονοῦσιν; 370 arist

EPODE: 371-380

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

εἰρήνα μὲν ἐμοί γ᾽ ἀρέ- 371 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑‒

σκει: σοὶ δ᾽, ὦ κακόφρων ἄναξ, 372 gly∫

⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

λέγω, εἰ πόλιν ἥξεις, 373 reizianum||

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

οὐχ οὕτως ἃ δοκεῖς κυρή- 374 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

σεις: οὐ σοὶ μόνῳ ἔγχος οὐδ᾽ 375 gly∫

‒⏑‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑

ἰτέα κατάχαλκος ἐστιν. 376 hipponactean||B

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἀλλ᾽, ὦ πολέμων ἐρα- 377 telesillean∫[^1]

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

στά, μή μοι δορὶ συνταρά- 378 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ξῃς τὰν εὖ χαρίτων ἔχου- 379 gly∫

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

σαν πόλιν, ἀλλ᾽ ἀνάσχου. 380 arist

**SECOND EPISODE: 381-607**

381-607: ia3 (nonlyric)

Diggle places 402 after 409

Diggle brackets 405

Diggle places 562-563 after 559

552: extrametric pheu, Iolaus

**2^ND^ STASIMON: 608-629**

*608-629: lyrics (lyric, includes responding stanzas)*

STROPHE: 608-617

‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑

οὔτινά φημι θεῶν ἄτερ ὄλβιον, οὐ βαρύποτμον, 608 6 da||B

‒ ⏑ ⏑ ‒ ‒

ἄνδρα γενέσθαι: 609 adonean||H

‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ⏑

οὐδὲ τὸν αὐτὸν ἀεὶ 'μβεβάναι δόμον 610 4 da

‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ‒ ‒ ‒

εὐτυχίᾳ: παρὰ δ᾽ ἄλλαν ἄλλα 611 4 da

‒ ⏑ ⏑‒ ‒

μοῖρα διώκει. 612 adon

‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑

τὸν μὲν ἀφ᾽ ὑψηλῶν βραχὺν ᾤκισε, 613 4 da

‒ ⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ‒

τὸν δ᾽ †ἀλήταν† εὐδαίμονα τεύχει. 614 corrupt

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ⏑

μόρσιμα δ᾽ οὔτι φυγεῖν θέμις, οὐ σοφί- 615 4 da∫

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

ᾳ τις ἀπώσεται, ἀλλὰ μάταν ὁ πρό- 616 4 da∫

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

θυμος ἀεὶ πόνον ἕξει. 617 D‒

ANTISTROPHE: 619-628

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἀλλὰ σὺ μὴ προπεσὼν τὰ θεῶν στένε, μηδ᾽ ὑπεράλγει 619[^2] 6 da||

‒ ⏑ ⏑ ‒ ‒

φροντίδα λύπᾳ: 620 adon||H

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

εὐδόκιμον γὰρ ἔχει θανάτου μέρος 621 4 da

‒ ⏑⏑‒ ⏑ ⏑ ‒ ‒ ‒ ‒

ἁ μελέα πρό τ᾽ ἀδελφῶν καὶ γᾶς: 622 4 da

‒ ⏑ ⏑ ‒ ‒

οὐδ᾽ ἀκλεής νιν 623 adon

‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑⏑

δόξα πρὸς ἀνθρώπων ὑποδέξεται: 624 4 da

‒ ⏑ ⏑ ‒ ‒ ‒ ⏑⏑ ‒ ‒

ἁ δ᾽ ἀρετὰ βαίνει διὰ μόχθων. 625 4 da

‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑⏑

ἄξια μὲν πατρός, ἄξια δ᾽ εὐγενί- 626 4 da∫

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

ας τάδε γίγνεται: εἰ δὲ σέβεις θανά- 627 4 da∫

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

τους ἀγαθῶν, μετέχω σοι. 628 D‒

**THIRD EPISODE: 630-747**

630[^3]-701: ia3 (nonlyric)

Diggle arranges as follows: 683, 688, 689, 690, 687, 684, 685, 686, 691

*702-708: anapests (nonlyric)*

‒ ˘ ˘ ‒ ‒ | ‒ ‒ ˘ ˘ ‒

ΧΟ. λῆμα μὲν οὔπω ϲτόρνυϲι χρόνοϲ 702 an2

˘ ˘ ‒ ‒ ‒ | ‒ ˘ ˘ ‒ ‒

τὸ ϲόν, ἀλλ' ἡβᾶι, ϲῶμα δὲ φροῦδον. 703 an2

˘ ˘ ‒ ‒ ‒ | ˘ ˘ ‒ ‒ ‒

τί πονεῖϲ ἄλλωϲ ἃ ϲὲ μὲν βλάψει, 704 an2

‒ ˘ ˘ ‒ ‒ | ˘ ˘ ‒ ˘ ˘ ‒

ϲμικρὰ δ' ὀνήϲει πόλιν ἡμετέραν; 705 an2

‒ ‒ ˘ ˘ ‒ | ‒ ‒ ˘ ˘ ‒

χρὴ γνωϲιμαχεῖν ϲὴν ἡλικίαν, 706 an2

˘ ˘ ‒ ˘ ˘ ‒ | ‒ ‒ ˘ ˘ ‒

τὰ δ' ἀμήχανʼ ἐᾶν· οὐκ ἔϲτιν ὅπωϲ 707 an2

‒ ‒ ‒ ‒ | ˘ ˘ ‒ ‒

ἥβην κτήϲηι πάλιν αὖθιϲ. 708 par

709-747: ia3 (nonlyric)

717a: extrametric pheu, Alcmene

739a: extrametric pheu, Iolaus

**THIRD STASIMON: 748-783**\
*748-783: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 748-758

‒ ‒ ‒ ⏑⏑‒ ⏑ ‒

Γᾶ καὶ παννύχιος σελά- 748 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑‒

να καὶ λαμπρόταται θεοῦ 749 gly

⏑ ⏑‒ ⏑ ⏑ ‒ ‒

φαεσιμβρότου αὐγαί, 750 reizianum||H

‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ‒

ἀγγελίαν μοι ἐνέγκαι, 751 D‒

⏑‒‒ ⏑⏑ ‒ ⏑ ‒

ἰαχήσατε δ᾽ οὐρανῷ 752 gly

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

καὶ παρὰ θρόνον ἀρχέταν 753 gly

‒ ‒ ⏑ ⏑ ‒ ‒

γλαυκᾶς τ᾽ ἐν Ἀθάνας. 754 reizianum

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒

μέλλω τᾶς πατριώτιδος 755 gly

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒

γᾶς, μέλλω καὶ ὑπὲρ δόμων 756 gly

⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἱκέτας ὑποδεχθεὶς 757 reizianum

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒ ⏑ ‒ ‒

κίνδυνον πολιῷ τεμεῖν σιδάρῳ. 758 phalaecian

ANTISTROPHE 1: 759-769

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

δεινὸν μὲν πόλιν ὡς Μυκή- 759 gly∫

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

νας εὐδαίμονα καὶ δορὸς 760 gly

⏑ ⏑‒ ⏑⏑ ‒ ‒

πολυαίνετον ἀλκᾷ 761 reiz

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

μῆνιν ἐμᾷ χθονὶ κεύθειν: 762 D‒

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

κακὸν δ᾽, ὦ πόλις, εἰ ξένους 763 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἱκτῆρας παραδώσομεν 764 gly

⏑ ‒ ⏑ ⏑ ‒ ‒

κελεύσμασιν Ἄργους. 765 reiz

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

Ζεύς μοι σύμμαχος, οὐ φοβοῦ- 766 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

μαι, Ζεύς μοι χάριν ἐνδίκως 767 gly

⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἔχει: οὔποτε θνατῶν 768 reiz

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

ἥσσους &lt;δαίμονες&gt; ἔκ γ᾽ ἐμοῦ φανοῦνται. 769 phal

STROPHE 2: 770-776

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒\
ἀλλ᾽, ὦ πότνια, σὸν γὰρ οὖ- 770 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

δας γᾶς, καὶ πόλις, ἇς σὺ μά- 771 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

τηρ δέσποινά τε καὶ φύλαξ 772 gly∫

⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

πόρευσον ἄλλᾳ τὸν οὐ δικαίως 773 ia+ith||Ha

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒

τᾷδ᾽ ἐπάγοντα δορυσσοῦν 774 D‒

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

στρατὸν Ἀργόθεν: οὐ γὰρ ἐμᾷ γ᾽ ἀρετᾷ 775 A

⏑ ‒ ⏑‒ ‒ ⏑ ‒ ⏑ ‒ ‒

δίκαιός εἰμ᾽ ἐκπεσεῖν μελάθρων. 776 ia+ith

ANTISTROPHE 2: 777-783

⏑ ‒ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

ἐπεί σοι πολύθυτος ἀεὶ 777 gly (resolution)

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

τιμὰ κραίνεται, οὐδὲ λά- 778 gly∫

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

θει μηνῶν φθινὰς ἁμέρα, 779 gly

⏑‒ ⏑‒ ‒ ⏑ ‒ ⏑ ‒ ‒

νέων τ᾽ ἀοιδαὶ χορῶν τε μολπαί. 780 ia+ith||H

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἀνεμόεντι δ᾽ ἐπ᾽ ὄχθῳ 781 D‒

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑⏑‒ ⏑ ⏑ ‒

ὀλολύγματα παννυχίοις ὑπὸ παρ- 782 A∫

⏑ ‒ ⏑‒ ‒ ⏑ ‒ ⏑ ‒ ‒

θένων ἰαχεῖ ποδῶν κρότοισιν. 783 ia+ith

**FOURTH EPISODE: 784-891**

784-891: ia3 (nonlyric)

Diggle assumes a lacuna of two verses after 805

**FOURTH STASIMON: 892-927**

*892-927: lyrics (lyric, includes responding stanzas)*; ia3 (lyric)
892=901

STROPHE 1: 892-900

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἐμοὶ χορὸς μὲν ἡδὺ καὶ λίγεια λω- 892 3ia

‒ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑

τοῦ χάρις ✝ἐνὶ δαί✝ 893 corrupt

‒ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

εἴη δ᾽ &lt;ἂν&gt;[^4] εὔχαρις Ἀφροδί- 894 sp+tel or ‒+gly

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

τα: τερπνὸν δέ τι καὶ φίλων 895 gly

‒ ‒ ⏑⏑‒ ⏑‒ ‒

ἆρ᾽ εὐτυχίαν ἰδέσθαι 896 hagesichorean

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

τῶν πάρος οὐ δοκούντων. 897 arist

‒ ⏑ ‒ ‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

πολλὰ γὰρ τίκτει Μοῖρα τελεσσιδώ- 989-9 cr+gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

τειρ᾽ Αἰών τε Χρόνου παῖς. 900 ph

ANTISTROPHE 1: 901-909

⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἔχεις ὁδόν τιν᾽, ὦ πόλις, δίκαιον: οὐ 901 3ia

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

χρή ποτε τοῦδ᾽ ἀφέσθαι, - 902 arist

‒ ‒ ⏑‒ ⏑ ⏑ ‒ ⏑ ‒

τιμᾶν θεούς: ὁ &lt;δὲ&gt; μή σε φά- 903 sp+tel or ‒+gly

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒

σκων ἐγγὺς μανιῶν ἐλαύ- 904 gly

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

νει, δεικνυμένων ἐλέγχων 905 hagesichorean

‒ ⏑⏑ ‒ ⏑ ‒ ‒

τῶνδ᾽: ἐπίσημα γάρ τοι 906 arist

‒ ⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

θεὸς παραγγέλλει, τῶν ἀδίκων παραι- 907-8 cr+gly∫

‒ ⏑ ‒ ⏑ ⏑ ‒ ‒

ρῶν φρονήματος αἰεί. 909 ph

STROPHE 2: 910-918

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

ἔστιν ἐν οὐρανῷ βεβα- 910 chor+ia∫

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

κὼς ὁ σὸς γόνος, ὦ γεραι- 911 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ά: φεύγω λόγον ὡς τὸν Ἅι- 912 gly∫

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

δα δόμον κατέβα, πυρὸς 913 gly

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

δεινᾷ φλογὶ σῶμα δαισθείς: 917 hag

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

Ἥβας τ᾽ ἐρατὸν χροΐ- 915 tel∫

‒ ⏑ ‒ ⏑ ⏑‒ ⏑ ‒ ‒

ζει λέχος χρυσέαν κατ᾽ αὐλάν. 916 hipponactean||

‒ ⏑ ⏑ ‒⏑ ‒

ὦ Ὑμέναιε, δισ- 917 dodrans∫

‒ ‒ ‒ ⏑⏑ ‒ ⏑‒ ‒

σοὺς παῖδας Διὸς ἠξίωσας. 918 hipponactean

ANTISTROPHE 2: 919-927

‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

συμφέρεται δὲ πολλὰ πολ- 919 chor+ia∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

λοῖς: καὶ γὰρ πατρὶ τῶνδ᾽ Ἀθά- 920 gly∫

‒ ⏑ ‒ ⏑⏑‒ ⏑ ‒

ναν λέγουσ᾽ ἐπίκουρον εἶ- 921 gly∫

‒ ‒ ‒ ⏑ ⏑‒ ⏑‒

ναι, καὶ τούσδε θεᾶς πόλις 922 gly

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

καὶ λαὸς ἔσωσε κείνας: 923 hag

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἔσχεν δ᾽ ὕβριν ἀνδρὸς ᾧ 924 tel

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑‒‒

θυμὸς ἦν πρὸ δίκας βίαιος. 925 hipponactean||

‒ ⏑ ⏑ ‒ ⏑ ‒

μήποτ᾽ ἐμοὶ φρόνη- 926 dodrans∫

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒ ‒

μα ψυχά τ᾽ ἀκόρεστος εἴη. 927 hipponactean

**EXODOS: 928-1055 (=5^th^ Episode)**

928-1052: ia3 (nonlyric)

Diggle places 950-951 after 947

Diggle proposes a lacuna of (at least?) three verses after 952

*1053-1055: anapests (nonlyric)*

**‒ ˘ ˘ ‒ ‒ | ‒ ˘ ˘ ‒ ‒**

ΧΟ. ταῦτα δοκεῖ μοι. ϲτείχετʼ, ὀπαδοί. 1053 an2

˘ ˘ ‒ ‒ ‒

τὰ γὰρ ἐξ ἡμῶν 1054 an

˘ ˘ ‒ ‒ ‒ | ˘ ˘ ‒ ∩

καθαρῶϲ ἔϲται βαϲιλεῦϲιν. 1055 par||B

[^1]: Alternate scansion on Lourenço p. 97.

[^2]: Lourenço has no 618.

[^3]: Lourenço has no 629.

[^4]: Different text in Diggle, same scansion.
