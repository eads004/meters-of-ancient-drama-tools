import sys

PLAYNAME = sys.argv[1]
FOLDER = sys.argv[2]
#currently runs solely on the text.xml of Alcestis in the syllabifier folder. Broaden path for general use.
XML_FILE_TO_PROCESS = 'work_files/templated/' + PLAYNAME + '.xml'
TIM_SCANSION = 'from_tim/' + FOLDER + '/json/Euripides' + PLAYNAME + '.json'
OUTPUT_FILE = 'work_files/scanned_jsons/' + PLAYNAME + '.scanned.json'

###                                PART 1                     ###

import glob, re, subprocess
from lxml import etree

line_text_xslt = etree.XML('''
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
    <xsl:template match="tei:milestone[@unit='word']"><xsl:text> </xsl:text></xsl:template>
    <xsl:template match="tei:seg[@type='line_text']"/>
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
</xsl:stylesheet>''')

line_text_transform = etree.XSLT(line_text_xslt)

print(XML_FILE_TO_PROCESS)

#
# REMOVE LEFT-TO-RIGHT MARKS IN PLAY
#

f = open(XML_FILE_TO_PROCESS, 'r', encoding='utf-8')
data = f.read()
f.close()

f = open('scrubbed.xml', 'w', encoding='utf-8')
f.write(data.replace('\u200e', '').replace('<del>', '[').replace("</del>", "]").replace("<add>", "⟨").replace("</add>", "⟩").replace('<gap reason="lost"/>', "⟨ ⟩").replace("<gap reason='lost'/>", "⟨ ⟩").replace("<sic>", "†").replace("</sic>", "†"))
f.close()

#
# RESUME SCANNING
#

tree = etree.parse('scrubbed.xml')

extracted_data = []

for l in tree.xpath('//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
    div1 = l.xpath('ancestor::tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

    if len(div1) == 0:
        div1 = None
    else:
        div1 = div1[0]

    div2 = l.xpath('ancestor::tei:div2', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

    if len(div2) == 0:
        div2 = None
    else:
        div2 = div2[0]

    # for xref in l.xpath('//tei:del', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        

    line_text = re.sub('\s+', ' ', str(line_text_transform(l)).strip()) # gets the text from the play

    # line_elements = etree.ElementTree(l)
    # print(etree.tostring(line_elements))
    # print(line_elements.xpath('//tei:del',namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}))

    # print(line_elements.xpath('//*'))
    # exit()

    
    # if len(line_elements.xpath('del')) > 0:
    #     print(line_text)
    #     exit()
    
    

    #https://stackoverflow.com/questions/51306306/how-do-i-replace-an-element-in-lxml-with-a-string/51306863#51306863 use this to replace tags with symbols

    div1_type = ''
    div2_type = ''
    div2_ana = ''

    if div1 != None:
        div1_type = div1.get('type')
    
    if div2 != None:
        div2_type = div2.get('type')

        div2_ana='' # unused???
        if div2.get('ana') != None:
            div2_ana = div2.get('ana')
    

    if re.sub('[\<\>\;\. ]', '', line_text).strip() > '':

        extracted_data.append({'type': 'line',
                                'play': PLAYNAME,
                                'div1_type': div1_type,
                                'div2_type': div2_type,
                                'div2_ana': div2_ana,
                                'n': l.get('n'),
                                'line_text': line_text,
                                'syllables': [],
                                '0_ac_scansion': [],
                                '8_final_scansion': [],
                                'comments': []})

#####                           DEBUGGING                   #####
#print()                                                        #
#print('len(extracted_data)', len(extracted_data))              #
#####                                                       #####
        

###                         PART 3                            ###

import json

tim_scansions = {}

f = open(TIM_SCANSION, 'r', encoding='utf-8')

tim_scansions[PLAYNAME] = json.load(f)

f.close()

#####                   DEBUGGING                           #####
#print(len(tim_scansions))                                      #
#print(len(tim_scansions[PLAYNAME]))                            #
#print()                                                        #
#print(tim_scansions[PLAYNAME][0])                              #
#####                                                       #####


        
###                         PART 4                            ###
# moved parts 3 and 4 before part 2 to allow Diggle's text to replace line_text before it becomes syllabified.

import copy
from Greek_Prosody.characters import *

from cltk.alphabet import grc
from Levenshtein import editops

# get the list of iota subscripts which do not require comparison

del grc.MAP_SUBSCRIPT_NO_SUB['Ἄ']

POSSIBLE_PROPER_DIPHTHONGS = ["ᾀ", "ᾁ",
    "ᾂ",
    "ᾃ",
    "ᾄ",
    "ᾅ",
    "ᾈ",
    "ᾉ",
    "ᾊ",
    "ᾋ",
    "ᾌ",
    "ᾍ",
    "ᾲ",
    "ᾳ",
    "ᾴ",
    "ᾼ"]

LOWER_CASE_IOTA = grc.MAP_SUBSCRIPT_NO_SUB.copy()

for k, v in LOWER_CASE_IOTA.items():
    LOWER_CASE_IOTA[k] = v[0] + "ι"

GUARANTEED_SUBSCRIPTS = {v: k for k, v in LOWER_CASE_IOTA.items() if k not in POSSIBLE_PROPER_DIPHTHONGS}
NOT_GUARANTEED_SUBSCRIPTS = {v: k for k, v in LOWER_CASE_IOTA.items() if k in POSSIBLE_PROPER_DIPHTHONGS}

print(GUARANTEED_SUBSCRIPTS)
print(NOT_GUARANTEED_SUBSCRIPTS)

# helper methods for replacing lunate sigmas with regular sigmas; copied from compare_text_diggle.py
def replace_lunate_strings(string):
    return " ".join([replace_lunate_sigmas(word) for word in string.split()])

def replace_lunate_sigmas(word):
    proper_sigma_word = word
    sigma_index = proper_sigma_word.find("ϲ")
    while sigma_index != -1:
        unpunctuated_word = re.sub("[,.·()\[\]〈〉\u200e†;]", "", proper_sigma_word) 
        if unpunctuated_word.find("ϲ") == len(unpunctuated_word)-1:
            proper_sigma_word = proper_sigma_word[:sigma_index] + "ς" + proper_sigma_word[sigma_index+1:]
        else:
            proper_sigma_word = proper_sigma_word[:sigma_index] + "σ" + proper_sigma_word[sigma_index+1:]
        sigma_index = proper_sigma_word.find("ϲ")
    proper_sigma_word = re.sub("Ϲ", "Σ", proper_sigma_word)
    return proper_sigma_word

n_matched = 0
n_corrected = 0

# line_numbers = []

# Print out all line numbers where the Diggle does not match with the 
# for row in extracted_data:
#     line_numbers.append(row['n'])

# last_line = 1
# for tim_line in tim_scansions[PLAYNAME]:
#     if len(tim_line[2]) > 0 and tim_line[2][0] not in line_numbers:
#         print(tim_line[2][0])

last_row = 1
num_after = 0

for tim_line in tim_scansions[PLAYNAME]:

    if len(tim_line[2]) == 0:
        continue

    found_in_rows = False

    # modify the tim_line_text, etc. as much as you can without reference to the Perseus text

    tim_scansion = tim_line[0].strip().split(' ') # get the "S S L..." etc.

    tim_line_text = re.sub("^[Α-Ω]{3,}", "", re.sub("^[Α-Ω][α-ω][.] ", "", replace_lunate_strings(tim_line[1]))) # the Greek text with character changes/abbreviations removed
            # tim_line_text_fixed = ' '.join(tim_line_text.split(' ')[1:]) # unused; probably removes the speaker info
    
    # change known iota adscripts to subscripts
    for k, v in GUARANTEED_SUBSCRIPTS.items():
        tim_line_text = re.sub(k, v, tim_line_text)
    
    tim_line_n = tim_line[2][0] # line number

    tim_line_met = ' '.join(tim_line[2][1:]) # metrical information
    
    has_synapheia = False
    if len(tim_line_met) >= 1 and tim_line_met[-1] == "∫":
        has_synapheia = True
        tim_line_met = tim_line_met[:-1]


    period = ""
    if len(tim_line_met.split('||')) > 1:
        period = "||" + tim_line_met.split('||')[1]
        tim_line_met = tim_line_met.split('||')[0]

    confident_iotas = True

    # compare with Perseus text; replace if matches

    for row_n, row in enumerate(extracted_data):

        if row['type'] == 'milestone':
            continue

        play = row['play']
        div2_type = row['div2_type']
        n = row['n']


        if len(tim_line[2]) > 0 and row['n'] == tim_line[2][0]: # if there's stuff inside the "postline data"

            n_matched += 1
            ops = editops(tim_line_text, extracted_data[row_n]['line_text'])
            if len(ops) != 0: # skip if there are no operations
            # test with Perseus to see if iota is an iota adscript
                # no_substrings = 0 # get the number of possible iota subscripts within line
                # for k, v in NOT_GUARANTEED_SUBSCRIPTS.items():
                #     no_substrings += tim_line_text.count(k)

                for k, v in NOT_GUARANTEED_SUBSCRIPTS.items():
                    replace_list = []
                    for match in re.finditer(k, tim_line_text):
                        index_tim = match.start()

                        total_replaces = 0
                        unmatching_replaces = 0
                        
                        odd_insert = False
                        odd_delete = False
                        for operation, index_a, index_b in ops:
                            # of the 'replace' operations that have indices referring to the potential iota subscript, if any replaces these letters with a letter with an iota subscript, that letter pair is an iota subscript

                            if operation == 'replace' and (index_a == index_tim or index_a == index_tim + 1):
                                total_replaces += 1
                                if extracted_data[row_n]['line_text'][index_b] == v:
                                    replace_list.append(index_tim)
                                else:
                                    unmatching_replaces += 1
                            
                            if operation == 'insert' and index_a == index_tim + 1:
                                odd_insert = True

                            if operation == 'delete' and (index_a == index_tim or index_a == index_tim + 1):
                                odd_delete = True

                        if total_replaces == unmatching_replaces and (total_replaces > 0 or odd_insert or odd_delete): # if there are replaces, but none of them go to an iota subscript, mark the line for human verification
                            confident_iotas = False

                        # print(extracted_data[row_n]['line_text'])
                        # print(tim_line_text, tim_line[2][0])
                        # exit() 
                    
                    shift = 0
                    for i in replace_list:
                        tim_line_text = tim_line_text[:i-shift] + v + tim_line_text[i+2-shift:]
                        shift += 1

            
            
            # if len(tim_scansion) == len(row['8_final_scansion']):
            #     n_corrected += 1
            extracted_data[row_n]['line_text'] = tim_line_text # replace the line text with Diggle's text

            extracted_data[row_n]['2_tim_scansion'] = tim_scansion
            extracted_data[row_n]['8_final_scansion'] = tim_scansion
            
            extracted_data[row_n]['tim_met'] = tim_line_met
            extracted_data[row_n]['has_synapheia'] = has_synapheia
            extracted_data[row_n]['period'] = period

            if not confident_iotas:
                extracted_data[row_n]['comments'].append('check whether diphthong(s) are iota subscripts')

            found_in_rows = True
            last_row = row_n
            num_after = 0
            break
    
    if not found_in_rows:
        # print(tim_line[2][0])

        for k, v in NOT_GUARANTEED_SUBSCRIPTS.items():
            if k in line_text:
                confident_iotas = False
                break

        line_info = {'type': 'line',
                    'play': PLAYNAME,
                    'div1_type': div1_type,
                    'div2_type': div2_type,
                    'div2_ana': div2_ana,
                    'n': tim_line[2][0],
                    'line_text': line_text,
                    'syllables': [],
                    '0_ac_scansion': [],
                    '8_final_scansion': [],
                    'comments': []}
        
        line_info['line_text'] = tim_line_text # replace the line text with Diggle's text

        line_info['2_tim_scansion'] = tim_scansion
        line_info['8_final_scansion'] = tim_scansion
        
        line_info['tim_met'] = tim_line_met
        line_info['has_synapheia'] = has_synapheia
        line_info['period'] = period
        
        if not confident_iotas:
            line_info['comments'].append('check whether diphthong(s) are iota subscripts')

        line_info['comments'].append("this line is according to Diggle: check the arrangement of the line numbers")

        extracted_data.insert(last_row + num_after + 1, line_info)
        num_after += 1

    


#####                       DEBUGGING                       #####
#print()                                                        #
#print('n_matched', n_matched)                                  #
#print('n_corrected', n_corrected)                              #
#####                                                       #####

###                              PART 2                       ###

from Greek_Prosody.prosody import get_prosody
from Greek_Prosody.syllables import syllabify_moore_rules

for row_n, row in enumerate(extracted_data):

#DEBUGGING STATEMENTS
    #print(row)
    #print()

    if 'line_text' in row and len(row['line_text'].strip()) > 0 and row['line_text'].strip() != "⟨ ⟩":

        syllables = None
        try:
            syllables = syllabify_moore_rules(row['line_text'])
        except IndexError:
            pass

        # print(row['line_text'])
        scansion = get_prosody(row['line_text'], use_moore_rules=True)

        extracted_data[row_n]['syllables'] = syllables
        extracted_data[row_n]['0_ac_scansion'] = scansion

        if len(extracted_data[row_n]['8_final_scansion']) == 0:
            extracted_data[row_n]['8_final_scansion'] = scansion
        else:
            n_corrected += 1

    # else:

    #     extracted_data[row_n]['syllables'] = []
    #     extracted_data[row_n]['0_ac_scansion'] = []
    #     extracted_data[row_n]['8_final_scansion'] = []

######                  DEBUGGING                           #####
#print()                                                        #
#print('len(extracted_data)', len(extracted_data))              #
#print()                                                        #
#                                                               #
#for row in extracted_data[:5]:                                 #
#    print()                                                    #
#    print(row)                                                 #
#    print(len(row['syllables']), len(row['0_ac_scansion']))    #
#                                                               #
#####                                                       #####




###                         PART 5                            ###

import joblib, glob

CLF_FOLDER = 'classifiers/'

classifiers = {}
encoders = {}

for p in glob.glob(CLF_FOLDER + '/*.joblib'):
    
    file_name = p.split('/')[-1]
    file_name_parts = file_name.split('.')

    if file_name_parts[0] == 'encoder':
        encoders[(file_name_parts[1], file_name_parts[2])] = joblib.load(p)
    else:
        classifiers[file_name_parts[1]] = joblib.load(p)

#####                       DEBUGGING                       #####
#print()                                                        #
#print(list(classifiers.keys()))                                #
#print()                                                        #
#print(list(encoders.keys()))[:5])                              #
#print()                                                        #
#print(len(classifiers), len(encoders))                         #
#####                                                       #####


###                         PART 6                            ###

import re, traceback
from map_syllables_to_words import *
from Greek_Prosody.characters import *
import pandas as pd
from sklearn import preprocessing
from sklearn import tree

#                                                               #
#                       LOCAL FUNCTION DEFS                     #
#                                                               #

def encode_variable(encoders, vowel, field_name, variable):
    vowels_and_names = [
            ['α','alpha'],
            ['ε','epsilon'],
            ['η','eta'],
            ['ι','iota'],
            ['ο','omnichron'], #misspelled omicron, but this aligns with the classifiers
            ['υ','upsilon'],
            ['ω','omega'],
            ['αι','alpha_iota'],
            ['ει','epsilon_iota'],
            ['οι','omnichron_iota'],
            ['υι','upsilon_iota'],
            ['αυ','alpha_upsilon'],
            ['ωυ','omega_upsilon'],
            ['ου','omnichron_upsilon'],
            ['ηυ','eta_upsilon']
            ]

    vowel_name=''
    for v in vowels_and_names:
        if v[0] == vowel:
            vowel_name = v[1]
            break
   
    #print(len(encoders), vowel_name, field_name)


    encoder = encoders[(vowel_name, field_name)]

    encoded_variable = None

    try:
        encoded_variable = encoder.transform([variable])[0]
    except ValueError:
        print('encoder error', vowel, field_name, variable)
        pass

    return encoded_variable

def load_length_encoder(encoders, vowel):

    vowels_and_names = [
            ['α','alpha'],
            ['ε','epsilon'],
            ['η','eta'],
            ['ι','iota'],
            ['ο','omnichron'], #misspelled omicron, but this aligns with the classifiers
            ['υ','upsilon'],
            ['ω','omega'],
            ['αι','alpha_iota'],
            ['ει','epsilon_iota'],
            ['οι','omnichron_iota'],
            ['υι','upsilon_iota'],
            ['αυ','alpha_upsilon'],
            ['ωυ','omega_upsilon'],
            ['ου','omnichron_upsilon'],
            ['ηυ','eta_upsilon']
            ]

    vowel_name = ''
    for v in vowels_and_names:
        if v[0] == vowel:
            vowel_name = v[1]
            break

    encoder = encoders[(vowel_name, 'length')]

    return encoder

def load_classifier(classifiers, vowel):

    
    vowels_and_names = [
            ['α','alpha'],
            ['ε','epsilon'],
            ['η','eta'],
            ['ι','iota'],
            ['ο','omnichron'], #misspelled omicron, but this aligns with the classifiers
            ['υ','upsilon'],
            ['ω','omega'],
            ['αι','alpha_iota'],
            ['ει','epsilon_iota'],
            ['οι','omnichron_iota'],
            ['υι','upsilon_iota'],
            ['αυ','alpha_upsilon'],
            ['ωυ','omega_upsilon'],
            ['ου','omnichron_upsilon'],
            ['ηυ','eta_upsilon']
            ]

    vowel_name = ''
    for v in vowels_and_names:
        if v[0] == vowel:
            vowel_name = v[1]
            break

    classifier = classifiers[vowel_name]

    return classifier

#####                   END OF FUNCTION DEFS                #####

n_lines_tried = 0
n_all_syllable_word_structures_0 = 0

total_n_syllables = 0
n_tried = 0
n_nonvowel_vowel = 0
n_encoder_error = 0
n_classifier_length_0 = 0

for row_n, row in enumerate(extracted_data):
    if row['type'] == 'milestone':
        continue

    if 'X' in row['8_final_scansion']:
        n_lines_tried += 1
        row_data = {'8_final_scansion':row['8_final_scansion'],
                'line_text': row['line_text'],
                'syllables': ' '.join(row['syllables'])}

        all_syllable_word_structures, error = get_all_syllable_word_structures(row_data)

        classifier_scansion = []

        encoder_error = False

        if len(all_syllable_word_structures) == 0:

            #print()
            #print('len(all_syllable_word_structures) == 0', error, row_data)
            n_all_syllable_word_structures_0 += 1

            continue

        encoder_error = False

        for s in all_syllable_word_structures:

            total_n_syllables += 1

            if s['length'] != 'X':
                classifier_scansion.append(s['length'])
            elif s['basic_vowel'] not in VOWELS:
                classifier_scansion.append(s['length'])
                n_nonvowel_vowel += 1
            else:
                n_tried += 1
                fields_to_select = ['basic_syllable', 'syllable',
                                    'vowel', 'basic_vowel',
                                    'consonant_before', 'consonant_after',
                                    'has_circumflex', 'is_capitalized',
                                    'is_last_syllable_of_word', 'is_last_syllable_of_line']

                variables = []

                for f in fields_to_select:

                    #print('encoding', s['basic_vowel'], 's', s, 'f', f)
                    #print('s[f]', s[f])

                    variables.append(encode_variable(encoders,
                                                    s['basic_vowel'],
                                                    f,
                                                    s[f]))


                this_one_bad = False

                for v in variables:
                    if v == None:
                        encoder_error = True
                        this_one_bad = True
                if this_one_bad:
                    n_encoder_error += 1

                if encoder_error == False:

                    length_encoder = load_length_encoder(encoders, s['basic_vowel'])

                    clf = load_classifier(classifiers, s['basic_vowel'])

                    classifier_results = clf.predict([variables])

                    classifier_scansion.append(length_encoder.classes_[classifier_results[0]])
                else:
                    break
        if encoder_error == False:
            if len(classifier_scansion) == 0:
                n_classifier_length_0 += 1
            else:
                extracted_data[row_n]['3_classifier_scansion'] = classifier_scansion
                extracted_data[row_n]['8_final_scansion'] = classifier_scansion

#####                               DEBUGGING               #####
#                                                               #
#print()                                                        #
#print('n_lines_tried', n_lines_tried)                          #
#print('n_all_syllable_word_structures_0', n_all_syllable_word_structures_0)
#print('total_n_syllables', total_n_syllables)                  #
#print('n_tried', n_tried)                                      #
#print('n_nonvowel_vowel', n_nonvowel_vowel)                    #
#print('n_encoder_error', n_encoder_error)                      #
#print('n_classifier_length_0, n_classifier_length_0)           #
#                                                               #
#####                                                       #####

### PART 6.5: check for differing syllable lengths and add a comment ###

for row_n, row in enumerate(extracted_data):
    if '2_tim_scansion' in row and len(extracted_data[row_n]['syllables']) != len(extracted_data[row_n]['2_tim_scansion']):
        extracted_data[row_n]['8_final_scansion'] = extracted_data[row_n]['0_ac_scansion']
        extracted_data[row_n]['comments'].append("Moore's scansion is " + " ".join(extracted_data[row_n]['2_tim_scansion']).replace('S', '⏑').replace('L', '‒').replace('N', '∩') + " : the number of parsed syllables in the XML is different from the number of syllables that Moore indicated")
        
###                                 PART 7                    ###

import json

f = open(OUTPUT_FILE, 'w', encoding='utf-8')

json.dump(extracted_data, f, indent=4, ensure_ascii=False)

f.close()

print(len(extracted_data))

total_syllables = 0
n_with_x = 0

for row in extracted_data:
    if row['type'] == 'line':
        if '8_final_scansion' in row:

            total_syllables += len(row['8_final_scansion'])

            if 'X' in row['8_final_scansion']:
                n_with_x += 1

print('total_syllables', total_syllables)
print('n_with_x', n_with_x)

