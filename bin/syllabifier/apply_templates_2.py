from docx import Document
import sys
import re
import copy
from lxml import etree

PLAYNAME = sys.argv[1]

DOC_TO_PROCESS = "lyric_templates/Euripides/" + PLAYNAME + ".docx"
XML_TO_PROCESS = "flattened_xml/" + PLAYNAME + "_Greek.xml"

doc = Document(DOC_TO_PROCESS)
tree = etree.parse(XML_TO_PROCESS)

def build_line_number_map(tree):
    line_number_map = {}
    l_elements = tree.xpath('//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
    for l_element in l_elements:
        line_number = l_element.get('n')
        if line_number is not None:
            line_number_map[line_number] = l_element
    return line_number_map

def find_closest_match(unit_ptr, line_number_map):
    if unit_ptr in line_number_map:
        return line_number_map[unit_ptr]
    closest_match = min((key for key in line_number_map if key >= unit_ptr), default=None)
    return line_number_map.get(closest_match, None)

def extract_formatted_runs(doc):
    bolded_text = []
    underlined_text = []

    for paragraph in doc.paragraphs:
        curr_bold_text = []
        curr_under_text = []

        for run in paragraph.runs:
            if run.bold:
                curr_bold_text.append(run.text)
            else:
                if curr_bold_text:
                    bolded_text.append(''.join(curr_bold_text))
                    curr_bold_text = []

            if run.underline:
                curr_under_text.append(run.text)
            else:
                if curr_under_text:
                    underlined_text.append(''.join(curr_under_text))
                    curr_under_text = []

        if curr_bold_text:
            bolded_text.append(''.join(curr_bold_text))
        if curr_under_text:
            underlined_text.append(''.join(curr_under_text))
    return bolded_text, underlined_text

def parse_docx(DOCUMENT):
    doc = Document(DOCUMENT)
    Bs, Us = extract_formatted_runs(doc)
    return Bs, Us

def ends_with_number(s):
    return bool(re.search(r'\d$', s))

def read_xml(xml_file):
    tree = etree.ElementTree.parse(xml_file)
    root = tree.getroot()
    return tree, root

def find_parent_div1(tree, start_element):
    parent_div1 = None
    for div1 in tree.xpath('//tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        if start_element in div1.xpath('.//tei:l | .//tei:milestone', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
            parent_div1 = div1
            break
    return parent_div1

def process_divs(tree, line_number_map, divs, div_type):
    for div in divs:
        unit_name, unit_resp, unit_begin, unit_end, unit_met, unit_type = "", "", "", "", "", ""

        if any(char.isdigit() for char in div):
            div_parts = div.split(':')

            if len(div_parts) == 2 and sum(c.isalpha() for c in div_parts[0]) > 1:
                if div_type == "div1":
                    unit_name = div_parts[0]
                else:
                    name_and_resp = div_parts[0].split()
                    unit_name = name_and_resp[0]

                    unit_resp = name_and_resp[1] if len(name_and_resp) > 1 else \
                            unit_name[-1] if ends_with_number(unit_name) else \
                            "1" if "STROPHE" in unit_name else "N/A"
                
                unit_begin, unit_end = map(str.strip, div_parts[1].strip().split('-'))
                unit_end = unit_end.strip().split('(')[0]
                
                if div_type == "div2":
                    unit_type = "lyric"
                else:
                    unit_type = "N/A"
            else:
                unit_type = 'nonlyric' if 'nonlyric' in div_parts[1] else 'lyric'
                unit_begin, unit_end = map(str.strip, div_parts[0].strip().split('-'))
                unit_end = unit_end.strip().split('(')[0]

                unit_name = "N/A"
                unit_met = "N/A"

            nodes_to_move = []
            state = 'before-section'
            starting_element = find_closest_match(unit_begin, line_number_map) #e.g. l 1
            ending_element = find_closest_match(unit_end, line_number_map) #e.g. l 145
            print(f"starting element line number: {starting_element.attrib['n']}")
            print(f"ending element line number: {ending_element.attrib['n']}")

            if starting_element is None or ending_element is None:
                print(f"Skipping div as starting_element or ending_element not found: {div}")
                continue

            for l in tree.xpath('//tei:l | //tei:milestone', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if l.text == starting_element.text:
                    state = 'to-copy'
                if state == 'to-copy':
                    nodes_to_move.append(l)
                if l.text == ending_element.text:
                    state = 'after-section'

            if div_type == "div1":
                div_element = etree.Element(div_type, name=unit_name)
                for node in nodes_to_move:
                    n = copy.deepcopy(node)
                    div_element.append(n)
                tree.getroot().append(div_element)
                for node in nodes_to_move:
                    if node.getparent() is not None:
                        node.getparent().remove(node)

            elif div_type == "div2":
                if unit_name != "N/A":
                    div2_element = etree.Element(div_type, name=unit_name, type=unit_type, n=unit_resp)
                else:
                    div2_element = etree.Element(div_type, type=unit_type, met=unit_met)
                for node in nodes_to_move:
                    n = copy.deepcopy(node)
                    div2_element.append(n)
                
                #all_div1s = tree.xpath('//tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
                #print(f"all_div1s: {len(all_div1s)}")
                x = 1
                parent_div1 = None
                for div1 in tree.xpath('//tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                    while div1.text != ending_element.text:
                        print(f"I've entered this loop {x} times!")
                        print(f"First line of section: {div1[0].text}")
                        x += 1
                        if x == 8:
                            break
                        div2_lines = div1.xpath('.//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}) # At the parodos, we suddenly think the div1 is 1000+ lines long. Please fix.
                   
                        print(f"div1 I'm currently looking at: {div1.get('name')}")
                        print(f"number of lines in the div1: {len(div2_lines)}")
                        #print()

                        for line in div2_lines:
                            #print(f"I'm comparing these lines and should append them if they match!")
                            #print(f"looking at this line: {line.text}")
                            #print(f"comparing to this line: {starting_element.text}")
                            if line.text == starting_element.text:
                                parent_div1 = div1
                            if parent_div1 is not None:
                                #print(f"LOOK AT ME LOOK AT ME LOOK AT ME: {parent_div1}")
                                parent_div1.append(div2_element)
                            for node in nodes_to_move:
                                if node.getparent() is not None:
                                    node.getparent().remove(node)
                                else:
                                    print("tree.getroot().remove(node)")
                                    tree.getroot().remove(node)             # In Andromache at the second div2 of the first div1, it breaks. reason: "Element is not a child of this node"
                                    
                            break
                        #DEBUG
                        if parent_div1 is None:
                            print(f"Error: No parent Div1 found for div2 starting at {unit_begin}")
                            print()
                        else:
                            print(f"Processed div2: {div}")
                            print(f"Parent div1 found: {parent_div1 is not None}")
                            print(f"Parent div1 lines: {len(parent_div1.xpath('.//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}))}")
                    print('------------------------------------------')

# Main logic
line_number_map = build_line_number_map(tree)
div1s, div2s = parse_docx(DOC_TO_PROCESS)

#print(f"DIV1s: {div1s}")
#print()
#print(f"DIV2s: {div2s}")

TMP_XML = 'work_files/temp.xml'
tree.write(TMP_XML, pretty_print=True, encoding='utf-8')


# Processing div1s
tree = etree.parse(TMP_XML)
line_number_map = build_line_number_map(tree)
process_divs(tree, line_number_map, div1s, "div1")

tree.write(TMP_XML, pretty_print=True, encoding='utf-8')

#print("After processing div1s:")
#print(etree.tostring(tree, pretty_print=True, encoding='utf-8').decode('utf-8'))

#DEBUG to confirm that the div1s are being added correctly from the terminal


# Processing div2s

#Rewriting the tree and trying to do the div2s breaks everything.
#if we do not rewrite the tree, however, then the div2s never get added.

tree = etree.parse(TMP_XML)
line_number_map = build_line_number_map(tree)


process_divs(tree, line_number_map, div2s, "div2")

OUTPUT_FILE = 'work_files/templated/' + PLAYNAME + '.xml'
tree.write(OUTPUT_FILE, pretty_print=True, encoding='utf-8')

#DEBUG to confirm that the div2s are being added correctly from the terminal
#print("After processing div2s:")
#print(etree.tostring(tree, pretty_print=True, encoding='utf-8').decode('utf-8'))"""
print("ok")