import sys, pathlib
from pathlib import Path
import os

PLAYNAME = sys.argv[1]

JSON_FILE_TO_PROCESS = 'work_files/scanned_jsons/' + PLAYNAME + '.scanned.json'
XML_FILE_TO_PROCESS = 'work_files/templated/' + PLAYNAME + '.xml'

# TODO: update XML header to use the correct xml-model tag

import json

lines_to_update = json.load(open(JSON_FILE_TO_PROCESS, 'r', encoding='utf-8'))

from lxml import etree
import subprocess, re

tree = etree.parse(XML_FILE_TO_PROCESS)

prev_line = ""

for line in lines_to_update:

    xml_lines = tree.xpath('//tei:l[@n="' + line['n'] + '"]', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

    scansion_to_use = '0_ac_scansion'
    scansion_backup = '8_final_scansion'
    if '2_tim_scansion' in line:
        scansion_to_use = '8_final_scansion'
        scansion_backup = '0_ac_scansion'
    if '9_hand_corrected' in line:
        scansion_to_use = '9_hand_corrected'

    if len(line['syllables']) == len(line[scansion_to_use]):


        # xml_line = xml_lines[0]

        line_text = line['line_text'].strip()

        check_line_text = re.sub('\s', '', line_text)
        check_syllable_text = ''.join(line['syllables'])

        if len(check_line_text) == len(check_syllable_text):
            syllable_word_markers = ['',]
            syllable_item_index = 0
            syllable_string_index = -1

            for c in line_text:
                if c == ' ':
                    syllable_word_markers[-1] += '<milestone unit="word"/>'
                else:
                    syllable_string_index += 1

                    if syllable_string_index > len(line['syllables'][syllable_item_index]) - 1:
                        syllable_word_markers.append('')
                        syllable_item_index += 1
                        syllable_string_index = 0

                    syllable_word_markers[-1] += \
                            line['syllables'][syllable_item_index][syllable_string_index] \
                            .replace('<', '&lt;').replace('>', '&gt;')
                    

                    
            if not("has_synapheia" in line and line['has_synapheia']):
                syllable_word_markers[-1] += '<milestone unit="word"/>'
                
            syllable_xml = []



            if len(line[scansion_to_use]) > 0:
                for a in range(0, len(syllable_word_markers)):
                    if line[scansion_to_use][a] == 'L':
                        ana = 'long'

                    elif line[scansion_to_use][a] == 'S':
                        ana = 'short'
                    
                    elif line[scansion_to_use][a] == 'N':
                        ana = 'shortn'

                    else:
                        ana = "[???]"

                        # marks unsure scansions as 'long [???]' or 'short [???]' to aid further human verification
                        if line[scansion_backup][a] == 'L':
                            ana = 'long ' + ana

                        elif line[scansion_backup][a] == 'S':
                            ana = 'short ' + ana
                    
                    syllable_xml.append('<seg type="syll" ana="' + ana + '">' + \
                            syllable_word_markers[a] + \
                            '</seg>')

#--------------------------------------------------------------------
#                           Inject Metra
#--------------------------------------------------------------------

            metron_injection_okay = True
            if len(xml_lines) > 0:

                metrons = xml_lines[0].xpath('descendant::tei:seg[@type="metron"]', namespaces={'tei': 'http://www.tei-c-org/ns/1.0'})
                
            else: 
                metrons = []

            if len(metrons) > 0:
                metron_syllable_numbers = []
                metron_total_syllables = 0
                last_syllable_number = 0

                for m in metrons:
                    met = m.get('met')
                    n_syllables = len(m.xpath('descendant::tei:seg[@type="syll"]', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}))

                    metron_syllable_numbers.append([met,
                                                    last_syllable_number,
                                                    (last_syllable_number + n_syllables - 1)
                        ])
                    last_syllable_number += n_syllables
                    metron_total_syllables += n_syllables
                
                if metron_total_syllables == len(line[scansion_to_use]):

                    for m in metron_syllable_numbers:
                        syllable_xml[m[1]] = '<seg met="' + m[0] + '" type="metron">' + syllable_xml[m[1]]
                        syllable_xml[m[2]] = syllable_xml[m[2]] + '</seg>'
                else:

                    metron_injection_okay = False

                    print('ERROR', line['play'], line['n'], "metron_total_syllables", metron_total_syllables, "len(line['" + scansion_to_use + "'])", len(line[scansion_to_use]))

#----------------------------------------------------------------
#               REPLACE THE LINE
#----------------------------------------------------------------

            if metron_injection_okay:

                # adds periods, synapheia, and meter as necessary
                met_string = ""
                synapheia_string = ""
                period_string = ""
                comments_string = ""

                if "tim_met" in line:
                    met_string = ' met="' + line["tim_met"] + '"'

                    if line["has_synapheia"]:
                        synapheia_string = '<milestone unit="synapheia"/>'
                    
                    if "||" in line["period"]:
                        ana_string = line["period"][2:]
                        if ana_string != "":
                            ana_string = ' ana="' + ana_string + '"'
                        period_string = '<milestone unit="period"' + ana_string + '/>'

                    if len(line['comments']) > 0:
                        comments_string = "<!-- [???] " + "; ".join(line['comments']) + " -->"
                    
                    



                
                syllable_xml = '<l xmlns="http://www.tei-c.org/ns/1.0" n="' + \
                                line['n'] + \
                                '"' + \
                                met_string + \
                                ' line_text="' + \
                                line_text.replace('<', '&lt;').replace('>', '&gt;') + \
                                '">' + \
                                ''.join(syllable_xml) + \
                                synapheia_string + \
                                period_string + \
                                comments_string + \
                                '</l>\n'
                
                syllable_xml = syllable_xml.replace('⟨<milestone unit="word"/>⟩<milestone unit="word"/>', '<gap extent="5"/>')

                try:
                    new_line = etree.XML(syllable_xml)

                    # if (line['n'] == "999"):
                    #     print(etree.tostring(prev_line))
                    #     print(etree.tostring(new_line))
                    #     print("999")
                    #     exit()

                    if len(xml_lines) > 0:
                        xml_lines[0].getparent().replace(xml_lines[0], new_line)

                    else:
                        prev_line.addnext(new_line)

                    prev_line = new_line

                except etree.XMLSyntaxError as e:
                    print()
                    print(e)
                    print('XMLSyntaxError', syllable_xml)
                    print('syllable_word_markers', syllable_word_markers)

            else:
                print('ERROR', line['play'], line['n'],
                        "len(check_line_text)" , len(check_line_text),
                        "len(check_syllable_text)", len(check_syllable_text))

        else:
            print('ERROR', line['play'], line['n'],
                    "len(line['syllables'])", len(line['syllables']),
                    "len(line['" + scansion_to_use + "'])" , len(line[scansion_to_use]))

    else:
        print('ERROR', line['play'], line['n'], 'len(xml_lines)', len(xml_lines))


        
#----------------------------------------------------------------
#               WRITE OUT THE XML FOR HUMAN READERS
#----------------------------------------------------------------

print("Writing Tree to temp...")
tree.write('work_files/temp.xml', encoding='utf-8', xml_declaration=True)

print("Tidying into temp2...")
subprocess.getoutput('tidy -xml -i -wrap work_files/temp.xml > work_files/temp2.xml')

f = open('work_files/temp2.xml', 'r', encoding='utf-8')
xml = f.read()
f.close()

print("Cleaning up milestones...")
xml = re.sub('\n\s+<milestone unit="word" />', '<milestone unit="word" />', xml, flags=re.MULTILINE)
xml = re.sub(' />', '/>', xml)

print("Writing to " + PLAYNAME +".xml")

if not os.path.exists("../../texts/TO_BE_CHECKED"):
    os.makedirs("../../texts/TO_BE_CHECKED")

output_path = Path.cwd().parent.parent
print("output path = " + str(output_path))
f = open(str(output_path) + '/texts/TO_BE_CHECKED/' + PLAYNAME + '.xml', 'w', encoding='utf-8')
f.write(xml)
f.close()

print()
print('all done')
