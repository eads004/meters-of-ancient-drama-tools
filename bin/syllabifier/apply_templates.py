﻿from docx import Document
import sys
import re
import copy
from lxml import etree

PLAYNAME = sys.argv[1]

DOC_TO_PROCESS = "lyric_templates/Euripides/" + PLAYNAME + ".docx"
XML_TO_PROCESS = "flattened_xml/" + PLAYNAME + "_Greek.xml"

doc = Document(DOC_TO_PROCESS)

tree = etree.parse(XML_TO_PROCESS)

# I'm not exactly sure why I wrote this. It is very necessary with how the program works though.
# Do not edit or remove it unless you really know what you're doing.
def build_line_number_map(tree):
    line_number_map = {}
    l_elements = tree.xpath('//tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
    for l_element in l_elements:
        line_number = l_element.get('n')
        if line_number is not None:
            line_number_map[line_number] = l_element
    return line_number_map

# This function should try to find the XML <l> tag that has the same n attribute of unit_ptr, if it cannot, it will recursively search backwards and return the nearest <l> tag.
# E.G. if one searches for 1225a and there is no such line, one should get 1225.
#       if there is no line 1225, one should get 1224, and so on until an <l> tag is found.
def find_closest_match(unit_ptr, line_number_map):
    if unit_ptr in line_number_map:
        return line_number_map[unit_ptr]
    closest_match = min((key for key in line_number_map if key >= unit_ptr), default=None)
    return line_number_map.get(closest_match, None)

# This function takes our docx lines and extracts ONLY the relevantly formatted information.
# It goes line by line (run by run) and appends text either to bolded_text or underlined_text
# if the run matches the desired formatting. If a line should be partially formatted,
# only the formatted characters are added for processing.
def extract_formatted_runs(doc):
    bolded_text = []
    underlined_text = []

    for paragraph in doc.paragraphs:
        curr_bold_text = []
        curr_under_text = []

        for run in paragraph.runs:
            if run.bold:
                curr_bold_text.append(run.text)
            else:
                if curr_bold_text:
                    bolded_text.append(''.join(curr_bold_text))
                    curr_bold_text = []

            if run.underline:
                curr_under_text.append(run.text)
            else:
                if curr_under_text:
                    underlined_text.append(''.join(curr_under_text))
                    curr_under_text = []

        if curr_bold_text:
            bolded_text.append(''.join(curr_bold_text))
        if curr_under_text:
            underlined_text.append(''.join(curr_under_text))
    return bolded_text, underlined_text

# I made this its own function for scalability. 
# It's a little redundant on its own in this script.
def parse_docx(DOCUMENT):
    doc = Document(DOCUMENT)
    Bs, Us = extract_formatted_runs(doc)

    return Bs, Us

def ends_with_number(s):
    return bool(re.search(r'\d$', s))


#KNOWN ISSUE: The divs are not populating the tree correctly. All debug statements suggest that we are correctly parsing the divs at least. 

#This singular function does the work of what was largely redundant in an older version of the script. Rather than individually parse div1s and div2s, now they can share a lot
#of the same engine. This function reads in the tree, our active line map, the list of divs we're going to process, and whether we are going to be processing div1s or div2s

#The format of the variables is as follows: (etree, dict, list, string)
#There should not be any direct output, as we are directly rewriting the pointers in the tree.
def process_divs(tree, line_number_map, divs, div_type):
    for div in divs:
        # We set default empty strings for our debug statements' benefit. If we ever mess with a variable but do not set a value, we use "N/A"
        unit_name, unit_resp, unit_begin, unit_end, unit_met, unit_type = "", "", "", "", "", ""

        # This first if-statement weeds out any malformed metra. Some lines in the templates bold longs and shorts, e.g., and we don't want those lines.
        # It's much easier to do this single conditional than weed out all the strange formatting artifacts.
        if any(char.isdigit() for char in div):
            div_parts = div.split(':')

            # The following if-statement is meant to capture lines that use text before the colon.
            # It should capture all div1s and some of the div2s.
            # E.G.  PROLOGOS: 1-40
            #       STROPHE 1: 222-240
            #       ANTISTROPHE2: 143-148

            # It will not capture div2s which begin with a numbered line
            # E.G. 1225a-1270: ia3 (nonlyric)

            if len(div_parts) == 2 and sum(c.isalpha() for c in div_parts[0]) > 1:
                if div_type == "div1":
                    unit_name = div_parts[0]
                else:
                    name_and_resp = div_parts[0].split()
                    unit_name = name_and_resp[0]

                    unit_resp = name_and_resp[1] if len(name_and_resp) > 1 else \
                            unit_name[-1] if ends_with_number(unit_name) else \
                            "1" if "STROPHE" in unit_name else "N/A"
                
                unit_begin, unit_end = map(str.strip, div_parts[1].strip().split('-'))
                unit_end = unit_end.strip().split('(')[0]
                
                if div_type == "div2":
                    unit_type = "lyric"
                else:
                    unit_type = "N/A"
            else:
                unit_type = 'nonlyric' if 'nonlyric' in div_parts[1] else 'lyric'
                unit_begin, unit_end = map(str.strip, div_parts[0].strip().split('-'))
                unit_end = unit_end.strip().split('(')[0]

                unit_name = "N/A"
                unit_met = "N/A"

            nodes_to_move = []
            state = 'before-section'
            starting_element = find_closest_match(unit_begin, line_number_map)
            ending_element = find_closest_match(unit_end, line_number_map)

            if starting_element is None or ending_element is None:
                print(f"Skipping div as starting_element or ending_element not found: {div}")
                continue

            for l in tree.xpath('//tei:l | //tei:milestone', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if l.text == starting_element.text:
                    state = 'to-copy'
                if state == 'to-copy':
                    nodes_to_move.append(l)
                if l.text == ending_element.text:
                    state = 'after-section'

            ###          ###
            #   DEBUGGING  #
            ###          ###

            print(f"Processing div: {div}")
            print(f"Unit begin: {unit_begin}, Unit end: {unit_end}")
            print(f"Unit name: {unit_name}, Unit type: {unit_type}")
            print(f"Unit met: {unit_met}, Unit resp: {unit_resp}")
            print()

            if div_type == "div1":
                div_element = etree.Element(div_type, name=unit_name)

                for node in nodes_to_move:
                    n = copy.deepcopy(node)
                    div_element.append(n)

                tree.getroot().append(div_element)

                for node in nodes_to_move:
                    if node.getparent() is not None:
                        node.getparent().remove(node)

            elif div_type == "div2":
                if unit_name != "N/A":
                    div2_element = etree.Element(div_type, name=unit_name, type=unit_type, n=unit_resp)
                else:
                    div2_element = etree.Element(div_type, type=unit_type, met=unit_met)

                for node in nodes_to_move:
                    n = copy.deepcopy(node)
                    div2_element.append(n)

                parent_element = starting_element.getparent()
                if parent_element is not None:
                    parent_element.append(div2_element)
                    for node in nodes_to_move:
                        if node.getparent() is not None:
                            node.getparent().remove(node)
                        else:
                            tree.getroot().remove(node)
            else:
                print(f"Error: starting_element {starting_element} has no parent")

#main, but not in a function because global variables...
line_number_map = build_line_number_map(tree)
div1s, div2s = parse_docx(DOC_TO_PROCESS)

#DEBUG CHECK
print(f"DIV1s: {div1s}")
print()
print(f"DIV2s: {div2s}")
    
TMP_XML = 'work_files/temp.xml'
tree.write(TMP_XML, pretty_print=True, encoding='utf-8')

tree = etree.parse(TMP_XML)
line_number_map = build_line_number_map(tree)
    
process_divs(tree, line_number_map, div1s, "div1")
tree.write(TMP_XML, pretty_print=True, encoding='utf-8')

#We rewrite the tree with the div1s in an attempt to find them as the parents of div2s
tree = etree.parse(TMP_XML)
line_number_map = build_line_number_map(tree)

process_divs(tree, line_number_map, div2s, "div2")

OUTPUT_FILE = 'work_files/templated/' + PLAYNAME + '.xml'
tree.write(OUTPUT_FILE, pretty_print=True, encoding='utf-8')
print("ok")
