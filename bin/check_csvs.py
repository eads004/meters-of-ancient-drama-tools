#!/usr/bin/env python

import glob, subprocess
from lxml import etree
import pandas as pd
from collections import Counter
  
XML_FOLDER = '../temp/'
CSV_FOLDER = '../dist/stats_crossfilter/data/'

FIELDS = [['div1_type', 2],
            ['lyric_non_lyric', 4],
            ['speaker', 6],
            ['gender', 7],
            ['status', 8],
            ['isGod', 9],
            ['isGreek', 10],
            ['line_meter_name', 13],
            ['line_meter_type', 14]]
            
cmd = 'rm ../temp/cleaned*'
subprocess.getoutput(cmd)
            
for p in glob.glob(XML_FOLDER + '*.xml'):
    play = p.split('/')[-1].split('.')[0]
    cmd = "cat " + p + " | sed -E 's/>/>\\n/g' > ../temp/cleaned_" + \
                            play + ".xml"
    subprocess.getoutput(cmd) 
    
# ----------------------------------------------------------------------
# 
# ----------------------------------------------------------------------

def make_qa_report(LABEL, path_to_csv, grep_patterns, DEBUG=False):

    report = {}

    for f in FIELDS:

        report[f[0]] = {}
        
        cmd = 'csvcut -c ' + str(f[1]) + ' ' + path_to_csv + ' | sort | uniq -c'
        
        if DEBUG:
            print()
            print(f[0], cmd)
            print()

        results = subprocess.getoutput(cmd)
        
        for line in results.split('\n'):
            if line.strip() > '':
                cols = line.strip().split(' ')
                n = int(cols[0].strip())
                v = cols[1]
                report[f[0]][v] = {'csv': n, 'xml': 0}

    mother_of_grep = "grep '" + grep_patterns[0] + "' ../temp/cleaned_*.xml"
    if len(grep_patterns) > 1:
        for g in grep_patterns[1:]:
            mother_of_grep = mother_of_grep + " | grep '" + g + "'"

    for f in FIELDS:
        
        cmd = mother_of_grep + " | " + \
                "sed -E 's/.+" + f[0] + "=\"//g' | " + \
                "sed -E 's/\".+//g' | " + \
                "sort | uniq -c"
        
        if DEBUG:
            print()
            print(cmd)
            print()
                
        results = subprocess.getoutput(cmd)
        
        for line in results.split('\n'):
            if line.strip() > '':
                cols = line.strip().split(' ')
                n = int(cols[0].strip())
                v = cols[1]
                if v not in report[f[0]]:
                    report[f[0]][v] = {'csv': 0, 'xml': 0}
                report[f[0]][v]['xml'] = n
                
    print()
    print(LABEL, 'EXCEPTIONS')
    print()
                
    for field in report.keys():
        for value in report[field]:
            if report[field][value]['csv'] != report[field][value]['xml']:
                print('\t', field, value, report[field][value]['csv'], report[field][value]['xml'])

    cmd = mother_of_grep + ' | sed -E "s/\:.+//g" | sort | uniq -c'
    
    if DEBUG:
        print()
        print(cmd)
        print()

    print()
    print('XML', LABEL, 'COUNT')
    print()
    print(subprocess.getoutput(cmd))
    print()

    cmd = 'csvcut -c 1 ' + path_to_csv + ' | sort | uniq -c'
        
    if DEBUG:
        print()
        print(cmd)
        print()

    print('CSV', LABEL, 'COUNT')
    print()
    print(subprocess.getoutput(cmd))
    print()
    
# ----------------------------------------------------------------------
# 
# ----------------------------------------------------------------------

make_qa_report('LINES', 
                '../dist/stats_crossfilter/data/lines.csv',
                ['<l ',])

make_qa_report('METRA', 
                '../dist/stats_crossfilter/data/metra.csv',
                ['<seg ', 'type="metron"'])

make_qa_report('SYLLS', 
                '../dist/stats_crossfilter/data/syllables.csv',
                ['<seg ', 'type="syll"'])

make_qa_report('WORDS', 
                '../dist/stats_crossfilter/data/words.csv',
                ['<milestone ', 'unit="word"'],
                DEBUG=False)
