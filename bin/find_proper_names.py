import re, sys
from bs4 import BeautifulSoup

try:
    XML_FILE = sys.argv[1]
    RESULT_FILE = sys.argv[2]
except:
    sys.exit("Syntax is python3 find_proper_names.py XML_FILEPATH RESULT_FILEPATH.")

with open(XML_FILE) as xmlfile: # may not work if xml file is way too big, but it can handle at least 20k lines
    content = xmlfile.readlines()
    content = "".join(content)
    soup = BeautifulSoup(content, "lxml")

try:
    with open("common_words_list.txt") as txtfile:
        common_words = [word.strip() for word in txtfile.readlines()] # a list of common words which should be excluded from the list
except:
    common_words = []
    print("No common word list found.")

final_list = "" # what will be written in results.txt

for text in soup.find_all("seg", {"type": "line_text"}): # get all <seg type="line_text"></seg> tags
    for word in text.text.split(): # splits text of tags into words
        unpunctuated_word = re.sub("[,.·()\[\]〈〉\u200e†;]", "", word) # remove punctuation
        if re.search(u'^[ΑΕΗΙΟΥΩᾼῌῼΡ´ΆΈΉΊΌΎΏ    `ᾺῈῊῚῸῪῺ    ᾿ἈἘἨἸὈὨᾈᾘᾨ ῎ἌἜἬἼὌὬᾌᾜᾬ ῍ἊἚἪἺὊὪᾊᾚᾪ ῏ἎἮἾὮᾎᾞᾮ ῾ἉἙἩἹὉὙὩᾉᾙᾩῬ῞ἍἝἭἽὍὝὭᾍᾝᾭ ῝ἋἛἫἻὋὛὫᾋᾛᾫ῟ἏἯἿὟὯᾏᾟᾯΑ-ΩϹ]', unpunctuated_word) and unpunctuated_word not in common_words: # if the first letter of the word is an uppercase Greek letter and is not in the common words
            final_list += text.parent["n"] + " " + unpunctuated_word + "\n" # append the line number and the word to the final string list

with open(RESULT_FILE, "w") as cursor:
    cursor.write(final_list)