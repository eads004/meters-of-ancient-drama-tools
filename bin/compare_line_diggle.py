# To compare between the xml version and Diggle's Oxford edition.

# HOW TO USE:
# Obtain the html of Diggle's text from Oxford Scholarly Editions through Inspect Element. Copy and paste ONLY the div tag (and everything underneath it) that corresponds with the "Main Body Text" to a new file on the computer. The div tag encompassing the main body text should look like either <div class="scroll-content columnContent work-top" style="height: 471px;"> or <div class="bodyCssRoot" id="contentRoot">. INCLUDE ALL THE OTHER TAGS UNDERNEATH IT when copying. This can easily be done on Chrome in the Inspect Element menu by collapsing all the other tags and then pressing control+C.
# The syntax is as follows: "python3 compare_lines.py XML_FILEPATH OXFORD_HTML_FILEPATH RESULT_FILEPATH LINES_TO_EXCLUDE [DIFFERENCES_TO_EXCLUDE]", where the all-caps words correspond with the required string as designated, and [DIFFERENCES_TO_EXCLUDE] allows for any number of arguments, designating which error types to exclude from the final document's line comparisons (though not the final tally).
# XML_FILEPATH: the XML file of the play
# OXFORD_HTML_FILEPATH: Diggle's edition of the play; how to obtain is explained above
# RESULT_FILEPATH: the file that is to contain the final product, which is a summary of all the differences found between the Diggle text and the XML text.
# LINES_TO_EXCLUDE: lines that the program should skip in analyzing. For example, lyric sections when using Diggle's text.
# Line Text refers to the text found in between the <seg type="line_text"> tag.
# Combined Text refers to the text found within the <seg type="syll"> tags that corresponds to the line of the text in question.

import re, sys
from Levenshtein import editops, distance
from bs4 import BeautifulSoup, Tag, NavigableString
import requests
from cltk.alphabet import grc
import copy
import inspect
from intspan import intspan

# make a set of pairs of greek letters: lowercase and uppercase

upper_lower_pairs = []
upper_lower_pairs.extend(zip(grc.UPPER, grc.LOWER))
upper_lower_pairs.extend(zip(grc.UPPER_ACUTE, grc.LOWER_ACUTE))
upper_lower_pairs.extend(zip(grc.UPPER_GRAVE, grc.LOWER_GRAVE))
upper_lower_pairs.extend(zip(grc.UPPER_SMOOTH, grc.LOWER_SMOOTH))
upper_lower_pairs.extend(zip(grc.UPPER_SMOOTH_ACUTE, grc.LOWER_SMOOTH_ACUTE))
upper_lower_pairs.extend(zip(grc.UPPER_SMOOTH_GRAVE, grc.LOWER_SMOOTH_GRAVE))
upper_lower_pairs.extend(zip(grc.UPPER_SMOOTH_CIRCUMFLEX, grc.LOWER_SMOOTH_CIRCUMFLEX))
upper_lower_pairs.extend(zip(grc.UPPER_ROUGH, grc.LOWER_ROUGH))
upper_lower_pairs.extend(zip(grc.UPPER_ROUGH_ACUTE, grc.LOWER_ROUGH_ACUTE))
upper_lower_pairs.extend(zip(grc.UPPER_ROUGH_GRAVE, grc.LOWER_ROUGH_GRAVE))
upper_lower_pairs.extend(zip(grc.UPPER_ROUGH_CIRCUMFLEX, grc.LOWER_ROUGH_CIRCUMFLEX))

del grc.LOWER_CONSONANTS[17] # deletes ς so that the other consonants line up

upper_lower_pairs.extend(zip(grc.UPPER_CONSONANTS, grc.LOWER_CONSONANTS))


# Oxford Scraping stuff

def scrape_oxford(soup, replace_lunate_string=True):
    diggle_text_dict = {} # key is line number, value is line_text
    line_no_order_list = [] # tells what order to read the dict in to get results

    line_number = -1
    text = ''
    for element in soup.contents[0].find_all_next("span"):
        if 'class' in element.attrs:
            if 'lineNumber' in element['class']:
                if line_number != -1:
                    if replace_lunate_string:
                        r = replace_lunate_strings(text)
                    else:
                        r = text
                    diggle_text_dict[str(line_number)] = re.sub(r" …", "…", re.sub(r"- ", "", r))
                    line_no_order_list.append(str(line_number))
                line_number = element.string
                text = ""
            # elif 'textNode' in element['class'] and 'lang-ell' in element['class']:
            #     text += element.string
            elif 'textNode' in element['class']:
                text += re.sub("–", "-", re.sub(r"\. ([,.·()[\]〈〉\u200e†;])", r".\1", re.sub(r"([;\.…])", r"\1 ", re.sub(r"[\u200e]", "", element.string))))
    if replace_lunate_string:
        r = replace_lunate_strings(text)
    else:
        r = text
    diggle_text_dict[str(line_number)] = re.sub(r" …", "…", re.sub(r"- ", "", r))
    line_no_order_list.append(str(line_number))

    return diggle_text_dict, line_no_order_list

def replace_lunate_strings(string):
    return " ".join([replace_lunate_sigmas(word) for word in string.split()])

def replace_lunate_sigmas(word):
    proper_sigma_word = word
    sigma_index = proper_sigma_word.find("ϲ")
    while sigma_index != -1:
        unpunctuated_word = re.sub("[,.·()\[\]〈〉\u200e†;]", "", proper_sigma_word) 
        if unpunctuated_word.find("ϲ") == len(unpunctuated_word)-1:
            proper_sigma_word = proper_sigma_word[:sigma_index] + "ς" + proper_sigma_word[sigma_index+1:]
        else:
            proper_sigma_word = proper_sigma_word[:sigma_index] + "σ" + proper_sigma_word[sigma_index+1:]
        sigma_index = proper_sigma_word.find("ϲ")
    proper_sigma_word = re.sub("Ϲ", "Σ", proper_sigma_word)
    return proper_sigma_word

# helper method
def get_line(str_):
    line_no = ""
    for char in str(str_):
        if char in '0123456789':
            line_no += char
        else:
            break
    return int(line_no)


def add_error(type_array, line_no, line_text, syll_text, oxford_text):
    if len(type_array) == 0:
        return
    global final_result
    for type_ in type_array:
        errors[type_] += 1
    type_array_set = set(type_array)
    if not type_array_set.issubset(DIFFERENCES_TO_EXCLUDE):
        final_result += line_no + ": " + " ".join(type_array) + "\n" + "Line Text:".ljust(15) + line_text + "\n" + "Combined Text:".ljust(15) + syll_text + "\n" + "Oxford Text:".ljust(15) + oxford_text + "\n\n"

def normalize_punctuation(string, old, new):
    result = string
    for i in range(len(old)):
        result = result.replace(old[i], new[i])
    return result

def find_faults(line_text, combined_text):
    def consider(consideration_list):
        a_str_cons = consideration_list[0][1]
        a_str_cons_value = True
        b_str_cons = consideration_list[0][2]
        b_str_cons_value = True
        for consideration in consideration_list[1:]:
            if a_str_cons_value and consideration[1] != a_str_cons:
                a_str_cons_value = False
            if b_str_cons_value and consideration[2] != b_str_cons:
                b_str_cons_value = False
            if not a_str_cons_value and not b_str_cons_value:
                return False
        for consideration in consideration_list:
            word_edits.add(consideration)
        return True

    edits = editops(line_text, combined_text)
    
    if len(edits) >= 3:

        # if there's a lot of insert tags and/or delete tags near the beginning or end of a line, then there are likely different line divisions
        insert_counter = 0
        delete_counter = 0 
        line_division_edits = set() 

        for edit in edits:
            if edit[0] == 'insert' and (edit[1] == 0 or edit[1] == len(line_text)):
                insert_counter += 1
                line_division_edits.add(edit)
            elif edit[0] == 'delete' and (edit[2] == 0 or edit[2] == len(combined_text)):
                delete_counter += 1
                line_division_edits.add(edit)
        if insert_counter >= 3 or delete_counter >= 3:
            error_set.add("line_division")
        else:
            line_division_edits = set() # reset the counter if line division is not an error
        
        edits = [edit for edit in edits if edit not in line_division_edits] # removes already processed edits in edits for the point changes check

    # if there's still a lot of concentrated insert, delete, or change tags, then there are likely differences in words

    if len(edits) >= 3:

        consideration_list = edits[:min(len(edits), 5)] 
        word_edits = set()
        
        
        if consider(consideration_list[:-1]) or consider(consideration_list[1:]):
                error_set.add("words")
        for edit in edits[min(len(edits), 5):]:
            consideration_list.append(edit)
            consideration_list = consideration_list[1:]
            if consider(consideration_list[:-1]) or consider(consideration_list[1:]):
                error_set.add("words")
        
        edits = [edit for edit in edits if edit not in word_edits]
    # else, if there's less than 5 edits, the differences are probably point changes
    if len(edits) < 5: 
        char_set = set()
        for edit in edits:
            if edit[0] in ['insert', 'replace']:
                char_set.add(combined_text[edit[2]])
            if edit[0] in ['delete', 'replace']:
                char_set.add(line_text[edit[1]])
        # print(char_set, line_text, combined_text, sep="\n")
        # exit()

        skip_set = set()
        for char in char_set:
            if char in '᾽\'ʼ",.·()[]〈〉\u200e†;\u037e!"#$%&()*+,-./:;<=>?@[\]^_`{|}~':
                error_set.add("punctuation")
            elif char in " \n":
                error_set.add("spacing")
            else: # error is probably a difference in letters
                for upper, lower in upper_lower_pairs:
                    if upper in char_set and lower in char_set:
                        error_set.add("capitalization")
                        skip_set.add(upper)
                        skip_set.add(lower)
                if len(char_set) > len(skip_set):
                    error_set.add("spelling")

def replace_marking_tag(tag_name, tag_start, tag_end, soup):
    for line_marking in soup.find_all(tag_name):
        if 'part' in line_marking.attrs:
            # print(line_marking)
            if line_marking.find_parent("seg", type="syll"):
                if line_marking['part'] == "I":
                    line_marking.insert_before(tag_start)
                    line_marking.unwrap()
                elif line_marking['part'] == "M":
                    line_marking.unwrap()
                elif line_marking['part'] == "F":
                    line_marking.insert_after(tag_end)
                    line_marking.unwrap()
            elif line_marking.find("seg", type="syll"): # if the tag is outside the <seg type="syll"> tag
                if line_marking['part'] == "I":
                    line_marking.find("seg", type="syll").contents[0].insert_before(tag_start)
                    line_marking.unwrap()
                elif line_marking['part'] == "M":
                    line_marking.unwrap()
                elif line_marking['part'] == "F":
                    no_string = True
                    for leaf in line_marking.find_all("seg", type="syll")[-1].contents[::-1]: # access the contents in reverse
                        # print(leaf, "\n")
                        if type(leaf) is NavigableString:
                            leaf.insert_after(tag_end)
                            no_string = False
                            break
                    if no_string:
                        leaf = type(line_marking.find_all("seg", type="syll")[-1])
                        if leaf is Tag:
                            leaf.string = tag_end
                        else:
                            print(inspect.getframeinfo(inspect.currentframe()).lineno, "exception to expectation", line_marking)
                            exit()
                    line_marking.unwrap()
            else:
                print(inspect.getframeinfo(inspect.currentframe()).lineno, "exception to expectation", line_marking)
                exit()

        else:
            if line_marking.find_parent("seg", type="syll"): # if the tag is inside a <seg type="syll"> tag
                line_marking.insert_before(tag_start)
                line_marking.insert_after(tag_end)
                line_marking.unwrap()
            elif line_marking.find("seg", type="syll"): # if the tag is outside the <seg type="syll"> tag
                line_marking.find("seg", type="syll").contents[0].insert_before(tag_start)
                no_string = True
                for leaf in line_marking.find_all("seg", type="syll")[-1].contents[::-1]:
                    if type(leaf) is NavigableString:
                        leaf.insert_after(tag_end)
                        no_string = False
                        break
                if no_string:
                    leaf = type(line_marking.find_all("seg", type="syll")[-1])
                    if leaf is Tag:
                        leaf.string = tag_end
                    print(inspect.getframeinfo(inspect.currentframe()).lineno, "exception to expectation", line_marking)
                    exit()
                line_marking.unwrap()
            else:
                print(inspect.getframeinfo(inspect.currentframe()).lineno, "exception to expectation", line_marking)
                exit()

if __name__ == "__main__":
    del grc.MAP_SUBSCRIPT_NO_SUB['Ἄ']

    # get file_names from terminal
    try:
        XML_FILE = sys.argv[1]
        OXFORD_HTML_FILE = sys.argv[2]
        RESULT_FILE = sys.argv[3]
        LINES_TO_EXCLUDE = sys.argv[4]
        DIFFERENCES_TO_EXCLUDE = set(sys.argv[5:])
    except:
        sys.exit("Syntax is python3 compare_lines_diggle.py XML_FILEPATH OXFORD_HTML_FILEPATH RESULT_FILEPATH LINES_TO_EXCLUDE [DIFFERENCES_TO_EXCLUDE].")

    # open xml file 
    with open(XML_FILE) as xmlfile: # may not work if xml file is way too big, but it can handle at least 20k lines
        content = xmlfile.readlines()
        content = "".join(content)
        xml_soup = BeautifulSoup(content, "lxml")

    with open(OXFORD_HTML_FILE) as oxford_html_file:
        content = oxford_html_file.readlines()
        content = "".join(content)
        oxford_soup = BeautifulSoup(content, "lxml")

    diggle_text_dict, line_order_list = scrape_oxford(oxford_soup)
    # print("DIGGLE TEXT DICT", "\n", diggle_text_dict)
    # print("LINE ORDER LIST", "\n", line_order_list)

    # parse regular xml file
    final_result = "DIFFERENCES:\n" # what should be written in RESULT_FILE
    errors = {"spacing": 0, "punctuation": 0, "spelling": 0, "capitalization": 0, "words": 0, "line_division": 0, "no_line_text": 0, "no_syllables": 0, "no_oxford": 0, "no_seg_tags": 0}

    replace_marking_tag("sic", "†", "†", xml_soup)
    replace_marking_tag("supplied", "〈", "〉", xml_soup)
    replace_marking_tag("surplus", "[", "]", xml_soup)

    for line in xml_soup.find_all("l"): # get all <l> tags
        print("Current line:", line['n'], end="         \r")
        if line['n'] != "0": # exclude non-line <l> tags
            # get line text
            if line['n'][-1] in "abαβ":
                line_no = line['n'][:-1]
            elif "-" in line['n']:
                line_no = line['n'].split("-")[0]
            else:
                line_no = line['n']
            if int(line_no) in intspan(LINES_TO_EXCLUDE):
                continue
            # print(line['n'], line_no)
            # print(line_no)
            line_text = line['line_text'] if line['line_text'] is not None else "" # get the line text, which is the first component to check; if line_text_seg is None, return nothing 
            line_text_expanded = grc.tonos_oxia_converter(grc.expand_iota_subscript(line_text))
            
            # get combined text
            combined_text = "" # the combined text of the syllables, which is the second component to check

            for syllable in line.find_all("seg", {"type": "syll"}): # get all <seg type="syll"> tags
                for milestone in syllable.find_all("milestone", {"unit": "word"}): # get all <milestone unit="word"> tags within the syllable tags
                    milestone.replace_with(" ") # replace the milestones with a space, indicating a new word
                combined_text += syllable.get_text()
            combined_text = combined_text.strip()
            combined_text_expanded = grc.tonos_oxia_converter(grc.expand_iota_subscript(combined_text))
            # if line['n'] == "964": #debug!
            #     print(combined_text, "\n", combined_text_expanded)

            # get oxford text
            try:
                oxford_text = diggle_text_dict[line_no]
            except:
                oxford_text = ""
            oxford_text_expanded = grc.tonos_oxia_converter(grc.expand_iota_subscript(oxford_text))
            
            # debugging code
            # final_result += line_text + "\n" + combined_text.strip() + "\n\n"
            
            # compare the two strings using editops
            error_set = set()
            if oxford_text_expanded == "":
                error_set.add("no_oxford")
                # find_faults(line_text_expanded, combined_text_expanded)
            elif line_text_expanded == "" and combined_text_expanded == "":
                error_set = {"no_seg_tags"}
            elif line_text_expanded == "":
                # error_set = {"no_line_text"}
                find_faults(combined_text_expanded, oxford_text_expanded)
            elif combined_text_expanded == "":
                error_set = {"no_syllables"}   
                find_faults(line_text_expanded, oxford_text_expanded)
            elif line_text_expanded == combined_text_expanded and line_text_expanded == oxford_text_expanded:
                pass
            else:
                # find_faults(line_text_expanded, combined_text_expanded) 
                find_faults(combined_text_expanded, oxford_text_expanded) # only comparing combined text and oxford text because line text does not actually show on the html
                # find_faults(line_text_expanded, oxford_text_expanded)    
            add_error(error_set, line["n"], line_text, combined_text, oxford_text)

    summary = "SUMMARY:\n"

    for key, value in errors.items():
        summary += "    " + key + ": " + str(value) + "\n"

    final_result = "XML: " + XML_FILE + "\nOXFORD: " + OXFORD_HTML_FILE + "\nEXCLUDED_LINES: " + LINES_TO_EXCLUDE + "\n\n" + summary + "\n" + final_result

    # record results
    with open(RESULT_FILE, "w") as cursor:
        cursor.write(final_result)



