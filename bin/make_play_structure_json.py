#!/usr/bin/env python

import glob, json, sys
from lxml import etree
from collections import Counter
  
FOLDER = '../texts/HANDCHECKED/'
#TEXTS_TO_PROCESS = ['Orestes', 'Hekabe', 'Elektra']
TEXTS_TO_PROCESS = ['Elektra']
          
# ----------------------------------------------------------------------

for p in glob.glob(FOLDER + '*.xml'):
    
    play = p.split('/')[-1].split('.')[0]
    
    if play not in TEXTS_TO_PROCESS:
        print('make_play_structure_json.py skipping', p)
        continue

    print()
    
    tree = etree.parse(p) 
    
    results = {'name': play, 'children': []}

    for div1 in tree.xpath('//tei:div1', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

        lines = div1.xpath('descendant::tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

        results['children'].append({'name': div1.get('type'), 
                                    'children': [], 
                                    'click': 0, 
                                    'start': lines[0].get('n'), 
                                    'end': lines[-1].get('n')})
                                    
        for div2 in div1.xpath('descendant::tei:div2', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

            lines2 = div2.xpath('descendant::tei:l', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
            
            div2_name = div2.get('met')
            if div2.get('ana') != None and div2.get('ana') > '':
                div2_name = div2.get('ana')
            if div2.get('type') != None and div2.get('type') > '':
                div2_name = div2.get('type')
            
            results['children'][-1]['children'].append({'name': div2_name, 
                                                        'size': 99999, 
                                                        'start': lines2[0].get('n'), 
                                                        'end': lines2[-1].get('n')})

    f = open(play + 'Structure.json', 'w', encoding='utf-8')
    f.write(json.dumps(results, indent=4, ensure_ascii=False))
    f.close()
                                    
    print(play, 'done!')
