# Meters of Ancient Drama: site tools

This repository contains code to generate the Meters of Ancient Drama site of Tim Moore. 

It will be undergoing reorganization.

## To build the site

Check out this repository: 

    git clone git@bitbucket.org:eads004/meters-of-ancient-drama-tools.git
    
Then check out the texts repository as a directory within this repository:

    cd meters-of-ancient-drama-tools
    git clone git@bitbucket.org:eads004/meters-of-ancient-drama-texts.git texts
    
    
Then build the site:

    ./bin/build.sh
    
The files for the static site will be created in the /dist directory.

