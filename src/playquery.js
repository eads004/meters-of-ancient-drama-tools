let playquery = {
    query: {
        Character: [],
        Gender: [],
        Meter: [],
        Metra: [],
        Type: [],
        God: [],
        Status: [],
        Greek: [],
        Lyric: [],
        Recitative: [],
        Proper: []
    },
    filtered: {
    },
    playdata: playdata,

    // returns a dictionary of values that will be added to filters 
    // in the sidebar automatically (characters, meters, metra, meter types) 
    // these are the values that change from play to play
    // called by the various drawXFilter() functions 
    getFiltervalues() {
        if (this.filtervalues && Object.keys(this.filtervalues).length > 0) {
            return this.filtervalues;
        };
        let filtervalues = {
            Character: [],
            Name: [],
            Metra: [],
            Type: [],
            Combination: [],
        };
        // finds all the characters in the play and adds them to the 
        // Character list in the filtervalues dictionary
        let speakers = this.playdata.getSpeakers();
        for (let i = 0; i < speakers.length; i++) {
            let speaker = speakers[i].getAttribute("n");
            if (!filtervalues.Character.includes(speaker) && speaker != "") {
                filtervalues.Character.push(speaker);
            }
        }
        // finds the names of all the meters, metra, and meter types 
        // in the play and adds them to the filtervalues dictionary
        for (let i = 0; i < this.playdata.lines.length; i++) {
            let meter = checkLineMeter(this.playdata.lines[i]);
            if (meter != "" && !filtervalues.Name.includes(meter)) {
                filtervalues.Name.push(meter);
            }
            let metra = [];
            let children = this.playdata.lines[i].children;
            for (let k = 0; k < children.length; k++) {
                if (children[k].getAttribute("type") == "metron") {
                    metra.push(children[k]);
                }
            }
            for (let m = 0; m < metra.length; m++) {
                let met = checkMeterName(metra[m]);
                if (!filtervalues.Metra.includes(met) && met != null && 
                    met != "") {
                    filtervalues.Metra.push(met);
                }
            }
            type = checkMeterType(this.playdata.lines[i]);
            if (type != "" && !filtervalues.Type.includes(type)) {
                filtervalues.Type.push(type);
            }
        }
        this.filtervalues = filtervalues;
        return this.filtervalues;
    },
    resetFiltered() {
        this.filtered = {};
    },
    // returns all the lines spoken by a character selected in the filters
    // called by getLinesIncl()
    getCharLines() {
        if (this.filtered.charLines) {
            return this.filtered.charLines;
        };
        let otherlines = [];
        let speeches = this.playdata.getSpeeches();
        let keys = Object.keys(speeches);
        for (let m = 0; m < this.query["Character"].length; m++) {
            for (let i = 0; i < keys.length; i++) {
                if (keys[i].replace(/[0-9]/g, '') == 
                    this.query["Character"][m]) {
                    for (let j = 0; j < speeches[keys[i]].length; j++) {
                        if (speeches[keys[i]][j].tagName == "l" && 
                            !otherlines.includes(speeches[keys[i]][j])) {
                            otherlines.push(speeches[keys[i]][j]);
                        }
                    }
                }
            }
        }
        this.filtered.charLines = otherlines;
        return otherlines;
    },
    // returns all the lines spoken by a gender selected in the filters
    // called by getLinesIncl()
    getGenderLines() {
        if (this.filtered.genderLines) {
            return this.filtered.genderLines;
        };
        let otherlines = [];
        let female = [];
        for (let i = 0; i < playdata.chardata.length; i++) {
            if (playdata.chardata[i].gender == "female") {
                female.push(playdata.chardata[i].name);
            }
        }
        let speeches = playdata.getSpeeches();
        let keys = Object.keys(speeches);
        for (let m = 0; m < this.query["Gender"].length; m++) {
            if (this.query["Gender"][m] == "Female") {
                for (let i = 0; i < keys.length; i++) {
                    if (female.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }
            } else if (this.query["Gender"][m] == "Male") {
                for (let i = 0; i < keys.length; i++) {
                    if (!female.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }
            }
        }
        this.filtered.genderLines = otherlines;
        return otherlines;
    },
    // returns all the (whole) lines which have the meter selected in the filters
    // called by getLinesIncl()
    getMeterLines() {
        if (this.filtered.meterLines) {
            return this.filtered.meterLines;
        };
        let otherlines = [];
        for (let m = 0; m < this.query["Meter"].length; m++) {
            for (let i = 0; i < playdata.lines.length; i++) {
                if (checkLineMeter(playdata.lines[i]) == 
                    this.query["Meter"][m]) {
                    otherlines.push(playdata.lines[i]);
                }
            }
        }
        this.filtered.meterLines = otherlines;
        return otherlines;
    },
    // returns the lines which contain a metron with the meter 
    // selected in the filters
    // called by getLinesIncl()
    getMetraLines() {
        if (this.filtered.metraLines) {
            return this.filtered.metraLines;
        };
        let otherlines = [];
        for (let m = 0; m < this.query["Metra"].length; m++) {
            for (let i = 0; i < playdata.lines.length; i++) {
                if (checkLineMeter(playdata.lines[i])
                    .includes(this.query["Metra"][m])) {
                    otherlines.push(playdata.lines[i]);
                }
            }
        }
        this.filtered.metraLines = otherlines;
        return otherlines;
    },

    // returns all the lines which have the type selected in the filters.
    // called by getLinesIncl()
    getTypeLines() {
        if (this.filtered.typeLines) {
            return this.filtered.typeLines;
        };
        let otherlines = [];
        for (let m = 0; m < this.query["Type"].length; m++) {
            for (let i = 0; i < playdata.lines.length; i++) {
                if (checkLineType(playdata.lines[i]) == 
                    this.query["Type"][m]) {
                    otherlines.push(playdata.lines[i]);
                }
            }
        }
        this.filtered.typeLines = otherlines;
        return otherlines;
    },
    // returns all the lines belonging to a god or human, if one or both 
    // of the god/human filters are selected
    // called by getLinesIncl()
    getGodLines() {
        if (this.filtered.godLines) {
            return this.filtered.godLines;
        };
        let otherlines = [];
        let god = [];
        for (let i = 0; i < playdata.chardata.length; i++) {
            if (playdata.chardata[i].isGod == "god") {
                god.push(playdata.chardata[i].name);
            }
        }
        let speeches = playdata.getSpeeches();
        let keys = Object.keys(speeches);
        for (let m = 0; m < playquery.query["God"].length; m++) {
            if (playquery.query["God"][m] == "God") {
                for (let i = 0; i < keys.length; i++) {
                    if (god.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }
            } else if (playquery.query["God"][m] == "Human") {
                for (let i = 0; i < keys.length; i++) {
                    if (!god.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }

            }
        }
        this.filtered.godLines = otherlines;
        return otherlines;
    },

    // returns all the lines belonging to a enslaved or free person, 
    // if one or both of the status filters are selected
    // called by getLinesIncl()
    getStatusLines() {
        if (this.filtered.statusLines) {
            return this.filtered.statusLines;
        };
        let otherlines = [];
        let enslaved = [];
        for (let i = 0; i < playdata.chardata.length; i++) {
            if (playdata.chardata[i].status == "enslaved") {
                enslaved.push(playdata.chardata[i].name);
            }
        }
        let speeches = playdata.getSpeeches();
        let keys = Object.keys(speeches);
        for (let m = 0; m < playquery.query["Status"].length; m++) {
            if (playquery.query["Status"][m] == "Enslaved") {
                for (let i = 0; i < keys.length; i++) {
                    if (enslaved.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }
            } else if (playquery.query["Status"][m] == "Free") {
                for (let i = 0; i < keys.length; i++) {
                    if (!enslaved.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }
            }
        }
        this.filtered.statusLines = otherlines;
        return otherlines;
    },

    // returns all the lines spoken by a Greek or non-Greek character, 
    // if one or both of the Greek/Non-Greek filters are selected
    // called by getLinesIncl()
    getGreekLines() {
        if (this.filtered.greekLines) {
            return this.filtered.greekLines;
        };
        let otherlines = [];
        let nonGreek = [];
        for (let i = 0; i < playdata.chardata.length; i++) {
            if (playdata.chardata[i].isGreek == "nonGreek") {
                nonGreek.push(playdata.chardata[i].name);
            }
        }
        let speeches = playdata.getSpeeches();
        let keys = Object.keys(speeches);
        for (let m = 0; m < playquery.query["Greek"].length; m++) {
            if (playquery.query["Greek"][m] == "nonGreek") {
                for (let i = 0; i < keys.length; i++) {
                    if (nonGreek.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }
            } else if (playquery.query["Greek"][m] == "Greek") {
                for (let i = 0; i < keys.length; i++) {
                    if (!nonGreek.includes(keys[i].replace(/[0-9]/g, ''))) {
                        for (let j = 0; j < speeches[keys[i]].length; j++) {
                            if (speeches[keys[i]][j].tagName == "l" && 
                                !otherlines.includes(speeches[keys[i]][j])) {
                                otherlines.push(speeches[keys[i]][j]);
                            }
                        }
                    }
                }

            }
        }
        this.filtered.greekLines = otherlines;
        return otherlines;
    },

    // returns all the lyric and non-lyric lines in the play, if one or both 
    // of the Lyric/Non-Lyric filters are selected
    // called by getLinesIncl()
    getLyricLines() {
        
        if (this.filtered.lyricLines) {
            return this.filtered.lyricLines;
        };
        let otherlines = [];
        let lyrics = [];
        let nonlyrics = [];
        let anapests = playdata.sortMeters()[1];
        for (let i = 0; i < playdata.lines.length; i++) {
            let d = playdata.lines[i];
            let meter = checkLineMeter(d);
            
            if (meter != null && meter != "" && !meter.includes("anapest") && 
                !meter.includes("iambic3") && !meter.includes("trochaic4") && 
                /*!meter.includes("unknown") &&*/ !meter.includes("extra metrum")) { //should allow 'unknown' lines to count as lyric
                lyrics.push(d);
            } else if (meter.includes("anapest")) {
                let isLyric = true;
                for (let k = 0; k < anapests.length; k++) {
                    for (let j = 0; j < anapests[k].children.length; j++) {
                        if (anapests[k].children[j] == d) {
                            isLyric = false;
                        }
                    }
                }
                
                if (isLyric == true) {
                    lyrics.push(d);
                } else {
                    nonlyrics.push(d);
                }
            } 
            else if (!meter.includes("unknown") && !meter.includes("extra metrum")) {
                nonlyrics.push(d);
            }
        }
        for (let m = 0; m < playquery.query["Lyric"].length; m++) {
            if (playquery.query["Lyric"][m] == "Lyric") {
                for (let i = 0; i < lyrics.length; i++) {
                    otherlines.push(lyrics[i]);
                }
            } else if (playquery.query["Lyric"][m] == "nonLyric") {
                for (let i = 0; i < nonlyrics.length; i++) {
                    otherlines.push(nonlyrics[i]);
                }
            }
        }
        this.filtered.lyricLines = otherlines;
        return otherlines;
    },
    // returns all the lines in the play containing proper names, 
    // if the Proper filter is selected
    // called by getLinesIncl()
    // as of April 2020, we haven't gotten to tagging all the proper names 
    // in the XML for Orestes
    getProperLines() {
        if (this.filtered.properLines) {
            return this.filtered.properLines
        };
        let otherlines = [];

        if (playquery.query["Proper"] == "Proper") {
            for (let i = 0; i < playdata.lines.length; i++) {
                let children = playdata.lines[i].children;
                for (let j = 0; j < children.length; j++) {
                    if (children[j].getAttribute("type") == "syll") {
                        let grands = children[j].children;
                        for (let k = 0; k < grands.length; k++) {
                            if (grands[k].getAttribute("type") == "proper") {
                                otherlines.push(playdata.lines[i]);
                            }
                        }
                    } else if (children[j].getAttribute("type") == "metron") {
                        let sylls = children[j].children;
                        for (let m = 0; m < sylls.length; m++) {
                            let greatgrands = sylls[m].children;
                            for (let h = 0; h < greatgrands.length; h++) {
                                if (greatgrands[h].getAttribute("type") == "proper") {
                                    otherlines.push(playdata.lines[i]);
                                }
                            }
                        }
                    }
                }
            }
        }
        this.filtered.properLines = otherlines;
        return otherlines;
    }
};

