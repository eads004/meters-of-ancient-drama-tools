
let metersUtilities = {
   metersData : [],
   //given an abbreviation return the full name of the meter
   getFullName(abbreviation) { 
       let fullName = this.metersData.find((data) => data.ours == abbreviation);

       if (fullName) {
            return fullName.name;
       } else {
            return null;
       }
    },
    //given an abbreviation return all the data found within the
    //metersComplete.json
    getFullData(abbreviation) {
       return this.metersData.find((data) => data.ours == abbreviation);
    }
};

//this reads the meterscomplete.json into the utlity
$.getJSON('meterscomplete.json', function (data) {
    metersUtilities.metersData = data;
});
