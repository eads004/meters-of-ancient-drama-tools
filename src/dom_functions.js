/* Interactive but DOM-only code; no dependencies on data variables */

//hides panel boxes, shows translation
function showTranslation() {
    document.getElementsByClassName("panel")[0].style.opacity = 0;
    document.getElementById("transbox").style.opacity = 1;
    document.getElementById("transbox").style.width = "420px";
    document.getElementById("transbox").style.zIndex = 4;
    document.getElementById("showTranslationButton").style.display = "none";
    document.getElementById("hideTranslationButton").style.display = "inline-block";
    document.getElementById("expandButton").classList.add("translationShown");
    document.getElementById("collapseButton").classList.add("translationShown");

    setTimeout(() => {
        adjustTransBox();
    }, 500)
}

//shows panel boxes, hides translation
function hideTranslation() {
    document.getElementsByClassName("panel")[0].style.opacity = 1;
    document.getElementById("transbox").style.opacity = 0;
    document.getElementById("transbox").style.width = "0px";
    document.getElementById("transbox").style.zIndex = 0;
    document.getElementById("showTranslationButton").style.display = "inline-block";
    document.getElementById("hideTranslationButton").style.display = "none";
    document.getElementById("expandButton").classList.remove("translationShown");
    document.getElementById("collapseButton").classList.remove("translationShown");
}

//collapses or expands panel boxes (rectangles to the right of the text box)
//adds or removes a class. style.css sets that class to be visible/hidden.
//called by the panelLabel elements in orestes.html
function toggleExpand(elem) {
    
    e = document.getElementById(elem);
    b = document.getElementById(elem + "box");
    
    if (!e.classList.contains("expand")) {
        e.classList.add("expand");
        b.classList.add("boxShown");
    } else {
        e.classList.remove("expand");
        b.classList.remove("boxShown");
        b.style.width = "0px";
    }
};

//open a single panel element (elem)
//called by expandAll()
function expand(elem) {
    e = document.getElementById(elem);
    b = document.getElementById(elem + "box");
    if (!e.classList.contains("expand")) {
        e.classList.add("expand");
        b.classList.add("boxShown");
    }
};

//close a single panel element (elem)
//called by collapseAll()
function collapse(elem) {
    e = document.getElementById(elem);
    b = document.getElementById(elem + "box");
    if (e.classList.contains("expand")) {
        e.classList.remove("expand");
        b.classList.remove("boxShown");
    }
}

//expands all the panel boxes at once 
//called by expandButton in orestes.html
function expandAll() {
    expand('meter');
    expand('lyric');
    expand('type');
    expand('responsion');
    let e = document.getElementById("expandButton");
    e.classList.toggle("panelButtonHidden");
    let c = document.getElementById("collapseButton");
    c.classList.toggle("panelButtonHidden");
}

//collapses all the panel boxes at once
//called by collapseButton in orestes.html
function collapseAll() {
    collapse('meter');
    collapse('lyric');
    collapse('type');
    collapse('responsion');
    let e = document.getElementById("expandButton");
    e.classList.toggle("panelButtonHidden");
    let c = document.getElementById("collapseButton");
    c.classList.toggle("panelButtonHidden");
}

//expands a drop-down menu in the filters (e.g. Characters, Meters)
//called by elements with the .dropbtn class in the sidebar section of orestes.html
function dropMenu(elem) {
    document.getElementById(elem).classList.toggle("show");
};

//open all filter dropdowns at once
//called by filter header in orestes.html sidebar
function dropAll() {
    let ids = ["charDropdown", "genDropdown", "metDropdown", "metraDropdown", "mortDropdown", "statDropdown", "typeDropdown", "greekDropdown", "lyricDropdown", "recitativeDropdown", "properDropdown"];
    for (let i in ids) {
        document.getElementById(ids[i]).classList.add("show");
    }
    document.getElementById("filter").style.zIndex = 0;
    document.getElementById("filter").style.opacity = 0;
    document.getElementById("filter2").style.zIndex = 11;
    document.getElementById("filter2").style.opacity = 1;
}

//close all filter dropdowns at once
//called by header filter2 (same thing as filter, just used to differentiate drop/hide menus) in sidebar
function hideAll() {
    let ids = ["charDropdown", "genDropdown", "metDropdown", "metraDropdown", "mortDropdown", "statDropdown", "typeDropdown", "greekDropdown", "lyricDropdown", "recitativeDropdown", "properDropdown"];
    for (let i in ids) {
        document.getElementById(ids[i]).classList.remove("show");
    }
    document.getElementById("filter2").style.zIndex = 0;
    document.getElementById("filter2").style.opacity = 0;
    document.getElementById("filter").style.zIndex = 11;
    document.getElementById("filter").style.opacity = 1;
}

//makes the section below the text box that displays filtered lines and information about the filters visible
//called by drawFilterBox() if the filters produce a nonzero number of lines
//scrolls down to the filter box
function showFilterBox() {
    document.getElementById("statsbox").style.display = "block";
    document.getElementById("footer").style.top = "1600px";
    window.scrollTo(0, document.body.scrollHeight / 3 || document.documentElement.scrollHeight / 3);
}

//makes the filter section below the text box invisible
//called by drawFilterBox() if the filters selected don't produce any lines, also called by clearBoxes()
function hideFilterBox() {
    document.getElementById("statsbox").style.display = "none";
    document.getElementById("footer").style.top = "850px";
}

