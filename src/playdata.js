let playdata = {
    getParts() {
        let parts = [];
        for (let i = 0; i < this.lines.length; i++) {
            if (!parts.includes(checkPart(this.lines[i]))) {
                parts.push(checkPart(this.lines[i]));}}
        return parts;
    },
    getNextPart(currentPart) {
        var a = this.parts.indexOf(currentPart.replace(/ /g, ''));
        if (a === -1) {
            a = this.parts.indexOf(currentPart.replace(/ /g, '_'))
        }
        return this.parts[a + 1];
    },
    getPrevPart(currentPart) {
        let b = this.parts.indexOf(currentPart.replace(/ /g, ''));
        if (b === -1) {
            b = this.parts.indexOf(currentPart.replace(/ /g, '_'))
        }
        return this.parts[b - 1];
    },
    getSpeakers() {
        if (this.speakers && this.speakers.length > 0) {
            return this.speakers;
        } else {
            this.speakers = [];
            for (let i = 0; i < this.lines.length; i++) {
                let sib = this.lines[i].previousElementSibling;
                if (sib != null && sib.tagName == "milestone" && 
                    sib.getAttribute("unit") == "speaker") {
                    this.speakers.push(sib);
                }
            }
            return this.speakers;
        }
    },
    getSpeeches() {
        let speakers = this.getSpeakers();
        let speeches = {};
        if (this.speeches && Object.keys(this.speeches).length > 0) {
            return this.speeches;
        };
        for (let i = 0; i < speakers.length; i++) {
            let lineholder = [];
            let holder = speakers[i];
            while (holder.nextElementSibling != null && 
                   holder.nextElementSibling.getAttribute('unit') != "speaker" && 
                   holder.nextElementSibling.tagName != "milestone") {
                lineholder.push(holder.nextElementSibling);
                holder = holder.nextElementSibling;
                if (holder.nextElementSibling == null && 
                    holder.parentElement.nextElementSibling != null && 
                    holder.parentElement.nextElementSibling.tagName == 
                    "div2" && 
                    holder.parentElement.nextElementSibling.firstElementChild
                    .getAttribute('unit') != "speaker") {
                    lineholder.push(holder.parentElement.nextElementSibling
                                    .firstElementChild)
                    holder = holder.parentElement.nextElementSibling
                        .firstElementChild;
                }
                if (holder.nextElementSibling != null && 
                    holder.nextElementSibling.tagName == "milestone" && 
                    holder.nextElementSibling.getAttribute('unit') != 
                    "speaker") {
                    holder = holder.nextElementSibling;
                }
            }
            key = i + speakers[i].getAttribute('n')
            speeches[key] = lineholder;
        }
        this.speeches = speeches;
        return this.speeches;
    },

    // a more consistent way of loading midline speakers (it used to not appear at all, much of the time)
    getMidlineSpeakers() {
        if (this.midlineSpeakers && this.midlineSpeakers.size > 0) {
            return this.midlineSpeakers;
        } else {
            this.midlineSpeakers = new Map();

            let speeches = this.getSpeeches();
            let keys = Object.keys(speeches);

            for (let i = 0; i < keys.length; i++) {
                let speech = speeches[keys[i]];
        
                for (j = 0; j < speech.length; j++) {
                    let midlineSpeaker = getMidlineSpeaker(speech[j]);
                    this.midlineSpeakers.set(speech[j], midlineSpeaker);

                    // console.log("got midline speaker for " + speech[j].getAttribute("n"));
                }
            }

            return this.midlineSpeakers;
        }
    },

    //returns the <div2> sections of XML that contain lines in non-lyric meters
    //called by countMetra(), getMetersIncl(), getTypesIncl()
    sortMeters() {
        if (this.meters && this.meters.length > 0) {
            return this.meters;
        };
        let meters = [];
        let iambics = [];
        let anapests = [];
        let trochees = [];
        let divs = this.tree.getElementsByTagName("div2");
        for (let i = 0; i < divs.length; i++) {
            if (divs[i].getAttribute("met") == "ia3" || 
                divs[i].getAttribute("met") == "iambic3") {
                iambics.push(divs[i])
            } else if (divs[i].getAttribute("met") == "anapests" && 
                       divs[i].getAttribute("type") != "lyric") {
                anapests.push(divs[i])
            } else if (divs[i].getAttribute("met") == "tr4^") {
                trochees.push(divs[i])
            }
        }
        meters.push(iambics);
        meters.push(anapests);
        meters.push(trochees);
        this.meters = meters;
        return meters;
    },

    //returns a list of all the strophes and antistrophes in the play.
    //called by drawResponsion()
    getStrophes () {
        if (this.responsion) {
            return this.responsion;
        };
        let responsion = [];
        let strophes = [];
        let antistrophes = [];
        let divs = playdata.tree.getElementsByTagName("div2");
        for (let i = 0; i < divs.length; i++) {
            if (divs[i].getAttribute("ana") == "strophe") {
                strophes.push(divs[i]);
            } else if (divs[i].getAttribute("ana") == "antistrophe") {
                antistrophes.push(divs[i]);
            }
        }
        responsion.push(strophes);
        responsion.push(antistrophes);
        this.responsion = responsion;
        return responsion;
    }
};
