let toggleSideBar = function () {
    document.getElementById('sidebar').classList.toggle("collapsed");
    document.getElementById('body').classList.toggle("collapsed");
    document.getElementById('nameOfCurrentPlayWhenCollapsed').classList.toggle("hidden");
}

document.addEventListener("click", function(e) {
  var element = document.getElementById('sidebar');

  if (e.target == element || e.target.parentElement == element) {
      toggleSideBar();
  }
});
