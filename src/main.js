/* Reducing global variables;
       plays:     a list of plays w/a little metadata;
       playdata:  static data relating to one play
       playquery: interactive, stateful data about user interaction w/play
*/
let plays = {};

/* WE NO LONGER MEASURE RECITATIVE / NON-RECITATIVE, this is a preliminary commenting-out of the code. Should it break, remove this spanned comment
// returns all the recitative and non-recitative lines in the play, 
// if one or both of the Recitative/Non-Recitative filters are selected
// called by getLinesIncl()
playquery.getRecitativeLines = function() {
    if (this.filtered.recitativeLines) {
        return this.filtered.recitativeLines;
    };
    let otherlines = [];
    let anapests = playdata.sortMeters()[1];
    let trochees = playdata.sortMeters()[2];
    let recLines = [];
    let nonLines = [];
    for (let m = 0; m < playquery.query["Recitative"].length; m++) {
        for (let i = 0; i < playdata.lines.length; i++) {
            for (let j = 0; j < anapests.length; j++) {
                let children = anapests[j].children;
                for (let k = 0; k < children.length; k++) {
                    if (children[k] == playdata.lines[i]) {
                        recLines.push(playdata.lines[i]);
                    }
                }
            }
            for (let j = 0; j < trochees.length; j++) {
                let children = trochees[j].children;
                for (let k = 0; k < children.length; k++) {
                    if (children[k] == playdata.lines[i]) {
                        recLines.push(playdata.lines[i]);
                    }
                }
            }
        }

        if (playquery.query["Recitative"][m] == "Recitative") {
            for (let i = 0; i < recLines.length; i++) {
                otherlines.push(recLines[i]);
            }
        } else if (playquery.query["Recitative"][m] == "nonRecitative") {
            for (let i = 0; i < playdata.lines.length; i++) {
                if (!recLines.includes(playdata.lines[i])) {
                    nonLines.push(playdata.lines[i]);
                }
            }
            for (let i = 0; i < nonLines.length; i++) {
                otherlines.push(nonLines[i]);
            }
        }

    }
    this.filtered.recitativeLines = otherlines;
    return otherlines;
}
*/

function checkPart(elem) {
    
    let parent = elem.parentNode;
    
    while (parent.tagName != "div1") {
        parent = parent.parentNode;
    }
    
    return parent.getAttribute("type"); // return the type of div1
};


(function () {

    function addListeners(elem) {
        let b = document.getElementById(elem.id + "box");
        let l = document.getElementById(elem.id + "label");
        l.addEventListener("mouseup", function () {
            if (elem.classList.contains("expand")) {
                b.style.width = "0px";
                elem.style.width = "15px";
            } else {
                b.style.width = b.getAttribute('value');
                elem.style.width = elem.getAttribute('value');
            }
        });
    }

    let containers = document.getElementsByClassName("accordion");
    for (let i = 0; i < containers.length; i++) {
        addListeners(containers[i]);
    }

    let ebutton = document.getElementById("expandButton");
    let cbutton = document.getElementById("collapseButton");

    ebutton.addEventListener("click", function () {
        for (let i = 0; i < containers.length; i++) {
            let b = document.getElementById(containers[i].id + "box");
            containers[i].style.width = containers[i].getAttribute('value');
            b.style.width = b.getAttribute('value');
        }
    });

    cbutton.addEventListener("click", function () {
        for (let i = 0; i < containers.length; i++) {
            let b = document.getElementById(containers[i].id + "box");
            containers[i].style.width = "15px";
            b.style.width = "0px";
        }
    });

})();

// this function makes it so that the boxes with the characters 
// and the line numbers scroll when the user scrolls the text box. 
// it does this by telling all elements that have the 
// class .content.scroll-box (assigned in orestes.html) to 
// scroll in sync with the linebox. 
(function () {
    // let boxes = document.querySelectorAll('.content.scroll-box');
    // for (let boxIndex = 0; boxIndex < boxes.length; boxIndex++) {
    //     boxes[boxIndex].onscroll = function (event) {
    //         console.log(event)
    //         for (let boxIndex2 = 0; boxIndex2 < boxes.length; boxIndex2++) {
    //             //if (boxes[boxIndex2] != boxes[boxIndex]) {
    //             if (boxIndex2 != boxIndex) {
    //                 boxes[boxIndex2].scrollTop = this.scrollTop;
    //             }
    //         }
    //     };
    // }

    let boxes = document.querySelectorAll('.content.scroll-box');
    document.getElementById("linebox").onscroll = function (event) {
        // console.log(event)
        for (let boxIndex2 = 0; boxIndex2 < boxes.length; boxIndex2++) {
            //if (boxes[boxIndex2] != boxes[boxIndex]) {
            if (boxes[boxIndex2] != document.getElementById('linebox')) {
                boxes[boxIndex2].scrollTop = this.scrollTop;
            }
        }
    };
})();

// $('.content.scroll-box').change(function(e) {
//     if (e.originalEvent !== undefined)
//     {
//       alert ('human');
//     } else {
//         alert("not human")
//     }
// });

/*
    playdata.index: variable for holding data from (e.g.) orestes.json, 
        which is used in drawIndex to generate the grid
    playdata.tree:  represents the entire XML document
    playdata.translationdata: stores data from (e.g.) orestesTranslation.json. 
        later used in drawTranslation
    playdata.meterdata: holds data from JSON file (e.g.) orestesMeters, 
        which is used to determine meter type and name.
    playdata.lines: holds every <l> element in the play
    playdata.parts: holds list of play parts generated by function getParts(). 
        used to update section

    playquery.currentPart: variable that is used to determine 
        sections data is drawn from in drawLines, etc. Also 
        used to highlight current section in grid
    playquery.searchTerm: holds information from 
        line search box/the end of the url.
    playquery.patternValue: holds input from the 
        pattern search box in the sidebar.
*/


// once all JSON files have loaded, reads them as arguments 
// and assigns them to the global variables defined above.
// calls the function drawEverything() to draw almost every
// element on the page
//calls onload() to check for line numbers to jump to.
function initialize(error, index_arg, lines_arg, translationdata_arg, 
                    meterdata_arg, chardata_arg, playmeterdata_arg) {
    playdata.index = index_arg;
    playdata.tree = lines_arg;
    playdata.translationdata = translationdata_arg;
    playdata.meterdata = meterdata_arg;
    playdata.lines = lines_arg.documentElement.getElementsByTagName("l");
    playdata.chardata = chardata_arg;
    playdata.playmeterdata = playmeterdata_arg;

    playdata.getSpeeches();

    playdata.parts = [];
    playdata.parts = playdata.getParts();
    playquery.playdata = playdata;
    playquery.currentPart = playdata.parts[0];
    
    drawEverything();
    // drawCharFilter();
    // drawMeterFilter();
    // drawMetraFilter();
    // drawTypeFilter();
    
    onload();

};


// lets users jump to line by attaching number to url/searching 
// in line search box
// called in function initialize()
function onload() {
    function querySt(ji) {
        hu = window.location.search.substring(1);
        gy = hu.split("&");
        for (let i = 0; i < gy.length; i++) {
            ft = gy[i].split("=");
            if (ft[0] == ji) {
                return ft[1];
            }
        }
    }
    let searchInput = querySt("searchInput");
    if (searchInput == null) {} else {
        document.getElementById('searchInput').value = searchInput;
    }
    let evt = new KeyboardEvent('keydown', {
        'keyCode': 13,
        'which': 13
    });
    search(evt);
}

// showScansion() makes the line-by-line scansion visible. 
// called by showScansion button in orestes.html
// hides showScansion button and shows hideScansion button
function showScansion() {
    let currentScrollFraction = document.getElementById("linebox").scrollHeight != 0 ? document.getElementById("linebox").scrollTop / document.getElementById("linebox").scrollHeight : 0;

    document.getElementById("linebox").style.letterSpacing = "2px";
    let boxes = document.getElementsByClassName("scroll-box");
    for (let i = 0; i < boxes.length; i++) {
        boxes[i].style.lineHeight = "40px";
    }
    // document.getElementById("filterbox").style.lineHeight = "40px";
    document.getElementById("hideScansion").style.opacity = 100;
    document.getElementById("hideScansion").style.zIndex = 5;
    document.getElementById("showScansion").style.opacity = 0;
    document.getElementById("showScansion").style.zIndex = 0;
    
    let scanButton = document.getElementById("hideScansion");
    updateScansionDisplay(scanButton);
    // let flines = getLinesIncl();
    // for (let i = 0; i < flines.length; i++) {
    //     if (checkPart(flines[i]) == playquery.currentPart && 
    //         flines[i] != null) {
    //         let elem = document.getElementById(flines[i].getAttribute("n"));
    //         elem.style.backgroundColor = "#cccccc";
    //     }
    // }
    // autoSearch(); // not it

    

    // keep the reader at the same place as they were on; the setTimeout thing is a bit of a hack...
    // setTimeout(() => {
        
    // }, 250);

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

    if (isSafari === true) {
        document.getElementById("linebox").scrollTo(0, Math.floor(document.getElementById("linebox").scrollHeight * currentScrollFraction));

        console.log(document.getElementById("linebox").scrollTop);
    
    }
}

//hideScansion() hides the line-by-line scansion.
//called by hideScansion button in orestes.html
//shows showScansion button and hides hideScansion button
function hideScansion() {
    let currentScrollFraction = document.getElementById("linebox").scrollHeight != 0 ? document.getElementById("linebox").scrollTop / document.getElementById("linebox").scrollHeight : 0;

    document.getElementById("linebox").style.letterSpacing = ".5px";
    let boxes = document.getElementsByClassName("scroll-box");
    for (let i = 0; i < boxes.length; i++) {
        boxes[i].style.lineHeight = "25px";
    }
    // document.getElementById("filterbox").style.lineHeight = "25px"
    document.getElementById("hideScansion").style.opacity = 0;
    document.getElementById("hideScansion").style.zIndex = 0;
    document.getElementById("showScansion").style.opacity = 100;
    document.getElementById("showScansion").style.zIndex = 5;
    
    let scanButton = document.getElementById("hideScansion");
    updateScansionDisplay(scanButton);
    // let flines = getLinesIncl();
    // for (let i = 0; i < flines.length; i++) {
    //     if (checkPart(flines[i]) == playquery.currentPart && flines[i] != null) {
    //         let elem = document.getElementById(flines[i].getAttribute("n"));
    //         elem.style.backgroundColor = "#cccccc";
    //     }
    // }
    // autoSearch();

    // keep the reader in the same place they were on
    // setTimeout(() => {
        
    // }, 500);

    document.getElementById("linebox").scrollTo(0, Math.floor(document.getElementById("linebox").scrollHeight * currentScrollFraction));
        
    console.log(document.getElementById("linebox").scrollTop);
}


//uncheck sidebar checkboxes and clear highlighted selections
//called by clearAll button in index.html
// function clearBoxes() {
//     let elems = document.getElementsByClassName("checkbox");
//     for (let i in elems) {
//         elems[i].checked = false;
//         for (let key in playquery.query) {
//             if (playquery.query[key].includes(elems[i].value)) {
//                 check(elems[i].value, key);
//             }
//         }
//     }
//     document.getElementById("patternSearch").value = "";
//     playquery.patternValue = null;
//     selIndex();
//     drawLines();
//     drawStats();
//     hideFilterBox();
// }

function drawBody() {
    drawLines();
    drawComments();
    drawNumbers();
    drawChars();
    drawPanel();
    drawTranslation();
}

// goes to the next section of the play 
// called by nextButton in the HTML
function goForward() {
    // change the global variable playquery.currentPart 
    // to the next section in the play
    playquery.currentPart = playquery.playdata.getNextPart(playquery.currentPart);
    // redraw all the text, line numbers, etc. so they correspond 
    // to the next section. 
    drawBody();
    document.getElementById('linebox').scrollTop = 0;
};

//goes to the previous section of the play and scrolls to the top of the page.
//called by prevButton in the HTML
function goBack() {
    playquery.currentPart = playquery.playdata.getPrevPart(playquery.currentPart);
    drawBody();
    document.getElementById('linebox').scrollTop = 0;
};

// when a checkbox is selected, add selection to query (defined above) 
// if it's not already in query
// if it's in query, then the checkbox is being unselected 
// and the element is removed
function check(selection_name, category) {
    if (playquery.query[category].includes(selection_name)) {
        let index = playquery.query[category].indexOf(selection_name);
        playquery.query[category].splice(index, 1);
    } else {
        playquery.query[category].push(selection_name);
    }
};

// for any given line, finds the set of lines in the XML (speeches) 
// it belongs to and returns the speaker of those lines.
function checkSpeaker(line) {
    let char = ""
    let speeches = playdata.getSpeeches();
    let keys = Object.keys(speeches);
    for  (let i = 0; i < keys.length; i++) {
        for (let j = 0; j < speeches[keys[i]].length; j++) {
            if (line == speeches[keys[i]][j]) {
                char = keys[i];
            }
        }
    }
    return char;
}

// filters, then calculates the # of lines, syllables, and metra in
// that selection and displays the info
// also displays the filtered lines and automatically adds interactive 
// function that jumps to a line in the text box if the corresponding 
// line in the filter box is clicked.
// called by highlight(), which is called by each checkbox in orestes.html
// function drawFilterBox() {
//     let justlines = getLinesIncl();
//     let total = justlines.length;
//     let syllstotal = countSylls(justlines).length;
//     let metratotal = countMetra(justlines);
//     if (total == 0) {
//         hideFilterBox();
//     } else {
//         showFilterBox();
//         document.getElementById("totalbox").innerText = "Number of Lines: " + total;
//         document.getElementById("totalMetraBox").innerText = "Number of Metra: " + metratotal;
//         document.getElementById("totalSyllsBox").innerText = "Number of Syllables: " + syllstotal;
//         let part = checkPart(justlines[0]);
//         let filterLines = d3.select("#filterbox")
//             .selectAll("div")
//             .data(justlines);
//         filterLines.enter()
//             .append("div")
//             .attr("class", "filterline");
//         filterLines.attr("id", function (d) {
//             return "filterbox" + d.getAttribute("n");
//         });
//         filterLines.html(function (d) {
//             let speaker = checkSpeaker(d).replace(/[0-9]/g, '');
//             let newpart = checkPart(d);
//             if (part != newpart || d == justlines[0]) {
//                 part = newpart;
//                 return "<p style='margin-left:65px;font-weight:bold'>" + newpart + "</p><b>" + speaker + "</b> &nbsp;&nbsp;&nbsp;&nbsp; " + d.outerHTML + "<span style='float:right;padding-right:45px'>" + d.getAttribute("n") + "</span>";
//             } else {
//                 return "<b>" + speaker + "</b> &nbsp;&nbsp;&nbsp;&nbsp; " + d.outerHTML + "<span style='float:right;padding-right:45px'>" + d.getAttribute("n") + "</span>";
//             }
//         });
//         filterLines.on("click", function (d) {
//             let c = document.getElementsByClassName("filterline");
//             for (let i = 0; i < c.length; i++) {
//                 c[i].style.backgroundColor = "transparent";
//             }
//             if (d.getAttribute("n") != null && d.getAttribute("n") != "") {
//                 document.getElementById("filterbox" + d.getAttribute("n")).style.backgroundColor = "rgba(231, 234, 30, .4)";
//                 document.getElementById("searchInput").value = d.getAttribute("n");
//                 autoSearch();
//             }
//         });
//         filterLines.exit().remove();
//         let scanButton = document.getElementById("showScansion");
//         if (scanButton.zIndex != 0) {
//             showScansion();
//         }
//     }
// }

// counts all the <seg type="metron"> elements in a set of lines (somelines)
// if there are no explicit metra, assigns a number of metra based on the 
// meter section the lines belong to (iambic, anapestic, or trochaic)
// called by drawFilterBox()
function countMetra(somelines) {
    let mCount = 0;
    let trochees = playdata.sortMeters()[2];
    let iambics = playdata.sortMeters()[0];
    let anapests = playdata.sortMeters()[1];
    for (let i = 0; i < somelines.length; i++) {
        let l = somelines[i];
        let children = l.children;
        let check = 0;
        for (let j = 0; j < children.length; j++) {
            if (children[j].getAttribute("type") == "metron") {
                mCount += 1;
                check += 1;
            }
        }
        if (check == 0) {
            for (let k = 0; k < trochees.length; k++) {
                let c = trochees[k].children;
                for (let h = 0; h < c.length; h++) {
                    if (c[h] == l) {
                        mCount += 4;
                    }
                }
            }
            for (let k = 0; k < iambics.length; k++) {
                let c = iambics[k].children;
                for (let h = 0; h < c.length; h++) {
                    if (c[h] == l) {
                        mCount += 3;
                    }
                }
            }
            for (let k = 0; k < anapests.length; k++) {
                let c = anapests[k].children;
                for (let h = 0; h < c.length; h++) {
                    if (c[h] == l) {
                        mCount += 1;
                    }
                }
            }
        }
    }
    return mCount;
}

// counts all the syllables (<seg type="syll">) in a set of lines
// called by drawFilterBox()
function countSylls(lines) {
    let sylls = [];
    for (let i = 0; i < lines.length; i++) {
        let l = lines[i];
        let children = l.children;
        for (let j = 0; j < children.length; j++) {
            if (children[j].getAttribute("type") == "syll") {
                sylls.push(children[j]);
            } else if (children[j].getAttribute("type") == "metron") {
                let gc = children[j].children;
                for (let k = 0; k < gc.length; k++) {
                    if (gc[k].getAttribute("type") == "syll") {
                        sylls.push(gc[k]);
                    }
                }
            }
        }
    }
    return sylls;
}

// adds checkboxes and labels for each character to the Character 
// dropdown menu in the sidebar 
// called by initialize()
// function drawCharFilter() {
//     let charvalues = playquery.getFiltervalues().Character;
//     charvalues.sort();
//     d3.select("#charDropdown").html("");
//     let charData = d3.select("#charDropdown")
//         .selectAll("div")
//         .data(charvalues, d => {
//             return playdata.index.name + d;
//         });
    
//     let CharFilter = charData.enter()
//         .append("div")
//         .attr("id", function (d) {
//             return d;
//         });
//     CharFilter.append("input")
//         .attr("type", "checkbox")
//         .attr("class", "checkbox")
//         .property("value", function (d) {
//             return d;
//         })
//         .attr("onchange", function (d) {
//             return "highlight(" + "'" + d + "'," + "'Character'" + ")";
//         })
//         .attr("value", function (d) {
//             return d;
//         });
//     CharFilter.append("label")
//         .html(function (d) {
//             return d;
//         });
//     CharFilter.append("p");
// }



// adds checkboxes for all the meters in the play to the Meter dropdown 
// in sidebar
// called by initialize()
// function drawMeterFilter() {
//     let metervalues = [];
//     let Name = playquery.getFiltervalues().Name;
//     for (a in Name) {
//         metervalues.push(Name[a]);
//     };
//     metervalues = metervalues.sort();

//     d3.select("#metDropdown").html("");

//     let meterData = d3.select("#metDropdown")
//         .selectAll("div")
//         .data(metervalues, d => {
//             return playdata.index.name + d;
//         });

//     meterData.exit().remove();

//     let MeterFilter = meterData.enter()
//         .append("div")
//         .attr("id", function (d) {
//             return d;
//         });
//     MeterFilter.append("input")
//         .attr("type", "checkbox")
//         .attr("class", "checkbox")
//         .property("value", function (d) {
//             return d;
//         })
//         .attr("id", function (d) {
//             return d;
//         });
//     MeterFilter.attr("onchange", function (d) {
//         return "highlight(" + "'" + d + "'," + "'Meter'" + ")";
//     });
//     MeterFilter.append("label")
//         .html(function (d) {
//             return "<a href='schema.html#" + d + "'>" + d + "</a>";
//         });
//     MeterFilter.append("p");
// }

// adds checkboxes for all the metra to Metra dropdown in the sidebar
// called by initialize()
// function drawMetraFilter() {
//     let metravalues = [];
//     let metra = playquery.getFiltervalues().Metra;
//     for (a in metra) {
//         metravalues.push(metra[a]);
//     };
//     metravalues = metravalues.sort();

//     d3.select("#metraDropdown").html("");
//     let metraData = d3.select("#metraDropdown")
//         .selectAll("div")
//         .data(metravalues, d => {
//             return playdata.index.name + d;
//         });

//     metraData.exit().remove();

//     let MetraFilter = metraData.enter()
//         .append("div")
//         .attr("id", function (d) {
//             return d;
//         });
//     MetraFilter.append("input")
//         .attr("type", "checkbox")
//         .attr("class", "checkbox")
//         .property("value", function (d) {
//             return d;
//         })
//         .attr("id", function (d) {
//             return d;
//         });
//     MetraFilter.attr("onchange", function (d) {
//         return "highlight(" + "'" + d + "'," + "'Metra'" + ")";
//     });
//     MetraFilter.append("label")
//         .html(function (d) {
//             return "<a href='schema.html#" + d + "'>" + d + "</a>";
//         });
//     MetraFilter.append("p");



// }

// add types to Type dropdown in sidebar
// called by initialize()
// function drawTypeFilter() {
//     let typevalues = playquery.getFiltervalues().Type;
//     typevalues.sort();
//     d3.select("#typeDropdown").html("");
//     let TypeFilter = d3.select("#typeDropdown")
//         .selectAll("div")
//         .data(typevalues)
//         .enter()
//         .append("div")
//         .attr("id", function (d) {
//             return d;
//         });
//     TypeFilter.append("input")
//         .attr("type", "checkbox")
//         .attr("class", "checkbox")
//         .property("value", function (d) {
//             return d;
//         })
//         .attr("id", function (d) {
//             return d;
//         });
//     TypeFilter.attr("onchange", function (d) {
//         return "highlight(" + "'" + d + "'," + "'Type'" + ")";
//     });
//     TypeFilter.append("label")
//         .html(function (d) {
//             return d;
//         });
//     TypeFilter.append("p");
// }

// calls check() to add selected checkbox to list of filters, 
// then draws everything needed to display the filtered lines + info
// highlights selections in playdata.index(navigational grid) and linebox
// attached as an "onchange" attribute to each checkbox 
// (activates when a checkbox is checked)
// function highlight(box, subcat) {
//     playquery.resetFiltered(); // clear filtered-line cache
//     check(box, subcat);
//     drawLines();
//     selIndex();
//     drawStats();
//     showFilterBox();
//     drawFilterBox();
// }

// finds the sections of the play the filtered lines belong to 
// and returns them as a list
// called by drawStats()
// function getLocations() {
//     let lineholder = getLinesIncl();
//     let locations = [];
//     for (let i = 0; i < lineholder.length; i++) {
//         let part = checkPart(lineholder[i]).replace(/ /g, '');
//         if (!locations.includes(part)) {
//             locations.push(part);
//         }
//     }
//     return locations;
// }


// takes the input from the pattern search box in the sidebar and 
// returns all lines containing that metrical pattern.
// called by getLinesIncl()
function getPattern(input) {
    let patternLines = [];
    for (let i = 0; i < playdata.lines.length; i++) {
        let patternString = "";
        for (let j = 0; j < playdata.lines[i].children.length; j++) {
            let segs = [];
            if (playdata.lines[i].children[j].getAttribute("type") == "syll") {
                segs.push(playdata.lines[i].children[j]);
            } else if (playdata.lines[i].children[j].getAttribute("type") == 
                       "metron") {
                let child = playdata.lines[i].children[j];
                for (let m = 0; m < child.children.length; m++) {
                    if (child.children[m].getAttribute("type") == "syll") {
                        segs.push(child.children[m]);
                    }
                }
            }
            for (let n = 0; n < segs.length; n++) {
                if (segs[n].getAttribute("ana") == "long") {
                    patternString += "-";
                } else if (segs[n].getAttribute("ana") == "short" || 
                           segs[n].getAttribute("ana") == "shortn") {
                    patternString += "u";
                } else if (segs[n].getAttribute("ana") == "unknown") {
                    patternString += "x";
                }
            }
        }
        let regex = new RegExp(input, "g");
        if (patternString.match(regex)) {
            patternLines.push(playdata.lines[i]);
        }
        patternString = "";
    }

    return patternLines;
}


// called by pressing enter in pattern search box, searches for the 
// entered pattern.
// attached to <input type="text" id="patternSearch"> in orestes.html
// function searchPattern(e) {
//     let key = e.keyCode || e.which;
//     if (key == 13) {
//         playquery.patternValue = document.getElementById("patternSearch").value;
//         drawStats();
//         drawFilterBox();
//     }
// }

// gets all the lines selected for in the filters, returning only 
// lines held in common across categories.
function getLinesIncl() {
    let charLines = [];
    let genderLines = [];
    let meterLines = [];
    let typeLines = [];
    let godLines = [];
    let statusLines = [];
    let greekLines = [];
    let lyricLines = [];
    let metraLines = [];
    let recitativeLines = [];
    let properLines = [];
    if (playquery.patternValue != null) {
        let patternLines = getPattern(playquery.patternValue);
    }
    let queryKeys = Object.keys(playquery.query);
    for (let k = 0; k < queryKeys.length; k++) {
        if (queryKeys[k] == "Character") {
            charLines = playquery.getCharLines();
        } else if (queryKeys[k] == "Gender") {
            genderLines = playquery.getGenderLines();
        } else if (queryKeys[k] == "Meter") {
            meterLines = playquery.getMeterLines();
        } else if (queryKeys[k] == "Metra") {
            metraLines = playquery.getMetraLines();
        } else if (queryKeys[k] == "Type") {
            typeLines = playquery.getTypeLines();
        } else if (queryKeys[k] == "God") {
            godLines = playquery.getGodLines();
        } else if (queryKeys[k] == "Status") {
            statusLines = playquery.getStatusLines();
        } else if (queryKeys[k] == "Greek") {
            greekLines = playquery.getGreekLines();
        } else if (queryKeys[k] == "Lyric") {
            lyricLines = playquery.getLyricLines();
        //} else if (queryKeys[k] == "Recitative") {
        //    recitativeLines = playquery.getRecitativeLines();
        } else if (queryKeys[k] == "Proper") {
            properLines = playquery.getProperLines();
        }
    }

    let lineholder = [];
    let arrays = [];
    if (playquery.patternValue != null) {
        arrays = [charLines, genderLines, meterLines, metraLines, typeLines, 
                  godLines, statusLines, greekLines, lyricLines, 
                  recitativeLines, properLines, patternLines];
    } else {
        arrays = [charLines, genderLines, meterLines, metraLines, typeLines, 
                  godLines, statusLines, greekLines, lyricLines, 
                  recitativeLines, properLines];
    }
    let notEmptys = [];
    for (let i = 0; i < arrays.length; i++) {
        if (arrays[i].length > 0) {
            notEmptys.push(arrays[i]);
        }
    }
    if (notEmptys.length > 0) {
        lineholder = commonValue(...notEmptys);
    }
    return lineholder;
}

// returns only the lines that are included in all the categories selected 
// in the filters
// called by getLinesIncl()
function commonValue(...array) {
    let intersection = array[0].filter(function (x) {
        return array.every((y) => y.includes(x))
    })
    return intersection;
}

// return meters included in selection made by filters
// called by drawStats()
// function getMetersIncl() {
//     let trochees = playdata.sortMeters()[2];
//     let iambics = playdata.sortMeters()[0];
//     let anapests = playdata.sortMeters()[1];
//     let l = getLinesIncl();
//     let meters = [];
//     for (let i = 0; i < l.length; i++) {
//         if (!meters.includes(checkLineMeter(l[i]))) {
//             meters.push(checkLineMeter(l[i]));
//         }
//     }
//     return meters;
// }

// return types of the meters included in the selection 
// called by drawStats()
// function getTypesIncl() {
//     let trochees = playdata.sortMeters()[2];
//     let iambics = playdata.sortMeters()[0];
//     let anapests = playdata.sortMeters()[1];
//     let l = getLinesIncl();
//     let types = [];
//     for (let i = 0; i < l.length; i++) {
//         if (!types.includes(checkMeterType(l[i]))) {
//             types.push(checkMeterType(l[i]));
//         }
//     }
//     return types;
// }

//display values of "filters selected," "location of lines," and "meters included" above the filtered lines box
//add onclick functionality so that clicking a line in the filtered box jumps to and highlights the same line in the main text box
//called by highlight()
// function drawStats() {
//     let flines = getLinesIncl();
//     let locations = getLocations();
//     let lineloc = d3.select("#locOfLines")
//         .selectAll("span")
//         .data(locations, d => d);
//     lineloc.enter()
//         .append("span")
//         .attr("class", "lineloc");
//     lineloc.html(function (d) {
//             return d;
//         })
//         .style("padding-left", "10px")
//         .style("text-decoration", "underline");
//     lineloc.on("click", function (d) {
//         playquery.currentPart = d;
//         drawBody();
//         let myElement;
//         let topPos;
//         for (let i = 0; i < flines.length; i++) {
//             if (checkPart(flines[i]) == playquery.currentPart) {
//                 myElement = document.getElementById(flines[i].getAttribute("n"));
//                 topPos = myElement.offsetTop;
//                 document.getElementById('linebox').scrollTop = topPos;
//                 break;
//             }
//         }
//         if (myElement == null) {
//             document.getElementById('linebox').scrollTop = 0;
//         }
//         for (let i = 0; i < flines.length; i++) {
//             if (checkPart(flines[i]) == playquery.currentPart && flines[i] != null) {
//                 let elem = document.getElementById(flines[i].getAttribute("n"));
//                 elem.style.backgroundColor = "#cccccc";
//             }
//         }
//     });
//     lineloc.exit().remove();

//     let queryvalues = [];
//     for (let key in playquery.query) {
//         for (let i in playquery.query[key]) {
//             queryvalues.push(playquery.query[key][i]);
//         }
//     };

//     if (playquery.patternValue != null && playquery.patternValue != "") {
//         queryvalues.push(playquery.patternValue);
//     }

//     let filtersel = d3.select("#filterChecked")
//         .selectAll("span")
//         .data(queryvalues, d => d);
//     filtersel.enter()
//         .append("span");
//     filtersel.attr("id", "filterContainer");
//     filtersel.append("span").html(function (d) {
//             return d;
//         })
//         .attr("id", "filterInner");
//     filtersel.exit().remove();

//     let metvalues = getMetersIncl();
//     let metersincluded = d3.select("#metIncl")
//         .selectAll("span")
//         .data(metvalues, d => d);
//     metersincluded.enter()
//         .append("span");
//     metersincluded.attr("id", "filterContainer");
//     metersincluded.append("span").html(function (d) {
//             return d;
//         })
//         .attr("id", "filterInner");
//     metersincluded.exit().remove();

//     let typevalues = getTypesIncl();
//     let typesincluded = d3.select("#typesIncl")
//         .selectAll("span")
//         .data(typevalues, d => d);
//     typesincluded.enter()
//         .append("span");
//     typesincluded.attr("id", "filterContainer");
//     typesincluded.append("span").html(function (d) {
//             return d;
//         })
//         .attr("id", "filterInner");
//     typesincluded.exit().remove();
// };

//gets input from line search box, jumps to and highlights the line, and highlights part of play containing line in the navigational grid
//attached to <input type="text" id="searchInput"> in orestes.html
function search(e) {
    let prevHolder = playquery.currentPart;
    let key = e.keyCode || e.which;
    if (key == 13) {
        console.log("search");
        playquery.searchTerm = document.getElementById("searchInput").value;
        drawBody();
        // document.getElementById(playquery.currentPart).style.fillOpacity = ".3";
        console.log(prevHolder) 
        console.log(playquery.currentPart)
        if (prevHolder != playquery.currentPart) {
            // document.getElementById(prevHolder).style.fillOpacity = ".8";
        }
        for (let i = 0; i < playdata.lines.length; i++) {
            if (playquery.searchTerm == playdata.lines[i].getAttribute("n") || 
                playquery.searchTerm == parseFloat(playdata.lines[i].getAttribute("n"))) {
                let myElement = document.getElementById(playdata.lines[i].getAttribute("n"));
                if (myElement != null) {
                    let topPos = myElement.offsetTop;
                    document.getElementById('linebox').scrollTop = topPos;
                    myElement.style.backgroundColor = "rgba(231, 234, 30, .4)";
                }
            }
        }
        playquery.searchTerm = null;

        // setTimeout(() => {
        //     adjustTransBox();
        // }, 500)

        // click somewhere to update navigation grid (not working!) too minor a bug to care about
        selIndex()
    }
};

//checks the line search box for previous input without the user needing to repeat the search
//called in drawFilterBox(), showScansion(), and hideScansion() to re-do a search after the page has been updated in some way
function autoSearch() {
    let evt = new KeyboardEvent('keydown', {
        'keyCode': 13,
        'which': 13
    });
    if (document.getElementById("searchInput").value != null && document.getElementById("searchInput").value != "") {
        console.log("autosearch");
        search(evt);
    }
}

//get lines in the part of play selected (by navigational grid/next or previous section button/line search input)
//called by drawLines(), drawChars(), etc.
function getSection() {
    
    let currentHolder;
    let check = "";
    
    for (let i = 0; i < playdata.lines.length; i++) {
        if (playquery.searchTerm == playdata.lines[i].getAttribute("n") && playquery.searchTerm != null) {
            check = playdata.lines[i];
        } else if (playquery.searchTerm == parseFloat(playdata.lines[i].getAttribute("n")) && playquery.searchTerm != null) {
            check = playdata.lines[i];
        }
    }
    
    if (check.length != 0) {
        currentHolder = checkPart(check);
        playquery.currentPart = currentHolder;
    } else {
        currentHolder = playquery.currentPart;
    }
    
    let sectiondata = [];
    for (let i = 0; i < playdata.lines.length; i++) {
        
        part = checkPart(playdata.lines[i]);
        
        if (part == currentHolder.replace(/[ ]/g, '') || part == currentHolder.replace(/[ ]/g, "_")) {
            sectiondata.push(playdata.lines[i]);
        }
    }
    
    return sectiondata;
}

//returns a list of all the lines where the speaker changes in the middle of the line.
//called by drawLines()
function findLineSwitches() {
    // my attempt at the function
    lines = [];
    for (const [key, value] of playdata.getMidlineSpeakers().entries()) {
        if (value != "") {
            lines.push(key);
        }
    }
    return lines;
    
    // speakers = [];
    // milestones = playdata.tree.getElementsByTagName("milestone");
    // for (let i = 0; i < milestones.length; i++) {
    //     if (milestones[i].getAttribute("unit") == "speaker") {
    //         speakers.push(milestones[i])
    //     }
    // }
    // parents = [];
    // for (let i = 0; i < speakers.length; i++) {
    //     if (speakers[i].parentNode.tagName == "l") {
    //         parents.push(speakers[i].parentNode);
    //     } else if (speakers[i].parentNode.tagName == "seg") {
    //         parents.push(speakers[i].parentNode.parentNode);
    //     }
    // }
    // return parents;
}

//rewrites a line where the speaker changes in the middle so that the speaker change is reflected in the text.
//called by drawLines()
function writeSwitchLine(node) {
    let children = node.children;
    let text = "";
    
    // my attempt: get every element within the switchline and then replace all the tags that have the speaker attribute with the bolded element with the speaker string inside

    // this has the disadvantage of directly modifying the HTML. However, since the <b> tag is only used in this specific scenario within the text itself, it should not be difficult to disambiguate when the HTML is treated in other places.
    for (const tag of node.querySelectorAll("*")) {
        if (tag.getAttribute("unit") == "speaker") {
            const boldText = document.createElement("b");
            const speakerName = document.createTextNode(tag.getAttribute("n") + ": ");
            boldText.appendChild(speakerName);

            if (tag.nextElementSibling == null || tag.nextElementSibling.tagName == null || tag.nextElementSibling.tagName.toLowerCase() != "b") { // only add a tag if the next element is not a <b> tag
                tag.after(boldText);
            }
        }
    }

    for (let i = 0; i < children.length; i++) {
        // if (children[i].getAttribute("unit") == "speaker") {
        //     text += "<b>" + children[i].getAttribute("n") + ": </b>"; 
        // } else 
        if (children[i].getAttribute("type") == "metron") {
            text += writeSwitchLine(children[i]);
        } else {
            text += children[i].outerHTML; // print the actual <seg> tags
        }
    }
    return text;
}

//displays a label for the current section of the play above the main line box
//called by drawLines()
function drawSectionHead() {
    
    let sectiondata = getSection();

    let section = playquery.currentPart;
    
    d3.selectAll('.parent').each(function (d) { // basically, if playquery.currentPart is equivalent to one of the sections on the navigation grid without a space, then copy the navigation grid name (with the space).
        if (d.name.replace(/[ ]/g, '') == section) {
            section = d.name;
        }
    });
    
    let start = sectiondata[0].getAttribute("n");
    let length = sectiondata.length;
    let end = sectiondata[length - 1].getAttribute("n");
    document.getElementById("sectionHead").innerHTML = section + " (" + start + "-" + end + ")";
}


// fill linebox with text from the current section of the play, 
// highlight selected lines
// called by drawEverything


function drawLines() {
    drawSectionHead(); 
    switches = findLineSwitches(); 
    d3.selectAll(".line").remove(); // **removes every line**
    let sectiondata = getSection(); 
    let playlines = d3.select("#horizontalscrolldiv")
        .selectAll("div")
        .data(sectiondata); // **populates div tags with corresponding lines**
    sectiondata = [];
    playlines.enter()
        .append("div"); // **adds the same number of div tags as there are lines in the sectiondata**

    // console.log(sectiondata);
    playlines.attr("class", function (d) 
        {
            // console.log(d.getAttribute("indent"));
            if (d.getAttribute("indent") != null) {
                if (d.getAttribute("indent") == "True") {
                    return "line indent_line";
                }
                else {
                    return "line";
                }
            }
            else {
                return "line";
            }
        })
        .attr("id", function (d) {
            if (d.getAttribute("n") != null) {
                return d.getAttribute("n");
            }
        })
	//This is where the code to handle <surplus> tags and such should go.
	//d appears to be a single line, the children then are all the tags within the <l> tags
        .html(function (d) {
            const start_time = Date.now();		
            
	    /*let children = d.children;
            for (let i = 0; i < children.length; i++) {
                if (children[i].tagName == "del" && children[i].innerHTML[0] != "[") {
                    children[i].innerHTML = "[" + children[i].innerHTML + "]";
                }
            }
	    */
	    //Currently looks for surplus tags outside of a line and wraps the lines enclosed with [ ]
   	    /*let surplus = $(d).closest('surplus');		
		if (surplus.length > 0) {
			console.log(d);		
			if ($(d).children('span[class="editorial_mark open"]').length == 0) {
				$(d).prepend('<span class="editorial_mark open">[</span>');
			}				
			if ($(d).children('span[class="editorial_mark close"]').length == 0) {
				$(d).append('<span class="editorial_mark close">]</span>');
			}
		}*/
	//These bits of code search for editorial tags and places the correct editorial mark on the tag
	// square brackets <surplus>, angle brackets <supplied>, and daggers <sic>
		// It should look for an attribute named part with values "I", "M", "F", or null
		// On a part="I" OR a part=null, we should see $(d).prepend('<span class="editorial_mark open">X</span>') 
										//where X is the proper mark for the tag
		// On a part="M" nothing should be prepended nor appended
		// On a part="F" OR a second part=null, we should see $(d).append('<span class="editorial_mark close">Y</span>') 
										//where Y is the proper mark for the tag
	    let surplus_marks = ['\[', '\]'];	let supplied_marks = ['&lt;', '&gt;'];	let sic_marks = ['\†'];

	    //Process the <surplus> tags to render
	    let surplus = $(d).find("surplus"); 
	    for (let s = 0; s < surplus.length; s++){
	    	if ($(surplus[s]).attr("part") == "I"){
	    		//place the opening editorial mark
	    		if ($(surplus[s]).find("span[class*='editorial_mark']").length == 0){
	    			$(surplus[s]).prepend('<span class="editorial_mark open">'+surplus_marks[0]+'</span>');
	    		}
	    	}
	    	else if ($(surplus[s]).attr("part") == "M"){
	    		//do nothing
	    	}
	    	else if ($(surplus[s]).attr("part") == "F"){
	    		//place the closing editorial mark
	    		if ($(surplus[s]).find("span[class*='editorial_mark']").length ==0){
                    $(surplus[s]).find("milestone[unit='word']").last().remove() // remove the last word milestone
	       			$(surplus[s]).append('<span class="editorial_mark close">'+surplus_marks[1]+'</span>');
                    $(surplus[s]).append("<milestone unit='word'></milestone>");
	    		}
	    	}
	    	else{
	    		//place both opening and closing marks
	    		if ($(surplus[s]).find("span[class*='editorial_mark']").length == 0){
	    			$(surplus[s]).prepend('<span class="editorial_mark open">'+surplus_marks[0]+'</span>');

                    $(surplus[s]).find("milestone[unit='word']").last().remove()
	    			$(surplus[s]).append('<span class="editorial_mark close">'+surplus_marks[1]+'</span>');
                    $(surplus[s]).append("<milestone unit='word'></milestone>");
	    		}
	    	}
	    }
	    
	    //Process the <supplied> tags to render
	    let supplied = $(d).find("supplied");
	    for (let s = 0; s < supplied.length; s++){
	    	if ($(supplied[s]).attr("part") == "I"){
	    		if ($(supplied[s]).find("span[class*='editorial_mark']").length == 0){
	    			$(supplied[s]).prepend('<span class="editorial_mark open">'+supplied_marks[0]+'</span>');
	    		}
	    	}
	    	else if ($(supplied[s]).attr("part") == "M"){
	    		//do nothing
	    	}
	    	else if ($(supplied[s]).attr("part") == "F"){
	    		if ($(supplied[s]).find("span[class*='editorial_mark']").length == 0){
                    $(supplied[s]).find("milestone[unit='word']").last().remove()
	    			$(supplied[s]).append('<span class="editorial_mark close">'+supplied_marks[1]+'</span>');
                    $(supplied[s]).append("<milestone unit='word'></milestone>");
	    		}
	    	}
	    	else {
	    		if ($(supplied[s]).find("span[class*='editorial_mark']").length == 0){
	    			$(supplied[s]).prepend('<span class="editorial_mark open">'+supplied_marks[0]+'</span>');

                    $(supplied[s]).find("milestone[unit='word']").last().remove()
	    			$(supplied[s]).append('<span class="editorial_mark close">'+supplied_marks[1]+'</span>');
                    $(supplied[s]).append("<milestone unit='word'></milestone>");
	    		}
	    	}
	    }
	    
	    let sic = $(d).find("sic");
	    for (let s = 0; s < sic.length; s++){
	    	if ($(sic[s]).attr("part") == "I"){
	    		if ($(sic[s]).find("span[class*='editorial_mark']").length == 0){
	    			$(sic[s]).prepend('<span class="editorial_mark open">'+sic_marks[0]+'</span>');
	    		}
	    	}
	    	
	    	else if ($(sic[s]).attr("part") == "M"){
	    		//do nothing
	    	}
	    	
	    	else if ($(sic[s]).attr("part") == "F"){
	    		if ($(sic[s]).find("span[class*='editorial_mark']").length == 0){
                    $(sic[s]).find("milestone[unit='word']").last().remove()
	    			$(sic[s]).append('<span class="editorial_mark close">'+sic_marks[0]+'</span>');
                    $(sic[s]).append("<milestone unit='word'></milestone>");
	    		}
	    	}
	    	
	    	else{
	    		if ($(sic[s]).find("span[class*='editorial_mark']").length == 0){
	    			$(sic[s]).prepend('<span class="editorial_mark open">'+sic_marks[0]+'</span>');

                    $(sic[s]).find("milestone[unit='word']").last().remove()
	    			$(sic[s]).append('<span class="editorial_mark close">'+sic_marks[0]+'</span>');
                    $(sic[s]).append("<milestone unit='word'></milestone>");
	    		}
	    	}
	    
	    }

        let gaps = $(d).find("gap");

        for (let g = 0; g < gaps.length; g++) {
            if ($(gaps[g]).text().length == 0) {
                let extent = $(gaps[g]).attr("extent")
                if (extent) {
                    $(gaps[g]).append("〈" + "\xA0".repeat(parseInt($(gaps[g]).attr("extent")) * 7) + "〉")
                } else {
                    $(gaps[g]).text("GAP MISSING EXTENT!")
                }
            }
        }

        // $(d).find("gap").each( function (index) { // for each gap, multiply it based on its extent
        //     console.log("gap funct", index, $(this))
        //     if ($(this).attr("extent")) {
        //         $(this).append("<span>" + "&nbsp;".repeat(parseInt($(this).attr("extent")) * 6) + "</span>")
        //     } else {
        //         $(this).text("GAP MISSING EXTENT!")
        //     }
        // });
            
            if (d.innerHTML == null) {
                // console.log("\t" + d.getAttribute("n").toString() + " .html() " + (Date.now()-start_time).toString());
                return "<br>";
            } else {
                if (switches.includes(d)) {
                    let text = writeSwitchLine(d);
                    // console.log("switched " + d.getAttribute("n"));
                    // console.log("\t" + d.getAttribute("n").toString() + " .html() " + (Date.now()-start_time).toString());
                    return text;
                } else {
                    // console.log(d.getAttribute("n") + " " + d.innerHTML);
                    // console.log("\t" + d.getAttribute("n").toString() + " .html() " + (Date.now()-start_time).toString());
                    return d.innerHTML;
                }
            }
        });
    // console.log("after playlines.attr().attr().html()" + (Date.now()-start_time).toString());
    drawButtons();
    // console.log("after drawButtons()" + (Date.now()-start_time).toString());
    let scanButton = document.getElementById("hideScansion");
    addScansion(scanButton);
    // console.log("after addScansion()" + (Date.now()-start_time).toString());
    // console.log("drawLines() " + (Date.now()-start_time).toString());
};


// helper function for the next function below
function getTextWithSpace(segTag) {
    let children = segTag.childNodes;
    let text = "";
    for (let i = 0; i < children.length; i++) {
        if (children[i].nodeType == Node.ELEMENT_NODE) {
            // if (children[i].getAttribute("unit") == "word") {
            //     text += " " + getTextWithSpace(children[i]);
            // } else 
            if (children[i].getAttribute("type") == "metron") {
                text += getTextWithSpace(children[i]);
            } else {
                text += " " + getTextWithSpace(children[i]);
            }
        } else if (children[i].nodeType == Node.TEXT_NODE) {
            text += children[i].wholeText;
        }
    }

    return text;
}
let vowelSet = new Set(['α', 'ε', 'η', 'ͱ', 'ι', 'ο', 'υ', 'ω', 'ᾳ', 'ῃ', 'ῳ', 'ά', 'έ', 'ή', 'ί', 'ό', 'ύ', 'ώ', 'ᾴ', 'ῄ', 'ῴ', 'ᾰ', 'ῐ', 'ῠ', 'ᾶ', 'ῆ', 'ῖ', 'ῦ', 'ῶ', 'ᾷ', 'ῇ', 'ῷ', 'ϊ', 'ϋ', 'ΐ', 'ΰ', 'Ἆ', 'Ἦ', 'Ἶ', 'Ὦ', 'ᾎ', 'ᾞ', 'ᾮ', 'Ἆ', 'Ἦ', 'Ἶ', 'Ὦ', 'ᾎ', 'ᾞ', 'ᾮ', 'ὰ', 'ὲ', 'ὴ', 'ὶ', 'ὸ', 'ὺ', 'ὼ', 'ᾲ', 'ῂ', 'ῲ', 'ᾱ', 'ῑ', 'ῡ', 'ἁ', 'ἑ', 'ἡ', 'ἱ', 'ὁ', 'ὑ', 'ὡ', 'ᾁ', 'ᾑ', 'ᾡ', 'ῥ', 'ἅ', 'ἕ', 'ἥ', 'ἵ', 'ὅ', 'ὕ', 'ὥ', 'ᾅ', 'ᾕ', 'ᾥ', 'ἇ', 'ἧ', 'ἷ', 'ὗ', 'ὧ', 'ᾇ', 'ᾗ', 'ᾧ', 'ἃ', 'ἓ', 'ἣ', 'ἳ', 'ὃ', 'ὓ', 'ὣ', 'ᾃ', 'ᾓ', 'ᾣ', 'ἀ', 'ἐ', 'ἠ', 'ἰ', 'ὀ', 'ὐ', 'ὠ', 'ᾀ', 'ᾐ', 'ᾠ', 'ῤ', 'ἄ', 'ἔ', 'ἤ', 'ἴ', 'ὄ', 'ὔ', 'ὤ', 'ᾄ', 'ᾔ', 'ᾤ', 'ἆ', 'ἦ', 'ἶ', 'ὖ', 'ὦ', 'ᾆ', 'ᾖ', 'ᾦ', 'ἂ', 'ἒ', 'ἢ', 'ἲ', 'ὂ', 'ὒ', 'ὢ', 'ᾂ', 'ᾒ', 'ᾢ', 'Α', 'Ε', 'Η', 'Ͱ', 'Ι', 'Ο', 'Υ', 'Ω', 'ᾼ', 'ῌ', 'ῼ', 'Ά', 'Έ', 'Ή', 'Ί', 'Ό', 'Ύ', 'Ώ', 'Ᾰ', 'Ῐ', 'Ῠ', 'Ϊ', 'Ϋ', 'Ὰ', 'Ὲ', 'Ὴ', 'Ὶ', 'Ὸ', 'Ὺ', 'Ὼ', 'Ᾱ', 'Ῑ', 'Ῡ', 'Ἁ', 'Ἑ', 'Ἡ', 'Ἱ', 'Ὁ', 'Ὑ', 'Ὡ', 'ᾉ', 'ᾙ', 'ᾩ', 'Ἅ', 'Ἕ', 'Ἥ', 'Ἵ', 'Ὅ', 'Ὕ', 'Ὥ', 'ᾍ', 'ᾝ', 'ᾭ', 'Ἇ', 'Ἧ', 'Ἷ', 'Ὗ', 'Ὧ', 'ᾏ', 'ᾟ', 'ᾯ', 'Ἃ', 'Ἓ', 'Ἣ', 'Ἳ', 'Ὃ', 'Ὓ', 'Ὣ', 'ᾋ', 'ᾛ', 'ᾫ', 'Ἀ', 'Ἐ', 'Ἠ', 'Ἰ', 'Ὀ', 'Ὠ', 'ᾈ', 'ᾘ', 'ᾨ', 'Ἄ', 'Ἔ', 'Ἤ', 'Ἴ', 'ῄ', 'Ὤ', 'ᾌ', 'ᾜ', 'ᾬ', 'Ἆ', 'Ἦ', 'Ἶ', 'Ὦ', 'ᾎ', 'ᾞ', 'ᾮ', 'Ἂ', 'Ἒ', 'Ἢ', 'Ἲ', 'Ὂ', 'Ὢ', 'ᾊ', 'ᾚ', 'ᾪ']);

// a function to calculate where to put the longs/shorts to better match the position of the vowels
function getBackgroundPercentage(segTag) {
    let toIterate = getTextWithSpace(segTag);

    if (toIterate.length <= 1) {
        return "33%";
    }

    let vowelScore = 0;
    let numVowels = 0;
    for (let i = 0; i < toIterate.length; i++) {
        if (vowelSet.has(toIterate[i])) {
            vowelScore += i;
            numVowels++;
        }
    }

    if (numVowels <= 0) {
        return "50%";
    }

    let percentage = Math.floor((vowelScore / numVowels / (toIterate.length-1)) * 100)

    if (percentage > 100) {
        return "100%";
    } else if (percentage < 0) {
        return "0%";
    }

    return percentage + "%";
}

// sorts syllables into lists of long, short, unknown, and 
// brevis in longo syllables and wraps them in <span> tags 
// so they can be assigned the right icon in style.css
// when called, checks if the "showScansion" or "hideScansion" buttons 
// have already been pressed and shows/hides scansion accordingly.
// called by drawLines()
function replaceHtml(el, html) {
    /*@cc_on // Pure innerHTML is slightly faster in IE
        oldEl.innerHTML = html;
        return oldEl;
    @*/
    var newEl = el.cloneNode(false);
    newEl.innerHTML = html;
    el.parentNode.replaceChild(newEl, el);
    /* Since we just removed the old element from the DOM, return a reference
    to the new element, which can be used to restore variable references. */
    return newEl;
};

function addScansion(b) {
    const start_time = Date.now();
    let segs = document.getElementsByTagName("seg");
    // console.log("after segs " + (Date.now()-start_time).toString())
    // let longs = [];
    // for (let i = 0; i < segs.length; i++) {
    //     if (segs[i].getAttribute("ana") == "long") {
    //         let newHTML = "<span class = 'longbg' style='background-position-x: " + getBackgroundPercentage(segs[i]) + "'></span>" + segs[i].innerHTML;
    //         // let newHTML = "<span class = 'longbg'></span>" + segs[i].innerHTML;
    //         segs[i].innerHTML = newHTML;
    //         longs.push(segs[i]);
    //     }
    // }

   

    let longs = [];
    let shorts = [];
    let unknowns = [];
    let brevis = [];
    let metra = [];
    for (let i = 0; i < segs.length; i++) {
        if (segs[i].getAttribute("ana") == "long") {
            let newHTML = "<span class = 'longbg' style='background-position-x: " + getBackgroundPercentage(segs[i]) + "'></span>" + segs[i].innerHTML;
            // // let newHTML = "<span class = 'longbg'></span>" + segs[i].innerHTML;
            segs[i] = replaceHtml(segs[i], newHTML)
            longs.push(segs[i]);
        } else if (segs[i].getAttribute("ana") == "short") {
            let newHTML = "<span class = 'shortbg' style='background-position-x: " + getBackgroundPercentage(segs[i]) + "'></span>" + segs[i].innerHTML;
            // let newHTML = "<span class = 'shortbg'></span>" + segs[i].innerHTML;
            segs[i] = replaceHtml(segs[i], newHTML)
            shorts.push(segs[i]);
        } else if (segs[i].getAttribute("ana") == "unknown") {
            unknowns.push(segs[i]);
        } else if (segs[i].getAttribute("ana") == "shortn") {
            brevis.push(segs[i]);
        }
        if (segs[i].getAttribute("type") == "metron") {
            metra.push(segs[i]);
        }
    }
    // console.log("after for-loop 1 " + (Date.now()-start_time).toString())
    // let unknowns = [];
    // for (let i = 0; i < segs.length; i++) {
    //     if (segs[i].getAttribute("ana") == "unknown") {
    //         unknowns.push(segs[i]);
    //     }
    // }
    // let brevis = [];
    // for (let i = 0; i < segs.length; i++) {
    //     if (segs[i].getAttribute("ana") == "shortn") {
    //         brevis.push(segs[i]);
    //     }
    // }
    // let metra = [];
    // for (let i = 0; i < segs.length; i++) {
    //     if (segs[i].getAttribute("type") == "metron") {
    //         metra.push(segs[i]);
    //     }
    // }
    
    // console.log("after for-loop 2 " + (Date.now()-start_time).toString())
    // let synapheia = [];
    // for (let i = 0; i < milestones.length; i++){
    //     if (milestones[i].getAttribute("unit") == "synapheia"){
    //         synapheia.push(milestones[i]);
    //     }		
    // }
    updateScansionDisplay(b);
    // console.log("after add/remove " + (Date.now()-start_time).toString())

    // console.log(longs);
    // console.log(shorts);
}

function updateScansionDisplay(b) {
    let segs = document.getElementsByTagName("seg");
    let caesuras = document.getElementsByClassName("caesura");
    let milestones = document.getElementsByTagName("milestone");
    let periods = [];
    let synapheia = [];
    for (let i = 0; i < milestones.length; i++) {
        if (milestones[i].getAttribute("unit") == "period") {
            periods.push(milestones[i]);
        } else if (milestones[i].getAttribute("unit") == "synapheia"){
            synapheia.push(milestones[i]);
        }	
    }

    if (b.style.zIndex == 5) {
        for (let i = 0; i < segs.length; i++) {
            segs[i].classList.add("showScan");
        }
        // for (let i = 0; i < longs.length; i++) {
        //     longs[i].classList.add("showScan");
        // }
        // for (let i = 0; i < shorts.length; i++) {
        //     shorts[i].classList.add("showScan");
        // }
        // for (let i = 0; i < unknowns.length; i++) {
        //     unknowns[i].classList.add("showScan");
        // }
        // for (let i = 0; i < brevis.length; i++) {
        //     brevis[i].classList.add("showScan");
        // }
        // for (let i = 0; i < metra.length; i++) {
        //     metra[i].classList.add("showScan");
        // }
        for (let i = 0; i < periods.length; i++) {
            periods[i].classList.add("showScan");
        }
        for (let i = 0; i < synapheia.length; i++){
            synapheia[i].classList.add("showScan");
        }
        for (let i = 0; i < caesuras.length; i++) {
            caesuras[i].classList.add("showScan");
        }
        
        //this is the second loop with periods in the if
        //for (let i = 0; i < periods.length; i++) {
        //    periods[i].innerHTML = periods[i].getAttribute("ana");
        //}
    } else {
        for (let i = 0; i < segs.length; i++) {
            segs[i].classList.remove("showScan");
        } 
        // for (let i = 0; i < longs.length; i++) {
        //     longs[i].classList.remove("showScan");
        // }
        // for (let i = 0; i < shorts.length; i++) {
        //     shorts[i].classList.remove("showScan");
        // }
        // for (let i = 0; i < unknowns.length; i++) {
        //     unknowns[i].classList.remove("showScan");
        // }
        // for (let i = 0; i < brevis.length; i++) {
        //     brevis[i].classList.remove("showScan");
        // }
        // for (let i = 0; i < metra.length; i++) {
        //     metra[i].classList.remove("showScan");
        // }
        for (let i = 0; i < periods.length; i++) {
            periods[i].classList.remove("showScan");
        }
        for (let i =0; i < synapheia.length; i++){
            synapheia[i].classList.remove("showScan");
        }
        for (let i = 0; i < caesuras.length; i++) {
            caesuras[i].classList.remove("showScan");
        }
        //for (let i = 0; i < periods.length; i++) {
        //    periods[i].innerHTML = "";
        //}
    }
}

// highlights the meter sections that include the lines selected by the filters
// called in highlight()
function selIndex() {
    d3.selectAll('.parent').style("fill-opacity", function (d) {
        if (d.name == playquery.currentPart || 
            d.name.replace(/ /g, '') == playquery.currentPart) {
            return .3;
        } else {
            return .85
        }
    });
    let selects = [];
    let qlength = 0;
    for (let q in playquery.query) {
        if (playquery.query[q].length > 0) {
            qlength += 1;
        }
    }
    let linenumbers = [];
    for (let x in playdata.lines) {
        let d = playdata.lines[x];
        let count = 0;
        for (let q in playquery.query) {
            if (playquery.query[q].includes(d[q])) {
                count = count + 1;
            }
        }
        if (count == qlength && count != 0) {
            if (d.getAttribute('n') != null) {
                b = parseFloat(d.getAttribute("n"));
                linenumbers.push(b);
                d3.selectAll(".child").each(function (a) {
                    start = parseFloat(a.start);
                    end = parseFloat(a.end);
                    if (start <= b && b <= end) {
                        if (!selects.includes(a)) {
                            selects.push(a);
                            linenumbers.push(b);
                        }
                    }
                });
            }
        }
    }
    d3.selectAll(".child").style("fill-opacity", function (a) {
        if (selects.includes(a)) {
            return .3;
        }
    });
    return linenumbers;
}

//displays the comments (made in the XML) for the relevant section of the play below the text.
//called by drawEverything()
function drawComments() {
    let sectiondata = getSection();
    let comments = d3.select("#commentbox")
        .selectAll("div")
        .data(sectiondata);
    comments.enter()
        .append("div");
    comments.attr("class", "comment feature")
        .html(function (d) {
            let children = d.children;
            for (let i = 0; i < children.length; i++) {
                if (children[i].getAttribute("unit") == "comment" && children[i].getAttribute("ana") != null) {
                    return d.getAttribute("n") + ": " + children[i].getAttribute("ana");
                }
            }
        });
    comments.exit().remove();
}

//fill line number box
//called by drawEverything()
function drawNumbers() {
    let sectiondata = getSection();
    let numbers = d3.select("#numbox")
        .selectAll("div")
        .data(sectiondata);
    numbers.enter()
        .append("div");
    numbers.attr("class", "number feature")
        .html(function (d) {
            if (d.getAttribute("n") != null && d.tagName == "l") {
                return d.getAttribute("n");
            } else {
                return "<br>"
            }
        });
    numbers.exit().remove();
};


//finds the character who speaks a given line.
//called by drawChars()
function getCharacter(d) {
    let speeches = playdata.getSpeeches();
    let char = "";
    let keys = Object.keys(speeches);
    let j; // to keep track of the element of the line array
    let speech;

    // goes through every item (array) in the speech dictionary
    loop1: for (let i = 0; i < keys.length; i++) {
        speech = speeches[keys[i]];

        // checks if each element of the item (array) corresponds to a line
        for (j = 0; j < speech.length; j++) {
            if (speech[j] == d) {
                char = keys[i].replace(/[0-9]/g, ''); 
                break loop1;
            }
        }
    }

    // // if there is some item, then check if any line before the current has a midline speaker. Take the latest midline speaker as the current speaker
    let midlineSpeaker = "";
    for (let k = 0; k < j; k++) {
        let potentialMS = playdata.getMidlineSpeakers().get(speech[k]);
        if (potentialMS != "") {
            midlineSpeaker = potentialMS;
        }
    }

    if (midlineSpeaker != "") {
        char = midlineSpeaker;
    }

    return char;
}

//finds the name of a speaker who starts speaking in the middle of a given line
//called by drawChars()
function getMidlineSpeaker(line) {
    let hc = "";
    if (line.innerHTML.includes("speaker")) {
        // a simpler, and more thorough search
        query = line.querySelectorAll("milestone[unit=\"speaker\"]")
        hc = query[query.length-1].getAttribute("n");

        // let c = line.children;
        // let children = [];
        // for (let i = 0; i < c.length; i++) {
        //     if (c[i].getAttribute("type") == "metron") {
        //         for (let j = 0; j < c[i].children.length; j++) {
        //             children.push(c[i].children[j]);
        //         }
        //     } else {
        //         children.push(c[i]);
        //     }
        // }
        // for (let k = 0; k < children.length; k++) {
        //     if (children[k].getAttribute("unit") == "speaker") {
        //         hc = children[k].getAttribute("n");
        //     }
        // }
    }
    return hc;
}

//fill the box to the left of the main text box that displays the speaker of each line
//called by drawEverything()
function drawChars() {
    let sectiondata = getSection();
    let currentChar;
    let newcurrentChar;
    let characters = d3.select("#charbox")
        .selectAll("div")
        .data(sectiondata);
    characters.enter()
        .append("div");
    let holdChar = "";
    let testVar = 0;
    characters.attr("class", "characters feature")
       .style("background", function (d) {
            holdChar = getMidlineSpeaker(d);
            char = getCharacter(d);
            if (char != currentChar) {
                currentChar = char;
                return "#669ac1";  // Changed to blue
            } else if (char != holdChar && holdChar != "") {
                newcurrentChar = char;
                return "#669ac1";  // Changed to blue
            } else {
                return "transparent";
            }
        });
    holdChar = "";
    newcurrentChar = "";
    characters.html(function (d) {
        let setNext;
        holdChar = getMidlineSpeaker(d);
        char = getCharacter(d);

        // if the current character is different from the midline speaker, set the next character to be checked as the midline speaker. If it's not, set the character to be checked as the normal speaker.
        if (char != holdChar && holdChar != "") {
            setNext = holdChar;
        } else {
            setNext = char;
        }

        // if the character is different from the next character, print their name. If not, then print a line break.

        if (char != newcurrentChar) {
            newcurrentChar = setNext;
            return char;
        } else {
            newcurrentChar = setNext;
            return "<br>";
        }
    });
    characters.exit().remove();
};

//updates the next and previous section buttons when the section changes.
//called by drawLines()
function drawButtons() {
    prevPart = playquery.playdata.getPrevPart(playquery.currentPart);
    nextPart = playquery.playdata.getNextPart(playquery.currentPart);
    if (prevPart == undefined) {
        d3.select("#prevSection").style("display", "none");
    } else {
        d3.select("#prevSection").style("display", "inline-block");
    }
    if (nextPart == undefined) {
        d3.select("#nextSection").style("display", "none");
    } else {
        d3.select("#nextSection").style("display", "inline-block");
    }
    d3.select("#prevButton").text("Previous section: " + prevPart);
    d3.select("#nextButton").text("Next section: " + nextPart);
};

// draws the accordion panel to the right of the text that displays 
// metrical information.
// called by drawEverything()
function drawPanel() {
    let sectiondata = getSection();
    drawMeter(sectiondata);
    drawLyric(sectiondata);
    drawType(sectiondata);
    drawResponsion(sectiondata);
};

// if a line contains metra, calls checkMeterName() to determine the meter 
// of the line; if not, returns the name of the non-lyric meter section 
// the line belongs to.
// called by drawMeter() and getMeterLines()

function checkLineMeter(l) {
    
    let result = "";
  
/*
    let children = l.children;    
    if (children[0].getAttribute("type") == "metron") {
        result = checkMeterName(l);
        return result;
    }
    
    if (l.parentNode.getAttribute("type") == "ia3") {
        result = "iambic3";
    } else if (l.parentNode.getAttribute("type") == "anapests") {
        result = "anapest";
    } else if (l.parentNode.getAttribute("type") == "tr4^") {
        result = "trochaic4"
    } 
*/
    
    if (l.getAttribute('met') > '') {
        result = l.getAttribute('met');
    }
    else {
        result = l.parentNode.getAttribute("met");
    }

    return result;

}

// checks the name of the metron segments in a line to determine 
// the entire line's meter
// called by checkLineMeter()
function checkMeterName(node) {
    
    children = [];
    for (let i = 0; i < node.children.length; i++) {
        if (node.children[i].getAttribute("met") != null) {
            children.push(node.children[i]);
        }
    }
    
    let holder = "";
    let c = false;
    
    if (node.getAttribute("type") == "metron") {
        for (let j = 0; j < playdata.meterdata.length; j++) {
            if (playdata.meterdata[j].Ours == node.getAttribute("met")) {
                holder = playdata.meterdata[j].name;
                c = true;
            }
        }
        if (!c) {
            holder = node.getAttribute("met");
        }
        return holder;
    }
    
    let meterstring = "";
    let meters = [];
    
    for (let i = 0; i < children.length; i++) {
        let met = children[i].getAttribute("met");
        met = met.replace(/\?/g, '');
        let check = false;
        for (let j = 0; j < playdata.meterdata.length; j++) {
            if (playdata.meterdata[j].Ours == met) {
                check = true;
                meters.push(playdata.meterdata[j].name);
                if (children.length > 1 && i != (children.length - 1)) {
                    meterstring += playdata.meterdata[j].name + " + ";
                } else {
                    meterstring += playdata.meterdata[j].name;
                }
            }
        }
        if (!check) {
            meterstring += met;
        }
    }

    let repeat = 0;
    for (let i = 0; i < (meters.length); i++) {
        if (meters[i] == meters[i + 1]) {
            repeat += 1;
        }
    }
    if (repeat == (meters.length - 1) && repeat > 0) {
        meterstring = meters[0] + (repeat + 1);
    }

    if (children.length > 0 && meterstring == "") {
        meterstring = "?"
    }
    
    return meterstring;
}

// draw meter box in the panels to the right of the main text box
// called by drawPanel()
function drawMeter(data) {

    var tooltipDiv = d3.select("#body").append("div")
         .attr("class", "tooltip")
         .style("opacity", 0);

    let iambics = playdata.sortMeters()[0];
    let anapests = playdata.sortMeters()[1];
    let trochees = playdata.sortMeters()[2];
    let meter = d3.select("#meterbox")
        .selectAll("div")
        .data(data);
    meter.enter()
        .append("div");

    meter.attr("class", "meter feature")
        .html(function (d) {
            if (checkLineMeter(d) != "") {
                return checkLineMeter(d);
            } else {
                for (let i = 0; i < iambics.length; i++) {
                    children = iambics[i].children;
                    for (let j = 0; j < children.length; j++) {
                        if (children[j] == d) {
                            return "ia3";
                        }
                    }
                }
                for (let i = 0; i < anapests.length; i++) {
                    children = anapests[i].children;
                    for (let j = 0; j < children.length; j++) {
                        if (children[j] == d) {
                            return "anap";
                        }
                    }
                }
                for (let i = 0; i < trochees.length; i++) {
                    children = trochees[i].children;
                    for (let j = 0; j < children.length; j++) {
                        if (children[j] == d) {
                            return "tr4^";
                        }
                    }
                }
                return "<br>";
            }
        }
    )
    .on('mouseover', function (d, i) {
            d3.select(this).transition()
                .duration('100')
                .attr("r", 7);

            let tooltipData = metersUtilities.getFullData(d3.select(this)[0][0].innerHTML);

            if (tooltipData) {
                //add a function call in here to get the full name of the d.name
                tooltipDiv.html(formatToolTip(tooltipData))
                    .style("left", (d3.event.pageX + 10) + "px")
                    .style("top", (d3.event.pageY - 15) + "px")
                    .transition()
                    .duration(100)
                    .style("z-index",100)
                    .style("opacity", 1); 
            }
        })
        .on('mouseout', function (d, i) {
            d3.select(this).transition()
            .duration('500')
            .attr("r", 5);

            tooltipDiv.transition()
                .duration(100)
                .style("opacity", 0);
        });
    
    meter.exit().remove();
};

//This function takes abbreviation data from meterscomplete and formats it for 
//the right side tooltip 
function formatToolTip(abbreviationData) {
    return '<table>' +  
    '<tr>' +
        '<th>Meter</th>' +
        '<th>Paradigm</th>'+
        '<th>Type</th>'+
    '</tr>'+
    '<tr>'+
        '<td>' + tooltipData.name + '</td>'+
        '<td>' + tooltipData.paradigm +  '</td>' +
        '<td>' + tooltipData.type + '</td>' +
    '</tr>'+
    '</table>';
}
// finds any comments made about the meter in the orestesMeters.json 
// file and returns them
// called by mouseOver()
function getNotes(node) {
    let notes = "";
    let children = [];
    for (let i = 0; i < node.children.length; i++) {
        if (node.children[i].getAttribute("type") == "metron") {
            children.push(node.children[i]);
        }
    }
    for (let j = 0; j < children.length; j++) {
        let name = children[j].getAttribute("met");
        for (let i = 0; i < playdata.meterdata.length; i++) {
            if (playdata.meterdata[i].Ours == 
                name && playdata.meterdata[i].notes != null) {
                notes += playdata.meterdata[i].notes;
            }
        }
    }
    if (notes == "") {
        notes = "none";
    }
    return notes;
}

// displays notes about the meters in the panel when the meter is hovered over
// called by drawMeters()
function mouseOver(d) {
    let position = this.offsetTop - d3.select("#meterbox").node().scrollTop;
    d3.select("#popup-text")
        .html("<div>" + getNotes(d) + "</div>")
        .style("visibility", "visible")
        .style("top", position + "px");
};

// hides meter pop-up note when the mouse is not on top of the meter
// called by drawMeters()
function mouseOut() {
    d3.select("#popup-text").style("visibility", "hidden");
};

//fills the lyric/nonlyric panel box
//called by drawPanel()

function drawLyric(data) {
    let anapests = playdata.sortMeters()[1];
    let lyric = d3.select("#lyricSidebarbox")
        .selectAll("div")
        .data(data);
    lyric.enter()
        .append("div");
    lyric.attr("class", "lyric feature")
        .html(function (d) {
            
            let meter = checkLineMeter(d);

            let lyric_non_lyric = 'lyric';
    
            if (d.getAttribute('type') > '') {
                lyric_non_lyric = d.getAttribute('type');
            }
            else {
                //if (d.parentNode.getAttribute('ana') > '') {
                //    if (d.parentNode.getAttribute('type') == 'lyric' ||
                //        d.parentNode.getAttribute('type') == 'nonlyric') {
                //        lyric_non_lyric = d.parentNode.getAttribute("type");
                //    }
                //}
                lyric_non_lyric = d.parentNode.getAttribute("type");
            }

            //console.log('d', d, 
            //            'd.parentNode', d.parentNode,
            //            'meter', meter, 'lyric_non_lyric', lyric_non_lyric);

            return lyric_non_lyric;
        }
    );

    lyric.exit().remove();
};

//calls checkMeterType() if a given line is in a lyric meter; if not, returns the non-lyric section the line belongs to
//called by getTypeLines()
function checkLineType(l) {
    let hold = l;
    while (hold.parentNode != null && hold.tagName != "div2") {
        hold = hold.parentNode;
    }
    let result = "";
    if (hold.getAttribute("met") == "ia3") {
        result = "iambic";
    } else if (hold.getAttribute("met") == "anapests") {
        result = "anapestic";
    } else if (hold.getAttribute("met") == "tr4^") {
        result = "trochaic"
    } else {
        result = checkMeterType(l);
    }
    return result;
}

//checks metron segments and matches them to the type given in orestesMeters.json for that meter
//called by checkLineType()
function checkMeterType(node) {
    children = [];
    for (let i = 0; i < node.children.length; i++) {
        if (node.children[i].getAttribute("met") != null) {
            children.push(node.children[i]);
        }
    }
    typestring = "";
    types = [];
    for (let i = 0; i < children.length; i++) {
        let met = children[i].getAttribute("met");
        met = met.replace(/\?/g, '');
        for (let j = 0; j < playdata.meterdata.length; j++) {
            if (playdata.meterdata[j].Ours == met) {
                types.push(playdata.meterdata[j].type);
                if (children.length > 1 && i != (children.length - 1)) {
                    typestring += playdata.meterdata[j].type + " + ";
                } else {
                    typestring += playdata.meterdata[j].type;
                }
            }
        }
    }
    let holder = 0;
    for (let i = 0; i < types.length; i++) {
        if (types[i] == types[i + 1]) {
            holder += 1;
        }
    }
    if (holder == (types.length - 1)) {
        typestring = types[0];
    }
    if (children.length > 0 && typestring == "") {
        typestring = "?"
    }
    if (typestring == "null" || typestring == null) {
        return "none";
    }
    return typestring;
}

// fills the type panel box 
// called by drawPanel()
function drawType(data) {
    let iambics = playdata.sortMeters()[0];
    let anapests = playdata.sortMeters()[1];
    let trochees = playdata.sortMeters()[2];
    let type = d3.select("#typebox")
        .selectAll("div")
        .data(data);
    type.enter()
        .append("div");
    type.attr("class", "type feature")
        .html(function (d) {
            // if the line number falls within the range of the "type override" array, then set the type of this line to the respective type override.
            let line_num = d.getAttribute("n");
            let s = line_num.search(/[^0-9]/);
            line_num = s > 0 ? line_num.slice(0, s): line_num; // gets the first digits of the line number (Ex: '1000' --> '1000', '1789-90' --> '1789', '138b' --> '138')
            let tOArr = playdata.playmeterdata["type_override"];
            for (let i = 0; i < tOArr.length; i++) {
                if (line_num >= tOArr[i]["start"] && line_num <= tOArr[i]["end"]) {
                    return tOArr[i]["type"];
                }
            }

            let meter = checkLineMeter(d);
            let meter_type = '<br>';

            for (var a = 0; a < playdata.meterdata.length; a++) {
                if (playdata.meterdata[a]['ours'] == meter) {
                    meter_type = playdata.meterdata[a]['type'];
                    return meter_type;
                }
            }
        });

    document.getElementById("type").classList.add("panelShown")
    type.exit().remove();
};

// for a line in a strophe, finds the corresponding line 
// in the antistrophe and vice versa.
// called by getAllPartners()
function getPartner(node, type) {
    let partner = "";
    let strophes = playdata.getStrophes()[0];
    let antistrophes = playdata.getStrophes()[1];
    if (type == "strophe") {
        for (let i = 0; i < strophes.length; i++) {
            let children = strophes[i].children;
            for (let j = 0; j < children.length; j++) {
                if (children[j] == node) {
                    partner = antistrophes[i].children[j];
                }
            }
        }
    } else if (type == "antistrophe") {
        for (let i = 0; i < antistrophes.length; i++) {
            let children = antistrophes[i].children;
            for (let j = 0; j < children.length; j++) {
                if (children[j] == node) {
                    partner = strophes[i].children[j];
                }
            }
        }
    }
    if (partner != "") {
        return partner;
    } else {
        return null;
    }
}

// finds all the correspondences between strophe line and antistrophe line
// in a given section.
// called by drawResponsion()
function getAllPartners(section) {
    let partners = [];
    for (let i = 0; i < section.length; i++) {
        let set = [];
        let part = getPartner(section[i], "strophe");
        if (part != null) {
            set.push(section[i]);
            set.push(part);
            partners.push(set);
        }
    }
    return partners;
}

// fills the responsion panel box 
// called by drawPanel()
function drawResponsion(data) {
    getAllPartners(data);
    let strophes = playdata.getStrophes()[0];
    let antistrophes = playdata.getStrophes()[1];
    let holder = 0;
    let responsion = d3.select("#responsionbox")
        .selectAll("div")
        .data(data);
    responsion.enter()
        .append("div")
        .attr("class", "responsion feature");
    responsion.attr("id", function (d) {
        return "responsion" + d.getAttribute("n");
    })
    responsion.html(function (d) {
        for (let i = 0; i < strophes.length; i++) {
            children = strophes[i].children;
            for (let j = 0; j < children.length; j++) {
                if (children[j] == d) {
                    holder += 1;
                    return "strophe" + strophes[i].getAttribute("n");
                }
            }
        }
        for (let i = 0; i < antistrophes.length; i++) {
            children = antistrophes[i].children;
            for (let j = 0; j < children.length; j++) {
                if (children[j] == d) {
                    holder += 1;
                    return "antistrophe" + strophes[i].getAttribute("n");
                }
            }
        }
        return "<br>"
    }).style("background-color", "transparent");
    holder >= 1 ? document.getElementById("responsion").classList.add("panelShown") : document.getElementById("responsion").classList.remove("panelShown");
    responsion.exit().remove();

    //highlight corresponding strophe/antistrophe line
    responsion.on("click", function (d) {
        let type = "";
        for (let i = 0; i < strophes.length; i++) {
            children = strophes[i].children;
            for (let j = 0; j < children.length; j++) {
                if (children[j] == d) {
                    type = "strophe";
                }
            }
        }
        if (type != "strophe") {
            type = "antistrophe";
        }
        let lineElem = document.getElementById(d.getAttribute("n"));
        let partner = document.getElementById(getPartner(d, type).getAttribute("n"));
        let respPartner = document.getElementById("responsion" + getPartner(d, type).getAttribute("n"));
        d3.selectAll(".line").style("background-color", "transparent");
        d3.selectAll(".responsion").style("background-color", "transparent");
        d3.select(this).style("background-color", "#d6eadf");
        lineElem.style.backgroundColor = "#d6eadf";
        partner.style.backgroundColor = "#d6eadf";
        respPartner.style.backgroundColor = "#d6eadf";
    });
    responsion.exit().remove();
};

//highlight lines corresponding to a meter box in the navigation grid if the box is clicked.
//called by click()
function highlightText(a) {
    // clearBoxes();
    d3.selectAll('.line').each(function (d) {
        if (a.start == d.getAttribute("n")) { // skip down to the line if the click is of the subsection
            let myElement = document.getElementById(d.getAttribute("n"));
            let topPos = myElement.offsetTop;
            document.getElementById('linebox').scrollTop = topPos;
            return;
        }
    }).style("background-color", function (d) {
        let x = parseFloat(d.getAttribute("n"));
        if (x >= parseFloat(a.start) && x <= parseFloat(a.end)) {
            return "gainsboro";
        }
    });
};

//fills the translation text box
//called by drawEverything()
function drawTranslation() {
    // console.log("drawTranslation")
    document.getElementById('transbox').scrollTop = 0;
    let sectiondata = playdata.translationdata.filter(function (d) {
        // console.log(playquery.currentPart)
        if (d.part == playquery.currentPart || d.part.replace(/ /g, '') == playquery.currentPart) // if the current part or the current part with its spaces removed is the same as the json part
        {
            return d;
        }
    });
    // console.log(sectiondata)
    let translation = d3.select("#transbox")
        .selectAll("div")
        .data(sectiondata);
    translation.enter()
        .append("div");
    translation.attr("class", "translation feature")
        .html(function (d) {
            // console.log("d exists " + (d != null).toString())
            if (d.translation == null) {
                return "<br><p class = 'directions'>" + d.directions + "</p>" + "";
            }

            transl_ln_div = d.translation.replaceAll(/\[.{1,4}\]/g, "<span id='$&' >$&</span>")
            
            if (d.character == null) {
                return "<p class = 'directions'>" + d.directions + "</p>" + "<p class = 'trans'>" + transl_ln_div + "</p>";
            } else if (d.directions == null) {
                return "<p class = 'char'>" + d.character + "</p>" + "<p class = 'trans'>" + transl_ln_div + "</p>";
            } else {
                return "<p class = 'char'>" + d.character + "</p>" + "<p class = 'directions'>" + d.directions + "</p><p class = 'trans'>" + transl_ln_div + "</p><p>";
            }
        });
    translation.exit().remove();
    
    setTimeout(() => {
        adjustTransBox();
    }, 500)
};

function adjustTransBox() {
    let transbox = document.getElementById("transbox");
    let linebox = document.getElementById("linebox");

    // method for determining which line the scroll is on
    let sec = getSection();
    let start = sec[0].getAttribute("n");
    let s = start.search(/[^0-9]/);
    start = parseInt(s > 0 ? start.slice(0, s): start);
    let scrollTo = Math.floor((linebox.scrollTop/linebox.scrollHeight*sec.length + start) / 5) * 5;
    // console.log(scrollTo)

    let toSpan = document.getElementById("[" + scrollTo.toString() + "]")

    if (scrollTo < start || isNaN(scrollTo) || toSpan == null) {
        transbox.scrollTop = 0
    } else {
        // method for scrolling to the exact place of a <div> element in the box 
        transbox.scrollBy(0, toSpan.getBoundingClientRect().top-transbox.getBoundingClientRect().top);
    }

}

//updates and displays most of the major elements of the page
function drawEverything() {
    drawBody();
    drawIndex(playdata.index);
    selIndex();
};

//global values that set the attributes of the navigation grid
let scalar = .33;
let Col1Width = 0;
let w = 300,
    h = 700,
    x = d3.scale.linear().domain([scalar, 1]).range([Col1Width, w]),
    y = d3.scale.linear().range([0, h]);

function getTooltip(abbreviation) {
    return metersUtilities.getFullName(abbreviation);
}

//adds a container div for the navigation grid to html of the page
let vis = d3.select("#body").append("div")
    .attr("class", "chart")
    .attr("id", "chartBox")
    .style("width", w + "px")
    .style("height", h + "px")
    .append("svg:svg")
    .attr("width", w)
    .attr("height", h);

//uses the d3 function partition() to organize the elements in orests.json
//result is used as the dataset for drawIndex()
let partition = d3.layout.partition()
    .value(function (d) {
        return d.size;
    });

//draws the navigation grid (index)
//called by drawEverything()
function drawIndex(index) {

    let root = index;
    let ix = vis.selectAll("g")
        .data(partition.nodes(root), 
              d => d.name + d.depth + d.start + d.end + d.value);

    ix.exit().remove();

    var tooltipDiv = d3.select("#body").append("div")
         .attr("class", "tooltip")
         .style("opacity", 0);

    let g = ix.enter().append("svg:g")
        .attr("transform", function (d) {
            return "translate(" + x(d.y) + "," + y(d.x) + ")";
        })
        .on("click", function (d) {
            d.click++;
            if ((d.click) % 2 != 0) {
                return click(d);
            }
            if ((d.click) % 2 == 0) {
                return click(d.parent);
            }
        })
        .on('mouseover', function (d, i) {
            d3.select(this).transition()
                .duration('100')
                .attr("r", 7);

            let tooltipText = getTooltip(d.name);

            if (tooltipText) {
                //add a function call in here to get the full name of the d.name
                tooltipDiv.html(tooltipText)
                    .style("left", (d3.event.pageX + 10) + "px")
                    .style("top", (d3.event.pageY - 15) + "px")
                    .transition()
                    .duration(100)
                    .style("z-index",100)
                    .style("opacity", 1); 
            }
        })
        .on('mouseout', function (d, i) {
            d3.select(this).transition()
            .duration('500')
            .attr("r", 5);

            tooltipDiv.transition()
                .duration(100)
                .style("opacity", 0);
        });

    
    let kx = (w - Col1Width) / (1 - scalar),
        ky = h / 1;

    g.append("svg:rect")
        .attr("width", function (d) {
            return d.children ? root.dy * kx : root.dy * kx * .4;
        })
        .attr("height", function (d) {
            return d.dx * ky;
        })
        .attr("class", function (d) {
            return d.children ? "parent" : "child";
        })
        .attr("id", function (d) {
            return d.name.replace(/ /g, '');
        })
        .style("fill", function (d) { //these are the colors of dominant meters
            if (d.name == "ia3" || d.name == "ia") {
                return "#669ac1";
            } else if (d.name == "anap") {
                return "#a093c6";
            } else if (d.name == "DA") {
                return "#ffd275";
            } else if (d.name == "DO") {
                return "#ff6d44";
            } else if (d.name == "ED") {
                return "#6fc45c";
            } else if (d.name == "AC") {
                return "#a5d6d0";
            } else if (d.name == "tr4^") {
                return "#ffef75";
            } else if (d.name == "TR") {
                return "#5d9b9b"
            } else if (d.name == "IA") {
                return "#ffadad";
            } else if (d.name == "Mixed") {
                return "#fac291";
            } else if (d.name == "DO & ia3") {
                return "#fac291";
            } else if (d.name == "AC & ia3") {
                return "#fac291";
            } else if (d.name == "dac-ep") {
		return "#ebce6a";
	    }

        })
        .style("fill-opacity", .8)
        .style("z-index", 0);

    g.append("svg:text")
        .attr("class", "text")
        .attr("transform", transform)
        .attr("dy", ".5em")
        .style("opacity", function (d) {
            if (d.depth == 0) {
                return 0;
            } else {
                return d.dx * ky > 12 ? 1 : 0;
            }
        })
        .style("font-size", "11px")
        .text(function (d) {
            if (d.children) {
                return d.name + " (" + d.start + "-" + d.end + ")";
            } else {
                return d.name;
            }
        });

    g.append("svg:text")
        .attr("class", "start")
        .attr("transform", startTransform)
        .attr("dy", ".8em")
        .style("font-size", "11px")
        .style("opacity", 0)
        .text(function (d) {
            return d.children ? "" : d.start;
        });

    g.append("svg:text")
        .attr("class", "end")
        .attr("transform", endTransform)
        .attr("dy", ".2em")
        .style("font-size", "11px")
        .style("opacity", 0)
        .text(function (d) {
            return d.children ? "" : d.end;
        });

   
        
    //if the user clicks outside the navigational grid in an empty spot of the page, zooms the navigational grid back out automatically
    d3.select(window)
        .on("click", function () {
            let target = d3.event.target;
            if (d3.event.clientX < 175) {
                d3.event.stopPropagation();
            } else if (target.classList.contains("versionButton") || target.classList.contains("feature") || target.classList.contains("inner") ||
                target.classList.contains("panelLabel") || target.classList.contains("panelButton") ||
                target.id == "searchInput" || target.classList.contains("translationButton") || target.classList.contains("scansionButton")) {
                d3.event.stopPropagation();
            } else {
                click(root);
            }
        });


    //controls the expansion/collapse of the navigation grid when section boxes are clicked.
    //attached to each box of the navigation grid by drawIndex()
    function click(d) {
        console.log("click d", d)
        if (d.name != root.name) { // that is, if you clicked a section but the navigation grid is not yet enlarged on one section
            if (!d.children) { // I think the idea is if you clicked on one of the subsections on the side
                playquery.currentPart = d.parent.name;
            } else {
                playquery.currentPart = d.name;
            }
            drawBody();
            document.getElementById('linebox').scrollTop = 0;
            if (!d.children) {
                highlightText(d);
                d3.event.stopPropagation(); // this should stop the event from triggering other events, but it doesn't
                return;
            }
        }


        kx = (w - Col1Width) / (d.y ? (1 - d.y) : (1 - scalar));
        ky = h / d.dx;
        x.domain([d.y ? d.y : scalar, 1]).range([Col1Width, w]);
        y.domain([d.x, d.x + d.dx]);

        let t = g.transition()
            .duration(d3.event.altKey ? 7500 : 750)
            .attr("transform", function (d) {
                return "translate(" + x(d.y) + "," + y(d.x) + ")";
            });

        t.select("rect")
            .attr("width", function (d) {
                return d.children ? root.dy * kx : root.dy * kx * .4;
            })
            .attr("height", function (d) {
                return d.dx * ky;
            }).attr("fill-opacity", function (d) {
                if (d.children) {
                    return d.name === playquery.currentPart ? .35 : .85;
                }
            });

        selIndex();

        t.select("text.text")
            .attr("transform", transform)
            .style("opacity", function (d) {
                if (d.depth == 0) {
                    return 0;
                } else {
                    return d.dx * ky > 12 ? 1 : 0;
                }
            });

        t.select("text.start")
            .attr("transform", startTransform)
            .style("opacity", function (d) {
                return d.dx * ky > 90 ? 1 : 0;
            });

        t.select("text.end")
            .attr("transform", endTransform)
            .style("opacity", function (d) {
                return d.dx * ky > 90 ? 1 : 0;
            });

        d3.event.stopPropagation();
    }

    function transform(d) {
        return "translate(8," + d.dx * ky / 2.3 + ")";
    }

    function startTransform() {
        return "translate(" + w / 9 + "," + 10 + ")";
    }

    function endTransform(d) {
        return "translate(" + w / 9 + "," + (d.dx * ky - 10) + ")";
    }
};


let setPlay = function(playname) {

    let play = plays[playname];
    d3.select("#title").html(playname);
    d3.select("#author").html(play.playwright);
    // d3.select("#statslink").attr("href", `${playname}Statistics.html`);
    d3.queue()
        .defer(d3.json, `${playname}Structure.json`) 
        .defer(d3.xml, `${playname}.xml`)
        .defer(d3.json, `${playname}Translation.json`)
        .defer(d3.json, `meterscomplete.json`)
        .defer(d3.json, `${playname}Characters.json`)
        .defer(d3.json, `${playname}Meters.json`)
        .await(initialize);
}

let getPlayFromURL = function() {
    let params = new URLSearchParams(document.location.search.substring(1));
    return params.get("play");
}

let setPlayList = function(plays, elementId) {
    let playlist = document.getElementById(elementId);
    playlist.innerHTML = "";
    for (play in plays) {
        playlist.insertAdjacentHTML(
            'beforeend', 
            `<a href="index.html?play=${play}">${play}</a>`);
    };
    
    playlist.insertAdjacentHTML(
        'beforeend', 
        `<br/>`);
    
    playlist.insertAdjacentHTML(
        'beforeend', 
        `<span id="menu_spacer_1"> </span>`);
        
    
    // playlist.insertAdjacentHTML(
    //     'beforeend', 
    //     `<a href="../combined_statistics/">Statistics</a>`);
}

let initializePlays = function(error, plays_arg) {
    console.log("initializePlays plays_arg", plays_arg)

    plays = plays_arg;
    setPlayList(plays, "plays");
    let newplay = getPlayFromURL();
    if (newplay) {
        setPlay(newplay);
    } else {
        setPlay("Orestes");
    }
}
