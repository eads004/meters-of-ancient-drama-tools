
var STAT_DATA = {
    'line_counts': '',
    'dimension_keys': '',
    'lines_cf': '',
    'lines_dims': {},
};

var dimension_keys = ['play', 'div1_type', 'lyric_non_lyric', 
                        'speaker', 'gender', 'status', 'isGod', 
                        'isGreek', 'line_meter_name', 'line_meter_type'];

function get_line_counts() {
                
    STAT_DATA['line_counts'] = {};
                
    for (var a = 0; a < dimension_keys.length; a++) {
    
        var dimension_key = dimension_keys[a];
        
        STAT_DATA['line_counts'][dimension_key] = {};
                            
        for (var b = 0; b < STAT_DATA['filtered_lines'].length; b++) {
        
            if (STAT_DATA['filtered_lines'][b][dimension_key] in STAT_DATA['line_counts'][dimension_key]) {
            }
            else {
                STAT_DATA['line_counts'][dimension_key][STAT_DATA['filtered_lines'][b][dimension_key]] = 0;
            }
            
            STAT_DATA['line_counts'][dimension_key][STAT_DATA['filtered_lines'][b][dimension_key]] += 1;
        }
    }
}

function set_cf_and_dimensions() {
    
    get_line_counts();
    
    STAT_DATA['lines_cf'] = crossfilter(STAT_DATA['filtered_lines']);
    STAT_DATA['lines_dims'] = {}
    
    for (var a = 0; a < dimension_keys.length; a++) {
        
        var dimension_key = dimension_keys[a];
    
        STAT_DATA['lines_dims'][dimension_key] = STAT_DATA['lines_cf'].dimension(
            function(d) {
                return d[dimension_key];
            }
        );
    }
}

function handle_byeach_change() { // called by the "Arraged By" dropdown box
 
    var by_each = $('#byEach_select').val();
    
    if (by_each == 'line_meter_name' || by_each == 'line_meter_type') {
        $('#in').css({'display': 'none'});
    }
    else {
        $('#in').css({'display': 'block'});
    }
}

function draw_charts() { // called by the "Submit" button
    
    var play = $('#whichPlay_select').val();
    var how_many = $('#howMany_select').val();   // Number Of: Lines, Metra, etc
    var by_each = $('#byEach_select').val();     // Arranged By: Meter, Character, etc
    var type_select = $('#type_select').val();   // Specific meter type
    var meter_select = $('#meter_select').val(); // Specific meter name
    
    if (by_each == 'Meter' || by_each == 'Meter') { // is this correctly formulated?
        type_select = '';
        meter_select = '';
    }
        
    STAT_DATA['filtered_lines'] = [];
        
    for (var a = 0; a < STAT_DATA[how_many].length; a++) {  // for each line/word/syllable, depending on what's selected

        // STAT_DATA[how_many] gets the csv file; this for-loop is a preliminary filter

        var keep_line = true;
        
        if (!(play == 'All' || play == STAT_DATA[how_many][a]['play'])) { // filter by play 
            keep_line = false;
        }
        
        if (!(type_select == '' || type_select == STAT_DATA[how_many][a]['line_meter_type'])) { // filter by meter type
            keep_line = false;
        }
        
        if (!(meter_select == '' || meter_select == STAT_DATA[how_many][a]['line_meter_name'])) { // filter by meter name
            keep_line = false;
        }

        if (document.getElementById('excludeTrimeters').checked && STAT_DATA[how_many][a]['line_meter_name'] == "iambic trimeter") {
            // if the checkbox is checked, don't include iambic trimeter lines
            keep_line = false;
        }

        if (document.getElementById('excludeBrackets').checked && STAT_DATA[how_many][a]['uncertain'] == "true") {
            // if checkbox is checked, don't include the uncertain lines
            keep_line = false
        }

        if (document.getElementById('includeOnlyPattern').value != "" && (how_many == "Lines" || how_many == "Metra") && !STAT_DATA[how_many][a]["syllables_string"].includes(document.getElementById('includeOnlyPattern').value)) {
            // if the pattern field is not empty, the how many option is set to Lines or Metra, and the line/metra does not contain the pattern, do not include the count.
            keep_line = false
        }
        
        if (keep_line == true) {
            STAT_DATA['filtered_lines'].push(STAT_DATA[how_many][a])
        }
    }      
    
    // do the actual counting and filtering. The results are stored in STAT_DATA['lines_dims'].

    set_cf_and_dimensions();
    
    var lines_list = STAT_DATA['lines_dims'][by_each].group().reduceCount(
        function(d) { 
            return d.n_lines; 
        }
    ).all();
    
    lines_list.sort(
        function(a, b) {
          if (a['value'] < b['value']) return -1;
          if (a['value'] > b['value']) return 1;
          return 0;
        }
    );
    lines_list.reverse();
                
    var total = 0;
    var x = [];
    var y = [];
    var pct = [];
    
    for (var a = 0; a < lines_list.length; a++) {
        total += lines_list[a]['value'];
        x.push(lines_list[a]['key']);
        y.push(lines_list[a]['value']);
    }
    
    for (var a = 0; a < lines_list.length; a++) {
        pct.push(Math.round(lines_list[a]['value'] / total * 100.0) + '%');
    }
    
    var display_by_each = by_each;
    if (display_by_each == 'speaker') {
        display_by_each = 'Character';
    }
    
    console.log('display_by_each', display_by_each);
    
    var title = '<b>' + play + ' ' + how_many + ' by ' + display_by_each +
                    ' ' + type_select + ' ' + meter_select + '</br>';

    //table
    var tablevalues = [
        x,
        y,
        pct
    ]
    
    var tabledata = [{
        type: 'table',
        header: {
            values: [
                [by_each],
                [how_many],
                ["<b>% of Total</b>"]
            ],
            align: "center",
            line: {
                width: 1,
                color: 'black'
            },
            fill: {
                color: "gray"
            },
            font: {
                family: "Lucida Bright, Georgia",
                size: 12,
                color: "white"
            }
        },
        cells: {
            values: tablevalues,
            align: "center",
            line: {
                color: "black",
                width: 1
            },
            font: {
                family: "Lucida Bright, Georgia",
                size: 11,
                color: ["black"]
            }
        }
    }]

    Plotly.newPlot('table', tabledata);
    
    //bar chart
    var bartrace = [{
        x: x,
        y: y,
        type: 'bar'
    }];
    var barlayout = {
        title: title,
        font: {
            family: "Lucida Bright, Georgia",
            size: 12,
        },
        xaxis: {
            tickangle: 45,
            automargin: true
        },
    };
    
    Plotly.newPlot("barChart", bartrace, barlayout);

    //pie chart
    var piedata = [{
        values: y,
        labels: x,
        type: 'pie',
        textinfo: 'percent',
        textposition: 'inside'
    }];

    var pielayout = {
        title: title,
        font: {
            family: "Lucida Bright, Georgia"
        }
    };

    Plotly.newPlot('pieChart', piedata, pielayout);
}

$('document').ready(function() {
    
    $('#inputs').css({'display': 'none'});
        
    queue().defer(d3.csv, 'data/lines.csv')
        .defer(d3.csv, 'data/words.csv')
        .defer(d3.csv, 'data/syllables.csv')
        .await(start);

    function start(error, lines_arg, words_arg, syllables_arg) { 
        
        // initialize STAT_DATA with csv values in "Lines", "Words", "Syllables"

        STAT_DATA['Lines'] = lines_arg;
        STAT_DATA['Words'] = words_arg;
        STAT_DATA['Syllables'] = syllables_arg;
        
        // derender loading message, and render inputs

        $('#inputs').css({'display': 'block'});
        $('#loading_message').css({'display': 'none'});

        // initialize type/meter dropdown boxes from the line_meter_type and line_meter_name of the lines in lines.csv
        
        var meter_types = [];
        var meter_names = [];
        
        for (var a = 0; a < STAT_DATA['Lines'].length; a++) {
            meter_types.push(STAT_DATA['Lines'][a]['line_meter_type']);
            meter_names.push(STAT_DATA['Lines'][a]['line_meter_name']);
        }
        
        var meter_types = Array.from(new Set(meter_types.sort()));
        var meter_names = Array.from(new Set(meter_names.sort()));
        
        for (var a = 0; a < meter_types.length; a++) {
            $('#type_select').append('<option value="' + meter_types[a] + '">' + 
                                        meter_types[a] + '</option>');
        }
        
        for (var a = 0; a < meter_names.length; a++) {
            $('#meter_select').append('<option value="' + meter_names[a] + '">' + 
                                        meter_names[a] + '</option>');
        }
    };
    
});
