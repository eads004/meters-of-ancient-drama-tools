function getSpeakers() {
    var speakers = [];
    for (var i = 0; i < lines.length; i++) {
        var sib = lines[i].previousElementSibling;
        if (sib != null && sib.tagName == "milestone" && sib.getAttribute("unit") == "speaker") {
            speakers.push(sib);
        }
    }
    return speakers;
};

function getFilterValues() {
    var filtervalues = {
        Character: [],
        Metra: [],
        Syllable: []
    };
    speakers = getSpeakers();
    for (var i = 0; i < speakers.length; i++) {
        speaker = speakers[i].getAttribute("n");
        if (!filtervalues.Character.includes(speaker) && speaker != "") {
            filtervalues.Character.push(speaker);
        }
    }

    for (var i = 0; i < lines.length; i++) {
        var children = [];
        for (var j = 0; j < lines[i].children.length; j++) {
            if (lines[i].children[j].getAttribute("type") == "metron") {
                children.push(lines[i].children[j]);
            }
        }
        for (var j = 0; j < children.length; j++) {
            var met = children[j].getAttribute("met");
            if (!filtervalues.Metra.includes(met) && met != null && met != "") {
                filtervalues.Metra.push(met);
            }
        }
    }



    return filtervalues;
};

function getSpeeches() {
    var speakers = getSpeakers();
    var speeches = {};
    for (var i = 0; i < speakers.length; i++) {
        var lineholder = [];
        var holder = speakers[i];
        while (holder.nextElementSibling != null && holder.nextElementSibling.getAttribute('unit') != "speaker" && holder.nextElementSibling.tagName != "milestone") {
            lineholder.push(holder.nextElementSibling);
            holder = holder.nextElementSibling;
            if (holder.nextElementSibling == null && holder.parentElement.nextElementSibling != null && holder.parentElement.nextElementSibling.tagName == "div2"  && holder.parentElement.nextElementSibling.firstElementChild.getAttribute('unit') != "speaker") {
                lineholder.push(holder.parentElement.nextElementSibling.firstElementChild)
                holder = holder.parentElement.nextElementSibling.firstElementChild;
            }
            if (holder.nextElementSibling != null && holder.nextElementSibling.tagName == "milestone" && holder.nextElementSibling.getAttribute('unit') != "speaker") {
                holder = holder.nextElementSibling;
            }
        }
        key = i + speakers[i].getAttribute('n')
        speeches[key] = lineholder;
    }
    return speeches;
}

function checkNonLyric(l) {
    var iambics = sortMeters()[0];
    var anapests = sortMeters()[1];
    var trochees = sortMeters()[2];
    for (var k = 0; k < iambics.length; k++) {
        var c = iambics[k].children;
        for (var h = 0; h < c.length; h++) {
            if (c[h] == l) {
                return "iambic3";
            }
        }
    }
    for (var k = 0; k < anapests.length; k++) {
        var c = anapests[k].children;
        for (var h = 0; h < c.length; h++) {
            if (c[h] == l) {
                return "anapest";
            }
        }
    }
    for (var k = 0; k < trochees.length; k++) {
        var c = trochees[k].children;
        for (var h = 0; h < c.length; h++) {
            if (c[h] == l) {
                return "trochaic4";
            }
        }
    }
}


function checkLineMeter(l) {
    var hold = l;
    while (hold.parentNode != null && hold.tagName != "div2") {
        hold = hold.parentNode;
    }
    var result = "";
    var children = l.children;
    if (children[0].getAttribute("type") == "metron") {
        result = checkMeterName(l);
        return result;
    }
    if (hold.getAttribute("type") == "ia3") {
        result = "iambic3";
    } else if (hold.getAttribute("type") == "anapests") {
        result = "anapest";
    } else if (hold.getAttribute("type") == "tr4^") {
        result = "trochaic4"
    } 
    return result;

}


function checkMeterName(node) {
    children = [];
    for (var i = 0; i < node.children.length; i++) {
        if (node.children[i].getAttribute("met") != null) {
            children.push(node.children[i]);
        }
    }
    var holder = "";
    var c = false;
    if (node.getAttribute("type") == "metron") {
        for (var j = 0; j < meterdata.length; j++) {
            if (meterdata[j].Ours == node.getAttribute("met")) {
                holder = meterdata[j].name;
                c = true;
            }
        }
        if (!c) {
            holder = node.getAttribute("met");
        }
        return holder;
    }
    var meterstring = "";
    var meters = [];
    for (var i = 0; i < children.length; i++) {
        var met = children[i].getAttribute("met");
        met = met.replace(/\?/g, '');
        var check = false;
        for (var j = 0; j < meterdata.length; j++) {
            if (meterdata[j].Ours == met) {
                check = true;
                meters.push(meterdata[j].name);
                if (children.length > 1 && i != (children.length - 1)) {
                    meterstring += meterdata[j].name + " + ";
                } else {
                    meterstring += meterdata[j].name;
                }
            }
        }
        if (!check) {
            meterstring += met;
        }
    }

    var repeat = 0;
    for (var i = 0; i < (meters.length); i++) {
        if (meters[i] == meters[i + 1]) {
            repeat += 1;
        }
    }
    if (repeat == (meters.length - 1) && repeat > 0) {
        meterstring = meters[0] + (repeat + 1);
    }

    if (children.length > 0 && meterstring == "") {
        meterstring = "?"
    }
    return meterstring;
}

function checkSpeaker(line) {
    char = ""
    speeches = getSpeeches();
    keys = Object.keys(speeches);
    for (var i = 0; i < keys.length; i++) {
        for (var j = 0; j < speeches[keys[i]].length; j++) {
            if (line == speeches[keys[i]][j]) {
                char = keys[i];
            }
        }
    }
    return char;
}

function checkGender(line) {
    var female = [];
    for (var i = 0; i < chardata.length; i++) {
        if (chardata[i].gender == "female") {
            female.push(chardata[i].name);
        }
    }   
    var char = checkSpeaker(line);
    if (female.includes(char)) {
        return "female";
    } else {
        return "male";
    }
}

function sortMeters() {
    var meters = [];
    var iambics = [];
    var anapests = [];
    var trochees = [];
    var divs = tree.getElementsByTagName("div2");
    for (var i = 0; i < divs.length; i++) {
        if (divs[i].getAttribute("type") == "ia3") {
            iambics.push(divs[i])
        } else if (divs[i].getAttribute("type") == "anapests") {
            anapests.push(divs[i])
        } else if (divs[i].getAttribute("type") == "tr4^") {
            trochees.push(divs[i])
        }
    }
    meters.push(iambics);
    meters.push(anapests);
    meters.push(trochees);
    return meters;
}


function universalCountSylls(type, inwhat) {
    if (type == "Meter") {
        var im = countSylls(getLinesByMeter());
        return im;
    } else if (type == "Type") {
        var t = countSylls(getLinesByType());
        return t;
    } else if (type == "Character") {
        if (inwhat != "") {
            var common = commonValue(CharacterCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var cSylls = countSylls(CharacterCountLines());
            return cSylls;
        }
    } else if (type == "Gender") {
        if (inwhat != "") {
            var common = commonValue(GenderCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var gSylls = countSylls(GenderCountLines());
            return gSylls;
        }
    } else if (type == "Status") {
        if (inwhat != "") {
            var common = commonValue(StatusCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var sSylls = countSylls(StatusCountLines());
            return sSylls;
        }
    } else if (type == "God/Human") {
        if (inwhat != "") {
            var common = commonValue(GodCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var sSylls = countSylls(GodCountLines());
            return sSylls;
        }
    } else if (type == "Greek/Non-Greek") {
        if (inwhat != "") {
            var common = commonValue(GreekCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var grSylls = countSylls(GreekCountLines());
            return grSylls;
        }
    } else if (type == "Lyric/Non-Lyric") {
        if (inwhat != "") {
            var common = commonValue(LyricCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var lyrSylls = countSylls(LyricCountLines());
            return lyrSylls;
        }
    } else if (type == "Recitative/Non-Recitative") {
        if (inwhat != "") {
            var common = commonValue(RecitativeCountLines(), countMeterLines(inwhat))
            return countSylls(common);
        } else {
            var rSylls = countSylls(RecitativeCountLines());
            return rSylls;
        }
    }
}

function countSylls(somelines) {
    var sylls = [];
    for (var m = 0; m < somelines.length; m++) {
        sylls[m] = [];
        for (var i = 0; i < somelines[m].length; i++) {
            var l = somelines[m][i];
            var children = l.children;
            for (var j = 0; j < children.length; j++) {
                if (children[j].getAttribute("type") == "syll") {
                    sylls[m].push(children[j]);
                } else if (children[j].getAttribute("type") == "metron") {
                    var gc = children[j].children;
                    for (var k = 0; k < gc.length; k++) {
                        if (gc[k].getAttribute("type") == "syll") {
                            sylls[m].push(gc[k]);
                        }
                    }
                }
            }
        }
    }

    return sylls;
}


function universalCountMetra(type, inwhat) {
    if (type == "Meter") {
        var im = countMetra(getLinesByMeter());
        return im;
    } else if (type == "Type") {
        var t = countMetra(getLinesByType());
        return t;
    } else if (type == "Character") {
        if (inwhat != "") {
            var common = commonValue(CharacterCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var cMet = countMetra(CharacterCountLines());
            return cMet;
        }
    } else if (type == "Gender") {
        if (inwhat != "") {
            var common = commonValue(GenderCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var gMet = countMetra(GenderCountLines());
            return gMet;
        }
    } else if (type == "Status") {
        if (inwhat != "") {
            var common = commonValue(StatusCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var sMet = countMetra(StatusCountLines());
            return sMet;
        }
    } else if (type == "God/Human") {
        if (inwhat != "") {
            var common = commonValue(GodCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var sMet = countMetra(GodCountLines());
            return sMet;
        }
    } 
    else if (type == "Greek/Non-Greek") {
        if (inwhat != "") {
            var common = commonValue(GreekCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var grMet = countMetra(GreekCountLines());
            return grMet;
        }
    } else if (type == "Lyric/Non-Lyric") {
        if (inwhat != "") {
            var common = commonValue(LyricCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var lyrMet = countMetra(LyricCountLines());
            return lyrMet;
        }
    } else if (type == "Recitative/Non-Recitative") {
        if (inwhat != "") {
            var common = commonValue(RecitativeCountLines(), countMeterLines(inwhat))
            return countMetra(common);
        } else {
            var rMet = countMetra(RecitativeCountLines());
            return rMet;
        }
    }
}



function countMetra(somelines) {
    var metra = [];
    var mCount = [];
    var trochees = sortMeters()[2];
    var iambics = sortMeters()[0];
    var anapests = sortMeters()[1];
    for (var m = 0; m < somelines.length; m++) {
        metra[m] = [];
        mCount[m] = 0;
        for (var i = 0; i < somelines[m].length; i++) {
            var l = somelines[m][i];
            var children = l.children;
            var check = 0;
            for (var j = 0; j < children.length; j++) {
                if (children[j].getAttribute("type") == "metron") {
                    metra[m].push(children[j]);
                    mCount[m] += 1;
                    check += 1;
                }
            }
            if (check == 0) {
                for (var k = 0; k < trochees.length; k++) {
                    var c = trochees[k].children;
                    for (var h = 0; h < c.length; h++) {
                        if (c[h] == l) {
                            mCount[m] += 4;
                        }
                    }
                }
                for (var k = 0; k < iambics.length; k++) {
                    var c = iambics[k].children;
                    for (var h = 0; h < c.length; h++) {
                        if (c[h] == l) {
                            mCount[m] += 3;
                        }
                    }
                }
                for (var k = 0; k < anapests.length; k++) {
                    var c = anapests[k].children;
                    for (var h = 0; h < c.length; h++) {
                        if (c[h] == l) {
                            mCount[m] += 1;
                        }
                    }
                }
            }
        }
    }
    return mCount;
}



function universalCountLines(type, inwhat) {
    if (type == "Meter") {
        var im = countUp(getLinesByMeter());
        return im;
    } else if (type == "Type") {
        var t = countUp(getLinesByType());
        return t;
    } else if (type == "Character") {
        if (inwhat != "") {
            var common = commonValue(CharacterCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var cLines = countUp(CharacterCountLines());
            return cLines;
        }
    } else if (type == "Gender") {
        if (inwhat != "") {
            var common = commonValue(GenderCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var gLines = countUp(GenderCountLines());
            return gLines;
        }
    } else if (type == "Status") {
        if (inwhat != "") {
            var common = commonValue(StatusCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var sLines = countUp(StatusCountLines());
            return sLines;
        }
    } else if (type == "God/Human") {
        if (inwhat != "") {
            var common = commonValue(GodCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var sLines = countUp(GodCountLines());
            return sLines;
        }
    } else if (type == "Greek/Non-Greek") {
        if (inwhat != "") {
            var common = commonValue(GreekCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var grLines = countUp(GreekCountLines());
            return grLines;
        }
    } else if (type == "Lyric/Non-Lyric") {
        if (inwhat != "") {
            var common = commonValue(LyricCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var lyrLines = countUp(LyricCountLines());
            return lyrLines;
        }
    } else if (type == "Recitative/Non-Recitative") {
        if (inwhat != "") {
            var common = commonValue(RecitativeCountLines(), countMeterLines(inwhat))
            return countUp(common);
        } else {
            var rLines = countUp(RecitativeCountLines());
            return rLines;
        }
    }
}

function countUp(array) {
    var nums = [];
    for (var i = 0; i < array.length; i++) {
        nums.push(array[i].length);
    }
    return nums;
}


function CharacterCountLines() {
    var charLines = [];
    var chars = getFilterValues()["Character"];
    for (var i = 0; i < chars.length; i++) {
        var l = countLines(chars[i]);
        charLines.push(l);
    }
    return charLines;
}


function GenderCountLines() {
    var genderLines = [];
    var femaleLines = [];
    var maleLines = [];
    var chars = getFilterValues()["Character"];
    var female = [];
    for (var i = 0; i < chardata.length; i++) {
        if (chardata[i].gender == "female") {
            female.push(chardata[i].name);
        }
    }
    for (var i = 0; i < chars.length; i++) {
        var hold = countLines(chars[i]);
        if (female.includes(chars[i])) {
            for (var j = 0; j < hold.length; j++) {
                femaleLines.push(hold[j]);
            }
        } else {
            for (var j = 0; j < hold.length; j++) {
                maleLines.push(hold[j]);
            }
        }
    }
    genderLines.push(femaleLines);
    genderLines.push(maleLines);
    return genderLines;
}


function StatusCountLines() {
    var statusLines = [];
    var enslavedLines = [];
    var freeLines = [];
    var chars = getFilterValues()["Character"];
    var enslaved = [];
    for(var i=0;i<chardata.length;i++){
        if(chardata[i].status == "enslaved"){
            enslaved.push(chardata[i].name);
        }
    }
    for (var i = 0; i < chars.length; i++) {
        var hold = countLines(chars[i]);
        if (enslaved.includes(chars[i])) {
            for (var j = 0; j < hold.length; j++) {
                enslavedLines.push(hold[j]);
            }
        } else {
            for (var j = 0; j < hold.length; j++) {
                freeLines.push(hold[j]);
            }
        }
    }
    statusLines.push(enslavedLines);
    statusLines.push(freeLines);
    return statusLines;
}

function GodCountLines(){
    var allLines = [];
    var humanLines = [];
    var godLines = [];
    var chars = getFilterValues()["Character"];
    var god = [];
    for(var i=0;i<chardata.length;i++){
        if(chardata[i].isGod == "god"){
            god.push(chardata[i].name);
        }
    }
    for (var i = 0; i < chars.length; i++) {
        var hold = countLines(chars[i]);
        if (god.includes(chars[i])) {
            for (var j = 0; j < hold.length; j++) {
                godLines.push(hold[j]);
            }
        } else {
            for (var j = 0; j < hold.length; j++) {
                humanLines.push(hold[j]);
            }
        }
    }
    allLines.push(godLines);
    allLines.push(humanLines);
    return allLines;
}

function GreekCountLines() {
    var grLines = [];
    var greekLines = [];
    var nonLines = [];
    var chars = getFilterValues()["Character"];
    var nonGreek = [];
    for(var i=0;i<chardata.length;i++){
        if(chardata[i].isGreek == "nonGreek"){
            nonGreek.push(chardata[i].name);
        }
    }
    for (var i = 0; i < chars.length; i++) {
        var hold = countLines(chars[i]);
        if (nonGreek.includes(chars[i])) {
            for (var j = 0; j < hold.length; j++) {
                nonLines.push(hold[j]);
            }
        } else {
            for (var j = 0; j < hold.length; j++) {
                greekLines.push(hold[j]);
            }
        }
    }
    grLines.push(greekLines);
    grLines.push(nonLines);
    return grLines;

}

function LyricCountLines() {
    var lyricLines = [];
    var lyrics = [];
    var nonlyrics = [];
    var anapests = sortMeters()[1];
    for (var i = 0; i < lines.length; i++) {
        var d = lines[i];
        var meter = checkLineMeter(d);
        if (meter != null && meter != "" && !meter.includes("anapest") && !meter.includes("iambic3") && !meter.includes("trochaic4") && !meter.includes("unknown") && !meter.includes("extra metrum")) {
            lyrics.push(d);
        } else if (meter.includes("anapest")) {
            var isLyric = true;
            for (var k = 0; k < anapests.length; k++) {
                for (var j = 0; j < anapests[k].children.length; j++) {
                    if (anapests[k].children[j] == d) {
                        isLyric = false;
                    }
                }
            }
            if (isLyric == true) {
                lyrics.push(d);
            } else {
                nonlyrics.push(d);
            }    
        } else if (!meter.includes("unknown") && !meter.includes("extra metrum")) {
            nonlyrics.push(d);
        }
    }

    lyricLines.push(lyrics);
    lyricLines.push(nonlyrics);

    return lyricLines;
}


function RecitativeCountLines() {
    var rLines = [];
    var recLines = [];
    var nonLines = [];
    var anapests = sortMeters()[1];
    var trochees = sortMeters()[2];


    for (var i = 0; i < lines.length; i++) {
        for (var j = 0; j < anapests.length; j++) {
            var children = anapests[j].children;
            for (var k = 0; k < children.length; k++) {
                if (children[k] == lines[i]) {
                    if (!recLines.includes(lines[i])) {
                        recLines.push(lines[i]);
                    }
                }
            }
        }
        for (var j = 0; j < trochees.length; j++) {
            var children = trochees[j].children;
            for (var k = 0; k < children.length; k++) {
                if (children[k] == lines[i]) {
                    if (!recLines.includes(lines[i])) {
                        recLines.push(lines[i]);
                    }
                }
            }
        }
    }
    for (var i = 0; i < lines.length; i++) {
        if (!recLines.includes(lines[i])) {
            nonLines.push(lines[i]);
        }
    }

    rLines.push(recLines);
    rLines.push(nonLines);

    return rLines;

}

function countLines(character) {
    var speeches = getSpeeches();
    var holder = [];
    var keys = Object.keys(speeches);
    for (var i = 0; i < keys.length; i++) {
        var stripped = keys[i].replace(/[0-9]/g, '');
        if (stripped == character) {
            for (var j = 0; j < speeches[keys[i]].length; j++) {
                if (speeches[keys[i]][j].tagName == "l") {
                    holder.push(speeches[keys[i]][j]);
                }
            }
        }
    }

    return holder;
}

function countMeterLines(meterArray) {
    meter = meterArray;
    var l = [];
    for (var i = 0; i < lines.length; i++) {
        if (meter.includes("(type)")) {
            var name = checkLineType(lines[i]) + " (type)";
        } else {
            var name = checkLineMeter(lines[i]);
        }
        if (meter == name) {
            l.push(lines[i]);
        }
    }
    return l;
}

function getAllMeters() {
    var allMeters = [];
    for (var i = 0; i < lines.length; i++) {
        meter = checkLineMeter(lines[i]);
        if (meter != "" && !allMeters.includes(meter)) {
            allMeters.push(meter);
        }
    }
    return allMeters;
}

function getLinesByMeter() {
    var allMeters = getAllMeters();
    var meterLines = [];
    for (var j = 0; j < allMeters.length; j++) {
        var lineArray = countMeterLines(allMeters[j]);
        meterLines.push(lineArray);
    }
    return meterLines;
}

function universalCountWords(type, inwhat) {
    if (type == "Meter") {
        var im = countWords(getLinesByMeter());
        return im;
    } else if (type == "Type") {
        var t = countWords(getLinesByType());
        return t;
    } else if (type == "Character") {
        if (inwhat != "") {
            var common = commonValue(CharacterCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var cWords = countWords(CharacterCountLines());
            return cWords;
        }
    } else if (type == "Gender") {
        if (inwhat != "") {
            var common = commonValue(GenderCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var gWords = countWords(GenderCountLines());
            return gWords;
        }
    } else if (type == "Status") {
        if (inwhat != "") {
            var common = commonValue(StatusCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var sWords = countWords(StatusCountLines());
            return sWords;
        }
    } else if (type == "God/Human") {
        if (inwhat != "") {
            var common = commonValue(GodCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var sWords = countWords(GodCountLines());
            return sWords;
        }
    } else if (type == "Greek/Non-Greek") {
        if (inwhat != "") {
            var common = commonValue(GreekCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var grWords = countWords(GreekCountLines());
            return grWords;
        }
    } else if (type == "Lyric/Non-Lyric") {
        if (inwhat != "") {
            var common = commonValue(LyricCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var lyrWords = countWords(LyricCountLines());
            return lyrWords;
        }
    } else if (type == "Recitative/Non-Recitative") {
        if (inwhat != "") {
            var common = commonValue(RecitativeCountLines(), countMeterLines(inwhat))
            return countWords(common);
        } else {
            var rWords = countWords(RecitativeCountLines());
            return rWords;
        }
    }
}

function countWords(somelines) {
    var words = [];
    var wCount = [];
    for (var m = 0; m < somelines.length; m++) {
        words[m] = [];
        wCount[m] = 0;
        for (var i = 0; i < somelines[m].length; i++) {
            var l = somelines[m][i];
            var children = l.children;
            for (var j = 0; j < children.length; j++) {
                var gc = children[j].children;
                if (children[j].getAttribute("type") == "metron") {
                    for (var k = 0; k < gc.length; k++) {
                        var sylls = gc[k].children;
                        for (var s = 0; s < sylls.length; s++) {
                            if (sylls[s].getAttribute("unit") == "word") {
                                wCount[m] += 1;
                            }
                        }
                    }
                } else {
                    for (var k = 0; k < gc.length; k++) {
                        if (gc[k].getAttribute("unit") == "word") {
                            wCount[m] += 1;
                        }
                    }
                }
            }
        }
    }
    return wCount;
}

function countCharLinesInMeter(character, meter) {
    var speeches = getSpeeches();
    var holder = [];
    var keys = Object.keys(speeches);
    for (var i = 0; i < keys.length; i++) {
        var stripped = keys[i].replace(/[0-9]/g, '');
        if (stripped == character) {
            for (var j = 0; j < speeches[keys[i]].length; j++) {
                if (speeches[keys[i]][j].tagName == "l") {
                    if (checkMeterName(speeches[keys[i]][j]) == meter) {
                        holder.push(speeches[keys[i]][j]);
                    }
                }
            }
        }
    }
    return holder;
}

function countLinesInMeter(meter) {
    var holder = [];
    for (var i = 0; i < lines.length; i++) {
        if (checkLineMeter(lines[i]) == meter) {
            holder.push(lines[i]);
        }
    }
    return holder;
}

function commonValue(typeArray, meterArray) {
    var common = [];
    for (var m = 0; m < typeArray.length; m++) {
        var holder = [];
        for (var i = 0; i < typeArray[m].length; i++) {
            if (meterArray.includes(typeArray[m][i])) {
                holder.push(typeArray[m][i]);
            }
        }
        common[m] = holder;
    }
    return common;
}

function checkMeterType(node) {
    children = [];
    for (var i = 0; i < node.children.length; i++) {
        if (node.children[i].getAttribute("met") != null) {
            children.push(node.children[i]);
        }
    }
    typestring = "";
    types = [];
    for (var i = 0; i < children.length; i++) {
        var met = children[i].getAttribute("met");
        met = met.replace(/\?/g, '');
        for (var j = 0; j < meterdata.length; j++) {
            if (meterdata[j].Ours == met) {
                types.push(meterdata[j].type);
                if (children.length > 1 && i != (children.length - 1)) {
                    typestring += meterdata[j].type + " + ";
                } else {
                    typestring += meterdata[j].type;
                }
            }
        }
    }
    var holder = 0;
    for (var i = 0; i < types.length; i++) {
        if (types[i] == types[i + 1]) {
            holder += 1;
        }
    }
    if (holder == (types.length - 1)) {
        typestring = types[0];
    }
    if (children.length > 0 && typestring == "") {
        typestring = "?"
    }
    if (typestring == "null" || typestring == null) {
        return "none";
    }
    return typestring;
}


function checkLineType(l) {
    var hold = l;
    while (hold.parentNode != null && hold.tagName != "div2") {
        hold = hold.parentNode;
    }
    var result = "";
    if (hold.getAttribute("type") == "ia3") {
        result = "iambic";
    } else if (hold.getAttribute("type") == "anapests") {
        result = "anapestic";
    } else if (hold.getAttribute("type") == "tr4^") {
        result = "trochaic"
    } else {
        result = checkMeterType(l);
    }
    return result;
}

function countTypeLines(type) {
    var l = [];
    for (var i = 0; i < lines.length; i++) {
        var name = checkLineType(lines[i]);
        if (type == name) {
            l.push(lines[i]);
        }
    }
    return l;
}

function getAllTypes() {
    var allTypes = [];
    for (var i = 0; i < lines.length; i++) {
        type = checkLineType(lines[i]);
        if (type != "" && !allTypes.includes(type)) {
            allTypes.push(type);
        }
    }
    return allTypes;
}

function getLinesByType() {
    var allTypes = getAllTypes();
    var typeLines = [];
    for (var j = 0; j < allTypes.length; j++) {
        var lineArray = countTypeLines(allTypes[j]);
        typeLines.push(lineArray);
    }
    return typeLines;
}

function getBrackets(somelines) {
    var excluded = [];
    for (var i = 0; i < somelines.length; i++) {
        var l = somelines[i];
        var children = l.children;
        var end = 1;
        for (var j = 0; j < children.length; j++) {
            if (children[j].getAttribute("type") == "syll") {
                if (children[j].innerHTML.includes("[")) {
                    end = 0;
                    excluded.push(children[j]);
                } else if (children[j].innerHTML.includes("]")) {
                    end = 1;
                    excluded.push(children[j]);
                } else if (end == 0) {
                    excluded.push(children[j]);
                }
            } else if (children[j].getAttribute("type") == "metron") {
                var gc = children[j].children;
                var end2 = 1;
                for (var k = 0; k < gc.length; k++) {
                    if (gc[k].getAttribute("type") == "syll") {
                        if (gc[k].innerHTML.includes("[")) {
                            end2 = 0;
                            excluded.push(gc[k]);
                        } else if (gc[k].innerHTML.includes("]")) {
                            end2 = 1;
                            excluded.push(gc[k]);
                        } else if (end2 == 0) {
                            excluded.push(gc[k]);
                        }
                    }
                }
            }
        }
    }
    return excluded;
}

function removeBrackets(sylls) {
    var excluded = getBrackets(lines);
    var update = [];
    for (var m = 0; m < sylls.length; m++) {
        update[m] = [];
        for (var k = 0; k < sylls[m].length;k++) {
            var check = 0;
            for (var i = 0; i <excluded.length; i++) {
                if (sylls[m][k].isSameNode(excluded[i])) {
                    check = 1;
                }
            }
            if(check==0){
                update[m].push(sylls[m][k]);
            }
        }
    }
    return update;
}