//VARIABLE DECLARATIONS

var input_file;
   
var lines; // every line in the play
var filtered_lines; // the lines to be displayed on the interface
var filter_values_dict = {}; // the lines that are selected

var lines_cf;
var lines_dims = {};
var line_counts = {};
var search_input = "";

	// var line_counts_corpus = {'lines': {}, 'syllables': {}, 'words': {}};
	// var lcc_recorded = {'lines': false, 'syllables': false, 'words': false};

let title;
let x = []
let y = []
let pct = []
let corpus_pct = []

let default_line_counts;

let line_text_table;

var have_zeroes = false;

var table;
var matrix;
var p_opt = "";
var one_d_check = false;

const dimension_keys = [
    'play',
    'div1_type',
    'speaker',
    'gender',
    'status',
    'isGod',
    'isGreek',
    'lyric_non_lyric',
    'original_met_values', // only difference between this and table_names
    'line_meter_type',
    'uncertain',
    'line_text'];

const table_names = [
    'play',
    'div1_type',
    'speaker',
    'gender',
    'status',
    'isGod',
    'isGreek',
    'lyric_non_lyric',
    'line_meter_name',
    'line_meter_type',
    'uncertain',
    'line_text'];


// As plays get added, put their abbreviations here
const play_abbreviations = {
    "E": "Elektra",
    "H": "Hekabe",
    "O": "Orestes"
};

// var currentCursorPos = 0;

$('document').ready(function() {
    
    $('.csv_link').css({'color': '#666666'});
    $('#a_lines').css({'color': '#000000'});
    
    input_file = 'data/lines.csv';
    
    page_load_csv_switch();

});

//FUNCTIONS//


function switch_csv(csv_type) {
    
    $('.csv_link').css({'color': '#666666'});
    $('#a_' + csv_type).css({'color': '#000000'});

    $("#includeOnlyPattern").val(""); // clear the pattern box

    if (csv_type === "syllables") {
        $("#met_pattern").css("display", "none");
    } else {
        $("#met_pattern").css("display", "block")
    }
    
    input_file = 'data/' + csv_type + '.csv';
    
    page_load_csv_switch();
}

function page_load_csv_switch() {
    
    $('#all_lists').html('');

    line_counts = {};
    
    loaddata();

    function loaddata() {
        
        queue().defer(d3.csv, input_file + '?' + Math.floor(Math.random() * 1000)) // the additional string to circumvent the browser cache 
            .await(start);
    }

    function start(error, lines_arg) {
        
        lines = lines_arg;
        filtered_lines = lines_arg;
        
        handle_filters();
        toggle_zeroes();
    };
}

function get_line_counts() {

    if (Object.keys(line_counts).length !== 0) {

        for (let [name, type] of Object.entries(line_counts)) {
            if (have_zeroes === false) {
                line_counts[name] = {} // this way makes the zeroes disappear
            }
            else {
                // make sure that everything has a zero that is displayed
                // this way keeps zeroes except for line_text
                if (name === "line_text") { // reset line_text to nothing instead of setting its attributes to 0
                    line_counts[name] = {}
                } else {
                    if (typeof default_line_counts !== "undefined") {
                        line_counts = structuredClone(default_line_counts)
                    } else {
                        for (let [key, value] of Object.entries(type)) {
                            type[key] = 0
                        }
                    }
                }
            }
        }
        
        if (have_zeroes === true && typeof default_line_counts === "undefined") {
            console.log()
            default_line_counts = structuredClone(line_counts);
        }
    }

    // console.log(line_counts)
                
    for (var a = 0; a < dimension_keys.length; a++) {
    
        var k = dimension_keys[a];

        if (line_counts[k] === undefined) {
            line_counts[k] = {};
        }
                            
        for (var b = 0; b < filtered_lines.length; b++) {
        
            if (filtered_lines[b][k] in line_counts[k]) {
            }
            else {
                line_counts[k][filtered_lines[b][k]] = 0;
            }
            
            line_counts[k][filtered_lines[b][k]] += 1;
        }
    }

    // // set line counts corpus (since the first line count is always of no filter, or the entire corpus)
    // if (!lcc_recorded[tally_object()]) {
    //     line_counts_corpus[tally_object()] = {...line_counts} // copy the dictionary
    //     lcc_recorded[tally_object()] = true;
    // }
}

function set_cf_and_dimensions() {
    
    get_line_counts();

    // console.log('get_line_counts DONE');
    
    lines_cf = crossfilter(filtered_lines);

    // console.log('crossfilter DONE');
                
    for (var a = 0; a < dimension_keys.length; a++) {
        
        var k = dimension_keys[a];
    
        lines_dims[k] = lines_cf.dimension(
            function(d) {
                return d[k];
            }
        );

        // console.log('lines_dims[k]', lines_dims[k])

        // console.log('dimension_keys[a]', dimension_keys[a]);
    }

    // console.log('dimension_keys DONE');
}

function set_table(data_matrix, row_names, column_names, row_total, col_total, mat_total) {
    if (table !== undefined) {
        table.destroy();
    }
    $("#table").html("")
    // data_matrix in following format: [[10, 20, 25], [30, 40, 50]]
    // row_names in the following format: [row1, row2]
    // column names in following format: [col1, col2, col3]
    // data_matrix[i][j] to get the row and column
    // row_total in the following format: [55, 120]
    // col_total in the following format: [40, 60, 75]
    // mat_total in the following format: 175

    var heading_html = ""

    for (var j = 0; j < column_names.length; j++) {
        heading_html += "<th><b>" + column_names[j] + "</b></th>";
    }

    var last_cell = ""

    if (row_total !== undefined) {
        if (column_names.length === 0) {
            if (p_opt === "1d-table") {
                last_cell = "<th><b>" + upperCase(tally_object()) + "</b></th>"
            } else {
                last_cell = "<th><b>% of Total " + (Object.keys(filter_values_dict).length > 0 || $('#includeOnlyPattern').val() !== "" ? "(Filtered) " : "") + upperCase(tally_object()) + " </b></th>"
            }
        } else {
            last_cell = "<th><b>Total</b></th>"
        }
    }

    heading_html = "<thead><tr><th></th>" + heading_html + last_cell + "</tr></thead>"

    $("#table").append(heading_html)


    var data_html = ""

    for (var i = 0; i < row_names.length; i++) {
        var row_html = ""
        for (var j = 0; j < column_names.length; j++) {
            row_html += "<td>" + data_matrix[i][j] + "</td>"
        }

        var last_cell = ""
        if (row_total !== undefined) {
            last_cell = row_total[i]
            if (column_names.length !== 0) { // only if the last cell is the total, and NOT the count
                last_cell = "<b>" + last_cell + "</b>"
            }
            last_cell = "<td>" + last_cell + "</td>"
        }
        row_html = "<tr><td><b>" + row_names[i] + "</b></td>" + row_html + last_cell + "</tr>"
        data_html += row_html
    }

    if (col_total !== undefined) {    
        var total_footer = "<th><b>Total</b></th>";

        for (var j = 0; j < column_names.length; j++) {
            total_footer += "<th>" + col_total[j] + "</th>"
        }

        if (mat_total !== undefined) {
            total_footer += "<th>" + mat_total + "</th>"
        }
        
        data_html += "<tfoot><tr>" + total_footer + "</tr></tfoot>"
        // <tfoot>
        //         <tr>
        //             <th colspan="4" style="text-align:right">Total:</th>
        //             <th></th>
        //         </tr>
        //     </tfoot>
    }


    $("#table").append("<tbody>" + data_html + "</tbody>")



    table = $("#table").DataTable({
        fixedColumns: {
            left: 1,
            right: 1
        },
        scrollX: column_names.length >= 6,
        scrollY: "47vh",
        scrollCollapse: true,
        searching: false,
        paging: false,
        info: false,
        ordering: false,

        columnDefs: {targets: "_all", width: (100 / (column_names.length+2)).toFixed(1) + "%"},

        dom: 't<".padtop"Bfrpi>',
        buttons: ['selectAll', 'selectNone', 'copy', 'csv']

    });

    table.columns.adjust().draw();
}

function get_matrix(row_cat, col_cat) { // for "row category" and "col category"

    var one_d = row_cat === col_cat || col_cat === "none";

    // SORT THE DICTIONARY, ACCORDING TO STACKOVERFLOW
    var row_items = Object.keys(line_counts[row_cat]).map(function(key) {
        return [key, line_counts[row_cat][key]];
    });

    if (row_cat !== "div1_type") {// Sort the array based on the second element
        row_items.sort(function(first, second) {
            return second[1] - first[1]; // sort in reverse order
        });
    }

    var row_labels = []
    var row_values = []

    for (const item of row_items) {
        row_labels.push(item[0]);
        row_values.push(item[1]);
    }

    if (one_d) {
        if (one_d_check === false) { // if one_d has changed from last time, then reset p_opt to something different
            p_opt = "1d-table"
            one_d_check = true
        }
        return [undefined, row_labels, [], row_values, []]
    } else {
        if (one_d_check === true) {
            p_opt = ""
            one_d_check = false
        }
    }


    var row_dict = {};

    for (var i = 0; i < row_items.length; i++) {
        row_dict[row_items[i][0]] = i
    }

 

    var col_items = Object.keys(line_counts[col_cat]).map(function(key) {
        return [key, line_counts[col_cat][key]];
    });

    if (col_cat !== "div1_type") {// Sort the array based on the second element
        col_items.sort(function(first, second) {
            return second[1] - first[1]; // sort in reverse order
        });
    }

    var col_labels = []
    var col_values = []

    for (const item of col_items) {
        col_labels.push(item[0]);
        col_values.push(item[1]);
    }

    var col_dict = {};

    for (var i = 0; i < col_items.length; i++) {
        col_dict[col_items[i][0]] = i
    }

    


    var matrix = new Array(row_items.length); // create an empty array of length n
    for (var i = 0; i < row_items.length; i++) {
        matrix[i] = new Array(col_items.length).fill(0); // make each element an array of zeroes.
    }

    for (const dict of filtered_lines) {
        matrix[row_dict[dict[row_cat]]][col_dict[dict[col_cat]]] += 1 // increment the corresponding matrix
    }

    return [matrix, row_labels, col_labels, row_values, col_values]
}

function get_percentage(matrix, row_total, col_total) {
    if (matrix === undefined) {
        if (p_opt === "1d-table") {
            return [undefined, row_total, [], filtered_lines.length]
        } else if (p_opt === "1d-percentage") {
            var row_p = new Array(row_total.length);
            for (var i = 0; i < row_total.length; i++) {
                row_p[i] = (row_total[i] / filtered_lines.length * 100).toFixed(1) + "%"
            }
            return [undefined, row_p, [], "100.0%"]
        }
    }
    if (p_opt === "") {
        return [matrix, row_total, col_total, filtered_lines.length]
    }
    
    var p_matrix = new Array(matrix.length);
    for (var i = 0; i < matrix.length; i++) {
        p_matrix[i] = new Array(matrix[0].length).fill(0); // make each element an array of zeroes.
    }

    var row_p = new Array(row_total.length);
    var col_p = new Array(col_total.length);
    var mat_p;

    if (p_opt === "row_total") {
        for (var i = 0; i < row_total.length; i++) {
            for (var j = 0; j < matrix[0].length; j++) {
                p_matrix[i][j] = (matrix[i][j] / row_total[i] * 100).toFixed(1) + "%";
            }
            row_p[i] = "100.0%";
        }

        for (var j = 0; j < matrix[0].length; j++) {
            col_p[j] = (col_total[j] / filtered_lines.length * 100).toFixed(1) + "%";
        }

        mat_p = "100.0%"
    }
    if (p_opt === "col_total") {
        for (var i = 0; i < row_total.length; i++) {
            for (var j = 0; j < matrix[0].length; j++) {
                p_matrix[i][j] = (matrix[i][j] / col_total[j] * 100).toFixed(1) + "%";
            }

            row_p[i] = (row_total[i] / filtered_lines.length * 100).toFixed(1) + "%"
        }

        for (var j = 0; j < matrix[0].length; j++) {
            col_p[j] = "100.0%"
        }

        mat_p = "100.0%";
    }
    if (p_opt === "mat_total") {
        for (var i = 0; i < row_total.length; i++) {
            for (var j = 0; j < matrix[0].length; j++) {
                p_matrix[i][j] = (matrix[i][j] / filtered_lines.length * 100).toFixed(1) + "%";
            }
            row_p[i] = (row_total[i] / filtered_lines.length * 100).toFixed(1) + "%"
        }

        for (var j = 0; j < matrix[0].length; j++) {
            col_p[j] = (col_total[j] / filtered_lines.length * 100).toFixed(1) + "%"
        }

        mat_p = "100.0%"
    }

    return [p_matrix, row_p, col_p, mat_p];
}
    
function output_list(dimension_name, list, list_id, clear_div) {
    
    var target_id = 'all_lists';
    if (dimension_name == 'line_text') {
        target_id = 'all_lines';
    }
    
    var table_heading = list_id.replace(/_list/g, '').replace(/_/g, ' ');
    if (table_heading == 'line text') {
        table_heading = '';
    }
    if (table_heading == 'div1 type') {
        table_heading = 'Part of Play';
    }
    if (table_heading == 'isGod') {
        table_heading = 'God/Human';
    }
    if (table_heading == 'isGreek') {
        table_heading = 'Greek/Non-Greek';
    }
    if (table_heading == 'lyric non lyric') {
        table_heading = 'Lyric/Non-Lyric';
    }
    if (table_heading == 'speaker') {
        table_heading = 'Character';
    }
    if (table_heading == 'line meter name') {
        table_heading = 'Line Meter';
    }
    if (table_heading == 'line meter type') {
        table_heading = 'Line Type';
    }
    if (table_heading == 'play') {
        table_heading = 'Play';
    }
    if (table_heading == 'gender') {
        table_heading = 'Gender';
    }
    if (table_heading == 'status') {
        table_heading = 'Enslaved/Free';
    }
    if (table_heading == 'uncertain') {
        table_heading = 'Uncertain Text'
    }
    
    $('#' + target_id).append('<div class="my_lists" id="' + list_id + '"><h2>' + 
                            table_heading + 
                            '</h2></div>');
    

    // set the column heading to something different depending on the file in question

    var html; 
    if (table_heading === "") {
        html = ['<thead><tr><th>Line Text</th><th>' + upperCase(tally_object()) + '</th></tr></thead><tbody>'];
    } else {
        html = ['<thead><tr><th>Value</th><th>' + upperCase(tally_object()) + '</th></tr></thead><tbody>'];
    }
    
    for (var a = 0; a < list.length; a++) {
        if (target_id === "all_lines") {
            let ln = list[a].key.substring(list[a].key.indexOf("<span>") + 8, list[a].key.indexOf("</span>"))

            let abbr = list[a].key.substring(list[a].key.indexOf("<span>") + 6, list[a].key.indexOf("."))

            html.push('<tr><td><a href="../index.html?play=' + play_abbreviations[abbr] + '&searchInput=' + ln +  '" target="foo">' + list[a].key + '</a></td><td class="integer_td">' + parseInt(list[a].n_lines) + '</td></tr>');
        } else {
            html.push('<tr><td>' + list[a].key + '</td><td class="integer_td">' + parseInt(list[a].n_lines) + '</td></tr>');
        }
    }
    
    $('#' + list_id).append('<table id="table_' + list_id + '" class="display">' + 
                            html.join('') + 
                            '</tbody></table>');
                            
    if (clear_div == true) {
        $('#all_lists').append('<div class="clear_div"> </div>');
    }
    
    if (target_id == 'all_lines') {
        line_text_table = $('#table_' + list_id).DataTable(
            { 
                columnDefs: [
                    {
                        orderable: false, 
                        targets: 0                        
                    },
                    {
                        className: "dt-head-center",
                        targets: "_all"
                    }
                ], 
                order: [],
                // 'pageLength': 100,

                scrollY: "47vh",
                scrollCollapse: true,
                // paging: false,

                scroller: true,

                dom: 't<".padtop"Bfrpi>',
                buttons: ['selectAll', 'selectNone', 'copy', 'csv'],
                select: true,
            });

        line_text_table.column(0).data().sort(function (a, b) { // sorts by line number.
            let a_ln = a.substring(a.indexOf("<span>") + 8, a.indexOf("</span>"))
            let b_ln = b.substring(b.indexOf("<span>") + 8, b.indexOf("</span>"))

            if (["a", "b"].includes(a_ln.slice(-1))) {
                a_ln = a_ln.substring(0, a_ln.length-1)
            } else if (a_ln.indexOf("-") !== -1) {
                a_ln = a_ln.split("-")[0]
            }

            if (["a", "b"].includes(b_ln.slice(-1))) {
                b_ln = b_ln.substring(0, b_ln.length-1)
            } else if (b_ln.indexOf("-") !== -1) {
                b_ln = b_ln.split("-")[0]
            }

            return b_ln - a_ln;
        });

        $('#table_' + list_id + ' tbody').on('click', 'tr', function() {
            $(this).toggleClass('selected'); // the row will be highlighted on click
        });

        $('#table_' + list_id + ' tbody tr td').on('click', 'a', function(event) {
            event.stopPropagation(); // prevents row from being highlighted if clicking on link.
        });

        line_text_table.columns.adjust().draw();
    }

    else {
//         play
// index.js?v=200:145 div1_type
// index.js?v=200:145 speaker
// index.js?v=200:145 gender
// index.js?v=200:145 status
// index.js?v=200:145 isGod
// index.js?v=200:145 isGreek
// index.js?v=200:145 lyric_non_lyric
// index.js?v=200:145 line_meter_name
// index.js?v=200:145 line_meter_type
// index.js?v=200:145 line_text
        let options = {
            scrollY: "200px",
            scrollCollapse: true,
            paging: false,

            dom: "Bfrti",
            buttons: ['selectAll', 'selectNone'],
            select: true,

            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: "_all"
                }
            ]
        }
        if (["play", "speaker", "gender", "status", "isGod", "isGreek", "lyric_non_lyric", "line_meter_name", "line_meter_type", "uncertain"].includes(list_id)) {
            options["order"] = [1, 'desc']; // set all of these to be descending on the count attribute
        }


        // console.log(options)
        $('#table_' + list_id).DataTable(options);
        $('#table_' + list_id + ' tbody').on('click', 'tr', function() {
            $(this).toggleClass('selected'); // the row will be highlighted on click
        });
    }
}

function list_one_result(dimension_name, table_name, add_clear) {
    

    // var lines_list = lines_dims[dimension_name].group()
    //     .reduceCount(
    //         function(d) { 
    //             return d.n_lines; 
    //         }
    //     )
    // .all();
     
    // var final_list = [];
        
    // for (var a = 0; a < lines_list.length; a++) {
        
    //     var key = lines_list[a].key;
    //     var minutes = lines_list[a].value;
        
    //     var n_lines = line_counts[dimension_name][key];
        
    //     final_list.push({'key': key, 'n_lines': n_lines});
    // }
    var final_list = [];

    for (const [key, value] of Object.entries(line_counts[dimension_name])) {
        final_list.push({'key': key, 'n_lines': value});
    }

    // console.log("final list", final_list)

    output_list(dimension_name, final_list, table_name, add_clear);
    
    // // experimental code: add checkboxes to the beginning of the table
    // let table_elem = document.getElementById("table_" + table_name);
    // let thead_elem = table_elem.getElementsByTagName("thead")[0];
    // let tr_elem = thead_elem.getElementsByTagName("tr")[0];

    // let thead_row_elem = document.createElement("th");
    // tr_elem.insertBefore(thead_row_elem, tr_elem.firstChild);
    
    // let tbody_elem = table_elem.getElementsByTagName("tbody")[0];

    // let row_elements = tbody_elem.getElementsByTagName("tr");

    // for (let tr_elem of row_elements) {
    //     let th_elem = document.createElement("th");

    //     let checkbox = document.createElement("input");
    //     checkbox.setAttribute("type", "checkbox");


    //     th_elem.appendChild(checkbox);

    //     tr_elem.insertBefore(th_elem, tr_elem.firstChild);
    // }

}

function list_results() {
    let current_scroll = $(".div3").scrollTop()
    // clears the html
    $('#all_lists').html('');
    $('#all_lines').html('');

    $('#filter_button').click(function() {
        // console.log($('table').DataTable().rows('.selected').data().length + ' row(s) selected')
    });
                
    for (var a = 0; a < dimension_keys.length; a++) {
        
        var k = dimension_keys[a];
        var table_name = table_names[a];
        
        var add_clear_div = false;
        //if ((a % 2) == 1) {
        //    add_clear_div = true;
        //}  
    
        list_one_result(k, table_name, add_clear_div);
    }

    // selects all these rows
    change_selection()

    $(".div3").scrollTop(current_scroll)
    
    // for (const value of selected_values) {
    //     $("tr").find("td").filter(function(index) {
    //         return this.innerHTML === value;
    //     }).parent().toggleClass("selected");
    // }
}


function find_filter_name(table_id) {
    var table_name = table_id.replace(/^table_/, '');
    
    var i = -1;
    
    for (a = 0; a < table_names.length; a++) {
        if (table_names[a] == table_name) {
            i = a;
            break;
        }
    }

    return dimension_keys[i]
}

// function handle_filter(table_id, filter_value) { // the "and" function 
//     var filter_name = find_filter_name(table_id);
    
//     var new_filtered_lines = [];
//     for (var a = 0; a < filtered_lines.length; a++) {
//         if (filtered_lines[a][filter_name] == filter_value) {
//             // console.log(filtered_lines[a]);
//             new_filtered_lines.push(filtered_lines[a]);
//         }
//     }
    
//     filtered_lines = new_filtered_lines;
        
//     set_cf_and_dimensions();
    
//     list_results();
// }

function handle_filters() {
    let new_filtered_lines = [];
    filter_values_dict = {} // reset filter_values_dict
    for (let i = 0; i < table_names.length; i++) {
        if ($("#table_" + table_names[i]).DataTable().rows(".selected").data().length == 0) {
            new_filtered_lines.push(lines); // if nothing is selected, add the entirety of the table category and move on to the next category
            continue;
        }
        new_filtered_lines.push([]);

        let filter_values = [];
        $("#table_" + table_names[i]).DataTable().rows(".selected").every(function(rowIdx, tableLoop, rowLoop) {
            if (table_names[i] === "line_text") {
                filter_values.push(this.node().firstChild.firstChild.innerHTML); // should get the child of the <a> tag instead of the <a> tag. 
            }
            filter_values.push(this.node().firstChild.innerHTML);
        }); // go through and add the various filters selected

        if (filter_values_dict[table_names[i]] === undefined) { // if unitialized, initialize the array in the dictionary
            filter_values_dict[table_names[i]] = [];
        }

        filter_values_dict[table_names[i]] = [...filter_values] // clone the array into filter_values_dict
    
        let filter_name = find_filter_name("table_" + table_names[i])
        // console.log("filter_name", filter_name);
        for (var a = 0; a < lines.length; a++) {
            for (const filter_value of filter_values) {
                // console.log("la", lines[a])
                if (lines[a][filter_name] == filter_value) {
                    new_filtered_lines[i].push(lines[a])
                }
            }
        }
    }

    console.log(filter_values_dict);

    // console.log(new_filtered_lines);

    let result_lines = []
    const include_input = $('#includeOnlyPattern').val()
    let search_pattern = include_input.replaceAll('∪', 's').replaceAll('-', 'l').replaceAll('⏓', '.').replaceAll('⏕', '(l|ss)').replaceAll('∩', 'n')
    search_input = include_input

    try {
        let re = new RegExp(search_pattern);
    } catch (e) {
        if (e instanceof SyntaxError) {
            alert(e)
        } else {
            throw e;
        }
    }

    for (const line of lines) {
        let filteredIn = true;
        for (const type of new_filtered_lines) {
            if (type.indexOf(line) == -1) { // if a certain line is not found within any of the new_filtered_lines, it is not filteredIn.
                filteredIn = false;
                break;
            }
        }

        if (filteredIn) {
            // filter based on metrical pattern
            if (include_input === '' || line.syllables_string.search(new RegExp(search_pattern)) !== -1) { 
                result_lines.push(line);
            }
        }
    }

    // console.log(result_lines.length);

    filtered_lines = result_lines;
        
    set_cf_and_dimensions();
    
    list_results();

    // draw the charts 

    let category = $('#byEach_select').val();

    // get the title
    
    let filteredStr = Object.keys(filter_values_dict).length > 0 || $('#includeOnlyPattern').val() !== "" ? "(Filtered) " : "";
    
    const title = wrap("Number of " + filteredStr + upperCase(tally_object()) + " by " + $('#byEach_select option:selected').text(), 64)

    console.log($('#byEach_select option:selected').text());

    let filters_notice = ""
    for (const filter_type of Object.keys(filter_values_dict)) {
        filters_notice += filter_type + ": "
        for (const filter of filter_values_dict[filter_type]) {
            filters_notice += filter + ", "
        }
        filters_notice = filters_notice.substring(0, filters_notice.length - 2) + "; "
    }
    filters_notice = filters_notice.substring(0, filters_notice.length - 2);

    // change "Current Filters"
    $('#filters_notice').text(filters_notice)
        
    // SORT THE DICTIONARY, ACCORDING TO STACKOVERFLOW
    var items = Object.keys(line_counts[category]).map(function(key) {
        return [key, line_counts[category][key]];
    });

    if (category !== "div1_type") {// Sort the array based on the second element
        items.sort(function(first, second) {
            return second[1] - first[1]; // sort in reverse order
        });
    }

    // variables
    var total = 0;
    // reset vars
    x = []
    y = []
    pct = []
    
    // corpus_pct = []

    
    for (const [key, value] of items) {
        total += value;
        x.push(key);
        y.push(value);
    }
    table_x = [...x, "<b>Total</b>"]
    table_y = [...y, "<b>" + total + "</b>",]

    console.log("items", items)

    // for (const value of Object.values(line_counts_corpus[tally_object()][category])) {
    //     corpus_total += parseInt(value);
    // }

    // console.log("corpus_total", corpus_total)

    for (const [key, value, alt_value] of items) {
        pct.push((value / total * 100.0).toFixed(1) + '%');
    }

    // table
    var tablevalues = [
        table_x,
        table_y,
        pct,
        [],
        [],
        [],
        [],
        [],[],[],[],[],
        [],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],
        [],[],[],[],[],
        [],
        [],
        [],
        []
    ]
    
    var tabledata = [{
        type: 'table',
        header: {
            values: [
                [""],
                [""],
                [""]//["<b>% of Total</b>"]
            ],
            align: "center",
            height: 0,
            line: {
                width: 0,
                color: 'white'
            },
            // fill: {
            //     color: "gray"
            // },
            font: {
                family: "Lucida Bright, Georgia",
                size: 1, // size: 12,
                color: ["black"], // color: "white"
            }
        },
        cells: {
            values: tablevalues,
            align: "center",
            line: {
                color: "black",
                width: 1
            },
            font: {
                family: "Lucida Bright, Georgia",
                size: 11,
                color: ["black"]
            }
        }
    }]

    var config = {responsive: true}

    // Plotly.newPlot('table', tabledata, undefined, config);
    let row_labels, col_labels;
    [matrix, row_labels, col_labels, row_total, col_total] = 
        get_matrix($("#byEach_select").val(), $("#byEach_select_col").val());

    [matrix, row_total, col_total, mat_total] = get_percentage(matrix, row_total, col_total)

    set_table(matrix, row_labels, col_labels, row_total, col_total, mat_total);
    

    //bar chart
    var bartrace = [{
        x: x,
        y: y,
        type: 'bar'
    }];
    var barlayout = {
        title: title,
        font: {
            family: "Lucida Bright, Georgia",
            size: 12,
        },
        xaxis: {
            tickangle: 45,
            automargin: true
        },
    };
    
    Plotly.newPlot("barChart", bartrace, barlayout, config);

    //pie chart
    var piedata = [{
        values: y,
        labels: x,
        type: 'pie',
        textinfo: 'percent',
        textposition: 'inside'
    }];

    var pielayout = {
        title: title,
        font: {
            family: "Lucida Bright, Georgia"
        }
    };

    Plotly.newPlot('pieChart', piedata, pielayout, config);

    var dims = []

    for (const dkey of [
        'gender',
        'status',
        'isGod',
        'isGreek',
        'lyric_non_lyric',
        'line_meter_type']) {
        // if (dkey === "div1_type" || dkey === "speaker" || dkey === "line_text" || dkey === "original_met_values") {
        //     continue;
        // }
        
        var dim = {label: dkey}
        if (dkey === "line_meter_type") {
            // dim["categoryarray"] = [0, 0.1, 0.15, 0.2, 0.3, 0.4, 0.45, 0.5, 0.6, 0.7, 0.75, 0.8, 0.9, 1]
            // dim["categoryarray"] = [0, 0.1, 0.15, 0.2, 0.3, 0.4, 0.45, 0.5, 0.6, 0.7, 0.75, 0.8, 0.9, 1]
        }
        dvals = []
        for (const ln of filtered_lines) {
            dvals.push(ln[dkey]);
        }
        dim.values = dvals;

        dims.push(dim);
        // if (dims.length > 3) {
        //     break;
        // }
    }

    console.log(dims)

    var color = [0, 0.1, 0.15, 0.2, 0.3, 0.4, 0.45, 0.5, 0.6, 0.7, 0.75, 0.8, 0.9, 1];
    var colorscale = [[0, 'lightsteelblue'], [1, 'mediumseagreen']];


    var trace1 = {
        type: 'parcats',
        dimensions: dims,
        // line: {color: color,
        // colorscale: colorscale},
        hoveron: "dimension",
        hoverinfo: 'count+probability'
        // dimensions: [
        //   {label: 'Hair',
        //    values: ['Black', 'Black', 'Black', 'Brown',
        //             'Brown', 'Brown', 'Red', 'Brown']},
        //   {label: 'Eye',
        //    values: ['Brown', 'Brown', 'Brown', 'Brown',
        //             'Brown', 'Blue', 'Blue', 'Blue']},
        //   {label: 'Sex',
        //    values: ['Female', 'Female', 'Female', 'Male',
        //             'Female', 'Male', 'Male', 'Male']}]
      };
      
      var data = [ trace1 ];
      
      
      Plotly.newPlot('parCatChart', data, undefined, config);
    
}

// var table_names = [
//     'play',
//     'div1_type',
//     'speaker',
//     'gender',
//     'status',
//     'isGod',
//     'isGreek',
//     'lyric_non_lyric',
//     'line_meter_name',
//     'line_meter_type',
//     'line_text'];

function upperCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

const wrap = (s, w) => s.replace( // thanks, StackOverflow!
    new RegExp(`(?![^\\n]{1,${w}}$)([^\\n]{1,${w}})\\s`, 'g'), '$1<br>'
);

function get_ln_from_html(html) {
    return $(html).find("span").text()
}

// function display_name(table_name) {
//     switch (display_name) {
//         case 'play':
//             return "Play";
//         case 'div1_type':
//             return "Part of Play";
//         case 'speaker':
//             return "Character";
//         case 'gender':
//             return 
//     }
//     return "???";
// }


function tally_object() {
    switch (input_file) {
        case 'data/lines.csv':
            return 'lines';
        case 'data/syllables.csv':
            return 'syllables';
        case 'data/words.csv':
            return 'words';
    }
    return '???'
}

function clear_all_filters() {
    
    filtered_lines = lines;
    filter_values_dict = {};
    // $("#excludeBrackets").prop("checked", false);
    $("#includeOnlyPattern").val("");
        
    set_cf_and_dimensions();
    
    list_results();

    handle_filters();
}

let initializePlays = function(error, plays_arg) {
    setPlayList(plays_arg, "plays");
}

let setPlayList = function(plays, elementId) {
    let playlist = document.getElementById(elementId);
    playlist.innerHTML = "";
    for (play in plays) {
        playlist.insertAdjacentHTML(
            'beforeend', 
            `<a href="../index.html?play=${play}">${play}</a>`);
    };
    
    playlist.insertAdjacentHTML(
        'beforeend', 
        `<br/>`);
    
    playlist.insertAdjacentHTML(
        'beforeend', 
        `<span id="menu_spacer_1"> </span>`);
        
    
    // playlist.insertAdjacentHTML(
    //     'beforeend', 
    //     `<a href="../combined_statistics/">Statistics</a>`);
}

function changeRadioButtons() {

}

let t_o = 1000;
function toggle_view(button, div) {
    $("button.tablinks").each(function (i) {
        $(this).removeClass("active");
    });

    $(button).addClass("active");

    $("div.statistics").each(function(i) {
        $(this).addClass("hidden")
    })

    $(div).removeClass("hidden");

    // if ($(".div4").hasClass("hidden")) { // change to line_text
    //     $(".div4").removeClass("hidden")
    //     $(".div5").addClass("hidden")

    //     line_text_table.columns.adjust().draw() // Why do we need to redraw the tables?
        
    //     // line_text_table.destroy()
    //     // let final_list = []
    //     // for (const [key, value] of Object.entries(line_counts["line_text"])) {
    //     //     final_list.push({'key': key, 'n_lines': value});
    //     // }
    //     // output_list("line_text", final_list, "line_text", false)
    // } else if ($(".div5").hasClass("hidden")) {
    //     $(".div5").removeClass("hidden")
    //     $(".div4").addClass("hidden")

    //     table.columns.adjust().draw();

    //     var config = {responsive: true}

    //     var divs = ['barChart', 'pieChart']

    //     for (const div of divs) {
    //         var displayDiv = document.getElementById(div);
    //         Plotly.newPlot(displayDiv, displayDiv.data, displayDiv.layout, config)
    //         console.log("SUP!!", displayDiv, displayDiv.data, displayDiv.layout)
    //     }
    // } else if ($(".div6").hasClass("hidden")) {
    // } else if ($(".div7").hasClass("hidden")) {
    // } 
}

function change_selection() {
    for (let i = 0; i < table_names.length; i++) {
        $("#table_" + table_names[i]).DataTable().rows().deselect();
    }
    for (const [key, value] of Object.entries(filter_values_dict)) {
        $("#table_" + key).DataTable().rows( function (index, data, node) {
            return value.includes(node.firstChild.innerHTML)
        }).select();
    }

    $('#includeOnlyPattern').val(search_input)
}

// function setCursorPos() {
//     currentCursorPos = $('#includeOnlyPattern').getCursorPosition();
// }

function insertTextAtCursor(str) {
    let pos = $('#includeOnlyPattern').getCursorPosition();

    $('#includeOnlyPattern').val(function (index, value) {
        console.log(pos);

        return value.substring(0, pos) + str + value.substring(pos, value.length);
    })

    document.getElementById("includeOnlyPattern").focus();
    document.getElementById("includeOnlyPattern").setSelectionRange(pos+1, pos+1);
}

function toggle_zeroes() {
    if (have_zeroes === true) {
        have_zeroes = false;
    }
    else if (have_zeroes === false) {
        // line_counts = {};
        // filtered_lines = lines;
        have_zeroes = true;
    }
    
    handle_filters();
}

function toggle_percentages() {
    switch (p_opt) {
        case "":
            p_opt = "row_total"
            break;
        case "row_total":
            p_opt = "col_total"
            break;
        case "col_total":
            p_opt = "mat_total"
            break;
        case "mat_total":
            p_opt = ""
            break;

        case "1d-table":
            p_opt = "1d-percentage"
            break;
        case "1d-percentage":
            p_opt = "1d-table"
            break;
    }
    handle_filters();
}

function update_by(value) {
    $(".by").each(function (i) {
        $(this).val(value);
    })
    handle_filters();
}
